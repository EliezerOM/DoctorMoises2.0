/** @type {function(Object): *} */
// const withMT =  require("@material-tailwind/html/utils/withMT");

module.exports = {
    // important: true,
    darkMode: 'class',
    content: [
        "./resources/**/*.blade.php",
        "./resources/**/*.js",
        "./resources/**/*.vue",
        "./node_modules/flowbite/**/*.js",
    ],
    theme: {
        extend: {},
    },
    plugins: [
        require('flowbite/plugin'),
    ],
}

//
// module.exports = withMT({
//     // important: true,
//     content: [
//         "./index.html",
//         "./resources/**/*.blade.php",
//         "./resources/**/*.js",
//         "./resources/**/*.vue",
//         "./node_modules/flowbite/**/*.js",
//     ],
//     theme: {
//     extend: {},
//   },
//   plugins: [
//       require('flowbite/plugin'),
//   ],
// })
