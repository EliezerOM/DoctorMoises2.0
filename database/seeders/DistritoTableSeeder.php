<?php

namespace Database\Seeders;

use DateTime;
use App\Models\Distrito;
use Illuminate\Database\Seeder;

class DistritoTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = array(
            [
                'descripcion' => 'Barreras',
                'id_municipio' => '2',
                'created_at'    => new DateTime,
                'updated_at'    => new DateTime
            ],
            [
                'descripcion' => 'Barro Arriba',
                'id_municipio' => '2',
                'created_at'    => new DateTime,
                'updated_at'    => new DateTime
            ],
            [
                'descripcion' => 'Clavellina',
                'id_municipio' => '2',
                'created_at'    => new DateTime,
                'updated_at'    => new DateTime
            ],
            [
                'descripcion' => 'Emma Balaguer Viuda Vallejo',
                'id_municipio' => '2',
                'created_at'    => new DateTime,
                'updated_at'    => new DateTime
            ],
            [
                'descripcion' => 'Las Barías-La Estancia',
                'id_municipio' => '2',
                'created_at'    => new DateTime,
                'updated_at'    => new DateTime
            ],
            [
                'descripcion' => 'Las Lomas',
                'id_municipio' => '2',
                'created_at'    => new DateTime,
                'updated_at'    => new DateTime
            ],
            [
                'descripcion' => 'Los Jovillos',
                'id_municipio' => '2',
                'created_at'    => new DateTime,
                'updated_at'    => new DateTime
            ],
            [
                'descripcion' => 'Puerto Viejo',
                'id_municipio' => '2',
                'created_at'    => new DateTime,
                'updated_at'    => new DateTime
            ],
            [
                'descripcion' => 'Hatillo',
                'id_municipio' => '5',
                'created_at'    => new DateTime,
                'updated_at'    => new DateTime
            ],
            [
                'descripcion' => 'Palmar de Ocoa',
                'id_municipio' => '5',
                'created_at'    => new DateTime,
                'updated_at'    => new DateTime
            ],
            [
                'descripcion' => 'Villapando',
                'id_municipio' => '6',
                'created_at'    => new DateTime,
                'updated_at'    => new DateTime
            ],
            [
                'descripcion' => 'Hato Nuevo-Cortés',
                'id_municipio' => '6',
                'created_at'    => new DateTime,
                'updated_at'    => new DateTime
            ],
            [
                'descripcion' => 'La Siembra',
                'id_municipio' => '7',
                'created_at'    => new DateTime,
                'updated_at'    => new DateTime
            ],
            [
                'descripcion' => 'Las Lagunas',
                'id_municipio' => '7',
                'created_at'    => new DateTime,
                'updated_at'    => new DateTime
            ],
            [
                'descripcion' => 'Los Fríos',
                'id_municipio' => '7',
                'created_at'    => new DateTime,
                'updated_at'    => new DateTime
            ],
            [
                'descripcion' => 'El Rosario',
                'id_municipio' => '9',
                'created_at'    => new DateTime,
                'updated_at'    => new DateTime
            ],
            [
                'descripcion' => 'Proyecto 4',
                'id_municipio' => '10',
                'created_at'    => new DateTime,
                'updated_at'    => new DateTime
            ],
            [
                'descripcion' => 'Ganadero',
                'id_municipio' => '10',
                'created_at'    => new DateTime,
                'updated_at'    => new DateTime
            ],
            [
                'descripcion' => 'Proyecto 2-C',
                'id_municipio' => '10',
                'created_at'    => new DateTime,
                'updated_at'    => new DateTime
            ],
            [
                'descripcion' => 'Amiama Gómez',
                'id_municipio' => '10',
                'created_at'    => new DateTime,
                'updated_at'    => new DateTime
            ],
            [
                'descripcion' => 'Los Toros',
                'id_municipio' => '10',
                'created_at'    => new DateTime,
                'updated_at'    => new DateTime
            ],
            [
                'descripcion' => 'Tábara Abajo',
                'id_municipio' => '10',
                'created_at'    => new DateTime,
                'updated_at'    => new DateTime
            ],
            [
                'descripcion' => 'El Palmar',
                'id_municipio' => '12',
                'created_at'    => new DateTime,
                'updated_at'    => new DateTime
            ],
            [
                'descripcion' => 'El Salado',
                'id_municipio' => '13',
                'created_at'    => new DateTime,
                'updated_at'    => new DateTime
            ],
            [
                'descripcion' => 'Las Clavellinas',
                'id_municipio' => '14',
                'created_at'    => new DateTime,
                'updated_at'    => new DateTime
            ],
            [
                'descripcion' => 'Cabeza de Toro',
                'id_municipio' => '15',
                'created_at'    => new DateTime,
                'updated_at'    => new DateTime
            ],
            [
                'descripcion' => 'Mena',
                'id_municipio' => '15',
                'created_at'    => new DateTime,
                'updated_at'    => new DateTime
            ],
            [
                'descripcion' => 'Monserrat',
                'id_municipio' => '15',
                'created_at'    => new DateTime,
                'updated_at'    => new DateTime
            ],
            [
                'descripcion' => 'Santa Bárbara-El 6',
                'id_municipio' => '15',
                'created_at'    => new DateTime,
                'updated_at'    => new DateTime
            ],
            [
                'descripcion' => 'Santana',
                'id_municipio' => '15',
                'created_at'    => new DateTime,
                'updated_at'    => new DateTime
            ],
            [
                'descripcion' => 'Uvilla',
                'id_municipio' => '15',
                'created_at'    => new DateTime,
                'updated_at'    => new DateTime
            ],
            [
                'descripcion' => 'El Cachón',
                'id_municipio' => '17',
                'created_at'    => new DateTime,
                'updated_at'    => new DateTime
            ],
            [
                'descripcion' => 'La Guázara',
                'id_municipio' => '17',
                'created_at'    => new DateTime,
                'updated_at'    => new DateTime
            ],
            [
                'descripcion' => 'Villa Central',
                'id_municipio' => '17',
                'created_at'    => new DateTime,
                'updated_at'    => new DateTime
            ],
            [
                'descripcion' => 'Arroyo Dulce',
                'id_municipio' => '20',
                'created_at'    => new DateTime,
                'updated_at'    => new DateTime
            ],
            [
                'descripcion' => 'Pescadería',
                'id_municipio' => '21',
                'created_at'    => new DateTime,
                'updated_at'    => new DateTime
            ],
            [
                'descripcion' => 'Palo Alto',
                'id_municipio' => '22',
                'created_at'    => new DateTime,
                'updated_at'    => new DateTime
            ],
            [
                'descripcion' => 'Bahoruco',
                'id_municipio' => '23',
                'created_at'    => new DateTime,
                'updated_at'    => new DateTime
            ],
            [
                'descripcion' => 'Los Patos',
                'id_municipio' => '25',
                'created_at'    => new DateTime,
                'updated_at'    => new DateTime
            ],
            [
                'descripcion' => 'Canoa',
                'id_municipio' => '27',
                'created_at'    => new DateTime,
                'updated_at'    => new DateTime
            ],
            [
                'descripcion' => 'Fondo Negro',
                'id_municipio' => '27',
                'created_at'    => new DateTime,
                'updated_at'    => new DateTime
            ],
            [
                'descripcion' => 'Quita Coraza',
                'id_municipio' => '27',
                'created_at'    => new DateTime,
                'updated_at'    => new DateTime
            ],
            [
                'descripcion' => 'Cañongo',
                'id_municipio' => '28',
                'created_at'    => new DateTime,
                'updated_at'    => new DateTime
            ],
            [
                'descripcion' => 'Manuel Bueno',
                'id_municipio' => '4',
                'created_at'    => new DateTime,
                'updated_at'    => new DateTime
            ],
            [
                'descripcion' => 'Capotillo',
                'id_municipio' => '30',
                'created_at'    => new DateTime,
                'updated_at'    => new DateTime
            ],
            [
                'descripcion' => 'Santiago de la Cruz',
                'id_municipio' => '30',
                'created_at'    => new DateTime,
                'updated_at'    => new DateTime
            ],
            [
                'descripcion' => 'Cenoví',
                'id_municipio' => '33',
                'created_at'    => new DateTime,
                'updated_at'    => new DateTime
            ],
            [
                'descripcion' => 'La Peña',
                'id_municipio' => '33',
                'created_at'    => new DateTime,
                'updated_at'    => new DateTime
            ],
            [
                'descripcion' => 'Presidente Don Antonio Guzmán Fernández',
                'id_municipio' => '33',
                'created_at'    => new DateTime,
                'updated_at'    => new DateTime
            ],
            [
                'descripcion' => 'Aguacate',
                'id_municipio' => '34',
                'created_at'    => new DateTime,
                'updated_at'    => new DateTime
            ],
            [
                'descripcion' => 'Las Coles',
                'id_municipio' => '34',
                'created_at'    => new DateTime,
                'updated_at'    => new DateTime
            ],
            [
                'descripcion' => 'Sabana Grande',
                'id_municipio' => '36',
                'created_at'    => new DateTime,
                'updated_at'    => new DateTime
            ],
            [
                'descripcion' => 'Agua Santa del Yuna',
                'id_municipio' => '39',
                'created_at'    => new DateTime,
                'updated_at'    => new DateTime
            ],
            [
                'descripcion' => 'Barraquito',
                'id_municipio' => '39',
                'created_at'    => new DateTime,
                'updated_at'    => new DateTime
            ],
            [
                'descripcion' => 'Cristo Rey de Guaraguao',
                'id_municipio' => '39',
                'created_at'    => new DateTime,
                'updated_at'    => new DateTime
            ],
            [
                'descripcion' => 'Las Táranas',
                'id_municipio' => '39',
                'created_at'    => new DateTime,
                'updated_at'    => new DateTime
            ],
            [
                'descripcion' => 'Pedro Sánchez',
                'id_municipio' => '40',
                'created_at'    => new DateTime,
                'updated_at'    => new DateTime
            ],
            [
                'descripcion' => 'San Francisco-Vicentillo',
                'id_municipio' => '40',
                'created_at'    => new DateTime,
                'updated_at'    => new DateTime
            ],
            [
                'descripcion' => 'Santa Lucía',
                'id_municipio' => '41',
                'created_at'    => new DateTime,
                'updated_at'    => new DateTime
            ],
            [
                'descripcion' => 'El Cedro',
                'id_municipio' => '41',
                'created_at'    => new DateTime,
                'updated_at'    => new DateTime
            ],
            [
                'descripcion' => 'La Gina',
                'id_municipio' => '41',
                'created_at'    => new DateTime,
                'updated_at'    => new DateTime
            ],
            [
                'descripcion' => 'Guayabo',
                'id_municipio' => '42',
                'created_at'    => new DateTime,
                'updated_at'    => new DateTime
            ],
            [
                'descripcion' => 'Sabana Larga',
                'id_municipio' => '42',
                'created_at'    => new DateTime,
                'updated_at'    => new DateTime
            ],
            [
                'descripcion' => 'Sabana Cruz',
                'id_municipio' => '42',
                'created_at'    => new DateTime,
                'updated_at'    => new DateTime
            ],
            [
                'descripcion' => 'Sabana Higüero',
                'id_municipio' => '43',
                'created_at'    => new DateTime,
                'updated_at'    => new DateTime
            ],
            [
                'descripcion' => 'Guanito',
                'id_municipio' => '44',
                'created_at'    => new DateTime,
                'updated_at'    => new DateTime
            ],
            [
                'descripcion' => 'Rancho de la Guardia',
                'id_municipio' => '45',
                'created_at'    => new DateTime,
                'updated_at'    => new DateTime
            ],
            [
                'descripcion' => 'Rio Limpio',
                'id_municipio' => '47',
                'created_at'    => new DateTime,
                'updated_at'    => new DateTime
            ],
            [
                'descripcion' => 'Canca La Reina',
                'id_municipio' => '48',
                'created_at'    => new DateTime,
                'updated_at'    => new DateTime
            ],
            [
                'descripcion' => 'El Higüerito',
                'id_municipio' => '48',
                'created_at'    => new DateTime,
                'updated_at'    => new DateTime
            ],
            [
                'descripcion' => 'José Contreras',
                'id_municipio' => '48',
                'created_at'    => new DateTime,
                'updated_at'    => new DateTime
            ],
            [
                'descripcion' => 'Juan López',
                'id_municipio' => '48',
                'created_at'    => new DateTime,
                'updated_at'    => new DateTime
            ],
            [
                'descripcion' => 'La Ortega',
                'id_municipio' => '48',
                'created_at'    => new DateTime,
                'updated_at'    => new DateTime
            ],
            [
                'descripcion' => 'José Contreras',
                'id_municipio' => '48',
                'created_at'    => new DateTime,
                'updated_at'    => new DateTime
            ],
            [
                'descripcion' => 'Las Lagunas',
                'id_municipio' => '48',
                'created_at'    => new DateTime,
                'updated_at'    => new DateTime
            ],
            [
                'descripcion' => 'Monte de la Jagua',
                'id_municipio' => '48',
                'created_at'    => new DateTime,
                'updated_at'    => new DateTime
            ],
            [
                'descripcion' => 'San Victor',
                'id_municipio' => '48',
                'created_at'    => new DateTime,
                'updated_at'    => new DateTime
            ],
            [
                'descripcion' => 'Joba Arriba',
                'id_municipio' => '50',
                'created_at'    => new DateTime,
                'updated_at'    => new DateTime
            ],
            [
                'descripcion' => 'Veragua',
                'id_municipio' => '50',
                'created_at'    => new DateTime,
                'updated_at'    => new DateTime
            ],
            [
                'descripcion' => 'Villa Magante',
                'id_municipio' => '50',
                'created_at'    => new DateTime,
                'updated_at'    => new DateTime
            ],
            [
                'descripcion' => 'Guayabo Dulce',
                'id_municipio' => '52',
                'created_at'    => new DateTime,
                'updated_at'    => new DateTime
            ],
            [
                'descripcion' => 'Mata Palacio',
                'id_municipio' => '52',
                'created_at'    => new DateTime,
                'updated_at'    => new DateTime
            ],
            [
                'descripcion' => 'Yerba Buena',
                'id_municipio' => '52',
                'created_at'    => new DateTime,
                'updated_at'    => new DateTime
            ],
            [
                'descripcion' => 'Elupina Cordero de Las Cañitas',
                'id_municipio' => '54',
                'created_at'    => new DateTime,
                'updated_at'    => new DateTime
            ],
            [
                'descripcion' => 'Jamao Afuera',
                'id_municipio' => '55',
                'created_at'    => new DateTime,
                'updated_at'    => new DateTime
            ],
            [
                'descripcion' => 'Blanco',
                'id_municipio' => '56',
                'created_at'    => new DateTime,
                'updated_at'    => new DateTime
            ],
            [
                'descripcion' => 'Boca de Cachón',
                'id_municipio' => '58',
                'created_at'    => new DateTime,
                'updated_at'    => new DateTime
            ],
            [
                'descripcion' => 'El Limón',
                'id_municipio' => '58',
                'created_at'    => new DateTime,
                'updated_at'    => new DateTime
            ],
            [
                'descripcion' => 'Batey 8',
                'id_municipio' => '59',
                'created_at'    => new DateTime,
                'updated_at'    => new DateTime
            ],
            [
                'descripcion' => 'Vengan a Ver',
                'id_municipio' => '60',
                'created_at'    => new DateTime,
                'updated_at'    => new DateTime
            ],
            [
                'descripcion' => 'La Colonia',
                'id_municipio' => '62',
                'created_at'    => new DateTime,
                'updated_at'    => new DateTime
            ],
            [
                'descripcion' => 'Guayabal',
                'id_municipio' => '63',
                'created_at'    => new DateTime,
                'updated_at'    => new DateTime
            ],
            [
                'descripcion' => 'La Otra Banda',
                'id_municipio' => '64',
                'created_at'    => new DateTime,
                'updated_at'    => new DateTime
            ],
            [
                'descripcion' => 'Lagunas de Nisibón',
                'id_municipio' => '64',
                'created_at'    => new DateTime,
                'updated_at'    => new DateTime
            ],
            [
                'descripcion' => 'Verón-Punta Cana',
                'id_municipio' => '64',
                'created_at'    => new DateTime,
                'updated_at'    => new DateTime
            ],
            [
                'descripcion' => 'Bayahibe',
                'id_municipio' => '65',
                'created_at'    => new DateTime,
                'updated_at'    => new DateTime
            ],
            [
                'descripcion' => 'Boca de Yuma',
                'id_municipio' => '65',
                'created_at'    => new DateTime,
                'updated_at'    => new DateTime
            ],
            [
                'descripcion' => 'Caleta',
                'id_municipio' => '66',
                'created_at'    => new DateTime,
                'updated_at'    => new DateTime
            ],
            [
                'descripcion' => 'Cumayasa',
                'id_municipio' => '68',
                'created_at'    => new DateTime,
                'updated_at'    => new DateTime
            ],
            [
                'descripcion' => 'El Ranchito',
                'id_municipio' => '69',
                'created_at'    => new DateTime,
                'updated_at'    => new DateTime
            ],
            [
                'descripcion' => 'Río Verde Arriba',
                'id_municipio' => '69',
                'created_at'    => new DateTime,
                'updated_at'    => new DateTime
            ],
            [
                'descripcion' => 'La Sabina',
                'id_municipio' => '70',
                'created_at'    => new DateTime,
                'updated_at'    => new DateTime
            ],
            [
                'descripcion' => 'Tireo',
                'id_municipio' => '70',
                'created_at'    => new DateTime,
                'updated_at'    => new DateTime
            ],
            [
                'descripcion' => 'Buena Vista',
                'id_municipio' => '70',
                'created_at'    => new DateTime,
                'updated_at'    => new DateTime
            ],
            [
                'descripcion' => 'Manabao',
                'id_municipio' => '71',
                'created_at'    => new DateTime,
                'updated_at'    => new DateTime
            ],
            [
                'descripcion' => 'Rincón',
                'id_municipio' => '72',
                'created_at'    => new DateTime,
                'updated_at'    => new DateTime
            ],
            [
                'descripcion' => 'Arroyo al Medio',
                'id_municipio' => '73',
                'created_at'    => new DateTime,
                'updated_at'    => new DateTime
            ],
            [
                'descripcion' => 'Las Gordas',
                'id_municipio' => '73',
                'created_at'    => new DateTime,
                'updated_at'    => new DateTime
            ],
            [
                'descripcion' => 'San José de Matanzas',
                'id_municipio' => '73',
                'created_at'    => new DateTime,
                'updated_at'    => new DateTime
            ],
            [
                'descripcion' => 'Arroyo Salado',
                'id_municipio' => '74',
                'created_at'    => new DateTime,
                'updated_at'    => new DateTime
            ],
            [
                'descripcion' => 'La Entrada',
                'id_municipio' => '74',
                'created_at'    => new DateTime,
                'updated_at'    => new DateTime
            ],
            [
                'descripcion' => 'El Pozo',
                'id_municipio' => '75',
                'created_at'    => new DateTime,
                'updated_at'    => new DateTime
            ],
            [
                'descripcion' => 'Arroyo Toro-Masipedro',
                'id_municipio' => '77',
                'created_at'    => new DateTime,
                'updated_at'    => new DateTime
            ],
            [
                'descripcion' => 'La Salvia-Los Quemados',
                'id_municipio' => '77',
                'created_at'    => new DateTime,
                'updated_at'    => new DateTime
            ],
            [
                'descripcion' => 'Jayaco',
                'id_municipio' => '77',
                'created_at'    => new DateTime,
                'updated_at'    => new DateTime
            ],
            [
                'descripcion' => 'Juma Bejucal',
                'id_municipio' => '77',
                'created_at'    => new DateTime,
                'updated_at'    => new DateTime
            ],
            [
                'descripcion' => 'Sabana del Puerto',
                'id_municipio' => '77',
                'created_at'    => new DateTime,
                'updated_at'    => new DateTime
            ],
            [
                'descripcion' => 'Juan Adrían',
                'id_municipio' => '79',
                'created_at'    => new DateTime,
                'updated_at'    => new DateTime
            ],
            [
                'descripcion' => 'Villa Sonador',
                'id_municipio' => '79',
                'created_at'    => new DateTime,
                'updated_at'    => new DateTime
            ],
            [
                'descripcion' => 'Palo Verde',
                'id_municipio' => '81',
                'created_at'    => new DateTime,
                'updated_at'    => new DateTime
            ],
            [
                'descripcion' => 'Cana Chapetón',
                'id_municipio' => '82',
                'created_at'    => new DateTime,
                'updated_at'    => new DateTime
            ],
            [
                'descripcion' => 'Hatillo Palma',
                'id_municipio' => '82',
                'created_at'    => new DateTime,
                'updated_at'    => new DateTime
            ],
            [
                'descripcion' => 'Vila Elisa',
                'id_municipio' => '82',
                'created_at'    => new DateTime,
                'updated_at'    => new DateTime
            ],
            [
                'descripcion' => 'Boyá',
                'id_municipio' => '86',
                'created_at'    => new DateTime,
                'updated_at'    => new DateTime
            ],
            [
                'descripcion' => 'Chirino',
                'id_municipio' => '86',
                'created_at'    => new DateTime,
                'updated_at'    => new DateTime
            ],
            [
                'descripcion' => 'Don Juan',
                'id_municipio' => '86',
                'created_at'    => new DateTime,
                'updated_at'    => new DateTime
            ],
            [
                'descripcion' => 'Gonzalo',
                'id_municipio' => '89',
                'created_at'    => new DateTime,
                'updated_at'    => new DateTime
            ],
            [
                'descripcion' => 'Majagual',
                'id_municipio' => '89',
                'created_at'    => new DateTime,
                'updated_at'    => new DateTime
            ],
            [
                'descripcion' => 'Los Botados',
                'id_municipio' => '90',
                'created_at'    => new DateTime,
                'updated_at'    => new DateTime
            ],
            [
                'descripcion' => 'José Francisco Peña Gomez',
                'id_municipio' => '91',
                'created_at'    => new DateTime,
                'updated_at'    => new DateTime
            ],
            [
                'descripcion' => 'Juancho',
                'id_municipio' => '91',
                'created_at'    => new DateTime,
                'updated_at'    => new DateTime
            ],
            [
                'descripcion' => 'Catalina',
                'id_municipio' => '93',
                'created_at'    => new DateTime,
                'updated_at'    => new DateTime
            ],
            [
                'descripcion' => 'El Carretón',
                'id_municipio' => '93',
                'created_at'    => new DateTime,
                'updated_at'    => new DateTime
            ],
            [
                'descripcion' => 'El Limonal',
                'id_municipio' => '93',
                'created_at'    => new DateTime,
                'updated_at'    => new DateTime
            ],
            [
                'descripcion' => 'Las Barías',
                'id_municipio' => '93',
                'created_at'    => new DateTime,
                'updated_at'    => new DateTime
            ],
            [
                'descripcion' => 'Matanzas',
                'id_municipio' => '93',
                'created_at'    => new DateTime,
                'updated_at'    => new DateTime
            ],
            [
                'descripcion' => 'Paya',
                'id_municipio' => '93',
                'created_at'    => new DateTime,
                'updated_at'    => new DateTime
            ],
            [
                'descripcion' => 'Sabana Buey',
                'id_municipio' => '93',
                'created_at'    => new DateTime,
                'updated_at'    => new DateTime
            ],
            [
                'descripcion' => 'Villa Fundación',
                'id_municipio' => '93',
                'created_at'    => new DateTime,
                'updated_at'    => new DateTime
            ],
            [
                'descripcion' => 'Villa Sombrero',
                'id_municipio' => '93',
                'created_at'    => new DateTime,
                'updated_at'    => new DateTime
            ],
            [
                'descripcion' => 'Pizarrete',
                'id_municipio' => '94',
                'created_at'    => new DateTime,
                'updated_at'    => new DateTime
            ],
            [
                'descripcion' => 'Santana',
                'id_municipio' => '94',
                'created_at'    => new DateTime,
                'updated_at'    => new DateTime
            ],
            [
                'descripcion' => 'Maimón',
                'id_municipio' => '86',
                'created_at'    => new DateTime,
                'updated_at'    => new DateTime
            ],
            [
                'descripcion' => 'Yásica Arriba',
                'id_municipio' => '86',
                'created_at'    => new DateTime,
                'updated_at'    => new DateTime
            ],
            [
                'descripcion' => 'Río Grande',
                'id_municipio' => '96',
                'created_at'    => new DateTime,
                'updated_at'    => new DateTime
            ],
            [
                'descripcion' => 'Navas',
                'id_municipio' => '99',
                'created_at'    => new DateTime,
                'updated_at'    => new DateTime
            ],
            [
                'descripcion' => 'Belloso',
                'id_municipio' => '100',
                'created_at'    => new DateTime,
                'updated_at'    => new DateTime
            ],
            [
                'descripcion' => 'Estrecho',
                'id_municipio' => '100',
                'created_at'    => new DateTime,
                'updated_at'    => new DateTime
            ],
            [
                'descripcion' => 'La Isabela',
                'id_municipio' => '100',
                'created_at'    => new DateTime,
                'updated_at'    => new DateTime
            ],
            [
                'descripcion' => 'Cabarete',
                'id_municipio' => '101',
                'created_at'    => new DateTime,
                'updated_at'    => new DateTime
            ],
            [
                'descripcion' => 'Sabaneta de Yásica',
                'id_municipio' => '101',
                'created_at'    => new DateTime,
                'updated_at'    => new DateTime
            ],
            [
                'descripcion' => 'Estero Hondo',
                'id_municipio' => '102',
                'created_at'    => new DateTime,
                'updated_at'    => new DateTime
            ],
            [
                'descripcion' => 'Gualete',
                'id_municipio' => '102',
                'created_at'    => new DateTime,
                'updated_at'    => new DateTime
            ],
            [
                'descripcion' => 'La Jaiba',
                'id_municipio' => '102',
                'created_at'    => new DateTime,
                'updated_at'    => new DateTime
            ],
            [
                'descripcion' => 'Arroyo Barril',
                'id_municipio' => '104',
                'created_at'    => new DateTime,
                'updated_at'    => new DateTime
            ],
            [
                'descripcion' => 'El Limón',
                'id_municipio' => '104',
                'created_at'    => new DateTime,
                'updated_at'    => new DateTime
            ],
            [
                'descripcion' => 'Las Galeras',
                'id_municipio' => '104',
                'created_at'    => new DateTime,
                'updated_at'    => new DateTime
            ],
            [
                'descripcion' => 'Hato Damas',
                'id_municipio' => '107',
                'created_at'    => new DateTime,
                'updated_at'    => new DateTime
            ],
            [
                'descripcion' => 'El Carril',
                'id_municipio' => '108',
                'created_at'    => new DateTime,
                'updated_at'    => new DateTime
            ],
            [
                'descripcion' => 'Cambita El Pueblecito',
                'id_municipio' => '109',
                'created_at'    => new DateTime,
                'updated_at'    => new DateTime
            ],
            [
                'descripcion' => 'La Cuchilla',
                'id_municipio' => '113',
                'created_at'    => new DateTime,
                'updated_at'    => new DateTime
            ],
            [
                'descripcion' => 'Medina',
                'id_municipio' => '113',
                'created_at'    => new DateTime,
                'updated_at'    => new DateTime
            ],
            [
                'descripcion' => 'San José del Puerto',
                'id_municipio' => '113',
                'created_at'    => new DateTime,
                'updated_at'    => new DateTime
            ],
            [
                'descripcion' => 'El Naranjal',
                'id_municipio' => '115',
                'created_at'    => new DateTime,
                'updated_at'    => new DateTime
            ],
            [
                'descripcion' => 'El Pinar',
                'id_municipio' => '115',
                'created_at'    => new DateTime,
                'updated_at'    => new DateTime
            ],
            [
                'descripcion' => 'La Ciénaga',
                'id_municipio' => '115',
                'created_at'    => new DateTime,
                'updated_at'    => new DateTime
            ],
            [
                'descripcion' => 'Nizao-Las Auyamas',
                'id_municipio' => '115',
                'created_at'    => new DateTime,
                'updated_at'    => new DateTime
            ],
            [
                'descripcion' => 'El Rosario',
                'id_municipio' => '118',
                'created_at'    => new DateTime,
                'updated_at'    => new DateTime
            ],
            [
                'descripcion' => 'Guanito',
                'id_municipio' => '118',
                'created_at'    => new DateTime,
                'updated_at'    => new DateTime
            ],
            [
                'descripcion' => 'Hato del Padre',
                'id_municipio' => '118',
                'created_at'    => new DateTime,
                'updated_at'    => new DateTime
            ],
            [
                'descripcion' => 'Hato Nuevo',
                'id_municipio' => '118',
                'created_at'    => new DateTime,
                'updated_at'    => new DateTime
            ],
            [
                'descripcion' => 'La Jagua',
                'id_municipio' => '118',
                'created_at'    => new DateTime,
                'updated_at'    => new DateTime
            ],
            [
                'descripcion' => 'Las Charcas de María Nova',
                'id_municipio' => '118',
                'created_at'    => new DateTime,
                'updated_at'    => new DateTime
            ],
            [
                'descripcion' => 'Pedro Corto',
                'id_municipio' => '118',
                'created_at'    => new DateTime,
                'updated_at'    => new DateTime
            ],
            [
                'descripcion' => 'Sabana Alta',
                'id_municipio' => '118',
                'created_at'    => new DateTime,
                'updated_at'    => new DateTime
            ],
            [
                'descripcion' => 'Sabaneta',
                'id_municipio' => '118',
                'created_at'    => new DateTime,
                'updated_at'    => new DateTime
            ],
            [
                'descripcion' => 'Arroyo Cano',
                'id_municipio' => '119',
                'created_at'    => new DateTime,
                'updated_at'    => new DateTime
            ],
            [
                'descripcion' => 'Yaque',
                'id_municipio' => '119',
                'created_at'    => new DateTime,
                'updated_at'    => new DateTime
            ],
            [
                'descripcion' => 'Batista',
                'id_municipio' => '120',
                'created_at'    => new DateTime,
                'updated_at'    => new DateTime
            ],
            [
                'descripcion' => 'Derrumbadero',
                'id_municipio' => '120',
                'created_at'    => new DateTime,
                'updated_at'    => new DateTime
            ],
            [
                'descripcion' => 'Jínova',
                'id_municipio' => '121',
                'created_at'    => new DateTime,
                'updated_at'    => new DateTime
            ],
            [
                'descripcion' => 'Carrera de Yegua',
                'id_municipio' => '122',
                'created_at'    => new DateTime,
                'updated_at'    => new DateTime
            ],
            [
                'descripcion' => 'Matayaya',
                'id_municipio' => '122',
                'created_at'    => new DateTime,
                'updated_at'    => new DateTime
            ],
            [
                'descripcion' => 'Jorjillo',
                'id_municipio' => '123',
                'created_at'    => new DateTime,
                'updated_at'    => new DateTime
            ],
            [
                'descripcion' => 'El Puerto',
                'id_municipio' => '129',
                'created_at'    => new DateTime,
                'updated_at'    => new DateTime
            ],
            [
                'descripcion' => 'Gautier',
                'id_municipio' => '129',
                'created_at'    => new DateTime,
                'updated_at'    => new DateTime
            ],
            [
                'descripcion' => 'Caballero',
                'id_municipio' => '130',
                'created_at'    => new DateTime,
                'updated_at'    => new DateTime
            ],
            [
                'descripcion' => 'Comedero Arriba',
                'id_municipio' => '130',
                'created_at'    => new DateTime,
                'updated_at'    => new DateTime
            ],
            [
                'descripcion' => 'Quita Sueño',
                'id_municipio' => '130',
                'created_at'    => new DateTime,
                'updated_at'    => new DateTime
            ],
            [
                'descripcion' => 'La Cueva',
                'id_municipio' => '131',
                'created_at'    => new DateTime,
                'updated_at'    => new DateTime
            ],
            [
                'descripcion' => 'Platanal',
                'id_municipio' => '131',
                'created_at'    => new DateTime,
                'updated_at'    => new DateTime
            ],
            [
                'descripcion' => 'Angelina',
                'id_municipio' => '133',
                'created_at'    => new DateTime,
                'updated_at'    => new DateTime
            ],
            [
                'descripcion' => 'La Bija',
                'id_municipio' => '133',
                'created_at'    => new DateTime,
                'updated_at'    => new DateTime
            ],
            [
                'descripcion' => 'Hernando Alonzo',
                'id_municipio' => '133',
                'created_at'    => new DateTime,
                'updated_at'    => new DateTime
            ],
            [
                'descripcion' => 'Baitoa',
                'id_municipio' => '134',
                'created_at'    => new DateTime,
                'updated_at'    => new DateTime
            ],
            [
                'descripcion' => 'Hato del Yaque',
                'id_municipio' => '134',
                'created_at'    => new DateTime,
                'updated_at'    => new DateTime
            ],
            [
                'descripcion' => 'La Canela',
                'id_municipio' => '134',
                'created_at'    => new DateTime,
                'updated_at'    => new DateTime
            ],
            [
                'descripcion' => 'Pedro García',
                'id_municipio' => '134',
                'created_at'    => new DateTime,
                'updated_at'    => new DateTime
            ],
            [
                'descripcion' => 'San Francisco de Jacagua',
                'id_municipio' => '134',
                'created_at'    => new DateTime,
                'updated_at'    => new DateTime
            ],
            [
                'descripcion' => 'El Caimito',
                'id_municipio' => '136',
                'created_at'    => new DateTime,
                'updated_at'    => new DateTime
            ],
            [
                'descripcion' => 'Juncalito',
                'id_municipio' => '136',
                'created_at'    => new DateTime,
                'updated_at'    => new DateTime
            ],
            [
                'descripcion' => 'Las Palomas',
                'id_municipio' => '137',
                'created_at'    => new DateTime,
                'updated_at'    => new DateTime
            ],
            [
                'descripcion' => 'Canabacoa',
                'id_municipio' => '137',
                'created_at'    => new DateTime,
                'updated_at'    => new DateTime
            ],
            [
                'descripcion' => 'Guayabal',
                'id_municipio' => '137',
                'created_at'    => new DateTime,
                'updated_at'    => new DateTime
            ],
            [
                'descripcion' => 'El Rubio',
                'id_municipio' => '140',
                'created_at'    => new DateTime,
                'updated_at'    => new DateTime
            ],
            [
                'descripcion' => 'La Cuesta',
                'id_municipio' => '140',
                'created_at'    => new DateTime,
                'updated_at'    => new DateTime
            ],
            [
                'descripcion' => 'Las Placetas',
                'id_municipio' => '140',
                'created_at'    => new DateTime,
                'updated_at'    => new DateTime
            ],
            [
                'descripcion' => 'Canca La Piedra',
                'id_municipio' => '141',
                'created_at'    => new DateTime,
                'updated_at'    => new DateTime
            ],
            [
                'descripcion' => 'El Limón',
                'id_municipio' => '142',
                'created_at'    => new DateTime,
                'updated_at'    => new DateTime
            ],
            [
                'descripcion' => 'Palmar Arriba',
                'id_municipio' => '142',
                'created_at'    => new DateTime,
                'updated_at'    => new DateTime
            ],
            [
                'descripcion' => 'San Luis',
                'id_municipio' => '146',
                'created_at'    => new DateTime,
                'updated_at'    => new DateTime
            ],
            [
                'descripcion' => 'La Caleta',
                'id_municipio' => '147',
                'created_at'    => new DateTime,
                'updated_at'    => new DateTime
            ],
            [
                'descripcion' => 'Palmarejo-Villa Linda',
                'id_municipio' => '148',
                'created_at'    => new DateTime,
                'updated_at'    => new DateTime
            ],
            [
                'descripcion' => 'Pantoja',
                'id_municipio' => '148',
                'created_at'    => new DateTime,
                'updated_at'    => new DateTime
            ],
            [
                'descripcion' => 'La Cuaba',
                'id_municipio' => '149',
                'created_at'    => new DateTime,
                'updated_at'    => new DateTime
            ],
            [
                'descripcion' => 'La Guáyiga',
                'id_municipio' => '149',
                'created_at'    => new DateTime,
                'updated_at'    => new DateTime
            ],
            [
                'descripcion' => 'Hato Viejo',
                'id_municipio' => '150',
                'created_at'    => new DateTime,
                'updated_at'    => new DateTime
            ],
            [
                'descripcion' => 'La Victoria',
                'id_municipio' => '151',
                'created_at'    => new DateTime,
                'updated_at'    => new DateTime
            ],
            [
                'descripcion' => 'Amina',
                'id_municipio' => '153',
                'created_at'    => new DateTime,
                'updated_at'    => new DateTime
            ],
            [
                'descripcion' => 'Guatapanal',
                'id_municipio' => '153',
                'created_at'    => new DateTime,
                'updated_at'    => new DateTime
            ],
            [
                'descripcion' => 'Jaibón (Pueblo Nuevo)',
                'id_municipio' => '153',
                'created_at'    => new DateTime,
                'updated_at'    => new DateTime
            ],
            [
                'descripcion' => 'Boca de Mao',
                'id_municipio' => '154',
                'created_at'    => new DateTime,
                'updated_at'    => new DateTime
            ],
            [
                'descripcion' => 'Jicomé',
                'id_municipio' => '154',
                'created_at'    => new DateTime,
                'updated_at'    => new DateTime
            ],
            [
                'descripcion' => 'Maizal',
                'id_municipio' => '154',
                'created_at'    => new DateTime,
                'updated_at'    => new DateTime
            ],
            [
                'descripcion' => 'Paradero',
                'id_municipio' => '154',
                'created_at'    => new DateTime,
                'updated_at'    => new DateTime
            ],
            [
                'descripcion' => 'Cruce de Guayacanes',
                'id_municipio' => '155',
                'created_at'    => new DateTime,
                'updated_at'    => new DateTime
            ],
            [
                'descripcion' => 'Jaibón',
                'id_municipio' => '155',
                'created_at'    => new DateTime,
                'updated_at'    => new DateTime
            ],
            [
                'descripcion' => 'La Caya',
                'id_municipio' => '155',
                'created_at'    => new DateTime,
                'updated_at'    => new DateTime
            ],
        );
        Distrito::insert($data);
    }
}
