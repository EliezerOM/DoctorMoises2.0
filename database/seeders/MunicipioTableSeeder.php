<?php

namespace Database\Seeders;

use DateTime;
use App\Models\Municipio;
use Illuminate\Database\Seeder;

class MunicipioTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = array(
            [
                'descripcion' => 'Distrito Nacional',
                'id_provincia' => '5',
                'created_at' 	=> new DateTime,
                'updated_at' 	=> new DateTime
            ],
            [
                'descripcion' => 'Azua de Compostela',
                'id_provincia' => '1',
                'created_at' 	=> new DateTime,
                'updated_at' 	=> new DateTime
            ],
            [
                'descripcion' => 'Estebanía',
                'id_provincia' => '1',
                'created_at' 	=> new DateTime,
                'updated_at' 	=> new DateTime
            ],
            [
                'descripcion' => 'Guayabal',
                'id_provincia' => '1',
                'created_at' 	=> new DateTime,
                'updated_at' 	=> new DateTime
            ],
            [
                'descripcion' => 'Las Charcas',
                'id_provincia' => '1',
                'created_at' 	=> new DateTime,
                'updated_at' 	=> new DateTime
            ],
            [
                'descripcion' => 'Las Yayas de Viajama',
                'id_provincia' => '1',
                'created_at' 	=> new DateTime,
                'updated_at' 	=> new DateTime
            ],
            [
                'descripcion' => 'Padre Las Casas',
                'id_provincia' => '1',
                'created_at' 	=> new DateTime,
                'updated_at' 	=> new DateTime
            ],
            [
                'descripcion' => 'Peralta',
                'id_provincia' => '1',
                'created_at' 	=> new DateTime,
                'updated_at' 	=> new DateTime
            ],
            [
                'descripcion' => 'Pueblo Viejo',
                'id_provincia' => '1',
                'created_at' 	=> new DateTime,
                'updated_at' 	=> new DateTime
            ],
            [
                'descripcion' => 'Sabana Yegua',
                'id_provincia' => '1',
                'created_at' 	=> new DateTime,
                'updated_at' 	=> new DateTime
            ],
            [
                'descripcion' => 'Tábara Arriba',
                'id_provincia' => '1',
                'created_at' 	=> new DateTime,
                'updated_at' 	=> new DateTime
            ],
            [
                'descripcion' => 'Neiba',
                'id_provincia' => '2',
                'created_at' 	=> new DateTime,
                'updated_at' 	=> new DateTime
            ],
            [
                'descripcion' => 'Galván',
                'id_provincia' => '2',
                'created_at' 	=> new DateTime,
                'updated_at' 	=> new DateTime
            ],
            [
                'descripcion' => 'Los Ríos',
                'id_provincia' => '2',
                'created_at' 	=> new DateTime,
                'updated_at' 	=> new DateTime
            ],
            [
                'descripcion' => 'Tamayo',
                'id_provincia' => '2',
                'created_at' 	=> new DateTime,
                'updated_at' 	=> new DateTime
            ],
            [
                'descripcion' => 'Villa Jaragua',
                'id_provincia' => '2',
                'created_at' 	=> new DateTime,
                'updated_at' 	=> new DateTime
            ],
            [
                'descripcion' => 'Barahona',
                'id_provincia' => '3',
                'created_at' 	=> new DateTime,
                'updated_at' 	=> new DateTime
            ],
            [
                'descripcion' => 'Cabral',
                'id_provincia' => '3',
                'created_at' 	=> new DateTime,
                'updated_at' 	=> new DateTime
            ],
            [
                'descripcion' => 'El Peñón',
                'id_provincia' => '3',
                'created_at' 	=> new DateTime,
                'updated_at' 	=> new DateTime
            ],
            [
                'descripcion' => 'Enriquillo',
                'id_provincia' => '3',
                'created_at' 	=> new DateTime,
                'updated_at' 	=> new DateTime
            ],
            [
                'descripcion' => 'Fundación',
                'id_provincia' => '3',
                'created_at' 	=> new DateTime,
                'updated_at' 	=> new DateTime
            ],
            [
                'descripcion' => 'Jaquimeyes',
                'id_provincia' => '3',
                'created_at' 	=> new DateTime,
                'updated_at' 	=> new DateTime
            ],
            [
                'descripcion' => 'La Ciénaga',
                'id_provincia' => '3',
                'created_at' 	=> new DateTime,
                'updated_at' 	=> new DateTime
            ],
            [
                'descripcion' => 'Las Salinas',
                'id_provincia' => '3',
                'created_at' 	=> new DateTime,
                'updated_at' 	=> new DateTime
            ],
            [
                'descripcion' => 'Paraíso',
                'id_provincia' => '3',
                'created_at' 	=> new DateTime,
                'updated_at' 	=> new DateTime
            ],
            [
                'descripcion' => 'Polo',
                'id_provincia' => '3',
                'created_at' 	=> new DateTime,
                'updated_at' 	=> new DateTime
            ],
            [
                'descripcion' => 'Vicente Noble',
                'id_provincia' => '3',
                'created_at' 	=> new DateTime,
                'updated_at' 	=> new DateTime
            ],
            [
                'descripcion' => 'Dajabón',
                'id_provincia' => '4',
                'created_at' 	=> new DateTime,
                'updated_at' 	=> new DateTime
            ],
            [
                'descripcion' => 'El Pino',
                'id_provincia' => '4',
                'created_at' 	=> new DateTime,
                'updated_at' 	=> new DateTime
            ],
            [
                'descripcion' => 'Loma de Cabrera',
                'id_provincia' => '4',
                'created_at' 	=> new DateTime,
                'updated_at' 	=> new DateTime
            ],
            [
                'descripcion' => 'Partido',
                'id_provincia' => '4',
                'created_at' 	=> new DateTime,
                'updated_at' 	=> new DateTime
            ],
            [
                'descripcion' => 'Restauración',
                'id_provincia' => '4',
                'created_at' 	=> new DateTime,
                'updated_at' 	=> new DateTime
            ],
            [
                'descripcion' => 'San Francisco de Macorís',
                'id_provincia' => '6',
                'created_at' 	=> new DateTime,
                'updated_at' 	=> new DateTime
            ],
            [
                'descripcion' => 'Arenoso',
                'id_provincia' => '6',
                'created_at' 	=> new DateTime,
                'updated_at' 	=> new DateTime
            ],
            [
                'descripcion' => 'Castillo',
                'id_provincia' => '6',
                'created_at' 	=> new DateTime,
                'updated_at' 	=> new DateTime
            ],
            [
                'descripcion' => 'Eugenio María de Hostos',
                'id_provincia' => '6',
                'created_at' 	=> new DateTime,
                'updated_at' 	=> new DateTime
            ],
            [
                'descripcion' => 'Las Guáranas',
                'id_provincia' => '6',
                'created_at' 	=> new DateTime,
                'updated_at' 	=> new DateTime
            ],
            [
                'descripcion' => 'Pimentel',
                'id_provincia' => '6',
                'created_at' 	=> new DateTime,
                'updated_at' 	=> new DateTime
            ],
            [
                'descripcion' => 'Villa Riva',
                'id_provincia' => '6',
                'created_at' 	=> new DateTime,
                'updated_at' 	=> new DateTime
            ],
            [
                'descripcion' => 'El Seibo',
                'id_provincia' => '8',
                'created_at' 	=> new DateTime,
                'updated_at' 	=> new DateTime
            ],
            [
                'descripcion' => 'Miches',
                'id_provincia' => '8',
                'created_at' 	=> new DateTime,
                'updated_at' 	=> new DateTime
            ],
            [
                'descripcion' => 'Comendador',
                'id_provincia' => '7',
                'created_at' 	=> new DateTime,
                'updated_at' 	=> new DateTime
            ],
            [
                'descripcion' => 'Bánica',
                'id_provincia' => '7',
                'created_at' 	=> new DateTime,
                'updated_at' 	=> new DateTime
            ],
            [
                'descripcion' => 'El Llano',
                'id_provincia' => '7',
                'created_at' 	=> new DateTime,
                'updated_at' 	=> new DateTime
            ],
            [
                'descripcion' => 'Hondo Valle',
                'id_provincia' => '7',
                'created_at' 	=> new DateTime,
                'updated_at' 	=> new DateTime
            ],
            [
                'descripcion' => 'Juan Santiago',
                'id_provincia' => '7',
                'created_at' 	=> new DateTime,
                'updated_at' 	=> new DateTime
            ],
            [
                'descripcion' => 'Pedro Santana',
                'id_provincia' => '7',
                'created_at' 	=> new DateTime,
                'updated_at' 	=> new DateTime
            ],
            [
                'descripcion' => 'Moca',
                'id_provincia' => '9',
                'created_at' 	=> new DateTime,
                'updated_at' 	=> new DateTime
            ],
            [
                'descripcion' => 'Cayetano Germosén',
                'id_provincia' => '9',
                'created_at' 	=> new DateTime,
                'updated_at' 	=> new DateTime
            ],
            [
                'descripcion' => 'Gaspar Hernández',
                'id_provincia' => '9',
                'created_at' 	=> new DateTime,
                'updated_at' 	=> new DateTime
            ],
            [
                'descripcion' => 'Jamao al Norte',
                'id_provincia' => '9',
                'created_at' 	=> new DateTime,
                'updated_at' 	=> new DateTime
            ],
            [
                'descripcion' => 'Hato Mayor del Rey',
                'id_provincia' => '10',
                'created_at' 	=> new DateTime,
                'updated_at' 	=> new DateTime
            ],
            [
                'descripcion' => 'El Valle',
                'id_provincia' => '10',
                'created_at' 	=> new DateTime,
                'updated_at' 	=> new DateTime
            ],
            [
                'descripcion' => 'Sabana de la Mar',
                'id_provincia' => '10',
                'created_at' 	=> new DateTime,
                'updated_at' 	=> new DateTime
            ],
            [
                'descripcion' => 'Salcedo',
                'id_provincia' => '11',
                'created_at' 	=> new DateTime,
                'updated_at' 	=> new DateTime
            ],
            [
                'descripcion' => 'Tenares',
                'id_provincia' => '11',
                'created_at' 	=> new DateTime,
                'updated_at' 	=> new DateTime
            ],
            [
                'descripcion' => 'Villa Tapia',
                'id_provincia' => '11',
                'created_at' 	=> new DateTime,
                'updated_at' 	=> new DateTime
            ],
            [
                'descripcion' => 'Jimaní',
                'id_provincia' => '12',
                'created_at' 	=> new DateTime,
                'updated_at' 	=> new DateTime
            ],
            [
                'descripcion' => 'Cristóbal',
                'id_provincia' => '12',
                'created_at' 	=> new DateTime,
                'updated_at' 	=> new DateTime
            ],
            [
                'descripcion' => 'Duvergé',
                'id_provincia' => '12',
                'created_at' 	=> new DateTime,
                'updated_at' 	=> new DateTime
            ],
            [
                'descripcion' => 'La Descubierta',
                'id_provincia' => '12',
                'created_at' 	=> new DateTime,
                'updated_at' 	=> new DateTime
            ],
            [
                'descripcion' => 'Mella',
                'id_provincia' => '12',
                'created_at' 	=> new DateTime,
                'updated_at' 	=> new DateTime
            ],
            [
                'descripcion' => 'Postrer Río',
                'id_provincia' => '12',
                'created_at' 	=> new DateTime,
                'updated_at' 	=> new DateTime
            ],
            [
                'descripcion' => 'Higüey',
                'id_provincia' => '13',
                'created_at' 	=> new DateTime,
                'updated_at' 	=> new DateTime
            ],
            [
                'descripcion' => 'San Rafael del Yuma',
                'id_provincia' => '13',
                'created_at' 	=> new DateTime,
                'updated_at' 	=> new DateTime
            ],
            [
                'descripcion' => 'La Romana',
                'id_provincia' => '14',
                'created_at' 	=> new DateTime,
                'updated_at' 	=> new DateTime
            ],
            [
                'descripcion' => 'Guaymate',
                'id_provincia' => '14',
                'created_at' 	=> new DateTime,
                'updated_at' 	=> new DateTime
            ],
            [
                'descripcion' => 'Villa Hermosa',
                'id_provincia' => '14',
                'created_at' 	=> new DateTime,
                'updated_at' 	=> new DateTime
            ],
            [
                'descripcion' => 'La Concepción de la Vega',
                'id_provincia' => '15',
                'created_at' 	=> new DateTime,
                'updated_at' 	=> new DateTime
            ],
            [
                'descripcion' => 'Constanza',
                'id_provincia' => '15',
                'created_at' 	=> new DateTime,
                'updated_at' 	=> new DateTime
            ],
            [
                'descripcion' => 'Jarabacoa',
                'id_provincia' => '15',
                'created_at' 	=> new DateTime,
                'updated_at' 	=> new DateTime
            ],
            [
                'descripcion' => 'Jima Abajo',
                'id_provincia' => '15',
                'created_at' 	=> new DateTime,
                'updated_at' 	=> new DateTime
            ],
            [
                'descripcion' => 'Nagua',
                'id_provincia' => '16',
                'created_at' 	=> new DateTime,
                'updated_at' 	=> new DateTime
            ],
            [
                'descripcion' => 'Cabrera',
                'id_provincia' => '16',
                'created_at' 	=> new DateTime,
                'updated_at' 	=> new DateTime
            ],
            [
                'descripcion' => 'El Factor',
                'id_provincia' => '16',
                'created_at' 	=> new DateTime,
                'updated_at' 	=> new DateTime
            ],
            [
                'descripcion' => 'Río San Juan',
                'id_provincia' => '16',
                'created_at' 	=> new DateTime,
                'updated_at' 	=> new DateTime
            ],
            [
                'descripcion' => 'Bonao',
                'id_provincia' => '17',
                'created_at' 	=> new DateTime,
                'updated_at' 	=> new DateTime
            ],
            [
                'descripcion' => 'Maimón',
                'id_provincia' => '17',
                'created_at' 	=> new DateTime,
                'updated_at' 	=> new DateTime
            ],
            [
                'descripcion' => 'Piedra Blanca',
                'id_provincia' => '17',
                'created_at' 	=> new DateTime,
                'updated_at' 	=> new DateTime
            ],
            [
                'descripcion' => 'Monte Cristi',
                'id_provincia' => '18',
                'created_at' 	=> new DateTime,
                'updated_at' 	=> new DateTime
            ],
            [
                'descripcion' => 'Castañuela',
                'id_provincia' => '18',
                'created_at' 	=> new DateTime,
                'updated_at' 	=> new DateTime
            ],
            [
                'descripcion' => 'Guayubin',
                'id_provincia' => '18',
                'created_at' 	=> new DateTime,
                'updated_at' 	=> new DateTime
            ],
            [
                'descripcion' => 'Las Matas de Santa Cruz',
                'id_provincia' => '18',
                'created_at' 	=> new DateTime,
                'updated_at' 	=> new DateTime
            ],
            [
                'descripcion' => 'Pepillo Salcedo',
                'id_provincia' => '18',
                'created_at' 	=> new DateTime,
                'updated_at' 	=> new DateTime
            ],
            [
                'descripcion' => 'Villas Vásquez',
                'id_provincia' => '18',
                'created_at' 	=> new DateTime,
                'updated_at' 	=> new DateTime
            ],
            [
                'descripcion' => 'Monte Plata',
                'id_provincia' => '19',
                'created_at' 	=> new DateTime,
                'updated_at' 	=> new DateTime
            ],
            [
                'descripcion' => 'Bayaguana',
                'id_provincia' => '19',
                'created_at' 	=> new DateTime,
                'updated_at' 	=> new DateTime
            ],
            [
                'descripcion' => 'Peralvillo',
                'id_provincia' => '19',
                'created_at' 	=> new DateTime,
                'updated_at' 	=> new DateTime
            ],
            [
                'descripcion' => 'Sabana Grande de Boyá',
                'id_provincia' => '19',
                'created_at' 	=> new DateTime,
                'updated_at' 	=> new DateTime
            ],
            [
                'descripcion' => 'Yamasá',
                'id_provincia' => '19',
                'created_at' 	=> new DateTime,
                'updated_at' 	=> new DateTime
            ],
            [
                'descripcion' => 'Perdernales',
                'id_provincia' => '20',
                'created_at' 	=> new DateTime,
                'updated_at' 	=> new DateTime
            ],
            [
                'descripcion' => 'Oviedo',
                'id_provincia' => '20',
                'created_at' 	=> new DateTime,
                'updated_at' 	=> new DateTime
            ],
            [
                'descripcion' => 'Baní',
                'id_provincia' => '21',
                'created_at' 	=> new DateTime,
                'updated_at' 	=> new DateTime
            ],
            [
                'descripcion' => 'Nizao',
                'id_provincia' => '21',
                'created_at' 	=> new DateTime,
                'updated_at' 	=> new DateTime
            ],
            [
                'descripcion' => 'Puerto Plata',
                'id_provincia' => '22',
                'created_at' 	=> new DateTime,
                'updated_at' 	=> new DateTime
            ],
            [
                'descripcion' => 'Altamira',
                'id_provincia' => '22',
                'created_at' 	=> new DateTime,
                'updated_at' 	=> new DateTime
            ],
            [
                'descripcion' => 'Guananico',
                'id_provincia' => '22',
                'created_at' 	=> new DateTime,
                'updated_at' 	=> new DateTime
            ],
            [
                'descripcion' => 'Imbert',
                'id_provincia' => '22',
                'created_at' 	=> new DateTime,
                'updated_at' 	=> new DateTime
            ],
            [
                'descripcion' => 'Los Hidalgos',
                'id_provincia' => '22',
                'created_at' 	=> new DateTime,
                'updated_at' 	=> new DateTime
            ],
            [
                'descripcion' => 'Luperón',
                'id_provincia' => '22',
                'created_at' 	=> new DateTime,
                'updated_at' 	=> new DateTime
            ],
            [
                'descripcion' => 'Sosúa',
                'id_provincia' => '22',
                'created_at' 	=> new DateTime,
                'updated_at' 	=> new DateTime
            ],
            [
                'descripcion' => 'Villa Isabela',
                'id_provincia' => '22',
                'created_at' 	=> new DateTime,
                'updated_at' 	=> new DateTime
            ],
            [
                'descripcion' => 'Villa Montellano',
                'id_provincia' => '22',
                'created_at' 	=> new DateTime,
                'updated_at' 	=> new DateTime
            ],
            [
                'descripcion' => 'Samaná',
                'id_provincia' => '23',
                'created_at' 	=> new DateTime,
                'updated_at' 	=> new DateTime
            ],
            [
                'descripcion' => 'Las Terrenas',
                'id_provincia' => '23',
                'created_at' 	=> new DateTime,
                'updated_at' 	=> new DateTime
            ],
            [
                'descripcion' => 'Sánchez',
                'id_provincia' => '23',
                'created_at' 	=> new DateTime,
                'updated_at' 	=> new DateTime
            ],
            [
                'descripcion' => 'San Cristóbal',
                'id_provincia' => '25',
                'created_at' 	=> new DateTime,
                'updated_at' 	=> new DateTime
            ],
            [
                'descripcion' => 'Bajos de Haina',
                'id_provincia' => '25',
                'created_at' 	=> new DateTime,
                'updated_at' 	=> new DateTime
            ],
            [
                'descripcion' => 'Cambita Garabito',
                'id_provincia' => '25',
                'created_at' 	=> new DateTime,
                'updated_at' 	=> new DateTime
            ],
            [
                'descripcion' => 'Los Cacaos',
                'id_provincia' => '25',
                'created_at' 	=> new DateTime,
                'updated_at' 	=> new DateTime
            ],
            [
                'descripcion' => 'Sabana Grande de Palenque',
                'id_provincia' => '25',
                'created_at' 	=> new DateTime,
                'updated_at' 	=> new DateTime
            ],
            [
                'descripcion' => 'San Gregorio de Nigua',
                'id_provincia' => '25',
                'created_at' 	=> new DateTime,
                'updated_at' 	=> new DateTime
            ],
            [
                'descripcion' => 'Villa Altagracia',
                'id_provincia' => '25',
                'created_at' 	=> new DateTime,
                'updated_at' 	=> new DateTime
            ],
            [
                'descripcion' => 'Yaguate',
                'id_provincia' => '25',
                'created_at' 	=> new DateTime,
                'updated_at' 	=> new DateTime
            ],
            [
                'descripcion' => 'San José de Ocoa',
                'id_provincia' => '26',
                'created_at' 	=> new DateTime,
                'updated_at' 	=> new DateTime
            ],
            [
                'descripcion' => 'Rancho Arriba',
                'id_provincia' => '26',
                'created_at' 	=> new DateTime,
                'updated_at' 	=> new DateTime
            ],
            [
                'descripcion' => 'Sabana Larga',
                'id_provincia' => '26',
                'created_at' 	=> new DateTime,
                'updated_at' 	=> new DateTime
            ],
            [
                'descripcion' => 'San Juan de la Maguana',
                'id_provincia' => '27',
                'created_at' 	=> new DateTime,
                'updated_at' 	=> new DateTime
            ],
            [
                'descripcion' => 'Bohechío',
                'id_provincia' => '27',
                'created_at' 	=> new DateTime,
                'updated_at' 	=> new DateTime
            ],
            [
                'descripcion' => 'El Cercado',
                'id_provincia' => '27',
                'created_at' 	=> new DateTime,
                'updated_at' 	=> new DateTime
            ],
            [
                'descripcion' => 'Juan de Herrera',
                'id_provincia' => '27',
                'created_at' 	=> new DateTime,
                'updated_at' 	=> new DateTime
            ],
            [
                'descripcion' => 'Las Matas de Farfán',
                'id_provincia' => '27',
                'created_at' 	=> new DateTime,
                'updated_at' 	=> new DateTime
            ],
            [
                'descripcion' => 'Vallejuelo',
                'id_provincia' => '27',
                'created_at' 	=> new DateTime,
                'updated_at' 	=> new DateTime
            ],
            [
                'descripcion' => 'San Pedro de Macorís',
                'id_provincia' => '28',
                'created_at' 	=> new DateTime,
                'updated_at' 	=> new DateTime
            ],
            [
                'descripcion' => 'Consuleo',
                'id_provincia' => '28',
                'created_at' 	=> new DateTime,
                'updated_at' 	=> new DateTime
            ],
            [
                'descripcion' => 'Guayacanes',
                'id_provincia' => '28',
                'created_at' 	=> new DateTime,
                'updated_at' 	=> new DateTime
            ],
            [
                'descripcion' => 'Quisqueya',
                'id_provincia' => '28',
                'created_at' 	=> new DateTime,
                'updated_at' 	=> new DateTime
            ],
            [
                'descripcion' => 'Ramón Santana',
                'id_provincia' => '28',
                'created_at' 	=> new DateTime,
                'updated_at' 	=> new DateTime
            ],
            [
                'descripcion' => 'San José de los Llanos',
                'id_provincia' => '28',
                'created_at' 	=> new DateTime,
                'updated_at' 	=> new DateTime
            ],
            [
                'descripcion' => 'Cotuí',
                'id_provincia' => '24',
                'created_at' 	=> new DateTime,
                'updated_at' 	=> new DateTime
            ],
            [
                'descripcion' => 'Cevicos',
                'id_provincia' => '24',
                'created_at' 	=> new DateTime,
                'updated_at' 	=> new DateTime
            ],
            [
                'descripcion' => 'Fantino',
                'id_provincia' => '24',
                'created_at' 	=> new DateTime,
                'updated_at' 	=> new DateTime
            ],
            [
                'descripcion' => 'La Mata',
                'id_provincia' => '24',
                'created_at' 	=> new DateTime,
                'updated_at' 	=> new DateTime
            ],
            [
                'descripcion' => 'Santiago',
                'id_provincia' => '29',
                'created_at' 	=> new DateTime,
                'updated_at' 	=> new DateTime
            ],
            [
                'descripcion' => 'Bisonó',
                'id_provincia' => '29',
                'created_at' 	=> new DateTime,
                'updated_at' 	=> new DateTime
            ],
            [
                'descripcion' => 'Jánico',
                'id_provincia' => '29',
                'created_at' 	=> new DateTime,
                'updated_at' 	=> new DateTime
            ],
            [
                'descripcion' => 'Licey al Medio',
                'id_provincia' => '29',
                'created_at' 	=> new DateTime,
                'updated_at' 	=> new DateTime
            ],
            [
                'descripcion' => 'Puñal',
                'id_provincia' => '29',
                'created_at' 	=> new DateTime,
                'updated_at' 	=> new DateTime
            ],
            [
                'descripcion' => 'Sabana Iglesia',
                'id_provincia' => '29',
                'created_at' 	=> new DateTime,
                'updated_at' 	=> new DateTime
            ],
            [
                'descripcion' => 'San José de las Matas',
                'id_provincia' => '29',
                'created_at' 	=> new DateTime,
                'updated_at' 	=> new DateTime
            ],
            [
                'descripcion' => 'Tamboril',
                'id_provincia' => '29',
                'created_at' 	=> new DateTime,
                'updated_at' 	=> new DateTime
            ],
            [
                'descripcion' => 'Villa González',
                'id_provincia' => '29',
                'created_at' 	=> new DateTime,
                'updated_at' 	=> new DateTime
            ],
            [
                'descripcion' => 'San Ignacio de Sabaneta',
                'id_provincia' => '30',
                'created_at' 	=> new DateTime,
                'updated_at' 	=> new DateTime
            ],
            [
                'descripcion' => 'Los Almácigos',
                'id_provincia' => '30',
                'created_at' 	=> new DateTime,
                'updated_at' 	=> new DateTime
            ],
            [
                'descripcion' => 'Monción',
                'id_provincia' => '30',
                'created_at' 	=> new DateTime,
                'updated_at' 	=> new DateTime
            ],
            [
                'descripcion' => 'Santo Domingo Este',
                'id_provincia' => '31',
                'created_at' 	=> new DateTime,
                'updated_at' 	=> new DateTime
            ],
            [
                'descripcion' => 'Boca Chica',
                'id_provincia' => '31',
                'created_at' 	=> new DateTime,
                'updated_at' 	=> new DateTime
            ],
            [
                'descripcion' => 'Los Alcarrizos',
                'id_provincia' => '31',
                'created_at' 	=> new DateTime,
                'updated_at' 	=> new DateTime
            ],
            [
                'descripcion' => 'Pedro Brand',
                'id_provincia' => '31',
                'created_at' 	=> new DateTime,
                'updated_at' 	=> new DateTime
            ],
            [
                'descripcion' => 'San Antonio de Guerra',
                'id_provincia' => '31',
                'created_at' 	=> new DateTime,
                'updated_at' 	=> new DateTime
            ],
            [
                'descripcion' => 'Santo Domingo Norte',
                'id_provincia' => '31',
                'created_at' 	=> new DateTime,
                'updated_at' 	=> new DateTime
            ],
            [
                'descripcion' => 'Santo Domingo Oeste',
                'id_provincia' => '31',
                'created_at' 	=> new DateTime,
                'updated_at' 	=> new DateTime
            ],
            [
                'descripcion' => 'Mao',
                'id_provincia' => '32',
                'created_at' 	=> new DateTime,
                'updated_at' 	=> new DateTime
            ],
            [
                'descripcion' => 'Esperanza',
                'id_provincia' => '32',
                'created_at' 	=> new DateTime,
                'updated_at' 	=> new DateTime
            ],
            [
                'descripcion' => 'Laguna Salada',
                'id_provincia' => '32',
                'created_at' 	=> new DateTime,
                'updated_at' 	=> new DateTime
            ]
        );
        Municipio::insert($data);
    }
}
