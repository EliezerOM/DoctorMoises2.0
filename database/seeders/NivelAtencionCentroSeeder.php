<?php

namespace Database\Seeders;

use App\Models\NivelAtencionCentro;
use Illuminate\Database\Seeder;

class NivelAtencionCentroSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        $data= array(
            ['descripcion' => 'Especial'],
            ['descripcion' => 'Normal'],
        );
        NivelAtencionCentro::insert($data);
    }
}
