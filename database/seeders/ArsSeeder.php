<?php

namespace Database\Seeders;

use App\Models\Ars;
use DateTime;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class ArsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = array(
            [
                'descripcion' => 'Humanos',
                'created_at' 	=> new DateTime,
                'Logo'=> 'https://humanoseguros.com/Style%20Library/NuevoAspecto/masterpage/images/logohumano.svg',
                'link'=>'https://humanoseguros.com',
            ],
            [
                'descripcion' => 'La Monumental',
                'created_at' 	=> new DateTime,
                'Logo'=> 'https://lamonumental.com.do/images/logo/LogoMonumental.svg',
                'link'=>'https://lamonumental.com.do',
            ],
            [
                'descripcion' => 'Angloamericana',
                'created_at' 	=> new DateTime,
                'Logo'=> 'http://www.angloamericana.com.do/images/Diapositiva1.png',
                'link'=>'http://www.angloamericana.com.do',
            ],

            [
                'descripcion' => 'Atlántica',
                'created_at' 	=> new DateTime,
                'Logo'=> 'https://www.atlantica.do/wp-content/uploads/2018/10/atlantica-seguros-logo.png',
                'link'=>'https://www.atlantica.do',
            ],
            [
                'descripcion' => 'BMI',
                'created_at' 	=> new DateTime,
                'Logo'=> 'https://www.bmicos.com/dominicana/wp-content/uploads/sites/7/2020/01/BMI-Logos_Rep-Dominicana.jpg',
                'link'=>'https://www.bmicos.com',
            ],

            [
                'descripcion' => 'Banesco',
                'created_at' 	=> new DateTime,
                'Logo'=> 'http://www.banescoseguros.com.do/wp-content/uploads/2016/09/logo.jpg',
                'link'=>'http://www.banescoseguros.com.do',
            ],

            [
                'descripcion' => 'Bupa Dominicana',
                'created_at' 	=> new DateTime,
                'Logo'=> 'https://7144241.fs1.hubspotusercontent-na1.net/hub/7144241/hubfs/bupa_logo_large-1.png?width=110&height=110&name=bupa_logo_large-1.png',
                'link'=>'https://info.bupasalud.com',
            ],

            [
                'descripcion' => 'Compañía Dominicana',
                'created_at' 	=> new DateTime,
                'Logo'=> 'https://dominicanadeseguros.com/site/wp-content/uploads/2016/10/logo.png',
                'link'=>'https://dominicanadeseguros.com',
            ],

            [
                'descripcion' => 'Cofedom',
                'created_at' 	=> new DateTime,
                'Logo'=> 'http://www.confedom.com/media/15obmyuy/logoluconfedom.png',
                'link'=>'http://www.confedom.com',
            ],
            [
                'descripcion' => 'Senasa',
                'created_at' 	=> new DateTime,
                'Logo'=> 'https://www.arssenasa.gob.do/wp-content/uploads/2018/11/Logo-SeNaSa-Variantes_Horizontal.png',
                'link'=>'https://www.arssenasa.gob.do',
            ],

            [
                'descripcion' => 'Sura',
                'created_at' 	=> new DateTime,
                'Logo'=> 'https://www.segurossura.com.do/wp-content/uploads/2019/04/sura-logo-act.png',
                'link'=>'https://www.segurossura.com.do',
            ],
            [
                'descripcion' => 'Universal',
                'created_at' 	=> new DateTime,
                'Logo'=> 'https://www.universal.com.do/Style%20Library/Images/---logo.png',
                'link'=>'https://www.universal.com.do',
            ],
            [
                'descripcion' => 'General De Seguros',
                'created_at' 	=> new DateTime,
                'Logo'=> 'https://www.generaldeseguros.com.do/images/general-de-seguros.jpg',
                'link'=>'https://www.generaldeseguros.com.do',
            ],
            [
                'descripcion' => 'hylseg',
                'created_at' 	=> new DateTime,
                'Logo'=> 'https://images.squarespace-cdn.com/content/v1/6155b3f74dfe2642dcba72c2/7b3b4208-3078-49fa-a950-abc3df0c3df6/logo_9-8-2019-removebg.png?format=1500w',
                'link'=>'https://www.hylseg.com',
            ],
            [
                'descripcion' => 'La Colonial',
                'created_at' 	=> new DateTime,
                'Logo'=> 'https://www.lacolonial.com.do/sites/default/files/logo-50-la_colonial.png',
                'link'=>'https://www.lacolonial.com.do/',
            ],
            [
                'descripcion' => 'La Internacional',
                'created_at' 	=> new DateTime,
                'Logo'=> 'https://www.la-internacional.com/wp-content/themes/lainternacional/img/logo.png',
                'link'=>'https://www.la-internacional.com/',
            ],
            [
                'descripcion' => 'Pepín',
                'created_at' 	=> new DateTime,
                'Logo'=> 'https://segurospepin.com/wp-content/uploads/2020/03/Layer-1.png',
                'link'=>'https://segurospepin.com/',
            ],
            [
                'descripcion' => 'Reservas',
                'created_at' 	=> new DateTime,
                'Logo'=> 'https://www.segurosreservas.com/SiteAssets/logo-seguros-sin-espacios.png',
                'link'=>'https://www.segurosreservas.com/Pages/Home.aspx',
            ],
            [
                'descripcion' => 'Futuro',
                'created_at' 	=> new DateTime,
                'Logo'=> 'https://www.arsfuturo.com/wp-content/uploads/2019/10/FUTURO_LOGOBLUE-1.svg',
                'link'=>'https://www.arsfuturo.com/',
            ],
            [
                'descripcion' => 'Cuna Mutual',
                'created_at' 	=> new DateTime,
                'Logo'=> 'https://www.cunamutual.com.do/wp-content/uploads/2016/06/logo.png',
                'link'=>'https://cunamutual.com.do/',
            ],
        );
        Ars::insert($data);
    }
}
