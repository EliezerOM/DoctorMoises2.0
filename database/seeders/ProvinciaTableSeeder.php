<?php

namespace Database\Seeders;

use DateTime;
use App\Models\Provincia;
use Illuminate\Database\Seeder;

class ProvinciaTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = array(
            [
                'descripcion' => 'Azua',
                'created_at' 	=> new DateTime,
                'updated_at' 	=> new DateTime
            ],
            [
                'descripcion' => 'Bahoruco',
                'created_at' 	=> new DateTime,
                'updated_at' 	=> new DateTime
            ],
            [
                'descripcion' => 'Barahona',
                'created_at' 	=> new DateTime,
                'updated_at' 	=> new DateTime
            ],
            [
                'descripcion' => 'Dajabón',
                'created_at' 	=> new DateTime,
                'updated_at' 	=> new DateTime
            ],
            [
                'descripcion' => 'Distrito Nacional',
                'created_at' 	=> new DateTime,
                'updated_at' 	=> new DateTime
            ],
            [
                'descripcion' => 'Duarte',
                'created_at' 	=> new DateTime,
                'updated_at' 	=> new DateTime
            ],
            [
                'descripcion' => 'Elías Piña',
                'created_at' 	=> new DateTime,
                'updated_at' 	=> new DateTime
            ],
            [
                'descripcion' => 'El Seibo',
                'created_at' 	=> new DateTime,
                'updated_at' 	=> new DateTime
            ],
            [
                'descripcion' => 'Espaillat',
                'created_at' 	=> new DateTime,
                'updated_at' 	=> new DateTime
            ],
            [
                'descripcion' => 'Hato Mayor',
                'created_at' 	=> new DateTime,
                'updated_at' 	=> new DateTime
            ],
            [
                'descripcion' => 'Hermanas Mirabal',
                'created_at' 	=> new DateTime,
                'updated_at' 	=> new DateTime
            ],
            [
                'descripcion' => 'Independencia',
                'created_at' 	=> new DateTime,
                'updated_at' 	=> new DateTime
            ],
            [
                'descripcion' => 'La Altagracia',
                'created_at' 	=> new DateTime,
                'updated_at' 	=> new DateTime
            ],
            [
                'descripcion' => 'La Romana',
                'created_at' 	=> new DateTime,
                'updated_at' 	=> new DateTime
            ],
            [
                'descripcion' => 'La Vega',
                'created_at' 	=> new DateTime,
                'updated_at' 	=> new DateTime
            ],
            [
                'descripcion' => 'María Trinidad Sánchez',
                'created_at' 	=> new DateTime,
                'updated_at' 	=> new DateTime
            ],
            [
                'descripcion' => 'Monseñor Nouel',
                'created_at' 	=> new DateTime,
                'updated_at' 	=> new DateTime
            ],
            [
                'descripcion' => 'Monte Cristi',
                'created_at' 	=> new DateTime,
                'updated_at' 	=> new DateTime
            ],
            [
                'descripcion' => 'Monte Plata',
                'created_at' 	=> new DateTime,
                'updated_at' 	=> new DateTime
            ],
            [
                'descripcion' => 'Pedernales',
                'created_at' 	=> new DateTime,
                'updated_at' 	=> new DateTime
            ],
            [
                'descripcion' => 'Peravia',
                'created_at' 	=> new DateTime,
                'updated_at' 	=> new DateTime
            ],
            [
                'descripcion' => 'Puerto Plata',
                'created_at' 	=> new DateTime,
                'updated_at' 	=> new DateTime
            ],
            [
                'descripcion' => 'Samaná',
                'created_at' 	=> new DateTime,
                'updated_at' 	=> new DateTime
            ],
            [
                'descripcion' => 'Sánchez Ramírez',
                'created_at' 	=> new DateTime,
                'updated_at' 	=> new DateTime
            ],
            [
                'descripcion' => 'San Cristóbal',
                'created_at' 	=> new DateTime,
                'updated_at' 	=> new DateTime
            ],
            [
                'descripcion' => 'San José de Ocoa',
                'created_at' 	=> new DateTime,
                'updated_at' 	=> new DateTime
            ],
            [
                'descripcion' => 'San Juan',
                'created_at' 	=> new DateTime,
                'updated_at' 	=> new DateTime
            ],
            [
                'descripcion' => 'San Pedro de Macorís',
                'created_at' 	=> new DateTime,
                'updated_at' 	=> new DateTime
            ],
            [
                'descripcion' => 'Santiago',
                'created_at' 	=> new DateTime,
                'updated_at' 	=> new DateTime
            ],
            [
                'descripcion' => 'Santiago Rodríguez',
                'created_at' 	=> new DateTime,
                'updated_at' 	=> new DateTime
            ],
            [
                'descripcion' => 'Santo Domingo',
                'created_at' 	=> new DateTime,
                'updated_at' 	=> new DateTime
            ],
            [
                'descripcion' => 'Valverde',
                'created_at' 	=> new DateTime,
                'updated_at' 	=> new DateTime
            ]
        );
        Provincia::insert($data);
    }
}
