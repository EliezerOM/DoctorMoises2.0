<?php

namespace Database\Seeders;

use App\Models\TipoCentro;
use Illuminate\Database\Seeder;

class TipoCentroSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = array(
            ['descripcion' => 'Clínica'],
            ['descripcion' => 'Hospital']
        );
        //
        TipoCentro::insert($data);
    }
}
