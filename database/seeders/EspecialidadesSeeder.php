<?php

namespace Database\Seeders;

use DateTime;
use App\Models\Especialidad;
use Illuminate\Database\Seeder;

class EspecialidadesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = array
        (
            //Especialidades clínicas
            ['descripcion' => 'Alergología', 'created_at' => new DateTime, 'updated_at' => new DateTime, 'id_tipo_especialidad' => 1],
            ['descripcion' => 'Anestesiología', 'created_at' => new DateTime, 'updated_at' => new DateTime, 'id_tipo_especialidad' => 1],
            ['descripcion' => 'Angiología', 'created_at' => new DateTime, 'updated_at' => new DateTime, 'id_tipo_especialidad' => 1],
            ['descripcion' => 'Cardiología', 'created_at' => new DateTime, 'updated_at' => new DateTime, 'id_tipo_especialidad' => 1],
            ['descripcion' => 'Endocrinología', 'created_at' => new DateTime, 'updated_at' => new DateTime, 'id_tipo_especialidad' => 1],
            ['descripcion' => 'Estomatología', 'created_at' => new DateTime, 'updated_at' => new DateTime, 'id_tipo_especialidad' => 1],
            ['descripcion' => 'Gastroenterología', 'created_at' => new DateTime, 'updated_at' => new DateTime, 'id_tipo_especialidad' => 1],
            ['descripcion' => 'Genética', 'created_at' => new DateTime, 'updated_at' => new DateTime, 'id_tipo_especialidad' => 1],
            ['descripcion' => 'Geriatría', 'created_at' => new DateTime, 'updated_at' => new DateTime, 'id_tipo_especialidad' => 1],
            ['descripcion' => 'Hematología', 'created_at' => new DateTime, 'updated_at' => new DateTime, 'id_tipo_especialidad' => 1],
            ['descripcion' => 'Hepatología', 'created_at' => new DateTime, 'updated_at' => new DateTime, 'id_tipo_especialidad' => 1],
            ['descripcion' => 'Infectología', 'created_at' => new DateTime, 'updated_at' => new DateTime, 'id_tipo_especialidad' => 1],
            ['descripcion' => 'Medicina aeroespacial', 'created_at' => new DateTime, 'updated_at' => new DateTime, 'id_tipo_especialidad' => 1],
            ['descripcion' => 'Medicina del deporte', 'created_at' => new DateTime, 'updated_at' => new DateTime, 'id_tipo_especialidad' => 1],
            ['descripcion' => 'Medicina familiar y comunitaria', 'created_at' => new DateTime, 'updated_at' => new DateTime, 'id_tipo_especialidad' => 1],
            ['descripcion' => 'Medicina física y rehabilitación', 'created_at' => new DateTime, 'updated_at' => new DateTime, 'id_tipo_especialidad' => 1],
            ['descripcion' => 'Medicina forense', 'created_at' => new DateTime, 'updated_at' => new DateTime, 'id_tipo_especialidad' => 1],
            ['descripcion' => 'Medicina intensiva', 'created_at' => new DateTime, 'updated_at' => new DateTime, 'id_tipo_especialidad' => 1],
            ['descripcion' => 'Medicina interna', 'created_at' => new DateTime, 'updated_at' => new DateTime, 'id_tipo_especialidad' => 1],
            ['descripcion' => 'Medicina preventiva y salud pública', 'created_at' => new DateTime, 'updated_at' => new DateTime, 'id_tipo_especialidad' => 1],
            ['descripcion' => 'Medicina del trabajo', 'created_at' => new DateTime, 'updated_at' => new DateTime, 'id_tipo_especialidad' => 1],
            ['descripcion' => 'Nefrología', 'created_at' => new DateTime, 'updated_at' => new DateTime, 'id_tipo_especialidad' => 1],
            ['descripcion' => 'Neumología', 'created_at' => new DateTime, 'updated_at' => new DateTime, 'id_tipo_especialidad' => 1],
            ['descripcion' => 'Nutriología', 'created_at' => new DateTime, 'updated_at' => new DateTime, 'id_tipo_especialidad' => 1],
            ['descripcion' => 'Oncología médica', 'created_at' => new DateTime, 'updated_at' => new DateTime, 'id_tipo_especialidad' => 1],
            ['descripcion' => 'Oncología radioterápica', 'created_at' => new DateTime, 'updated_at' => new DateTime, 'id_tipo_especialidad' => 1],
            ['descripcion' => 'Pediatría', 'created_at' => new DateTime, 'updated_at' => new DateTime, 'id_tipo_especialidad' => 1],
            ['descripcion' => 'Psiquiatría', 'created_at' => new DateTime, 'updated_at' => new DateTime, 'id_tipo_especialidad' => 1],
            ['descripcion' => 'Reumatología', 'created_at' => new DateTime, 'updated_at' => new DateTime, 'id_tipo_especialidad' => 1],
            ['descripcion' => 'Toxicología', 'created_at' => new DateTime, 'updated_at' => new DateTime, 'id_tipo_especialidad' => 1],


            //Especialidades Quirúrgicas
            ['descripcion' => 'Cirugía cardíaca', 'created_at' => new DateTime, 'updated_at' => new DateTime, 'id_tipo_especialidad' => 2],
            ['descripcion' => 'Cirugía general', 'created_at' => new DateTime, 'updated_at' => new DateTime, 'id_tipo_especialidad' => 2],
            ['descripcion' => 'Cirugía oral y maxilofacial', 'created_at' => new DateTime, 'updated_at' => new DateTime, 'id_tipo_especialidad' => 2],
            ['descripcion' => 'Cirugía ortopédica', 'created_at' => new DateTime, 'updated_at' => new DateTime, 'id_tipo_especialidad' => 2],
            ['descripcion' => 'Cirugía pediátrica', 'created_at' => new DateTime, 'updated_at' => new DateTime, 'id_tipo_especialidad' => 2],
            ['descripcion' => 'Cirugía plástica', 'created_at' => new DateTime, 'updated_at' => new DateTime, 'id_tipo_especialidad' => 2],
            ['descripcion' => 'Cirugía torácica', 'created_at' => new DateTime, 'updated_at' => new DateTime, 'id_tipo_especialidad' => 2],
            ['descripcion' => 'Cirugía vascular', 'created_at' => new DateTime, 'updated_at' => new DateTime, 'id_tipo_especialidad' => 2],
            ['descripcion' => 'Neurocirugía', 'created_at' => new DateTime, 'updated_at' => new DateTime, 'id_tipo_especialidad' => 2],

            //Especialidades médico-quirúrgicas
            ['descripcion' => 'Dermatología', 'created_at' => new DateTime, 'updated_at' => new DateTime, 'id_tipo_especialidad' => 3],
            ['descripcion' => 'Ginecología y obstetricia o tocología', 'created_at' => new DateTime, 'updated_at' => new DateTime, 'id_tipo_especialidad' => 3],
            ['descripcion' => 'Medicina de emergencia', 'created_at' => new DateTime, 'updated_at' => new DateTime, 'id_tipo_especialidad' => 3],
            ['descripcion' => 'Oftalmología', 'created_at' => new DateTime, 'updated_at' => new DateTime, 'id_tipo_especialidad' => 3],
            ['descripcion' => 'Otorrinolaringología', 'created_at' => new DateTime, 'updated_at' => new DateTime, 'id_tipo_especialidad' => 3],
            ['descripcion' => 'Traumatología', 'created_at' => new DateTime, 'updated_at' => new DateTime, 'id_tipo_especialidad' => 3],
            ['descripcion' => 'Urología', 'created_at' => new DateTime, 'updated_at' => new DateTime, 'id_tipo_especialidad' => 3],

            //Especialidades de laboratorio o diagnósticas
            ['descripcion' => 'Análisis clínico', 'created_at' => new DateTime, 'updated_at' => new DateTime, 'id_tipo_especialidad' => 4],
            ['descripcion' => 'Anatomía patológica', 'created_at' => new DateTime, 'updated_at' => new DateTime, 'id_tipo_especialidad' => 4],
            ['descripcion' => 'Bioquímica clínica', 'created_at' => new DateTime, 'updated_at' => new DateTime, 'id_tipo_especialidad' => 4],
            ['descripcion' => 'Farmacología', 'created_at' => new DateTime, 'updated_at' => new DateTime, 'id_tipo_especialidad' => 4],
            ['descripcion' => 'Genética médica', 'created_at' => new DateTime, 'updated_at' => new DateTime, 'id_tipo_especialidad' => 4],
            ['descripcion' => 'Inmunología', 'created_at' => new DateTime, 'updated_at' => new DateTime, 'id_tipo_especialidad' => 4],
            ['descripcion' => 'Medicina nuclear', 'created_at' => new DateTime, 'updated_at' => new DateTime, 'id_tipo_especialidad' => 4],
            ['descripcion' => 'Neurofisiología clínica', 'created_at' => new DateTime, 'updated_at' => new DateTime, 'id_tipo_especialidad' => 4],
            ['descripcion' => 'Radiología', 'created_at' => new DateTime, 'updated_at' => new DateTime, 'id_tipo_especialidad' => 4],

        );

        Especialidad::insert($data);
    }
}
