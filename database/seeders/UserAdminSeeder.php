<?php

namespace Database\Seeders;

use App\Models\CentroDoctor;
use App\Models\Doctor;
use App\Models\EspecialidadDoctor;
use App\Models\Horario;
use App\Models\Paciente;
use Database\Factories\CentroDoctorFactory;
use DateTime;
use App\Models\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

class UserAdminSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $user = array
        (
            ['name' => 'admin', 'email'=>'admin@doctormoises.com', 'status' => 'A', 'email_verified_at'=>new DateTime,'password'=>Hash::make('moises$2021'), 'created_at' => new DateTime, 'updated_at' => new DateTime],
        );
        User::insert($user);
    }
}
