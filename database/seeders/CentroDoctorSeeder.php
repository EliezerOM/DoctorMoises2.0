<?php

namespace Database\Seeders;

use App\Models\CentroDoctor;
use Illuminate\Database\Seeder;

class CentroDoctorSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        CentroDoctor::factory()->count(10)->create();
    }
}
