<?php

namespace Database\Seeders;

use App\Models\CentroMedico;
use App\Models\NivelAtencionCentro;
use App\Models\Provincia;
use App\Models\TipoCentro;
use Illuminate\Database\Seeder;

class CentrosMedicosTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $id_santiago = Provincia::where('descripcion','=','Santiago')->first()->id;
        $data = array(
                [
                    'nombre' => 'Corominas',
                    'id_ciudad'=> $id_santiago,
                    'id_pais' => null,
                    'direccion' => 'Restauración 57, Santiago, Rep. Dom.',
                    'id_nivel_atencion' => NivelAtencionCentro::where('descripcion', '=', 'Especial')->first()->id,
                    'id_tipo_centro' => TipoCentro::where('descripcion', '=', 'Clínica')->first()->id,
                    'telefono' => '(809) 580-1171',
                    'telefono2'=>null,
                    'whatsapp'=>null,
                    'web' => 'http://www.clinicacorominas.com.do/',
                    'email' => 'info@clinicacorominas.com.do',
                    'youtube'=> null,
                    'facebook'=>'https://www.facebook.com/clinicacorominas',
                    'twitter'=>null,
                    'nota'=>null,
                    'instagram'=>'https://www.instagram.com/clinica.corominas/',
                    'logo'=>'https://corominas.com.do/wp-content/uploads/2016/04/logo.png',
                    'coordenada' => '19.452946327797655, -70.7002875423278',
                ],
            [
                'nombre' => 'HOMS',
                'id_ciudad'=> $id_santiago,
                'id_pais' => null,
                'direccion' => 'Autop. Juan Pablo Duarte 28, Santiago De Los Caballeros 51000',
                'id_nivel_atencion' => NivelAtencionCentro::where('descripcion', '=', 'Especial')->first()->id,
                'id_tipo_centro' => TipoCentro::where('descripcion', '=', 'Hospital')->first()->id,
                'telefono' => '(829) 947-2222',
                'telefono2'=>null,
                'whatsapp'=>null,
                'web' => 'https://homs.com.do/',
                'email' => 'info@homs.com.do',
                'youtube'=> null,
                'facebook'=>'https://www.facebook.com/HospitalHOMS/',
                'twitter'=>null,
                'instagram'=>'https://www.instagram.com/HospitalHOMS',
                'nota'=>'El HOMS es un centro de medicina avanzada al servicio de la salud, para responder a las necesidades de atención médica integral de sus usuarios.',
                'logo'=>'https://homs.com.do/wp-content/uploads/2021/06/homs_logo_blanco.svg',
                'coordenada' => '19.435154773183797, -70.66122914203922',
            ],

            [
                'nombre' => 'Cabral Y Baez',
                'id_ciudad'=>$id_santiago,
                'id_pais' => null,
                'direccion' => 'Calle Sabana Larga, esquina Av. 27 de Febrero, Santiago De Los Caballeros 51000, Rep. Dom.',
                'id_nivel_atencion' => NivelAtencionCentro::where('descripcion', '=', 'Especial')->first()->id,
                'id_tipo_centro' => TipoCentro::where('descripcion', '=', 'Hospital')->first()->id,
                'telefono' => '(809) 242-8929',
                'telefono2'=>null,
                'whatsapp'=>null,
                'web' => 'https://hospitalcabralybaez.gob.do/',
                'email' => 'info@hospitalcabralybaez.gob.do',
                'youtube'=>'https://www.youtube.com/channel/UCBeSJvRYT4sck8vAlnJkYKA',
                'facebook'=>'https://www.facebook.com/HRJMCB/',
                'twitter'=>'https://twitter.com/hospitalcabral5',
                'nota'=>'Somos un hospital tercer nivel altamente especializado que ofrece servicios de salud, perteneciente a la Regional de Salud Norcentral',
                'instagram'=>'https://www.instagram.com/hcabralybaez/',
                'logo'=>'https://hospitalcabralybaez.gob.do/wp-content/uploads/2021/10/logo-hosp-cabralybaez.svg',
                'coordenada' => '19.45535167731276, -70.69992633998257',
            ],
            [
                'nombre' => 'Centro Médico Cibao',
                'id_ciudad'=> $id_santiago,
                'id_pais' => null,
                'direccion' => 'Av. Juan Pablo Duarte #64, Santiago, R.D',
                'id_nivel_atencion' => NivelAtencionCentro::where('descripcion', '=', 'Normal')->first()->id,
                'id_tipo_centro' => TipoCentro::where('descripcion', '=', 'Clínica')->first()->id,
                'telefono' => '(809) 582-6661',
                'telefono2'=>null,
                'whatsapp'=>null,
                'web' => 'https://www.centromedicocibao.com.do/',
                'email' => 'info@cmc.com.do',
                'youtube'=> null,
                'facebook'=>'https://www.facebook.com/pages/Centro-Medico-Cibao/138979846155021',
                'twitter'=>null,
                'nota'=>null,
                'instagram'=>'https://www.instagram.com/centromedicocibao/',
                'logo'=>'https://www.centromedicocibao.com.do/estructura/image/CMUtesa-PNG.png',
                'coordenada' => '19.454972508873105, -70.69622365889558',
            ],

            [
                'nombre' => 'Materno Infantil',
                'id_ciudad'=> $id_santiago,
                'id_pais' => null,
                'direccion' => 'Av. Juan Pablo Duarte,esq. Bisonó Toribio, Santiago de los Caballeros',
                'id_nivel_atencion' => NivelAtencionCentro::where('descripcion', '=', 'Especial')->first()->id,
                'id_tipo_centro' => TipoCentro::where('descripcion', '=', 'Clínica')->first()->id,
                'telefono' => '(809) 581-4445',
                'telefono2'=>'(809) 587-3707',
                'whatsapp'=>null,
                'web' => 'https://institutomaternoinfantil.com/',
                'email' => 'maternoinfantil@codetel.net.do',
                'youtube'=> null,
                'facebook'=>'https://www.facebook.com/institutomaternoinfantil',
                'twitter'=>null,
                'nota'=>null,
                'instagram'=>'https://www.instagram.com/institutomaterno/',
                'logo'=>'https://institutomaternoinfantil.com/wp-content/uploads/2018/06/Logo-1.png',
                'coordenada' => '19.45881253935198, -70.68315513100812',
            ],

            [
                'nombre' => 'Unión Médica Del Norte',
                'id_ciudad'=> $id_santiago,
                'id_pais' => null,
                'direccion' => 'Av. Juan Pablo Duarte # 176, Santiago de los Caballeros',
                'id_nivel_atencion' => NivelAtencionCentro::where('descripcion', '=', 'Especial')->first()->id,
                'id_tipo_centro' => TipoCentro::where('descripcion', '=', 'Clínica')->first()->id,
                'telefono' => '(809) 226-8686',
                'telefono2'=>null,
                'whatsapp'=>null,
                'web' => 'https://clinicaunionmedica.com/',
                'email' => 'info@clinicaunionmedica.com',
                'youtube'=>'https://www.youtube.com/channel/UCL9QR85Vs8EyQ6MkHJARqdQ',
                'facebook'=>null,
                'twitter'=>'https://twitter.com/union_medica',
                'nota'=>null,
                'instagram'=>'https://www.instagram.com/UNIONMEDICA/',
                'logo'=>'https://clinicaunionmedica.com/wp-content/uploads/2021/04/RRSS-CLINICA-UNIVERSITARIA-UNION-MEDICA_Mesa-de-trabajo-1-copia-4_Mesa-de-trabajo-1-copia-4_Mesa-de-trabajo-1-copia-4_Mesa-de-trabajo-1-copia-4.png',
                'coordenada' => '19.459578131418326, -70.68019089815179',
            ],


            [
                'nombre' => 'La Bonilla',
                'id_ciudad'=> $id_santiago,
                'id_pais' => null,
                'direccion' => 'Calle Independencia 124, Santiago De Los Caballeros 51000',
                'id_nivel_atencion' => NivelAtencionCentro::where('descripcion', '=', 'Normal')->first()->id,
                'id_tipo_centro' => TipoCentro::where('descripcion', '=', 'Clínica')->first()->id,
                'telefono' => '(809) 247-3888',
                'telefono2'=>null,
                'whatsapp'=>'(849) 633-8661',
                'web' => 'https://clinicadrbonilla.com.do/',
                'email' => 'info@clinicadrbonilla.com.do',
                'youtube'=> null,
                'facebook'=>'https://www.facebook.com/bonillaclinica',
                'twitter'=>null,
                'nota'=>null,
                'instagram'=>'https://www.instagram.com/clinicadrbonilla/',
                'logo'=>'https://clinicadrbonilla.com.do/images/logo/logotipocdb.jpg',
                'coordenada' => '19.45351426890899, -70.70311317791136',
            ],
        );
        CentroMedico::insert($data);
    }
}
