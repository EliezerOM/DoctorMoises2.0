<?php

namespace Database\Seeders;

use DateTime;
use App\Models\GrupoSangre;
use Illuminate\Database\Seeder;

class GrupoSangreSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      $data = array(
          [
              'descripcion' => 'A+',
              'created_at'    => new DateTime,
              'updated_at'    => new DateTime
          ],
          [
              'descripcion' => 'B+',
              'created_at'    => new DateTime,
              'updated_at'    => new DateTime
          ],
          [
              'descripcion' => 'O+',
              'created_at'    => new DateTime,
              'updated_at'    => new DateTime
          ],
          [
              'descripcion' => 'AB+',
              'created_at'    => new DateTime,
              'updated_at'    => new DateTime
          ],
          [
              'descripcion' => 'A-',
              'created_at'    => new DateTime,
              'updated_at'    => new DateTime
          ],       [
              'descripcion' => 'B-',
              'created_at'    => new DateTime,
              'updated_at'    => new DateTime
          ],
          [
              'descripcion' => 'O-',
              'created_at'    => new DateTime,
              'updated_at'    => new DateTime
          ],
          [
              'descripcion' => 'AB-',
              'created_at'    => new DateTime,
              'updated_at'    => new DateTime
          ],
      );
        GrupoSangre::insert($data);

    }
}
