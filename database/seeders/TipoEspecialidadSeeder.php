<?php

namespace Database\Seeders;

use App\Models\TipoEspecialidad;
use Illuminate\Database\Seeder;
use DateTime;

class TipoEspecialidadSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = array
            (
                ['descripcion' => 'Especialidades Clínicas', 'created_at' => new DateTime, 'updated_at' => new DateTime,
                    'comentario' =>'Las especialidades clínicas se corresponden con la figura tradicional de "médico": asisten personalmente al paciente con actividades preventivas, diagnósticas y terapéuticas, generalmente utilizando técnicas NO quirúrgicas.'],
                ['descripcion' => 'Especialidades quirúrgicas', 'created_at' => new DateTime, 'updated_at' => new DateTime,
                    'comentario'=>'Las especialidades quirúrgicas se corresponden con la figura de cirujano, y utilizan medios invasivos para tratar, modificar o extirpar físicamente la estructura patológica. Se dividen por sistemas.'],
                ['descripcion' => 'Especialidades médico-quirúrgicas', 'created_at' => new DateTime, 'updated_at' => new DateTime,
                    'comentario'=>'Son las que habitualmente usan tanto técnicas invasivas (quirúrgicas) como no invasivas (farmacológicas, etc).'],
                ['descripcion' => 'Especialidades de laboratorio o diagnósticas', 'created_at' => new DateTime, 'updated_at' => new DateTime,
                    'comentario'=>'De apoyo a los demás médicos, realizan diagnósticos y sugieren tratamientos a los clínicos, por lo que en ellas la relación con el paciente es reducida.'],
            );

        TipoEspecialidad::insert($data);
    }
}
