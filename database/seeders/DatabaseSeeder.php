<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        //$this->call(UserSeeder::class);
        $this->call(ProvinciaTableSeeder::class);
        $this->call(MunicipioTableSeeder::class);
        $this->call(DistritoTableSeeder::class);
        $this->call(GrupoSangreSeeder::class);
        $this->call(TipoEspecialidadSeeder::class);
        $this->call(EspecialidadesSeeder::class);
        $this->call(TipoCentroSeeder::class);
        $this->call(NivelAtencionCentroSeeder::class);
        $this->call(CentrosMedicosTableSeeder::class);
        $this->call(UserAdminSeeder::class);
        $this->call(DoctorSeeder::class);//test
        $this->call(EspecialidadDoctorSeeder::class);//test
        $this->call(CentroDoctorSeeder::class); //test
        $this->call(HorarioSeeder::class); //test
        $this->call(PacienteSeeder::class); //test
        $this->call(ArsSeeder::class);
        $this->call(TipoSeguroSeeder::class);
        $this->call(CentroDoctorSeguroSeeder::class); //test

    }
}
