<?php

namespace Database\Seeders;

use App\Models\CentroDoctor;
use App\Models\CentroDoctorSeguro;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class CentroDoctorSeguroSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        CentroDoctorSeguro::factory()->count(2)->create()->unique;
    }
}
