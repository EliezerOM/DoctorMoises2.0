<?php

namespace Database\Seeders;

use App\Models\EspecialidadDoctor;
use Illuminate\Database\Seeder;

class EspecialidadDoctorSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        EspecialidadDoctor::factory()->count(70)->create();
    }
}
