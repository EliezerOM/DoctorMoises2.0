<?php

namespace Database\Seeders;

use App\Models\Cita;
use App\Models\Paciente;
use App\Models\User;
use Database\Factories\CitaFactory;
use Illuminate\Database\Seeder;

class PacienteSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        User::factory()->count(50)->has(Paciente::factory()->has(Cita::factory()->count(50), 'schedule'), 'patient')->create()-> unique;
    }
}
