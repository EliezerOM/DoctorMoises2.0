<?php

namespace Database\Seeders;

use App\Models\Ars;
use App\Models\TipoSeguro;
use DateTime;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class TipoSeguroSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $senasa = Ars::where('descripcion', '=', 'Senasa')->first();
        $humano = Ars::where('descripcion', '=', 'Humanos')->first();
        $data = array(
            [
                'descripcion' => 'Contributivo',
                'id_ars'=>$senasa->id,
                'created_at' 	=> new DateTime,
            ],
            [
                'descripcion' => 'Subsidiado',
                'id_ars'=>$senasa->id,
                'created_at' 	=> new DateTime,
            ],

            [
                'descripcion' => 'Básico',
                'id_ars'=>$humano->id,
                'created_at' 	=> new DateTime,
            ],

            [
                'descripcion' => 'Complementario',
                'id_ars'=>$humano->id,
                'created_at' 	=> new DateTime,
            ],
        );
        TipoSeguro::insert($data);
    }
}
