<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AlterColumnsUserCentrosMedicosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('centros_medicos', function (Blueprint $table) {
            $table->dropForeign('fk_centros_medicos_users');
            $table->dropColumn('id_usuario');
            Schema::enableForeignKeyConstraints();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('centros_medicos', function (Blueprint $table) {
//            Schema::disableForeignKeyConstraints();
//            $table->dropColumn('id_usuario');
//            Schema::enableForeignKeyConstraints();
        });
    }
}
