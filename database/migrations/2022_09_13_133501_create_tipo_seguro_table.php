<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTipoSeguroTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tipo_seguros', function (Blueprint $table) {
            $table->increments('id');
            $table->string('descripcion', 250);
            $table->enum('status', ['A', 'D'])->default('A')->comment('A => Activo, D => Desactivado');
            $table->integer('id_ars')->unsigned();
            $table->timestamps();
            $table->unique(['descripcion', 'id_ars']);

            $table->foreign('id_ars')->references('id')->on('ars')->onDelete('cascade');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tipo_seguro');
    }
}
