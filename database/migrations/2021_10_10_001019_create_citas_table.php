<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCitasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('citas', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('id_paciente')->unsigned();
            $table->string('centro')->comment('Descripcion del centro medico');
            $table->string('especialidad')->comment('Descripcion de la especialidad');
            $table->integer('id_horario')->unsigned();
            $table->enum('valoracion', [null, 0, 1, 2, 3, 4])->nullable()->default(null);
            $table->string('comentario')->nullable();
            $table->string('nota_paciente')->nullable();
            $table->decimal('valor_cita', 15, 2)->nullable()->comment('Precio de la consulta');
            $table->string('nota', 1000)->nullable();
            $table->enum('status', ['C', 'D', 'F', 'R', 'S', 'P'])->default('S')->comment('C => Confirmada, D => Desactivada, F => Finalizada, R => Rechazada, S => Solicitada, P=> En Proceso');
            $table->date('fecha');
            $table->integer('id_cita_sustituto')->unsigned()->nullable();
            $table->integer('id_usuario')->unsigned();
            $table->timestamps();

            //$table->unique(['id_paciente','id_doctor','id_centro','id_especialidad', 'id_horario'], 'uq_citas');

            $table->foreign('id_usuario', 'fk_citas_users')
                ->references('id')
                ->on('users')
                ->onDelete('cascade');

            $table->foreign('id_cita_sustituto', 'fk_citas_citas')
                ->references('id')
                ->on('citas')
                ->onDelete('cascade');

            $table->foreign('id_paciente', 'fk_citas_pacientes')
                ->references('id')
                ->on('pacientes')
                ->onDelete('cascade');

            $table->foreign('id_horario', 'fk_citas_horarios')
                ->references('id')
                ->on('horarios')
                ->onDelete('cascade');



        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::disableForeignKeyConstraints();
        Schema::dropIfExists('citas');
        Schema::enableForeignKeyConstraints();
    }
}
