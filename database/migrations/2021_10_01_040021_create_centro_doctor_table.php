<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCentroDoctorTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('centro_doctor', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('id_centro')->unsigned();
            $table->integer('id_especialidad_doctor')->unsigned();
            $table->decimal('valor_cita',15,2)->default(0)->comment('Precio de la consulta');
            $table->integer('id_usuario')->unsigned();
            $table->string('extension',11)->nullable();
            $table->string('edificio', 100)->nullable();
            $table->string('planta', 50)->nullable();
            $table->string('consultorio', 50)->nullable();
            $table->string('nota', 1000)->nullable();
            $table->string('telefono', 15)->nullable();
            $table->string('flota', 15)->nullable();
            $table->enum('status', ['A', 'D'])->default('A')->comment('A => Activo, D => Desactivado');
            $table->string('coordenadas')->nullable();

            $table->timestamps();
            $table->unique(['id_centro', 'id_especialidad_doctor']);


            $table->foreign('id_centro', 'fk_centro_doctor_centros_medicos')
                 ->references('id')
                 ->on('centros_medicos')
                 ->onDelete('cascade');

            $table->foreign('id_usuario', 'fk_centro_doctor_user')
                ->references('id')
                ->on('users')
                ->onDelete('cascade');
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::disableForeignKeyConstraints();
        Schema::dropIfExists('centro_doctor');
        Schema::enableForeignKeyConstraints();
    }
}
