<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateEspecialidadDoctorTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('especialidad_doctor', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('id_doctor')->unsigned();
            $table->integer('id_usuario')->unsigned();
            $table->integer('id_especialidad')->unsigned();
            $table->enum('status', ['A', 'D'])->default('A')->comment('A=> Activo, D=> Desactivado');
            $table->timestamps();

            $table->foreign('id_doctor', 'fk_espcialidad_doctor')
                ->references('id')
                ->on('doctores')
                ->onDelete('cascade');

            $table->foreign('id_usuario', 'fk_especialidad_doctor_users')
                ->references('id')
                ->on('users')
                ->onDelete('cascade');

            $table->foreign('id_especialidad', 'fk_espcialidad_doctor_especialidades')
                ->references('id')
                ->on('especialidades')
                ->onDelete('cascade');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::disableForeignKeyConstraints();
        Schema::dropIfExists('especialidad_doctor');
        Schema::enableForeignKeyConstraints();
    }
}
