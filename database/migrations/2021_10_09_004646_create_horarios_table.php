<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateHorariosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('horarios', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('id_centro_doctor')->unsigned();
            $table->integer('id_usuario')->unsigned();
            $table->enum('dia', [1, 2, 3, 4, 5, 6, 7])->default(1)->comment('1 => Lunes, 2 => Martes ...');
            $table->enum('tanda', [1, 2])->default(1)->comment('1 => Mañana, 2 => Tarde');
            $table->time('hora_inicio');
            $table->time('hora_fin');
            $table->enum('status', ['A', 'D'])->default('A')->comment('A => Activo, D=> Desactivado');
            $table->integer('cantidad_maxima')->nullable()->comment('Cantidad maxima de cuotas de pacientes para el horario');

            $table->foreign('id_usuario', 'fk_horarios_users')
                ->references('id')
                ->on('users')
                ->onDelete('cascade');

            $table->foreign('id_centro_doctor', 'fk_horarios_centro_doctor')
                ->references('id')
                ->on('centro_doctor')
                ->onDelete('cascade');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::disableForeignKeyConstraints();
        Schema::dropIfExists('horarios');
        Schema::enableForeignKeyConstraints();
    }
}
