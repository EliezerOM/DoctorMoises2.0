<?php

use App\Models\CentroDoctor;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AlterCentroDoctorTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('centro_doctor', function (Blueprint $table) {

            $table->foreign('id_especialidad_doctor', 'fk_especialidad_doctor')
                ->references('id')
                ->on('especialidad_doctor')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {

    }
}
