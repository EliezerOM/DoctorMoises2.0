<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateArsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ars', function (Blueprint $table) {
            $table->increments('id');
            $table->string('descripcion', 300)->unique();
            $table->enum('status', ['A', 'D'])->default('A')->comment('A=> Activo, D=> Desactivado');
            $table->string('logo')->nullable();
            $table->text('link')->nullable();
            $table->timestamps();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('ars');
    }
}
