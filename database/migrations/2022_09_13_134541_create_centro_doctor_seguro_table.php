<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCentroDoctorSeguroTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('centro_doctor_seguros', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('id_centro_doctor')->unsigned();
            $table->integer('id_tipo_seguro')->unsigned();
            $table->enum('status', ['A', 'D'])->default('A')->comment('A=> Activo, D=> Desactivado');
            $table->foreign('id_centro_doctor')->references('id')->on('centro_doctor')->onDelete('cascade');
            $table->foreign('id_tipo_seguro')->references('id')->on('tipo_seguros')->onDelete('cascade');
            $table->unique(['id_tipo_seguro', 'id_centro_doctor'], 'uq_tipo_centro');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('centro_doctor_seguro');
    }
}
