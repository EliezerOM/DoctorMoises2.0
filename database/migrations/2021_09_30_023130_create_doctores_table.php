<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDoctoresTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('doctores', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('id_usuario')->unsigned();
            $table->string('nombres', 80);
            $table->string('apellidos', 80);
            $table->enum('sexo', ['M', 'F'])->default('M')->comment('M=> Masculino, F=> Femenino');
            $table->date('fecha_nacimiento');
            $table->string('foto_perfil')->nullable();
            $table->string('whatsapp', 20)->nullable();
            $table->text('web')->nullable();
            $table->string('instagram' )->nullable();
            $table->string('mail' )->nullable();
            $table->string('facebook' )->nullable();
            $table->string('cedula', 20)->nullable();
            $table->string('celular', 20)->nullable();
            $table->string('telefono', 20)->nullable();
            $table->string('nota', 1000)->nullable();
            $table->enum('doctor_valido', ['S','N'])->default('N');
            $table->string('numero_folio')->nullable();
            $table->string('numero_decreto')->nullable();
            $table->date('fecha_decreto')->nullable();
            $table->string('numero_libro')->nullable();
            $table->string('universidad')->nullable();
            $table->string('numero_registro')->nullable();
            $table->date('fecha_numero_registro')->nullable();
            $table->string('exequatur')->nullable();
            $table->string('direccion',1000)->nullable();
            $table->string('coordenada')->nullable();

            $table->enum('status', ['A', 'D'])->default('A')->comment('A=> Activo, D=> Desactivado');

            $table->foreign('id_usuario', 'fk_doctores_users')
                ->references('id')
                ->on('users')
                ->onDelete('cascade');

            $table->unique(['id_usuario', 'id'], 'uq_doctor');

            $table->timestamps();
        });

//        DB::statement("ALTER TABLE doctores DROP CONSTRAINT CK_DOCTORES;");
//        DB::statement("ALTER TABLE doctores ADD CONSTRAINT CK_DOCTORES CHECK sexo IN('F','M');");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::statement('SET FOREIGN_KEY_CHECKS=0;');
        Schema::dropIfExists('doctores');
        DB::statement('SET FOREIGN_KEY_CHECKS=1;');
    }
}
