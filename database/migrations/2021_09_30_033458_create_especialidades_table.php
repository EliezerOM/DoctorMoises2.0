<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateEspecialidadesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('especialidades', function (Blueprint $table) {
            $table->increments('id');
            $table->string('descripcion', 200);
            $table->string('icono',200)->nullable()->default('none.png');
            $table->enum('status', ['A', 'D'])->default('A')->comment('A=> Activo, D=> Desactivado');
            $table->integer('id_usuario')->unsigned()->nullable();
            $table->integer('id_tipo_especialidad')->unsigned();
            $table->timestamps();

            $table->unique('descripcion');

            $table->foreign('id_usuario', 'fk_especialidades_users')
                ->references('id')
                ->on('users')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::disableForeignKeyConstraints();
        Schema::dropIfExists('especialidades');
        Schema::enableForeignKeyConstraints();
    }
}
