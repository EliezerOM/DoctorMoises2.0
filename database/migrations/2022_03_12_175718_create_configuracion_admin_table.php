<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateConfiguracionAdminTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('configuraciones', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('id_usuario')->unique()->unsigned();
            $table->string('idioma')->default('es')->comment('configuración de idioma');
            $table->timestamps();


            $table->foreign('id_usuario', 'fk_configuraciones_users')
                ->references('id')
                ->on('users')
                ->onDelete('cascade');

      });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {

    }
}
