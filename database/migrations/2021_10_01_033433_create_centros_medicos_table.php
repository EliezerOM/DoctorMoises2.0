<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCentrosMedicosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('centros_medicos', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('id_usuario')->unsigned()->nullable();
            $table->string('nombre', 150)->unique();
            $table->string('logo')->nullable();

            $table->string('nota', 1000)->nullable();
            $table->string('telefono', 20)->nullable();
            $table->string('telefono2', 20)->nullable();
            $table->string('email', 50)->nullable();
            $table->string('whatsapp', 20)->nullable();
            $table->string('instagram')->nullable();
            $table->string('facebook')->nullable();
            $table->string('youtube')->nullable();
            $table->string('twitter')->nullable();
            $table->string('coordenada')->nullable();
            $table->string('direccion', 1000)->nullable();
            $table->text('web')->nullable();

            $table->enum('status', ['A', 'D'])->default('D')->comment('A=> Activo, D=> Desactivado');

            //
            $table->timestamps();

            $table->foreign('id_usuario', 'fk_centros_medicos_users')
                ->references('id')
                ->on('users')
                ->onDelete('cascade');


        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::disableForeignKeyConstraints();
        Schema::dropIfExists('centros_medicos');
        Schema::enableForeignKeyConstraints();
    }
}
