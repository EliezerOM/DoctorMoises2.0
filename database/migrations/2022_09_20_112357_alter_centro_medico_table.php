<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AlterCentroMedicoTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('centros_medicos', function (Blueprint $table) {
            $table->addColumn('integer','id_nivel_atencion')->unsigned();
            $table->addColumn('integer','id_tipo_centro')->unsigned();
            $table->integer('id_ciudad')->unsigned();
            $table->integer('id_pais')->nullable();

            $table->foreign('id_tipo_centro')->references('id')->on('tipo_centros')->onDelete('cascade');
            $table->foreign('id_nivel_atencion')->references('id')->on('nivel_atencion_centros')->onDelete('cascade');
            $table->foreign('id_ciudad')->references('id')->on('provincias')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
