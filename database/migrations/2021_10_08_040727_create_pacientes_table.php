<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePacientesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pacientes', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('id_usuario')->unsigned();
            $table->string('nombres', 80);
            $table->string('apellidos', 80);
            $table->date('fecha_nacimiento')->nullable();
            $table->text('foto_perfil')->nullable();
            $table->string('cedula', 20)->nullable();
            $table->string('celular', 20)->nullable();
            $table->string('telefono', 20)->nullable();
            $table->enum('estado_civil', ['S', 'C'])->default('S')->comment('S=>Soltero, C=>Casado');
            $table->enum('sexo', ['M', 'F'])->default('M')->comment('M=> Masculino, F=> Femenino');
            $table->enum('status', ['A', 'D'])->default('A')->comment('A=> Activo, D=> Desactivado');
            $table->string('direccion', 1000)->nullable();
            $table->integer('id_grupo_sangre')->unsigned()->nullable();
            $table->timestamps();

            $table->foreign('id_usuario', 'fk_pacientes_users')
                ->references('id')
                ->on('users')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::disableForeignKeyConstraints();
        Schema::dropIfExists('pacientes');
        Schema::enableForeignKeyConstraints();
    }
}
