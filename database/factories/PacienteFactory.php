<?php

namespace Database\Factories;

use App\Models\Paciente;
use Illuminate\Database\Eloquent\Factories\Factory;

class PacienteFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        $image =$this->faker->imageUrl(600,600, null,false,true);
        $array = array($image, null);
        $index = array_rand($array);
        $foto_perfil = $array[$index];
        return [
            'nombres' => $this->faker->firstName,
            'apellidos' => $this->faker->lastName,
            'sexo'=>$this->faker->randomElement(['M','F']),
            'fecha_nacimiento'=>$this->faker->dateTimeBetween('-50 years','-18 years'),
            'foto_perfil' => $foto_perfil,
            'celular' => $this->faker->randomElement([$this->faker->numerify('+1 (###)###-####'), null]),
            'direccion' => $this->faker->address,
            'estado_civil' => $this->faker->randomElement(['C', 'S']),
            'telefono' => $this->faker->randomElement([$this->faker->numerify('+1 (###)###-####'), null]),
            'cedula' => $this->faker->numerify('###-#######-#'),
        ];
    }
}
