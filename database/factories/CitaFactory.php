<?php

namespace Database\Factories;

use App\Models\CentroDoctor;
use App\Models\Horario;
use App\Models\Paciente;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Facades\DB;

class CitaFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        $patient = Paciente::inRandomOrder()->first();
        $centro_doctor = CentroDoctor::whereExists(function ($q) {
            $q->select(DB::raw(1))->from('horarios')->whereColumn('id_centro_doctor', 'centro_doctor.id');
        })->inRandomOrder()->first();
        $speciality = $centro_doctor->speciality_doctor->speciality;
        $centro = $centro_doctor->center;
        $schedule = $centro_doctor->schedules->first();
        $status = $this->faker->randomElement(['R', 'F', 'C', 'S']);
        return [
            'id_paciente' => $patient->id,
            'centro' => $centro->nombre,
            'especialidad' => $speciality->descripcion,
            'id_horario' => $schedule->id,
            'valoracion' => $status === 'F' ? $this->faker->randomElement([1, 2, 3, 4]) : null,
            'comentario' => $this->faker->text,
            'valor_cita' => $centro_doctor->valor_cita,
            'nota' => $this->faker->text,
            'status' => $status,
            'fecha' => $this->faker->date,
            'id_usuario' => $patient->user->id
        ];
    }
}
