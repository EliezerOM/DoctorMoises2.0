<?php

namespace Database\Factories;

use App\Models\CentroDoctor;
use App\Models\Especialidad;
use Illuminate\Database\Eloquent\Factories\Factory;

class HorarioFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {

        $centro_doctor = CentroDoctor::inRandomOrder()->first();
        return [
            'id_centro_doctor' => $centro_doctor->id,
            'id_usuario' => $centro_doctor->speciality_doctor()->first()->doctor->user->id,
            'dia' => $this->faker->numberBetween(1, 7),
            'tanda' => $this->faker->numberBetween(1, 2),
            'hora_inicio' => $this->faker->time(),
            'hora_fin' => $this->faker->time(),
            'cantidad_maxima' => $this->faker->numberBetween(5, 20)
        ];
    }
}
