<?php

namespace Database\Factories;

use App\Models\Doctor;
use App\Models\Especialidad;
use Illuminate\Database\Eloquent\Factories\Factory;

class EspecialidadDoctorFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        $id_especialidad = $this->faker->randomElements(Especialidad::all()->pluck('id')->toArray())[0];
        $id_doctor = $this->faker->randomElements(Doctor::all()->pluck('id')->toArray())[0];
        return [
            'id_doctor' => $id_doctor,
            'id_especialidad' =>  $id_especialidad,
            'id_usuario' => 1
        ];
    }
}
