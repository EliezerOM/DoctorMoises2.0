<?php

namespace Database\Factories;

use App\Models\CentroMedico;
use App\Models\Doctor;
use App\Models\Especialidad;
use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Facades\DB;

class CentroDoctorFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
//        $doctor = Doctor::whereNotExists(function($query){
//            $query->from('centro_doctor')->whereRaw('doctores.id = centro_doctor.id_doctor');
//        })->inRandomOrder()->first();
        $id_centro = $this->faker->randomElements(CentroMedico::all()->pluck('id')->toArray())[0];
        $id_especialidad_doctor = $this->faker->randomElements(Especialidad::all()->pluck('id')->toArray())[0];
        $valor = $this->faker->randomElements([2500, 3000, 5000, 7000, 10000])[0];

        return [
            'id_centro' => $id_centro,
            'id_especialidad_doctor' => $id_especialidad_doctor,
            'valor_cita' => $valor,
            'id_usuario' => 1
        ];
    }
}
