<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;

class DoctorFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        $image =$this->faker->imageUrl(600,600, null,false,true);
        $array = array($image, null);
        $index = array_rand($array);
        $foto_perfil = $array[$index];
        $name = $this->faker->firstName;
        $last_name = $this->faker->lastName;
        return [
            'nombres' => $name,
            'apellidos' => $last_name,
            'sexo'=>$this->faker->randomElement(['M','F']),
            'telefono' =>  $this->faker->randomElement([$this->faker->numerify('+1 (###)###-####'), null]),
            'celular' =>  $this->faker->randomElement([$this->faker->numerify('+1 (###)###-####'),null]),
            'whatsapp' =>  $this->faker->randomElement([$this->faker->numerify('+1 (###)###-####'), null]),
            'web' =>  $this->faker->randomElement([$this->faker->url, null]),
            'mail' => $this->faker->randomElement([$this->faker->email, null]),
            'facebook' => $this->faker->randomElement(['https://www.facebook.com/'.$name.'.'.$last_name.'.'.$this->faker->numberBetween(10, 9999), null]),
            'foto_perfil' => $foto_perfil,
            'instagram' => $this->faker->randomElement(['https://www.instagram.com/'.$name.$last_name.'_'.$this->faker->numberBetween(10, 9999).'/', null]),
            'cedula' => $this->faker->numerify('###-#######-#'),
            'direccion' => $this->faker->randomElement([$this->faker->address, null]),
            'fecha_nacimiento'=>now(),
        ];
    }
}
