<?php

namespace Database\Factories;

use App\Models\CentroDoctor;
use App\Models\TipoSeguro;
use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\CentroDoctorSeguro>
 */
class CentroDoctorSeguroFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition()
    {
        $tipo_seguro = TipoSeguro::inRandomOrder()->first();
        $centro_doctor = CentroDoctor::all()->unique()->random();
        return [
            'id_centro_doctor' => $centro_doctor->id,
            'id_tipo_seguro'=> $tipo_seguro->id,
            'created_at'=> now(),
        ];
    }
}
