<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>

    <meta charset="UTF-8">

    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <meta name="turbolinks-cache-control" content="no-cache">
    <link type="image/x-icon" href="{{asset('images/icons/icon-72x72.png')}}" rel="icon">

    {{--    <meta name="turbolinks-visit-control" content="reload">--}}
    <title>{{env('APP_NAME')}}</title>
{{--    @laravelPWA--}}
{{--    <script src="{{asset('serviceworker.js')}}"></script>--}}
    <!-- Styles -->
    <x-styles/>
    <!-- Styles -->

</head>
<style>
    [x-cloak] {
        display: none !important;
    }
</style>
<body class="relative dark:bg-gray-800" x-data="options" x-cloak @keydown.escape="isOverlay = false">


<!-- Header -->
<x-header/>
<!-- Header -->

<x-notify/>


<div class="flex  ">
    <x-modales/>

    <!-- SiderBar-->
    <x-siderbar/>
    <!-- SiderBar-->


    <main id="main"
          class=" main  min-h-[600px] mt-5 mb-5 w-full {{Route::is(listRoute()) ? 'md:ml-0' :'md:ml-[16rem] md:mt-[50px] p-5'}} ">

        <x-overlay/>

        <!-- Content Page -->
        {{ $slot??'' }}
        <!-- Content Page -->
    </main>

</div>
<!-- Crear Cita -->
<button type="button"
        class="text-gray-800 tooltipped dark:text-gray-200
        shadow-xl
                            hover:bg-gray-100 h-fit dark:hover:bg-gray-700
                            focus:outline-none focus:ring-4 focus:ring-green-500
                            focus:bg-white dark:focus:ring-gray-900
                           dark:bg-gradient-to-br dark:from-gray-700 dark:via-gray-400 dark:to-gray-600
                            bg-gradient-to-br from-green-500 via-green-400 to-green-600 bg-gray-200
                            rounded-full
                            text-sm p-2.5 fixed z-40 right-0 mr-2 bottom-16"
        data-position="left" data-tooltip="Reservar Cita">
    <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke-width="1.5"
         stroke="currentColor" class="w-14 h-14 stroke-2 text-gray-100">
        <path stroke-linecap="round" stroke-linejoin="round"
              d="M6.75 3v2.25M17.25 3v2.25M3 18.75V7.5a2.25 2.25 0 012.25-2.25h13.5A2.25 2.25 0 0121 7.5v11.25m-18 0A2.25 2.25 0 005.25 21h13.5A2.25 2.25 0 0021 18.75m-18 0v-7.5A2.25 2.25 0 015.25 9h13.5A2.25 2.25 0 0121 11.25v7.5m-9-6h.008v.008H12v-.008zM12 15h.008v.008H12V15zm0 2.25h.008v.008H12v-.008zM9.75 15h.008v.008H9.75V15zm0 2.25h.008v.008H9.75v-.008zM7.5 15h.008v.008H7.5V15zm0 2.25h.008v.008H7.5v-.008zm6.75-4.5h.008v.008h-.008v-.008zm0 2.25h.008v.008h-.008V15zm0 2.25h.008v.008h-.008v-.008zm2.25-4.5h.008v.008H16.5v-.008zm0 2.25h.008v.008H16.5V15z"/>
    </svg>
</button>
<!-- End Crear Cita -->

<!-- Button Back To Top -->
<x-back-to-top/>
<!-- Button Back To Top -->

<!-- Footer -->
<x-footer/>
<!-- Footer -->

<!-- Scripts -->
<x-scripts/>
<!-- Scripts -->

</body>


</html>
