<form wire:submit.prevent="forgot" method="post" class="flex justify-center">
    <div class="min-w-[310px] card p-2 border shadow-xl rounded-lg dark:bg-gray-700 dark:border-gray-800">
        <div class="card-header flex justify-center">
            <h4 class="dark:text-gray-100 font-semibold text-lg">Recuperar Cuenta</h4>
        </div>
        <div class="card-body">
            <x-form.input type="email"
                            autofocus
                          required
                          id="recovery"
                          name="recovery"
                          wire:model.defer="email"
                          text="Correo electrónico"
            />

        </div>
        <div class="card-footer">
            <button type="submit" class="btn-om-primary w-full" data-ripple-light="true">
                ENVIAR
            </button>
            <a class="flex justify-center   w-full no-underline hover:underline"
               href="{{route('login')}}" data-ripple-dark="true">
                <div class="flex items-center m-2">
                    <i class="fa-solid fa-user-plus text-light-green-800 dark:text-gray-100 mr-1"></i>
                    <p class="dark:text-gray-100 dark:underline">Ya tienes una cuenta?</p>
                </div>
            </a>
        </div>
    </div>
</form>


