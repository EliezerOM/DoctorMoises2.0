<x-card class="m-2 md:w-96 w-full">
    <x-card.header class="-mt-0 !-mx-0" :header-outside="false">
        <div class="w-full rounded-t-lg dark:bg-gray-800 bg-gray-200 shadow-md">
            <p class="font-bold text-gray-900 text-lg text-center dark:text-gray-100 shadow-md">Recibos Recientes</p>
        </div>

    </x-card.header>
    <x-card.content class="overscroll-y-auto h-52">

    </x-card.content>
</x-card>

