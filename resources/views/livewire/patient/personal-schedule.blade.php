<div x-data="{slideInfo: @entangle('slide').defer, tabs2:0, firstTabDoctor: false, appointment: null}">

    {{--    Mapa--}}
    <x-dialog-x class="w-[88%]" name-dialog="map">
        <x-dialog-x.header>
            <p class="md:text-2xl text-center dark:text-gray-100 font-semibold w-full border-b dark:border-gray-900 p-2">
                Ubicación Del Centro (Mapa)
            </p>
        </x-dialog-x.header>

        <x-dialog-x.body>
            <iframe

                id="ifrm"
                src="https://www.google.com/maps/embed/v1/place?key={{env('GOOGLE_API_KEY')}}&q=60,70&zoom=15"
                class="w-full md:h-[calc(100vh-20rem)] h-[calc(100vh-16rem)]"
                allowfullscreen
                referrerpolicy="no-referrer-when-downgrade">
            </iframe>
        </x-dialog-x.body>
        <x-dialog-x.footer>
            <p class="dark:text-gray-100 text-sm pl-1">Nota: recuerde permitir "Conocer tu ubicación", para
                proporcionarle las coordenadas correctas del centro desde su posición actual, si acaba de permitir favor
                de volver a cargar el mapa</p>
        </x-dialog-x.footer>

    </x-dialog-x>
    {{--End Mapa--}}

    <x-tab.tab>
        <x-tab.transition is-overlay="isOverlay" is-slide="slideInfo">
            <x-tab.header>
                <x-tab.header-column tabs="tabs2" class="w-full flex justify-center dark:text-gray-100 dark:bg-gray-800"
                                     :index="0"
                                     color-slide="bg-gray-700"
                                     color-text="text-gray-700 dark:text-gray-100"
                                     icon="fa-solid fa-calendar-check text-gray-700"
                                     name="Cita"/>

                <x-tab.header-column tabs="tabs2" class="w-full flex justify-center dark:text-gray-100 dark:bg-gray-800"
                                     :index="1"
                                     name="Doctor"
                                     color-text="text-blue-700 dark:text-gray-100"
                                     color-slide="bg-blue-700"
                                     icon="fa-solid fa-user-doctor text-blue-700"/>

                <x-tab.header-column tabs="tabs2" class="w-full flex justify-center dark:text-gray-100 dark:bg-gray-800"
                                     :index="2"
                                     color-text="text-green-700 dark:text-gray-100"
                                     name="Centro"
                                     color-slide="bg-green-700"
                                     icon="fa-solid fa-hospital text-green-700"/>
            </x-tab.header>
            <x-tab.content>
                <!-- Info Appointment -->
                <x-tab.content-item tabs="tabs2" :index="0"
                                    class="bg-gradient-to-t from-blue-500 via-purple-500 to-blue-700 h-full ">
                    <livewire:patient.component.info-appointment/>
                </x-tab.content-item>
                <!-- End Info Appointment -->

                <!--Info Doctor -->
                <x-tab.content-item tabs="tabs2" :index="1">
                    <livewire:patient.component.info-doctor/>
                </x-tab.content-item>
                <!-- End Info Doctor -->

                <!-- Medical Center -->
                <x-tab.content-item tabs="tabs2" :index="2">
                    <livewire:patient.component.medical-center/>
                </x-tab.content-item>
                <!-- End Medical Center -->
            </x-tab.content>
        </x-tab.transition>
    </x-tab.tab>


    <x-card>
        <x-card.header>
            <h4 class=" mb-0 text-center font-bold text-gray-200 text-3xl">MI AGENDA</h4>
        </x-card.header>

        <x-card.content>
            <div class=" shadow-md sm:rounded-lg border dark:bg-gray-700 dark:border-gray-700" x-trap="true">
                <div class="md:flex justify-between bg-white dark:bg-gray-700 m-2 items-end " wire:ignore
                     wire:key="select2_">

                    <label
                        class="flex items-center md:min-w-[310px] dark:text-gray-200   m-2 p-2 h-fit rounded h-auto relative  md:order-3">
                        <input placeholder="Buscar"
                               om-select-all
                               wire:model.debounce.500ms="search"
                               class="!border-none  dark:placeholder-gray-200 text-gray-200 z-10 peer focus:!shadow-none text-gray-700  focus:text-white group focus:border-none !h-[24px] !m-0  focus:placeholder:text-white"/>
                        <span
                            class="bg-gray-300 peer-focus:rounded-full  peer-focus:bg-gradient-to-r peer-focus:from-sky-400 peer-focus:to-sky-600 peer-focus:transition-all peer-focus:duration-500 peer-focus:-translate-x-1  absolute  bottom-0 top-0 left-0 right-0 -p-2 bg-opacity-30 rounded"></span>
                        <i class="fa-solid fa-magnifying-glass text-gray-400 dark:text-gray-200 mr-1 peer-focus:text-white z-10"></i>
                    </label>

                    <div class="flex md:justify-start md:inline-flex w-full grid items-end ">

                        <div class="flex grid m-2">
                            <label class="text-base">Rango de Fecha</label>
                            <input type="text" om-datepicker value="" placeholder="dd/mm/yyyy - dd/mm/yyyy"
                                   x-mask="99/99/9999 - 99/99/9999"
                                   id="date"
                                   om-select-all
                                   class="md:!w-[200px] !w-full !mb-0 placeholder:text-gray-300"/>
                        </div>

                        <div class="flex grid m-2 items-end">
                            <label class="text-base mb-[5px]">Estado de la cita...</label>
                            <select class="select2-icons  browser-default"
                                    id="select2-icons"
                                    multiple
                                    wire:model="type_appointment">
                                <option value="S" data-icon="fa-solid fa-clock text-yellow-400 mr-2">Solicitadas
                                </option>
                                <option value="C" data-icon="fa-solid fa-thumbs-up text-blue-600 mr-2">Aprobadas
                                </option>
                                <option value="F" data-icon="fa-solid fa-check text-green-600 mr-2">Concluída
                                </option>
                                <option value="R" data-icon="fas fa-backspace text-red-600 mr-2">Rechazadas</option>
                            </select>
                        </div>
                    </div>
                </div>
            </div>
            <div class=" mt-4 flex-col relative space-y-2   dark:bg-gray-800 rounded-2xl rounded-b-lg p-1">

                <div x-show="!slideInfo">
                    <div wire:loading>
                        <x-spinner class-overlay="rounded-2xl"/>
                    </div>
                </div>

                @isset($appointments)
                    <p class="text-xs bg-green-700 text-white md:hidden text-center rounded-lg p-1">Deslice de izquierda
                        a derecha</p>
                    <x-table class="overflow-hidden rounded-2xl shadow-lg  mx-[1px] ">
                        <x-table.head>
                            <x-table.header sortable wire:click.prevent="sortBy('id')" :direction="$sorts['id']??null">
                                Cita
                            </x-table.header>
                            <x-table.header sortable wire:click.prevent="sortBy('fecha')"
                                            :direction="$sorts['fecha']??null">Fecha
                            </x-table.header>
                            <x-table.header sortable wire:click.prevent="sortBy('status')"
                                            :direction="$sorts['status']??null" class="cursor-pointer">Estado
                            </x-table.header>
                            <x-table.header>Opciones</x-table.header>
                        </x-table.head>

                        <x-table.body>
                            @forelse($appointments as $item)
                                <x-table.row class="md:w-auto">
                                    <x-table.cell
                                        class="flex !inline-flex  items-center px-2 !py-0 md:!p-1  text-gray-900 dark:text-gray-100 dark:text-white">
                                        <x-img-profile :photo="$item->doctor->foto_perfil"
                                                       :gender="$item->doctor->sexo"/>
                                        <div class="pl-2">
                                            <p om-capitalize
                                               class="dark:text-gray-100 md:text-base text-xs md:font-normal font-semibold"> {{ ($item->doctor->sexo === 'M'? 'Dr. ' : 'Dra. ').$item->doctor->nombres.' '.$item->doctor->apellidos }}</p>
                                            <div class="md:flex md:space-x-1 space-x-0 items-center">
                                                <p class="text-gray-500 dark:text-gray-300 text-xs"
                                                   om-capitalize>{{$item->speciality->descripcion}}</p>
                                                <p class="hidden md:inline text-gray-500 dark:text-gray-300 text-xs">
                                                    -</p>
                                                <p class="text-gray-500 dark:text-gray-300 text-xs">{{$item->center->nombre}}</p>
                                            </div>

                                            <strong
                                                class="text-xs">Cita: {{ str_pad($item->id,10,'0',STR_PAD_LEFT)}}</strong>
                                        </div>
                                    </x-table.cell>
                                    <x-table.cell class="!py-2">
                                        <div class="flex inline-flex dark:text-gray-100 md:inline-block">
                                            <p>{{ typeDate($item->fecha) }}</p>
                                        </div>

                                    </x-table.cell>
                                    <x-table.cell class="!py-4">
                                        <div class="flex items-center dark:text-gray-100">
                                            @switch($item->status)
                                                @case('S')
                                                <x-point-color :color="'bg-yellow-400'"/>
                                                <p>Solcitada</p>
                                                @break
                                                @case('R')
                                                <x-point-color :color="'bg-red-600'"/>
                                                <p>Rechazada</p>
                                                @break
                                                @case('C')
                                                <x-point-color :color="'bg-blue-600'" :animate="'animate-ping'"/>
                                                <p>Aprobada</p>
                                                @break
                                                @case('F')
                                                <x-point-color :color="'bg-green-400'"/>
                                                <p>Concluída</p>
                                                @break
                                            @endswitch
                                        </div>
                                    </x-table.cell>
                                    <x-table.cell>
                                        <div class="flex justify-around items-center space-x-4 md:justify-start">
                                            <x-tooltip text="Ver Cita">
                                                <i class="fa-solid fa-calendar-check  dark:text-gray-100 text-gray-500 fa-lg cursor-pointer md:text-2xl text-4xl "
                                                   wire:click="$emit('searchAppointment',{{$item}})"
                                                   @click.stop="tabs = 0; slideInfo = true; isOverlay = true; tabs2 = 0;"></i>
                                            </x-tooltip>


                                            @switch($item->status)
                                                @case('S')
                                                <livewire:patient.component.cancelar-cita :idCita="$item->id"
                                                                                          :wire:key="'S_'.$item->id"/>

                                                @break
                                                @case('C')
                                                <x-tooltip text="Ubicación Del Centro">
                                                    <x-dialog-x.trigger name-dialog="map">
                                                        <i class="fas fa-map-marked-alt dark:text-gray-100 text-green-500 fa-lg cursor-pointer md:text-2xl text-4xl "
                                                           {{--                                                           @click="loadIframe('ifrm','https://www.google.com/maps/embed/v1/place?key={{env('GOOGLE_API_KEY')}}&q={{explode(',', $item->center->coordenada)[0]}},{{explode(',', $item->center->coordenada)[1]}}&zoom=18')"--}}

                                                           @click="loadIframe('ifrm', {{explode(',', $item->center->coordenada)[0]}}, {{explode(',', $item->center->coordenada)[1]}})"
                                                        ></i>
                                                    </x-dialog-x.trigger>
                                                </x-tooltip>
                                                <x-tooltip text="Cancelar Cita">
                                                    <livewire:patient.component.cancelar-cita :idCita="$item->id"
                                                                                              :wire:key="'C_'.$item->id"/>
                                                </x-tooltip>

                                                @break
                                                @case('F')
                                                <x-tooltip text="Historial Médico">
                                                    <i class="fas fa-file-medical-alt  dark:text-gray-100 text-yellow-500 fa-lg cursor-pointer md:text-2xl text-4xl"></i>
                                                </x-tooltip>

                                                <x-tooltip text="Cita Nueva">
                                                    <i class="fa-solid fa-file-circle-plus  modal-trigger text-green-500 fa-lg cursor-pointer  md:text-2xl text-4xl"
                                                       data-target="modal1"
                                                    ></i>
                                                </x-tooltip>


                                                @break
                                                @case('R')
                                                <x-tooltip text="Cita Nueva">
                                                    <i class="fa-solid fa-file-circle-plus modal-trigger  text-green-500 fa-lg cursor-pointer  md:text-2xl text-4xl"
                                                       data-target="modal1"
                                                    ></i>
                                                </x-tooltip>
                                                @break
                                            @endswitch
                                        </div>
                                    </x-table.cell>
                                </x-table.row>
                            @empty
                                <x-table.row class="bg-white  border-b">
                                    <x-table.cell colspan="5"
                                                  class="text-center text-gray-400 font-bold text-xl p-8 dark:text-gray-100">
                                        !Oops, No hay datos...
                                    </x-table.cell>
                                </x-table.row>
                            @endforelse
                        </x-table.body>
                    </x-table>

                    {{$appointments->links()}}

                @endisset
            </div>
        </x-card.content>
    </x-card>


</div>


@push('scripts')

    <script data-turbolinks-eval="false">


        function getLocation() {
            if (navigator.geolocation) {
                navigator.geolocation.getCurrentPosition(showPosition);
            } else {
                alert("Geolocation is not supported by this browser.");
            }
        }


        function showPosition(position) {
            window.localStorage.setItem("latitude", JSON.stringify(position.coords.latitude));
            window.localStorage.setItem("longitude", JSON.stringify(position.coords.longitude));
        }

        function loadIframe(iframeName, lat, lon) {

            getLocation();
            const latitude = window.localStorage.getItem('latitude');
            const longitude = window.localStorage.getItem('longitude');
            console.log(latitude);
            console.log(longitude);
            const url = 'https://www.google.com/maps/embed/v1/directions?key={{env('GOOGLE_API_KEY')}}&origin=' + latitude + ',' + longitude + '&destination=' + lat + ',' + lon + '&zoom=12';
            const $iframe = $('#' + iframeName);
            if ($iframe.length) {
                $iframe.attr('src', url);
                return false;
            }
            return true;
        }


        document.addEventListener('livewire:load', function () {
            $(".select2-icons").select2({
                dropdownAutoWidth: true,
                theme: 'material',
                width: '100%',
                minimumResultsForSearch: Infinity,
                templateResult: iconFormat,
                templateSelection: iconFormat,
                escapeMarkup: function (es) {
                    return es;
                }
            });
            $(".select2-icons").on('change', function () {
                @this.
                set('type_appointment', $(this).val());
            });

            $('input[om-datepicker]').on('apply.daterangepicker', function (ev, picker) {
                $(this).val(picker.startDate.format('MM/DD/YYYY') + ' - ' + picker.endDate.format('MM/DD/YYYY'));
                @this.
                set('startDate', picker.startDate.format('YYYY-MM-DD'));
                @this.
                set('endDate', picker.endDate.format('YYYY-MM-DD'));
            });

            $('input[om-datepicker]').on('change', function (e) {
                let range = rangeDate(e.target.value);
                @this.
                set('startDate', range.start);
                @this.
                set('endDate', range.end);

            });
        })


    </script>
@endpush

