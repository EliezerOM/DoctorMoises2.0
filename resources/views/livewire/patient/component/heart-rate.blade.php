<div>
    <form wire:submit.prevent="heartRateRefresh"
    x-data="{valueMax: @entangle('valueMax').defer, valueMin: @entangle('valueMin').defer}">

        <x-dialog-x name-dialog="heart_modal" class="p-2"
                    @show-dialg.window="Alpine.store('modal_heart_modal').heart_modal =  $event.detail.show">
            <x-dialog-x.header>
                <p class="text-center dark:text-gray-100">Frecuencia Cardíaca</p>
            </x-dialog-x.header>
            <x-form.input name="valueMin"
                          x-ref="min"
                          id="min"
                          text="Baja"
                          om-number
                          maxlength="3"
                          x-model="valueMin"
                          om-max-value="110"></x-form.input>

            <x-form.input text="Alta"
                          name="valueMax"
                          om-number
                          maxlength="3"
                          x-model="valueMax"
                          om-max-value="220"></x-form.input>

                <x-btn-outline class="w-full" type="submit">Registrar</x-btn-outline>
        </x-dialog-x>
    </form>

    <x-card class="m-2 bg-gradient-to-r w-52 h-[188px]  !from-purple-800 !via-purple-500 !to-purple-700">
        <x-card.header :header-outside="false">
            <p class="text-center font-semibold text-gray-100">Frecuencia Cardíaca</p>
        </x-card.header>
        <x-card.content>
            <div class="flex space-x-3">
                <i class="fa-solid fa-heart-pulse fa-4x text-white"></i>
                <div>
                    @if(!$model)
                        <p class="ordinal font-semibold text-sm text-gray-100">Frecuencia cardíaca no registrado</p>
                    @else
                        <p class="ordinal font-semibold text-4xl text-gray-100 diagonal-fractions">{{$min}}/{{$max}}</p>
                    @endif
                </div>
            </div>
            @if($model)
                <div class="flex grid">
                    <p class="text-sm font-semibold text-center text-gray-200">Última actualización</p>
                    <p class="text-sm font-semibold text-center text-gray-200">{{typeDate($lastUpdate)}}</p>
                </div>
            @endif
        </x-card.content>
        <x-card.footer class="!p-0 flex grid">
            <x-dialog-x.trigger name-dialog="heart_modal">
                <x-btn-outline tipo-btn="warning"
                               class="rounded-b-lg rounded-none w-full !bg-purple-300 !text-purple-800"
                               @click="
                    $nextTick(() => {
                        setTimeout(() => {
                        document.getElementById('min').focus();
                        document.getElementById('min').select(); },200);
                        });">Registrar nuevo
                    examen
                </x-btn-outline>
            </x-dialog-x.trigger>
        </x-card.footer>
    </x-card>
</div>

