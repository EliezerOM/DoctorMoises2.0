<div>

    <div wire:loading class="w-full p-4 ">
        <x-skeleton.info-doctor/>
    </div>

    <div wire:loading.class="hidden">

        @isset($appointment)
            <div class="relative m-0 mb-10 w-[115%]"
                 :class="tabs2 !== 0 && !slideInfo ? 'invisible': '' ">

                <img src="{{asset('img/main/portada2.png')}}" class="w-[120%] shadow-lg rounded-r rounded-l-lg -ml-16"
                     x-transition
                     :class="tabs2 !== 0 && !slideInfo ? 'invisible': '' ">
                <i class="fa-solid fa-bookmark fa-9x absolute z-40  top-[-1rem] left-[-6rem] shadow-r-xl shadow-l-xl shadow-t-xl fa-gradient"
                   style="transform: scale(0.3,.8);"></i>
                <x-img-profile :photo="$appointment->doctor->foto_perfil??''"
                               class="w-[30%] h-[51%] shadow-xl ml-2 bg-white absolute -bottom-10 ring-2"/>

                <div
                    class="bg-gradient-to-l {{$appointment->doctor->doctor_valido??'' === 'S' ? 'from-green-600 to-green-700': 'from-red-600 to-red-700'}} absolute  ring-2 z-0 flex items-center py-0.5 px-2 rounded-xl"
                    style="left: 30%; transform: translateX(-80%); bottom: -35px;">
                    @if($appointment->doctor->doctor_valido??'' === 'S')
                        <i class="fa-solid fa-circle-check text-white mr-1"></i>
                        <p class="text-white">Activo</p>
                    @else
                        <i class="fas fa-times-circle text-white mr-1"></i>
                        <p class="text-white">Sin Servicios</p>
                    @endif
                </div>

            </div>

            <!-- Info Doctor -->
            <div class="mx-2 space-y-2">
                <div class="m-2 flex justify-between items-center">
                    <div class=" space-y-2">
                        <p om-capitalize
                           class="font-semibold md:text-2xl text-sm dark:text-gray-100">{{($appointment->doctor->sexo??'' === 'M'? 'Dr. ' : 'Dra. ')}} {{$appointment->doctor->nombres??''}}  {{$appointment->doctor->apellidos??''}}</p>

                        <div class="flex justify-start items-center">
                            <i class="fa-solid fa-stethoscope fa-xl text-blue-700 mr-2"></i>
                            <p class="font-semibold md:text-xl text-xs dark:text-gray-100">{{$appointment->speciality->descripcion??''}}</p>
                        </div>
                    </div>
                    <div class="mr-1">
                        <p class="font-semibold md:text-xl text-sm dark:text-gray-100">Calificación</p>
                        <div class="relative flex justify-center items-center">
                            <p class="font-bold absolute text-white dark:text-gray-100">{{$appointment->doctor->qualification??0}}</p>
                            <i class="fa-solid fa-certificate fa-3x fa-gradient-blue "></i>
                        </div>
                    </div>
                </div>
                <p class="font-semibold md:text-lg text-center md:text-left dark:text-gray-100">Contactos</p>
                <div class="h-0.5 w-[98%] bg-gray-300 my-1 mr-2 "></div>
                <div class="flex grid md:grid-cols-2 md:justify-around space-y-4 justify-center border shadow-lg p-2 mr-2">

                    @isset($appointment->doctor->whatsapp)
                        <div class=" flex space-x-2 items-center">
                            <i class="fa-brands fa-whatsapp text-green-500"></i>
                            <a class="hover:underline hover:cursor-pointer dark:text-gray-100">{{$appointment->doctor->whatsapp}}</a>
                        </div>
                    @endisset

                    @isset($appointment->doctor->telefono)
                        <div class=" flex space-x-2 items-center">
                            <i class="fa-solid fa-phone text-blue-500"></i>
                            <a class="hover:underline hover:cursor-pointer dark:text-gray-100">{{$appointment->doctor->telefono}}</a>
                        </div>
                    @endisset



                    @isset($appointment->doctor->user->email)
                        <div class=" flex space-x-2 items-center">
                            <i class="fa-solid fa-envelope text-red-500"></i>
                            <a class="hover:underline hover:cursor-pointer dark:text-gray-100">{{$appointment->doctor->user->email}}</a>
                        </div>
                    @endisset

                    @isset($appointment->doctor->web)
                        <div class=" flex space-x-2 items-center">
                            <i class="fa-solid fa-globe text-blue-500"></i>
                            <a class="hover:underline hover:cursor-pointer dark:text-gray-100"
                               href="{{$appointment->doctor->web}}"
                               target="_blank">Sitio Web</a>
                        </div>
                    @endisset

                    @isset($appointment->doctor->facebook)
                        <div class=" flex space-x-2 items-center">
                            <i class="fab fa-facebook text-blue-600 "></i>
                            <a class="hover:underline hover:cursor-pointer dark:text-gray-100" om-capitalize
                               href="{{$appointment->doctor->facebook}}" target="_blank">Facebook
                                - {{$appointment->doctor->nombres}}{{$appointment->doctor->apellidos}}</a>
                        </div>
                    @endisset

                    @isset($appointment->doctor->instagram)
                        <div class=" flex space-x-2 items-center">
                            <i class="fa-brands fa-instagram  text-pink-800"></i>
                            <a class="hover:underline hover:cursor-pointer dark:text-gray-100"
                               href="{{$appointment->doctor->instagram}}"
                               target="_blank">Instagram
                                - {{$appointment->doctor->nombres}}{{$appointment->doctor->apellidos}}</a>
                        </div>
                    @endisset
                </div>

            </div>
            <x-link class="w-[95%] mx-2 my-1 justify-center flex bg-gradient-to-r from-blue-600 to-blue-900 dark:from-blue-800 dark:to-blue-900 rounded-none rounded-b-full ring-2 shadow-lg">
                <p class="text-gray-100 md:text-xl">Acerca de mí</p>
            </x-link>
            <!-- End Info Doctor -->
        @endisset
    </div>

</div>
@once
    @push('styles')
        <style>
            .fa-gradient {
                /*background: -webkit-gradient(linear, left top, left bottom, from(#E9D71C), to(#EF8F00));*/
                background: -webkit-gradient(linear, left top, left bottom, from(#4F2C2C), to(#f00));
                -webkit-background-clip: text;
                -webkit-text-fill-color: transparent;
            }

            .fa-gradient-yellow {
                background: -webkit-gradient(linear, left top, left bottom, from(#EF8F00), to(#E9D71C));
                -webkit-background-clip: text;
                -webkit-text-fill-color: transparent;
            }

            .fa-gradient-blue {
                background: -webkit-gradient(linear, left top, left bottom, from(#22397A), to(#1953D4));
                -webkit-background-clip: text;
                -webkit-text-fill-color: transparent;
            }
        </style>
    @endpush
@endonce

