<div class="flex justify-center items-center" x-data="{dropdown_notify: false}" >
    <button type="button"
            @click="dropdown_notify = !dropdown_notify"
            class="text-gray-800 relative  dark:text-gray-200
                            hover:bg-gray-100 h-fit dark:hover:bg-gray-700
                            focus:outline-none focus:ring-4 focus:ring-gray-200
                            focus:bg-white dark:focus:ring-gray-900
                            dark:bg-gray-800 bg-gray-200 rounded-full
                            text-sm p-2.5 mr-3">
        <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke-width="1.5"
             stroke="currentColor" class="w-6 h-6">
            <path stroke-linecap="round" stroke-linejoin="round"
                  d="M14.857 17.082a23.848 23.848 0 005.454-1.31A8.967 8.967 0 0118 9.75v-.7V9A6 6 0 006 9v.75a8.967 8.967 0 01-2.312 6.022c1.733.64 3.56 1.085 5.455 1.31m5.714 0a24.255 24.255 0 01-5.714 0m5.714 0a3 3 0 11-5.714 0M3.124 7.5A8.969 8.969 0 015.292 3m13.416 0a8.969 8.969 0 012.168 4.5"/>
        </svg>
        @if($unread->count() > 0 )
        <div class="inline-flex absolute -top-2 -right-2 justify-center items-center w-6 h-6 text-xs font-bold text-white bg-red-500 rounded-full border-2 border-white dark:border-gray-900">{{$unread->count()}}</div>
        @endif
    </button>

<div x-show="dropdown_notify"
    x-transition
    x-on:click.outside="dropdown_notify = false"
    class="w-[25rem] rounded-lg dark:bg-gradient-to-t dark:to-gray-700 dark:via-gray-400 dark:from-gray-800 absolute p-1 shadow-lg ring-1 ring-gray-100 overflow-y-scroll justify-center  grid md:h-[32rem] h-[38rem]  z-40 right-2 top-20 text-gray-900 bg-gray-50">
        <x-list>
            @forelse($notifications as $notify)
                <x-list.item>
                        <div class="!p-0.5 border-b-2 hover:bg-gray-100 dark:hover:bg-gray-800 rounded-lg   {{$notify->read_at ? ' opacity-50': 'bg-white  dark:bg-gray-800'}}"
                              wire:click="leido({{$notify}})">
                            <div class="cursor-pointer   ">
                                <div class="flex items-center space-x-1 ">
                                    <x-img-profile class="h-[32px] w-[32px]"
                                                   photo="{{$notify->data['foto']}}"/>

                                    <div>
                                        <p class="text-base  font-bold text-gray-900 dark:text-white text-wrap ">
                                            {{$notify->data['mensaje']}}
                                        </p>

                                        <p class="dark:text-gray-300 text-xs pt-1 font-semibold text-gray-700">{{str_pad($notify->data['id_cita'],10,'0',STR_PAD_LEFT)}}</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                </x-list.item>

            @empty
                <p class="text-gray-400 dark:text-gray-300 text-center font-bold ">No hay notificaciones recientes</p>
            @endforelse
        </x-list>
</div>

</div>
