<x-card class="m-2 bg-gradient-to-r w-52 h-[188px] !from-teal-600 !via-teal-400 !to-teal-500 cursor-pointer">
    <x-card.header :header-outside="false">
        <p class="text-center font-semibold text-gray-100"> Estado IMC</p>
    </x-card.header>
    <x-card.content>
        <div class="flex space-x-3">
            <i class="fa-solid fa-file-waveform fa-4x text-white"></i>
            <div>
                @if(!$imc)
                    <p class="ordinal font-semibold text-gray-100">Registre Altura Y Peso</p>
                @else
                    <p class="ordinal font-semibold text-3xl text-gray-100">{{$imc}}</p>
                @endif
            </div>
        </div>
        <div class="flex grid">
            <p class="text-sm font-semibold text-center text-gray-200 mt-3">Indice De Masa Corporal</p>
        </div>
    </x-card.content>

</x-card>

