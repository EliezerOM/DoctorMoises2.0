<div>
    @isset($appointment->center)
        <div class="space-y-2 m-2 mr-5 ">
            <div class="grid md:grid-cols-2 justify-items-center space-x-1">
                <x-img-profile :photo="$appointment->center->logo"
                               class="rounded-2xl h-44 w-52 bg-gradient-to-r from-gray-400 via-blue-200 to-gray-300 ring-1 shadow-lg dark:shadow-white border-0 p-2 m-1"/>
                <div class="justify-center flex grid items-center">
                    <p class="md:text-4xl text-center dark:text-gray-100 text-lg font-semibold">{{$appointment->center->nombre}}</p>
                    <p class="dark:text-gray-100">{{$appointment->center->direccion}}</p>
                </div>
            </div>
            <div>
                <p class="ml-2 dark:text-gray-100 text-xl">Contactos</p>
                <div class="h-0.5 w-full bg-gray-300 my-1 mr-2 "></div>
            </div>
            <div class="mx-2 flex md:justify-start justify-center grid md:grid-cols-2 space-y-2">
                @isset($appointment->center->telefono)
                    <div class=" flex space-x-2 items-center">
                        <i class="fa-solid fa-phone text-blue-500"></i>
                        <a class="hover:underline hover:cursor-pointer dark:text-gray-100">{{$appointment->center->telefono}}</a>
                    </div>
                @endisset
                @isset($appointment->center->telefono2)
                    <div class=" flex space-x-2 items-center">
                        <i class="fa-solid fa-phone text-blue-500"></i>
                        <a class="hover:underline hover:cursor-pointer dark:text-gray-100">{{$appointment->center->telefono2}}</a>
                    </div>
                @endisset
                @isset($appointment->center->whatsapp)
                    <div class=" flex space-x-2 items-center">
                        <i class="fa-brands fa-whatsapp text-green-500"></i>
                        <a class="hover:underline hover:cursor-pointer dark:text-gray-100">{{$appointment->center->whatsapp}}</a>
                    </div>
                @endisset
                @isset($appointment->center->email)
                    <div class=" flex space-x-2 items-center">
                        <i class="fa-solid fa-envelope text-red-500"></i>
                        <a class="hover:underline hover:cursor-pointer dark:text-gray-100">{{$appointment->center->email}}</a>
                    </div>
                @endisset

                @isset($appointment->center->web)
                    <div class=" flex space-x-2 items-center">
                        <i class="fa-solid fa-globe text-blue-500"></i>
                        <a class="hover:underline hover:cursor-pointer dark:text-gray-100"
                           href="{{$appointment->center->web}}"
                           target="_blank">Sitio Web</a>
                    </div>
                @endisset

                @isset($appointment->center->facebook)
                    <div class=" flex space-x-2 items-center">
                        <i class="fab fa-facebook text-blue-600 "></i>
                        <a class="hover:underline hover:cursor-pointer dark:text-gray-100" om-capitalize
                           href="{{$appointment->center->facebook}}" target="_blank">Facebook</a>
                    </div>
                @endisset

                @isset($appointment->center->instagram)
                    <div class=" flex space-x-2 items-center">
                        <i class="fa-brands fa-instagram  text-pink-800"></i>
                        <a class="hover:underline hover:cursor-pointer dark:text-gray-100"
                           href="{{$appointment->doctor->instagram}}"
                           target="_blank">Instagram</a>
                    </div>
                @endisset
            </div>
            <div class="pb-5">
                <iframe
                    src="https://www.google.com/maps/embed/v1/place?key={{env('GOOGLE_API_KEY')}}&q={{explode(',', $appointment->center->coordenada)[0]}},{{explode(',', $appointment->center->coordenada)[1]}}&zoom=18"
                    class="w-full md:h-[400px] h-[250px] bg-red-400 rounded-xl"
                    allowfullscreen
                    referrerpolicy="no-referrer-when-downgrade"/>
            </div>
            <p class="dark:text-gray-100">{{$appointment->center->coordenada}}</p>
        </div>

    @endisset
</div>
