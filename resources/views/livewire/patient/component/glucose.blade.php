<div>
    <form wire:submit.prevent="registerGlucose"
          x-data="{glucosa:@entangle('value').defer}">

        <x-dialog-x name-dialog="glucose_modal" class="p-2"
                    @show-dialg.window="Alpine.store('modal_glucose_modal').glucose_modal =  $event.detail.show"
        >
            <x-dialog-x.header>
                <p class="text-center dark:text-gray-100">Nivel De Glucosa</p>
            </x-dialog-x.header>
            <x-form.input text="Nivel"
                          x-model="glucosa"
                          om-decimal
                          id="glucose"
            name="value"></x-form.input>

            <x-btn-outline class="w-full" type="submit">Registrar</x-btn-outline>
        </x-dialog-x>
    </form>

    <x-card class="m-2 bg-gradient-to-r w-52 h-[188px]  !from-amber-800 !via-amber-500 !to-amber-700">
        <x-card.header :header-outside="false">
            <p class="text-center font-semibold text-gray-100">Nivel De Glucosa</p>
        </x-card.header>
        <x-card.content>
            <div class="flex space-x-3">
                <i class="fa-solid fa-cubes-stacked fa-4x text-white"></i>
                <div>
                    @if(!$model)
                        <p class="ordinal font-semibold text-sm text-gray-100">Nivel de glucosa no registrado</p>
                    @else
                        <p class="ordinal font-semibold text-xl text-gray-100">{{$glucose}} <span
                                class="text-sm font-bold">mg/dl</span>
                        </p>
                    @endif
                </div>
            </div>
            @if($model)
                <div class="flex grid">
                    <p class="text-sm font-semibold text-center text-gray-200">Última actualización</p>
                    <p class="text-sm font-semibold text-center text-gray-200">10/15/2022</p>
                </div>
            @endif
        </x-card.content>
        <x-card.footer class="!p-0 flex grid">
            <x-dialog-x.trigger name-dialog="glucose_modal" >
                <x-btn-outline tipo-btn="warning" class="rounded-b-lg rounded-none w-full"
                               @click="
                    $nextTick(() => {
                        setTimeout(() => {
                        document.getElementById('glucose').focus();
                        document.getElementById('glucose').select(); },200);
                        });">Registrar nuevo examen
                </x-btn-outline>
            </x-dialog-x.trigger>
        </x-card.footer>
    </x-card>
</div>

