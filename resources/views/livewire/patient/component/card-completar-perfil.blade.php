<x-card
    class="border m-2 md:w-96 w-full bg-gradient-to-r via-white from-gray-200  to-gray-100 dark:from-transparent dark:to-transparent">
    <x-card.header class="-mt-0" :header-outside="false">
        <div class="w-full">
            <p class="font-bold text-gray-900 text-lg text-center dark:text-gray-100">Completa Tu Perfil</p>
            <div class="w-full bg-gray-300 h-0.5 dark:bg-gray-500"></div>
        </div>

    </x-card.header>
    <x-card.content class="overscroll-y-auto h-auto md:h-52">
        <div class="md:justify-start justify-center flex  ">
            <x-progress-bar-circular :percent="round(($per_configuration+$per_profile+$per_subscription)/3,2)"/>
            <x-list>
                <x-list.item :is-check="$per_configuration >= 100">
                    <p class="text-blue-600 font-bold dark:text-blue-300">Configuración <span
                            class="text-xs text-blue-400 font-bold dark:text-blue-600">{{round($per_configuration,2)}}%</span>
                    </p>
                </x-list.item>

                <x-list.item :is-check="$per_profile >= 100">
                    <p class="text-blue-600 font-bold dark:text-blue-300">Perfil de usuario <span
                            class="text-xs text-blue-400 font-bold dark:text-blue-600">{{round($per_profile,2)}}%</span>
                    </p>
                </x-list.item>

                <x-list.item :is-check="$per_subscription >= 100">
                    <p class="text-blue-600 font-bold dark:text-blue-300">Suscripción <span
                            class="text-xs text-blue-400 font-bold dark:text-blue-600">{{round($per_subscription,2)}}%</span>
                    </p>
                </x-list.item>

            </x-list>
        </div>
    </x-card.content>
    <x-card.footer>
        <x-dialog-x.trigger name-dialog="guideProfile">
            <x-btn-outline class="w-full text-center text-base">Completar Perfil</x-btn-outline>
        </x-dialog-x.trigger>
    </x-card.footer>
    <div x-data="{page:0}">
        <x-dialog-x name-dialog="guideProfile" class="top-[5rem] !fixed">
            <!-- This is an example component -->
            <x-stepper class="h-[40rem]"
                       style="contain: content;">

                <x-stepper.header number-tab="3" >
                    <x-stepper.tab-step :index="1">
                        <div class="text-lg font-bold text-gray-700 leading-tight  dark:text-gray-100">Configuración
                        </div>
                    </x-stepper.tab-step>
                    <x-stepper.tab-step :index="2">
                        <div class="text-lg font-bold text-gray-700 leading-tight  dark:text-gray-100">Perfil De Usuario
                        </div>
                    </x-stepper.tab-step>
                    <x-stepper.tab-step :index="3">
                        <div class="text-lg font-bold text-gray-700 leading-tight  dark:text-gray-100">Suscripción</div>
                    </x-stepper.tab-step>
                </x-stepper.header>

                <x-stepper.content-complete>
                    <div class="bg-white rounded-lg p-10 flex items-center shadow justify-between">
                        <div>
                            <svg class="mb-4 h-20 w-20 text-green-500 mx-auto" viewBox="0 0 20 20"
                                 fill="currentColor">
                                <path fill-rule="evenodd"
                                      d="M10 18a8 8 0 100-16 8 8 0 000 16zm3.707-9.293a1 1 0 00-1.414-1.414L9 10.586 7.707 9.293a1 1 0 00-1.414 1.414l2 2a1 1 0 001.414 0l4-4z"
                                      clip-rule="evenodd"/>
                            </svg>

                            <h2 class="text-2xl mb-4 text-gray-800 text-center font-bold">Registration
                                Success</h2>

                            <div class="text-gray-600 mb-8">
                                Thank you. We have sent you an email to demo@demo.test. Please click the link in
                                the message to activate your account.
                            </div>
                            <x-stepper.home-back>
                                <x-btn>Inicio</x-btn>
                            </x-stepper.home-back>
                        </div>
                    </div>
                </x-stepper.content-complete>

                <x-stepper.content>
                    <!-- Step Content -->
                    <x-stepper.page :index="1">
                        Lorem ipsum dolor sit amet, consectetur adipisicing elit. Eius iure optio sit voluptatum! At
                        culpa delectus eum ipsum minus nulla sunt velit voluptas voluptatibus? Error nam pariatur
                        quaerat quas sapiente.
                        Lorem ipsum dolor sit amet, consectetur adipisicing elit. Eius iure optio sit voluptatum! At
                        culpa delectus eum ipsum minus nulla sunt velit voluptas voluptatibus? Error nam pariatur
                        quaerat quas sapiente.      Lorem ipsum dolor sit amet, consectetur adipisicing elit. Eius iure optio sit voluptatum! At
                        culpa delectus eum ipsum minus nulla sunt velit voluptas voluptatibus? Error nam pariatur
                        quaerat quas sapiente.
                        Lorem ipsum dolor sit amet, consectetur adipisicing elit. Eius iure optio sit voluptatum! At
                        culpa delectus eum ipsum minus nulla sunt velit voluptas voluptatibus? Error nam pariatur
                        quaerat quas sapiente.
                        Lorem ipsum dolor sit amet, consectetur adipisicing elit. Eius iure optio sit voluptatum! At
                        culpa delectus eum ipsum minus nulla sunt velit voluptas voluptatibus? Error nam pariatur
                        quaerat quas sapiente.
                        Lorem ipsum dolor sit amet, consectetur adipisicing elit. Eius iure optio sit voluptatum! At
                        culpa delectus eum ipsum minus nulla sunt velit voluptas voluptatibus? Error nam pariatur
                        quaerat quas sapiente.
                        Lorem ipsum dolor sit amet, consectetur adipisicing elit. Eius iure optio sit voluptatum! At
                        culpa delectus eum ipsum minus nulla sunt velit voluptas voluptatibus? Error nam pariatur
                        quaerat quas sapiente.
                        Lorem ipsum dolor sit amet, consectetur adipisicing elit. Eius iure optio sit voluptatum! At
                        culpa delectus eum ipsum minus nulla sunt velit voluptas voluptatibus? Error nam pariatur
                        quaerat quas sapiente.
                        Lorem ipsum dolor sit amet, consectetur adipisicing elit. Eius iure optio sit voluptatum! At
                        culpa delectus eum ipsum minus nulla sunt velit voluptas voluptatibus? Error nam pariatur
                        quaerat quas sapiente.
                        Lorem ipsum dolor sit amet, consectetur adipisicing elit. Eius iure optio sit voluptatum! At
                        culpa delectus eum ipsum minus nulla sunt velit voluptas voluptatibus? Error nam pariatur
                        quaerat quas sapiente.
                        Lorem ipsum dolor sit amet, consectetur adipisicing elit. Eius iure optio sit voluptatum! At
                        culpa delectus eum ipsum minus nulla sunt velit voluptas voluptatibus? Error nam pariatur
                        quaerat quas sapiente.
                    </x-stepper.page>

                    <x-stepper.page :index="2">
                        Lorem ipsum dolor sit amet, consectetur adipisicing elit. Dolores esse natus officia
                        perspiciatis quod, sunt velit vero? Adipisci asperiores autem culpa dolor ex facilis ipsam
                        natus, optio possimus quae totam.
                    </x-stepper.page>

                    <x-stepper.page :index="3">
                        Lorem ipsum dolor sit amet, consectetur adipisicing elit. A accusamus alias assumenda beatae
                        corporis ex fugit libero maiores minus molestiae, natus nisi odit omnis quae quod reiciendis
                        tempora vel voluptatibus.
                    </x-stepper.page>
                    <!-- / Step Content -->
                </x-stepper.content>

                <!-- Footer Navigation -->
                <x-stepper.footer>
                    <x-stepper.previous>
                        <x-btn ::disabled="step <= 1">Regresar</x-btn>
                    </x-stepper.previous>
                    <x-stepper.next>
                        <x-btn>Siguiente</x-btn>
                    </x-stepper.next>
                    <x-stepper.complete>
                        <x-btn>Completar</x-btn>
                    </x-stepper.complete>
                </x-stepper.footer>
            </x-stepper>
            <!--  Footer Navigation-->

        </x-dialog-x>
    </div>

</x-card>

