<div>
    <form wire:submit.prevent="refreshBloodPresure"
          x-data="{valueSistolica: @entangle('valueSistolica').defer, valueDiastolica: @entangle('valueDiastolica').defer}">
        <x-dialog-x name-dialog="pressure_modal" class="p-2 "
                    @show-dialg.window="Alpine.store('modal_pressure_modal').pressure_modal =  $event.detail.show">
            <x-dialog-x.header>
                <p class="text-center dark:text-gray-100 text-xl md:text-base">Presión Sanguínea</p>
            </x-dialog-x.header>
            <x-form.input text="Sistólica"
                          x-model="valueSistolica"
                          name="valueSistolica"
                          x-ref="sistolica"
                          id="valueSistolica"
                          om-number
                          maxlength="3"
                          om-max-value="300"></x-form.input>
            <x-form.input text="Diastólica" x-model="valueDiastolica"
                          name="valueDiastolica"
                          x-ref="diastolica"
                          om-number
                          maxlength="3"
            om-max-value="200"></x-form.input>

            <x-btn-outline class="w-full" type="submit">Registrar</x-btn-outline>
        </x-dialog-x>
    </form>

    <x-card class="m-2 bg-gradient-to-r w-52  h-[188px] !from-red-800 !via-red-500 !to-red-700">
        <x-card.header :header-outside="false">
            <p class="text-center font-semibold text-gray-100">Presión Sanguínea</p>
        </x-card.header>
        <x-card.content>
            <div class="flex space-x-3">
                <i class="fa-solid fa-stethoscope fa-4x text-white"></i>
                <div>

                    @if(!$model)
                        <p class="ordinal font-semibold text-sm text-gray-100">Presión sanguínea no registrado</p>

                    @else
                        <p class="ordinal font-semibold text-lg text-gray-100">{{$sistolica}} <span
                                class="text-xs font-bold">Sistólica</span>
                        </p>
                        <p class="ordinal font-semibold text-lg text-gray-100">{{$diastolica}} <span
                                class="text-xs font-bold">Diastólica</span></p>
                    @endif
                </div>

            </div>
            @if($model)
                <div class="flex grid">
                    <p class="text-sm font-semibold text-center text-gray-200">Última actualización</p>
                    <p class="text-sm font-semibold text-center text-gray-200">{{typeDate($lastUpdate)}}</p>
                </div>
            @endif
        </x-card.content>
        <x-card.footer class="!p-0 flex grid">
            <x-dialog-x.trigger name-dialog="pressure_modal">
                <x-btn-outline tipo-btn="error" class="rounded-b-lg rounded-none w-full text-white "
                               @click="
                    $nextTick(() => {
                        setTimeout(() => {
                        document.getElementById('valueSistolica').focus();
                        document.getElementById('valueSistolica').select(); },200);
                        });">Registrar nuevo
                    examen
                </x-btn-outline>
            </x-dialog-x.trigger>
        </x-card.footer>
    </x-card>
</div>

