<div>
    <form wire:submit.prevent="tallRefresh"
          x-data="{typeTall: @entangle('typeTall').defer, value:@entangle('value').defer, METRO: 3.281}"
          x-init="$watch('typeTall', ()=>{value = (typeTall? value / METRO: value * METRO).toFixed(2);})">

        <x-dialog-x name-dialog="tall_modal" class="p-2"
                    @show-dialg.window="Alpine.store('modal_tall_modal').tall_modal =  $event.detail.show">
            <div class="flex items-center">
                <x-form.input id="refAltura" name="value" text="Estatura" class-input="!w-[102px]" om-decimal x-model="value"
                              x-ref="refAltura"></x-form.input>
                <p class="dark:text-gray-100 border p-[9px] pl-[10px] rounded-r-lg dark:bg-gray-900 bg-gray-300  text-gray-700 font-semibold text-lg"
                   x-text="typeTall?'M':'P'"></p>

            </div>

            <div class="flex my-4 space-x-2 w-full justify-center">
                <span class="text-sm font-medium text-gray-900 dark:text-gray-300">Pie</span>
                <label for="small-toggle2" class="inline-flex relative items-center  cursor-pointer">
                    <input type="checkbox" x-model="typeTall" id="small-toggle2" class="sr-only peer">
                    <div
                        class="w-9 h-5 bg-red-700 peer-focus:outline-none  rounded-full peer dark:bg-gray-700 peer-checked:after:translate-x-full peer-checked:after:border-white after:content-[''] after:absolute after:top-[2px] after:left-[2px] after:bg-white after:border-gray-300 after:border after:rounded-full after:h-4 after:w-4 after:transition-all dark:border-gray-600 peer-checked:bg-blue-600"></div>
                </label>
                <span class=" text-sm font-medium text-gray-900 dark:text-gray-300">Metros</span>
            </div>
                <x-btn-outline class="w-full" type="submit">Registrar</x-btn-outline>
        </x-dialog-x>
    </form>

    <x-card class="m-2 bg-gradient-to-r w-52 h-[188px]  w-full !from-indigo-800 !via-indigo-500 !to-indigo-700">
        <x-card.header :header-outside="false">
            <p class="text-center font-semibold text-gray-100">Su Estatura</p>
        </x-card.header>
        <x-card.content>
            <div class="flex space-x-3">
                <i class="fa-solid fa-person-arrow-up-from-line fa-4x text-white"></i>
                <div>
                    @if(!$tall)
                        <p class="ordinal font-semibold text-sm text-gray-100">Estatura no registrado</p>
                    @else
                        <p class="ordinal font-semibold text-2xl text-gray-100">{{number_format(round($foot,2), 2)}}
                            <span
                                class="text-sm font-bold">P</span>
                        </p>
                        <p class="ordinal font-semibold text-2xl text-gray-100">{{number_format(round($meter,2),2)}}
                            <span
                                class="text-sm font-bold">M</span></p>
                    @endif
                </div>
            </div>
            @if($tall)
                <div class="flex grid">
                    <p class="text-sm font-semibold text-center text-gray-200">Última actualización</p>
                    <p class="text-sm font-semibold text-center text-gray-200">{{typeDate($lastUpdate)}}</p>
                </div>
            @endif
        </x-card.content>
        <x-card.footer class="!p-0 flex grid">
            <x-dialog-x.trigger name-dialog="tall_modal">
                <x-btn-outline tipo-btn="warning"
                               class="rounded-b-lg rounded-none w-full !bg-indigo-300 !text-indigo-800"
                               @click="
                    $nextTick(() => {
                        setTimeout(() => {
                        document.getElementById('refAltura').focus();
                        document.getElementById('refAltura').select(); },200);
                        });">Registrar nuevo
                    examen
                </x-btn-outline>
            </x-dialog-x.trigger>
        </x-card.footer>
    </x-card>
</div>

