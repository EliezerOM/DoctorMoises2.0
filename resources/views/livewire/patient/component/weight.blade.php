<div >
    <form wire:submit.prevent="weightRefresh"
          x-data="{typeWeight: @entangle('typeWeight').defer, weight:@entangle('value').defer, KILO: 2.205, modal: false}"
          x-init="$watch('typeWeight', ()=>{weight = (typeWeight? weight / KILO: weight * KILO).toFixed(2);})"
    >
        <x-dialog-x name-dialog="weigth_modal" class="p-2"
                    @show-dialg.window="Alpine.store('modal_weigth_modal').weigth_modal =  $event.detail.show">
            <div class="flex items-center">
            <x-form.input id="refPeso" name="value" text="Peso" class-input="!w-[102px]" om-decimal x-model="weight" x-ref="refPeso"></x-form.input>
                <p class="dark:text-gray-100 border p-[9px] pl-[10px] rounded-r-lg dark:bg-gray-900 bg-gray-300  text-gray-700 font-semibold text-lg" x-text="typeWeight?'Kg':'Lib'"></p>
            </div>

            <div class="flex my-4 space-x-2 w-full justify-center">
                <span class="text-sm font-medium text-gray-900 dark:text-gray-300">Libras</span>
                <label for="small-toggle" class="inline-flex relative items-center  cursor-pointer">
                    <input type="checkbox" x-model="typeWeight"  id="small-toggle" class="sr-only peer">
                    <div
                        class="w-9 h-5 bg-red-700 peer-focus:outline-none  rounded-full peer dark:bg-gray-700 peer-checked:after:translate-x-full peer-checked:after:border-white after:content-[''] after:absolute after:top-[2px] after:left-[2px] after:bg-white after:border-gray-300 after:border after:rounded-full after:h-4 after:w-4 after:transition-all dark:border-gray-600 peer-checked:bg-blue-600"></div>
                </label>
                <span class=" text-sm font-medium text-gray-900 dark:text-gray-300">Kilogramos</span>
            </div>
                <x-btn-outline class="w-full" type="submit" >Registrar</x-btn-outline>
        </x-dialog-x>
    </form>

        <x-card class="m-2 bg-gradient-to-r  w-full h-[188px] !from-blue-800 w-52 !via-blue-500 !to-blue-700">
            <x-card.header :header-outside="false">
                <p class="text-center font-semibold text-gray-100">Su Peso</p>
            </x-card.header>
            <x-card.content>
                <div class="flex space-x-3">
                    <i class="fa-solid fa-scale-balanced fa-4x text-white"></i>
                    <div>
                        @if(!$weight)
                            <p class="ordinal font-semibold text-sm text-gray-100">Peso no registrado</p>
                        @else
                            <p class="ordinal font-semibold text-2xl text-gray-100">{{number_format(round($pounds,2), 2)}} <span
                                    class="text-sm font-bold">L</span>
                            </p>
                            <p class="ordinal font-semibold text-2xl text-gray-100">{{number_format(round($kilograms,2),2)}} <span
                                    class="text-sm font-bold">Kg</span></p>
                        @endif
                    </div>
                </div>
                @if($weight)
                    <div class="flex grid">
                        <p class="text-sm font-semibold text-center text-gray-200">Última actualización</p>
                        <p class="text-sm font-semibold text-center text-gray-200">{{typeDate($lastUpdate)}}</p>
                    </div>
                @endif
            </x-card.content>
            <x-card.footer class="!p-0 flex grid">
                <x-dialog-x.trigger name-dialog="weigth_modal" >
                    <x-btn-outline   class="rounded-b-lg rounded-none w-full"  @click="
                    $nextTick(() => {
                        setTimeout(() => {
                        document.getElementById('refPeso').focus();
                        document.getElementById('refPeso').select(); },200);
                        });" >Registrar nuevo examen</x-btn-outline>
                </x-dialog-x.trigger>
            </x-card.footer>
        </x-card>

</div>

