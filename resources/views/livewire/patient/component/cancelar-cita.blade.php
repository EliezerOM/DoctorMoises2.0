
<div>

        @if($mini)
        <i class="fa-solid fa-circle-xmark  text-red-600 fa-lg cursor-pointer  md:text-2xl text-4xl {{$classIcon??''}}"
           wire:click.prevent="confirmAppointmentSeletedId"></i>
        @else
            <x-btn :tipo-btn="'danger'"
                   class="{{$classButton??''}}"
                   wire:click.prevent="confirmAppointmentSeletedId">
                Cancelar
            </x-btn>
        @endif
    </div>

