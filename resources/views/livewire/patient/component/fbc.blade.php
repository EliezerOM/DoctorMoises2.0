<x-card class="m-2 bg-gradient-to-r w-52 h-[172px] !from-cyan-600 !via-cyan-400 !to-cyan-500">
    <x-card.header :header-outside="false">
        <p class="text-center font-semibold text-gray-100">Estado FBC</p>
    </x-card.header>
    <x-card.content>
        <div class="flex space-x-3">
            <i class="fa-solid fa-file-waveform fa-4x text-white"></i>
            <div>
                <p class="ordinal font-semibold text-3xl text-gray-100">Estable
                </p>
            </div>
        </div>
        <div class="flex grid">
            <p class="text-sm font-semibold text-center text-gray-200">Ultima actualización</p>
            <p class="text-sm font-semibold text-center text-gray-200">10/15/2022</p>
        </div>
    </x-card.content>

</x-card>


