<div>
    <div wire:loading class="w-full p-4 ">
        <x-skeleton.info-appointment/>
    </div>

    @isset($appointmentSeleted)
        <div class="grid justify-center relative">
            <div
                class="rounded-full flex m-2 mt-4 ring-4 shadow-xl justify-center items-center h-[12em] w-[12em]  bg-blue-800">
                <i class="fa-solid fa-9x fa-2x text-gray-100 fa-calendar-check"></i>
            </div>
            <p class="font-bold text-3xl text-gray-200 font-italic">
                #{{ str_pad($appointmentSeleted->id,10,'0',STR_PAD_LEFT)}}</p>
            <div class=" flex justify-center w-auto ">

                @switch($appointmentSeleted->status)
                    @case('S')
                    <div
                        class="flex items-center shadow-xl px-2 ring-2 ring-yellow-400  rounded-full bg-gray-200  justify-center">
                        <x-point-color color="bg-yellow-400"/>
                        <p class="font-semibold">Solcitada</p>
                    </div>
                    @break
                    @case('R')
                    <div
                        class="flex items-center shadow-xl px-2 ring-red-600  ring-2 rounded-full bg-gray-200  justify-center">
                        <x-point-color color="bg-red-600"/>
                        <p class="font-semibold">Rechazada</p>
                    </div>
                    @break
                    @case('C')
                    <div
                        class="flex items-center shadow-xl ring-2 px-2 ring-blue-600  rounded-full bg-gray-200  justify-center">
                        <x-point-color color="bg-blue-600" animate="animate-ping"/>
                        <p class="font-semibold">Aprobada</p>
                    </div>
                    @break
                    @case('F')
                    <div
                        class="flex items-center shadow-xl px-2 ring-green-400 ring-2 rounded-full bg-gray-200  justify-center">
                        <x-point-color color="bg-green-400"/>
                        <p class="font-semibold">Concluída</p>
                    </div>
                    @break
                @endswitch
            </div>


        </div>

        <div class="flex justify-around py-2 w-full">
            <div class="grid">
                <p class="text-gray-200">Fecha</p>
                <p class="text-gray-200">{{typeDate($appointmentSeleted->fecha)}}</p>
            </div>
            <div class="grid">
                <p class="text-gray-200">Desde</p>
                <p class="text-gray-200">{{typeHour($appointmentSeleted->schedule->hora_inicio??'')}}</p>
            </div>
            <div class="grid">
                <p class="text-gray-200">Hasta</p>
                <p class="text-gray-200">{{typeHour($appointmentSeleted->schedule->hora_fin??'')}}</p>
            </div>
        </div>

        <div class="h-[1px] w-full bg-gray-400 my-2"></div>

        <div class="md:flex grid justify-center md:justify-between mx-4">
            <div class="shadow-xl border-b-2 w-auto m-2 h-auto ">
                <p class="text-lg text-gray-200 font-semibold">Valor
                    RD$ {{number_format($appointmentSeleted->valor_cita, 2)}}</p>
            </div>
            @if($appointmentSeleted->status === 'S')
                <div class="grid space-y-2 md:space-y-0 md:space-x-2 md:flex">
                    <livewire:patient.component.cancelar-cita :mini="0"
                                                              class-button="w-full"
                                                              wire:key="{{'slide-'.$appointmentSeleted->id}}"
                                                              :id-cita="$appointmentSeleted->id"/>

                    <x-btn :tipo-btn="'success'">Re-Agendar</x-btn>
                </div>
            @elseif($appointmentSeleted->status === 'C')
                <div class="grid space-y-2 md:space-y-0 md:space-x-2 md:flex">
                    <livewire:patient.component.cancelar-cita :mini="0"
                                                              class-button="w-full"
                                                              wire:key="{{'slide2-'.$appointmentSeleted->id}}"
                                                              :id-cita="$appointmentSeleted->id"/>

                    <x-btn :tipo-btn="'info'">Solicitar Cambio</x-btn>
                </div>
            @elseif($appointmentSeleted->status === 'F' || $appointmentSeleted->status === 'R')
                <div class="grid space-y-2 md:flex">
                    <x-btn :tipo-btn="'success'" class="w-full">Nueva Cita</x-btn>
                </div>
            @endif
        </div>

        @if($appointmentSeleted->status === 'R')
            <p class="mt-5 mx-5 text-white text-xl font-semibold"> Motivo Solicitud Rechazada</p>
            <div class="h-[1px] w-full bg-gray-400 mb-2"></div>
            <p class="text-gray-100 mx-2">Lorem ipsum dolor sit amet, consectetur adipisicing elit.
                Aperiam
                atque dolorem dolores ea eligendi esse fugiat illum, incidunt obcaecati praesentium
                quae,
                repudiandae, sit unde ut velit voluptate voluptatem voluptates voluptatibus?</p>
        @endif
        <p class="mt-5 mx-5 text-white text-xl font-semibold"> Seguros que aplican</p>
        <div class="h-[1px] w-full bg-gray-400 mb-2"></div>

        @forelse ($appointmentSeleted->center_doctor->center_doctor_insurance??[] as $insurance)
            <div
                class="flex  justify-start md:border-b md:border-x-0 md:border-t-0 mx-4 border-gray-300 shadow-xl rounded-xl px-2 mt-2 md:mt-0  border">
                <div class="flex justify-start space-x-2 items-center my-2">
                    <x-img-profile class="bg-gray-100 p-[2px] h-[6rem] w-[6rem] object-contain"
                                   :photo="$insurance->type_insurance->ars->logo"/>
                    <div>
                        <p class="text-gray-100 font-semibold md:text-xl text-sm">{{$insurance->type_insurance->ars->descripcion}}
                            - {{$insurance->type_insurance->descripcion}}</p>
                        @if($insurance->type_insurance->ars->descripcion)
                            <a href="{{$insurance->type_insurance->ars->link}}" target="_blank"
                               class="italic border-b text-gray-100">Ir a la página</a>
                        @endif
                    </div>
                </div>
            </div>
        @empty
            <div
                class="flex mx-4 px-2 mt-2 justify-around ">
                <div class="flex">
                    <i class="fas fa-ban text-gray-100 fa-2x mr-1"></i>
                    <p class="text-gray-100 font-italic text-xl ">No aplica ningún seguro</p>
                </div>

            </div>
        @endforelse

    @endisset
</div>
