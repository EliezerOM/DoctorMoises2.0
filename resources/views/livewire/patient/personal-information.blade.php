<?php
$patient = $user->patient;
?>
<div class="relative">

    <!-- Foto-->
    <div class="mx-2 absolute md:-top-[6rem]     -top-[2.5rem] flex w-full items-center">
        <div class="relative">
            <x-img-profile
                photo="https://pixinvent.com/materialize-material-design-admin-template/app-assets/images/user/12.jpg"
                class="md:w-52 w-24"/>
            <divc class="absolute  left-2 md:top-[50%] top-[4.8rem] ">
                <i class="fa-solid fa-circle-check md:text-3xl text-lg -top-3 md:top-1 md:left-1 -left-1 z-20 absolute text-blue-600"></i>
                <div class="md:w-4 w-3 md:h-4 h-3 bg-white md:top-3 left-0 -top-1 md:left-3 absolute z-10"></div>
            </divc>
            <div class="shadow-2xl
                        bg-gray-100
                        w-fit
                        rounded-r-2xl
                        dark:bg-gray-800
                        my-1
                        -ml-[0.5rem]
                        p-1
                        ">
                <p class="md:text-2xl text-sm font-bold text-blue-800 md:m-2 mx-1 dark:text-blue-100">{{$patient->fullName()}}</p>
                <div class="flex items-center md:m-2 ">
                    <svg aria-hidden="true" class="md:w-5 md:h-5 w-3 h-3 text-yellow-400" fill="currentColor"
                         viewBox="0 0 20 20" xmlns="http://www.w3.org/2000/svg"><title>First star</title>
                        <path
                            d="M9.049 2.927c.3-.921 1.603-.921 1.902 0l1.07 3.292a1 1 0 00.95.69h3.462c.969 0 1.371 1.24.588 1.81l-2.8 2.034a1 1 0 00-.364 1.118l1.07 3.292c.3.921-.755 1.688-1.54 1.118l-2.8-2.034a1 1 0 00-1.175 0l-2.8 2.034c-.784.57-1.838-.197-1.539-1.118l1.07-3.292a1 1 0 00-.364-1.118L2.98 8.72c-.783-.57-.38-1.81.588-1.81h3.461a1 1 0 00.951-.69l1.07-3.292z"></path>
                    </svg>
                    <svg aria-hidden="true" class="md:w-5 md:h-5 w-3 h-3 text-yellow-400" fill="currentColor"
                         viewBox="0 0 20 20" xmlns="http://www.w3.org/2000/svg"><title>Second star</title>
                        <path
                            d="M9.049 2.927c.3-.921 1.603-.921 1.902 0l1.07 3.292a1 1 0 00.95.69h3.462c.969 0 1.371 1.24.588 1.81l-2.8 2.034a1 1 0 00-.364 1.118l1.07 3.292c.3.921-.755 1.688-1.54 1.118l-2.8-2.034a1 1 0 00-1.175 0l-2.8 2.034c-.784.57-1.838-.197-1.539-1.118l1.07-3.292a1 1 0 00-.364-1.118L2.98 8.72c-.783-.57-.38-1.81.588-1.81h3.461a1 1 0 00.951-.69l1.07-3.292z"></path>
                    </svg>
                    <svg aria-hidden="true" class="md:w-5 md:h-5 w-3 h-3 text-yellow-400" fill="currentColor"
                         viewBox="0 0 20 20" xmlns="http://www.w3.org/2000/svg"><title>Third star</title>
                        <path
                            d="M9.049 2.927c.3-.921 1.603-.921 1.902 0l1.07 3.292a1 1 0 00.95.69h3.462c.969 0 1.371 1.24.588 1.81l-2.8 2.034a1 1 0 00-.364 1.118l1.07 3.292c.3.921-.755 1.688-1.54 1.118l-2.8-2.034a1 1 0 00-1.175 0l-2.8 2.034c-.784.57-1.838-.197-1.539-1.118l1.07-3.292a1 1 0 00-.364-1.118L2.98 8.72c-.783-.57-.38-1.81.588-1.81h3.461a1 1 0 00.951-.69l1.07-3.292z"></path>
                    </svg>
                    <svg aria-hidden="true" class="md:w-5 md:h-5 w-3 h-3 text-yellow-400" fill="currentColor"
                         viewBox="0 0 20 20" xmlns="http://www.w3.org/2000/svg"><title>Fourth star</title>
                        <path
                            d="M9.049 2.927c.3-.921 1.603-.921 1.902 0l1.07 3.292a1 1 0 00.95.69h3.462c.969 0 1.371 1.24.588 1.81l-2.8 2.034a1 1 0 00-.364 1.118l1.07 3.292c.3.921-.755 1.688-1.54 1.118l-2.8-2.034a1 1 0 00-1.175 0l-2.8 2.034c-.784.57-1.838-.197-1.539-1.118l1.07-3.292a1 1 0 00-.364-1.118L2.98 8.72c-.783-.57-.38-1.81.588-1.81h3.461a1 1 0 00.951-.69l1.07-3.292z"></path>
                    </svg>
                    <svg aria-hidden="true" class="md:w-5 md:h-5 w-3 h-3 text-gray-300 dark:text-gray-500"
                         fill="currentColor" viewBox="0 0 20 20" xmlns="http://www.w3.org/2000/svg"><title>Fifth
                            star</title>
                        <path
                            d="M9.049 2.927c.3-.921 1.603-.921 1.902 0l1.07 3.292a1 1 0 00.95.69h3.462c.969 0 1.371 1.24.588 1.81l-2.8 2.034a1 1 0 00-.364 1.118l1.07 3.292c.3.921-.755 1.688-1.54 1.118l-2.8-2.034a1 1 0 00-1.175 0l-2.8 2.034c-.784.57-1.838-.197-1.539-1.118l1.07-3.292a1 1 0 00-.364-1.118L2.98 8.72c-.783-.57-.38-1.81.588-1.81h3.461a1 1 0 00.951-.69l1.07-3.292z"></path>
                    </svg>
                    <p class="md:ml-2 ml-1 md:text-sm text-xs font-bold text-gray-500 dark:text-gray-400">4.95 / 5</p>
                </div>
            </div>
        </div>

        <p class="h-fit bg-gradient-to-br cursor-pointer waves-light waves-effect text-center via-green-500 rounded-2xl p-0.5 m-0.5 text-white from-green-600 to-green-800 w-40 md:ml-1 font-extrabold p-0 md:text-base -top-[2rem] md:-top-[3.3rem]">
            Seguir <i class="fa-solid fa-plus"></i></p>
        <p class="h-fit bg-gradient-to-br cursor-pointer waves-light waves-effect text-center via-blue-500 rounded-2xl p-0.5 m-0.5 text-white from-blue-600 to-blue-800 w-40 md:ml-1 font-extrabold p-0 md:text-base -top-[2rem] md:-top-[3.3rem]">
            Editar <i class="fa-solid fa-pen"></i></p>
    </div>

    <!-- Portada-->
    <img src="https://pixinvent.com/materialize-material-design-admin-template/app-assets/images/gallery/profile-bg.png"
         class=" shadow-2xl rounded shadow-inner">

    <div
        class="border-b-2 border-blue-700 dark:border-gray-400  bg-gray-100 dark:bg-gradient-to-t dark:from-gray-800 dark:to-gray-700 w-fit  rounded-b-xl w-full grid grid-cols-3 md:h-28">
        <div class="border-r border-gray-400 text-center">
            <p class="md:text-3xl text-xs md:m-2 mx-2 text-gray-600 font-bold dark:text-gray-100">Seguidores</p>
            <p class="md:text-2xl text-xs   md:m-2 font-semibold text-center dark:text-purple-400 text-purple-800">
                453</p>
        </div>

        <div class="border-r border-gray-400 text-center">
            <p class="md:text-3xl text-xs md:m-2 mx-2 text-gray-600 font-bold dark:text-gray-100">Seguidos</p>
            <p class="md:text-2xl text-xs   md:m-2 font-semibold text-center  dark:text-purple-400 text-purple-800">
                386</p>
        </div>

        <div class="text-center">
            <p class="md:text-3xl text-xs md:m-2 mx-2 text-gray-600 font-bold dark:text-gray-100">Publicaciones</p>
            <p class="md:text-2xl text-xs  md:m-2 font-semibold text-center  dark:text-purple-400 text-purple-800">
                15</p>
        </div>
    </div>

    <!-- Body-->
    <div
        class="grid md:grid-cols-4 h-screen shadow mt-1 bg-gray-50   dark:bg-gradient-to-t dark:from-gray-800 dark:to-gray-700 rounded-t-xl">

        <!-- Right -->
        <div class="dark:bg-gray-800  bg-gray-100 space-y-3 p-2 ">
            <!-- Specialities -->
            <p class="md:text-lg text-center font-medium text-gray-800">Especialidades <i
                    class=" fa-solid fa-user-doctor"></i></p>
            <x-list>
                <x-list.item class="text-gray-900">Ginecóloga</x-list.item>
                <x-list.item class="text-gray-900">Cardióloga</x-list.item>
            </x-list>
            <div class="h-0.5 w-full bg-gray-300"></div>
            <!--End specialities -->
            <!-- Medical Center -->
            <p class="md:text-lg text-center font-medium text-gray-800">Centros Médicos <i
                    class="fa-solid fa-hospital"></i></p>
            <x-list>
                <x-list.item class="text-gray-900">HOMS</x-list.item>
                <x-list.item class="text-gray-900">Coromina</x-list.item>
            </x-list>
            <div class="h-0.5 w-full bg-gray-300"></div>
            <!-- End Medical Center-->
            <p class="md:text-lg text-center font-medium text-gray-800">Seguros Médicos <i
                    class="fa-solid fa-file-shield"></i></p>
            <x-list>
                <x-list.item class="text-gray-900">Senasa</x-list.item>
                <x-list.item class="text-gray-900">ARS Humano</x-list.item>
            </x-list>
            <div class="h-0.5 w-full bg-gray-300"></div>
        </div>
        <!-- End right-->

        <!-- Center -->
        <div class="col-span-2 m-5 ">

            <x-tab.tab x-data="{slideInfo: false, tabs2:0}" class="!overflow-hidden">
                <x-tab.header>
                    <x-tab.header-column tabs="tabs2"
                                         class="w-full flex justify-center dark:text-gray-100 dark:bg-gray-800"
                                         :index="0"
                                         color-slide="bg-gray-700"
                                         color-text="text-gray-700 dark:text-gray-100"
                                         icon="fa-solid fa-square-rss text-gray-700"
                                         name="Noticias"/>

                    <x-tab.header-column tabs="tabs2"
                                         class="w-full flex justify-center dark:text-gray-100 dark:bg-gray-800"
                                         :index="1"
                                         color-slide="bg-gray-700"
                                         color-text="text-gray-700 dark:text-gray-100"
                                         icon="fa-solid fa-highlighter text-gray-700"
                                         name="Destacados"/>

                </x-tab.header>

                <x-tab.content class="p-5 ">
                    <x-tab.content-item tabs="tabs2" :index="0">
                        <x-card>
                            <x-card.header :header-outside="false" class="m-2">
                                <p>Noticia</p>
                            </x-card.header>
                            <x-card.content>
                                <img
                                    src="https://pixinvent.com/materialize-material-design-admin-template/app-assets/images/gallery/post-1.png"
                                    class="h-72 w-full" alt="">
                                <p>Designing Services</p>
                                <p>UI/UX & Graphics Design</p>
                            </x-card.content>
                            <x-card.footer class="p-5">
                                <div class="flex space-x-3 ">
                                    <div class="flex items-center">
                                        <i class="fa-regular fa-thumbs-up text-blue-600"></i><p>124</p>
                                    </div>
                                    <div class="flex items-center">
                                        <i class="fa-regular fa-comment text-blue-600"></i><p>55</p>
                                    </div>
                                </div>

                            </x-card.footer>

                        </x-card>
                    </x-tab.content-item>

                    <x-tab.content-item tabs="tabs2" :index="1">
                        <x-card>
                            <x-card.header :header-outside="false">
                                <p>Destacado</p>
                            </x-card.header>
                            <x-card.content>
                                <img
                                    src="https://pixinvent.com/materialize-material-design-admin-template/app-assets/images/gallery/post-3.png"
                                    class="h-72 w-full" alt="">
                            </x-card.content>
                        </x-card>
                    </x-tab.content-item>
                </x-tab.content>
            </x-tab.tab>
        </div>

        <!--End center -->

        <!-- Left -->
        <div class="dark:bg-gray-800 bg-gray-100">

        </div>
        <!-- End left -->

    </div>

</div>

