<x-card class="md:w-96 w-full m-2 ">
    <x-card.header class="-mt-0 !-mx-0" :header-outside="false">
        <div class="w-full rounded-t-lg dark:bg-gray-800 bg-gray-200 shadow-md">
            <p class="font-bold text-gray-900 dark:text-gray-100 text-lg text-center">Notificaciones </p>
        </div>

    </x-card.header>
    <x-card.content class=" h-52  overflow-y-scroll">
        <x-list>
            @forelse($notifications as $notify)
                <x-list.item>
                    <x-card
                        class="border-none  {{$notify->read_at ? ' bg-gray-200':'bg-white'}} w-full waves-effect dark:waves-light waves-dark drop-shadow-md">
                        <x-card.content class="!p-0.5">
                            <div class="cursor-pointer   ">
                                <div class="flex items-center space-x-1 ">
                                    <x-img-profile class="h-[48px] w-[48px]"
                                                   photo="{{$notify->data['foto']}}"/>

                                    <div>
                                        <p class="text-sm  font-bold text-gray-900  dark:text-white text-wrap">
                                            {{$notify->data['mensaje']}}
                                        </p>
                                        <p class="text-sm text-gray-700 italic truncate font-semibold  dark:text-gray-400 w-72">
                                            {{$notify->data['sexo'] === 'M'?'Dr. ': 'Dra. '}} {{$notify->data['doctor']}}
                                        </p>
                                        <p class="dark:text-gray-500 text-xs pt-1 font-semibold text-gray-700">{{str_pad($notify->data['id_cita'],10,'0',STR_PAD_LEFT)}}</p>
                                    </div>
                                </div>
                            </div>
                        </x-card.content>
                    </x-card>
                </x-list.item>

            @empty
                <p class="text-gray-400 dark:text-gray-300 text-center font-bold ">No hay notificaciones recientes</p>
            @endforelse
        </x-list>
    </x-card.content>
    <x-card.footer class=" text-sm text-center dark:text-gray-200">
        @if($notifications->count() > 0)
            <div class="flex justify-center items-center space-x-1">
                <i class="fa-solid fa-angles-right"></i>
                <a href="" class="hover:underline">Ver Más</a>
                <i class="fa-solid fa-angles-right"></i>
            </div>
        @endif
    </x-card.footer>
</x-card>
