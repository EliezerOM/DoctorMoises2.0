<div>

    <x-card>
        <x-card.header>
            <p class="text-4xl text-gray-100 text-center font-bold">DASHBOARD</p>
        </x-card.header>
        <x-card.content>

            <div class="flex  flex-wrap md:justify-start justify-center">
                <div class="w-full mt-2 md:h-[188px] !from-blue-800 md:w-72 md:mr-5 mx-8 md:pl-2">
                    <x-btn tipo-btn="success" class="w-full items-center flex justify-between  bg-gradient-to-r from-green-500 via-green-400 to-green-700 " >
                        <span class="text-xl ">Reservar Cita</span>
                        <i class="fa-solid fa-calendar-check  fa-2x text-white"></i>
                    </x-btn>
                    <x-btn class="w-full items-center md:my-6 my-2 flex justify-between  bg-gradient-to-r from-blue-700 via-blue-500 to-blue-800"  >
                        <span class="text-xl">Catálogo De Doctores</span>
                        <i class="fa-solid fa-list-ul text-white fa-2x"></i>
                    </x-btn>
                    <x-btn tipo-btn="danger" class="w-full items-center flex justify-between  bg-gradient-to-r from-red-600 via-red-400 to-red-700" >
                        <span class="text-xl">Noticias</span>
                        <i class="fa-solid fa-square-rss text-white fa-2x"></i>
                    </x-btn>
                </div>
                <livewire:patient.component.weight/>
                <livewire:patient.component.tall/>
                <livewire:patient.component.heart-rate/>
                <livewire:patient.component.blood-pressure/>
                <livewire:patient.component.glucose/>
                <livewire:patient.component.imc/>
                {{--            <livewire:patient.component.fbc/>--}}
            </div>
            <div class="flex md:justify-between flex-wrap ">
                <livewire:patient.notification/>
                <livewire:patient.cita-recientes/>
                <livewire:patient.recibos/>
                <livewire:patient.component.card-completar-perfil/>
            </div>
        </x-card.content>
    </x-card>
</div>
{{--@push('scripts')--}}
{{--    <script>--}}
{{--        Echo.private('Users.{!! Auth::user()->id !!}')--}}
{{--            .notification((notification)=>{--}}
{{--               console.log(notification);--}}
{{--            });--}}
{{--    </script>--}}
{{--@endpush--}}

