<div>
    <x-card class="!bg-transparent border-0 shadow-none">
        <x-card.header>

    <div class="flex justify-center">
        <p class="text-gray-200 text-3xl font-bold">CONFIGURACIÓN</p>
    </div>
        </x-card.header>

        <x-card.content >

        <div class=" grid md:grid-cols-3 lg:grid-cols-4  grid-cols-1  ">

        <x-card-link :route="route('patient.personal-information')"
                     :detail="'Encontrarás tu información de perfil y otras opciones'"
                     :text-button="'Ir a Información Personal'"
                     :image-background="asset('img/main/my-account.png')"
        />

        <x-card-link :route="route('patient.security-privacy')"
                     :detail="'Cambie de contraseña y configure la privacidad de su perfil'"
                     :text-button="'Ir a Seguridad & Privacidad'"
                     :image-background="asset('img/main/security.png')"
        />
        <x-card-link :route="route('patient.subscripcion-payments')"
                     :detail="'Asociar método de pago, verifique todas sus transacciones de pagos'"
                     :text-button="'Ir a Suscripción & Pagos'"
                     :image-background="asset('img/main/pay.png')"
        />
    </div>
        </x-card.content>

    </x-card>

</div>
