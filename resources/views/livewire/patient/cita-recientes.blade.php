<div x-data="{slideInfo: false, tabs2:0}" class="flex flex-col  md:w-96 w-full m-2">
    <!--  Information on the appointment !-->
    <x-tab.tab class="!w-auto !h-auto">
        <x-tab.transition is-overlay="isOverlay" is-slide="slideInfo">
            <x-tab.header>
                <x-tab.header-column tabs="tabs2" class="w-full flex justify-center dark:text-gray-100 dark:bg-gray-800"
                                     :index="0"
                                     color-slide="bg-gray-700"
                                     color-text="text-gray-700 dark:text-gray-100"
                                     icon="fa-solid fa-calendar-check text-gray-700"
                                     name="Cita"/>

                <x-tab.header-column tabs="tabs2" class="w-full flex justify-center dark:text-gray-100 dark:bg-gray-800"
                                     :index="1"
                                     name="Doctor"
                                     color-text="text-blue-700 dark:text-gray-100"
                                     color-slide="bg-blue-700"
                                     icon="fa-solid fa-user-doctor text-blue-700"/>

                <x-tab.header-column tabs="tabs2" class="w-full flex justify-center dark:text-gray-100 dark:bg-gray-800"
                                     :index="2"
                                     color-text="text-green-700 dark:text-gray-100"
                                     name="Centro"
                                     color-slide="bg-green-700"
                                     icon="fa-solid fa-hospital text-green-700"/>
            </x-tab.header>
            <x-tab.content>
                <!-- Info Appointment -->
                <x-tab.content-item tabs="tabs2" :index="0"
                                    class="bg-gradient-to-t from-blue-500 via-purple-500 to-blue-700 h-full ">
                    <livewire:patient.component.info-appointment/>
                </x-tab.content-item>
                <!-- End Info Appointment -->

                <!--Info Doctor -->
                <x-tab.content-item tabs="tabs2" :index="1">
                    <livewire:patient.component.info-doctor/>
                </x-tab.content-item>
                <!-- End Info Doctor -->

                <!-- Medical Center -->
                <x-tab.content-item tabs="tabs2" :index="2">
                    <livewire:patient.component.medical-center/>
                </x-tab.content-item>
                <!-- End Medical Center -->
            </x-tab.content>
        </x-tab.transition>
    </x-tab.tab>
    <!--  End Information on the appointment !-->

    <x-card class="w-full ">
        <x-card.header class="-mt-0 !-mx-0" :header-outside="false">
            <div class="w-full rounded-t-lg dark:bg-gray-800 bg-gray-200 shadow-md ">
                <p class="font-bold text-gray-900 text-lg text-center dark:text-gray-100">Citas Recientes</p>
            </div>

        </x-card.header>
        <x-card.content class="!p-1 h-56 overflow-y-scroll  h-full relative">
            <x-list>

                @forelse ($appointments as $appointment)
                    <x-list.item>
                        <x-card wire:click="$emit('searchAppointment',{{$appointment}})"
                                @click.stop="slideInfo = true; isOverlay = true; tabs2 = 0;"
                                class="border-none m-0.5 bg-gray-100 w-full waves-effect dark:waves-light waves-dark drop-shadow-md">
                            <x-card.content class="!p-0.5">
                                <div class="cursor-pointer   ">
                                    <div class="flex items-center space-x-1 ">

                                        <div>
                                            <p class="text-sm  font-bold text-gray-900 truncate dark:text-white ">
                                                <i class="fa-solid fa-calendar-check"></i> {{Str::padLeft($appointment->id, 9, 0)}}
                                            </p>

                                        @switch($appointment->status)
                                            @case('F')
                                                <span class="bg-green-200  text-green-800 text-sm font-medium mr-2 px-2.5 py-0.5 rounded absolute right-0 top-0">Concluída</span>
                                            @break
                                                @case('C')
                                                <span class="bg-blue-200  text-blue-800 text-sm font-medium mr-2 px-2.5 py-0.5 rounded absolute right-0 top-0">Aprobada</span>
                                                @break
                                                @case('R')
                                                <span class="bg-red-200  text-red-800 text-sm font-medium mr-2 px-2.5 py-0.5 rounded absolute right-0 top-0">Rechazada</span>
                                                @break
                                                @case('S')
                                                <span class="bg-yellow-200  text-yellow-800 text-sm font-medium mr-2 px-2.5 py-0.5 rounded absolute right-0 top-0">Solicitada</span>
                                                @break
                                            @endswitch
                                            <p class="text-sm text-gray-700 italic truncate font-semibold  dark:text-gray-400 w-72">

                                            </p>
                                            <p class="dark:text-gray-500 text-xs pt-1 font-semibold text-gray-700">
                                                <i class="fa-solid fa-location-dot"></i> {{$appointment->center->direccion}}
                                            </p>
                                            <div class="flex md:space-x-0 space-x-2 md:block">
                                                <p class="dark:text-gray-500 text-xs pt-1 font-semibold text-gray-700 ">
                                                    <i class="fa-solid fa-user-doctor pr-1"></i> {{ ($appointment->doctor->sexo === 'M'?'Dr. ':'Dra. ').$appointment->doctor->nombres.' '.$appointment->doctor->apellidos}}
                                                </p>
                                                <p class="dark:text-gray-500 text-xs pt-1 font-semibold text-gray-700">
                                                    <i class="fa-solid fa-clock pr-1"></i>{{typeDate($appointment->fecha).', '. typeHour($appointment->schedule->hora_inicio).' - '.typeHour($appointment->schedule->hora_fin)}}
                                                </p>
                                            </div>

                                        </div>
                                    </div>
                                </div>
                            </x-card.content>
                        </x-card>
                    </x-list.item>
                @empty
                    <p class="text-gray-400 dark:text-gray-300 text-center font-bold ">No hay citas recientes</p>
                @endforelse
            </x-list>
        </x-card.content>
        <x-card.footer class=" text-sm text-center dark:text-gray-200">
            @if($appointments->count() > 0)
                <div class="flex justify-center items-center space-x-1">
                    <i class="fa-solid fa-angles-right"></i>
                    <a href="{{route('patient.personal-schedule')}}" class="hover:underline">Ver Más</a>
                    <i class="fa-solid fa-angles-right"></i>
                </div>
            @endif
        </x-card.footer>
    </x-card>
</div>

