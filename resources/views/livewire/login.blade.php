<div class="login  ">
    <form wire:submit.prevent="logon" method="post" class="flex justify-center ">
        <!-- Card -->
        <div class="card mx-2 my-12 border rounded-lg border-grey-300 dark:border-gray-900 dark:bg-gray-800 shadow-xl w-auto md:w-auto w-full">

            <div class="card-title flex justify-center">
                    <h3 class="text-gray-700 font-bold dark:text-gray-100">Login</h3>
                <button type="button" class="btn bg-red-800 hover:bg-red-900  modal-close absolute right-0 m-0 rounded-tr-lg {{Route::is(['login']) ? 'invisible': ''}}">
                    <i class="fa fa-close"></i>
                </button>
            </div>
            <div class="card-content">

                <x-form.input id="name"
                              name="name"
                              text="Usuario / Correo"
                              required
                              om-without-space
                              class-content="mt-5"
                              wire:model.defer="name"/>

                <x-form.input id="pass"
                              name="pass"
                              text="Contraseña"
                              required
                              type="password"
                              om-without-space
                              class-content="mt-5"
                              wire:model.defer="pass"/>

                <label for="remember" class="flex items-center mb-4 p-2 cursor-pointer rounded border  border-gray-200 dark:border-gray-700 w-full">
                    <input type="checkbox" id="remember" wire:model.defer="remember" />
                    <span class="dark:text-gray-100">Recordar mi cuenta</span>
                    @error('remember')<p id="standard_error_help"
                                         class="mt-2 text-xs text-red-600 dark:text-red-400"><span
                            class="font-medium">{{ $message }}!</span></p> @enderror
                </label>

                <div class="flex">
                    <button type="submit" class="btn-om-primary w-full" data-ripple-light="true">
                        ACCEDER
                    </button>
                </div>

            </div>

            <div class="card-action !rounded-lg ">
                <div class="md:flex  md:justify-between ">
                        <a  href="{{route('forgot-password')}}"
                            class="flex justify-center  !p-0  text-sm no-underline  hover:underline  decoration-blue-800"
                           data-ripple-dark="true">
                            <div class="flex items-center !w-full  md:!w-[350px]  ">
                                <i class="fa-solid fa-key dark:text-gray-100 mr-2 text-blue-800"></i>
                                <p class=" text-blue-800 dark:text-gray-100 underline ">Olvide Mi Contraseña</p>
                            </div>
                        </a>
                </div>
            </div>
        </div>
        <!-- Card -->
    </form>

</div>



