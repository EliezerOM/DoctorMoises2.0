<div>
    @if (session()->has('message'))
        <div>
            {{ session('message') }}
        </div>
    @endif
    <h3>¡Tu correo electrónico no está verificado!</h3>
    <p>por favor verifique su dirección de correo electrónico.</p>

    <p>hemos enviado el correo de verificación a {{$email}}. Si no puede encontrar el correo de verificación de correo
        electrónico en la carpeta de índice. por favor revise la carpeta de correo basura/spam.</p>
    <p>si no recibió el correo de verificación de correo electrónico, haga clic en el botón reenviar.</p>
    <button wire:click.prevent="resend">Reenviar correo de verificación</button>
</div>
