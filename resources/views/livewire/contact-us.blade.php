<form wire:submit.prevent="enviar" class="md:p-4 p-4" >
    <x-card class="bg-gray-800 dark:bg-gray-800 bg-opacity-50 m-4">
        <x-card.header style="    margin-top: 0px;">
            <h5 class="font-bold text-white ">¡ Cuéntanos Tu Opinión !</h5>
        </x-card.header>

        <x-card.content>

                <div class="grid w-full">
                    <div class="grid grid-cols-1 md:grid-cols-2 ">
                    <div class="relative mx-2 z-0 my-4">
                        <input type="text" id="name_contact"
                               autocomplete="off"
                               om-capitalize
                               required
                               wire:model.defer="name"
                               class="block  focus:!border-white  py-2.5 px-0  w-full text-lg text-gray-200 bg-transparent border-0 border-b-2 border-white
                               appearance-none dark:text-white dark:border-gray-600 dark:focus:border-blue-500 focus:outline-none focus:ring-0 focus:border-white peer"
                               placeholder=" "/>
                        <label for="name_contact"
                               class="absolute !text-gray-200 text-lg dark:text-gray-400 duration-300 transform -translate-y-6 scale-100 top-3 -z-10 origin-[0] peer-focus:left-0
                           peer-focus:text-white peer-focus:dark:text-blue-500 peer-placeholder-shown:scale-100 peer-placeholder-shown:translate-y-0
                           peer-focus:scale-100 peer-focus:-translate-y-6">Nombre</label>
                        @error('name')<p id="standard_error_help" class="mt-2 text-xs text-red-600 dark:text-red-400">
                            <span class="font-medium">{{ $message }}!</span></p> @enderror
                    </div>

                    <div class="relative mx-2 z-0 my-4">
                        <input type="text" id="email_contact"
                               autocomplete="off"
                                wire:model.defer="email"
                               typeof="email"
                               required
                               class="block focus:!border-white  py-2.5 px-0 w-full text-lg text-gray-200 bg-transparent border-0 border-b-2 border-gray-300 appearance-none
                               dark:text-white dark:border-gray-600 dark:focus:border-blue-500 focus:outline-none focus:ring-0 focus:border-white peer"
                               placeholder=" "/>
                        <label for="email_contact"
                               om-capitalize
                               class="absolute !text-gray-200 text-lg dark:text-gray-400 duration-300 transform -translate-y-6 scale-100 top-3 -z-10
                           origin-[0] peer-focus:left-0 peer-focus:text-white peer-focus:dark:text-blue-500 peer-placeholder-shown:scale-100
                           peer-placeholder-shown:translate-y-0 peer-focus:scale-100 peer-focus:-translate-y-6">correo
                            electrónico
                        </label>
                        @error('email')<p id="standard_error_help" class="mt-2 text-xs text-red-600 dark:text-red-400">
                            <span class="font-medium">{{ $message }}!</span></p> @enderror
                    </div>

                    </div>

                    <div class="relative mx-2 z-0 my-4">
                    <textarea type="text" id="comment"
                              autocomplete="off"
                              required
                               wire:model.defer="comment"
                              maxlength="1000"
                              class="block h-auto py-2.5 text-lg px-0 w-full text-sm text-gray-200 bg-transparent border-0 border-b-2 border-gray-400 appearance-none
                             focus:outline-none focus:ring-0 focus:border-white peer"
                              placeholder=" "
                              rows="3">
                        </textarea>
                        <label for="comment"
                               class="absolute text-gray-200 dark:text-gray-400 text-lg duration-300 transform -translate-y-8 scale-125 top-3 -z-10 origin-[0] peer-focus:left-0
                           peer-focus:text-white peer-focus:dark:text-blue-500 peer-placeholder-shown:scale-100 peer-placeholder-shown:translate-y-0
                           peer-focus:scale-100 peer-focus:-translate-y-8">Comentario</label>
                        @error('comment')<p id="standard_error_help" class="mt-2 text-xs text-red-600 dark:text-red-400">
                            <span class="font-medium">{{ $message }}!</span></p> @enderror
                    </div>
                </div>


            <button type="submit" class="btn-om-primary w-auto waves-effect waves-light" >
                ENVIAR
            </button>
        </x-card.content>


    </x-card>
</form>
