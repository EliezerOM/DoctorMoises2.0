<div class="flex justify-center h-auto ">
    <form wire:submit.prevent="register" class="md:max-w-[550px] m-2 p-2 p-0 w-full" method="post">

        <div class="card mx-2 my-6 shadow-lg border rounded-lg border-grey-300 dark:border-gray-700 dark:bg-gray-700  shadow-xl "
             x-data="{ tipo_usuario: @entangle('tipo_usuario').defer}">


            <div class="card-title">
                <div class="flex justify-center -mt-14 mb-10">
                    <h4 class="text-grey-600 font-semibold dark:text-gray-100">Registro de Usuario</h4>
                </div>

                <div class="flex justify-around items-center mx-2 md:mx-4 -mt-7 h-10 ">
                    <label x-on:click="tipo_usuario = 'P'"
                           class="w-full mr-1 cursor-pointer  !border !border-blue-800 rounded-lg group bg-gray-100 hover:bg-gray-100 relative  !shadow-xl  "
                           :class="tipo_usuario === 'P' ?'bg-gradient-to-l from-blue-500 to-blue-800 dark:from-blue-800 dark:to-blue-900':'dark:bg-gray-700'">
                        <input name="tipo_usuario" type="radio" checked
                               wire:model.defer="tipo_usuario"
                               value="P"
                               class="peer"/>
                        <span class="md:m-2 m-1 md:font-bold "
                              :class="tipo_usuario === 'P' ?'text-white after:!bg-white ':'text-gray-800 dark:text-gray-100'">Soy Paciente</span>
                        <i class="fa-solid fa-user-injured fa-2x"
                           :class="tipo_usuario === 'P' ?'text-white after:!bg-white ':'text-gray-500 dark:text-gray-100'"></i>
                    </label>


                    <label x-on:click="tipo_usuario = 'D'"
                           class="w-full !border !border-blue-800 ml-1 cursor-pointer  rounded-lg group bg-gray-100 hover:bg-gray-100 relative  !shadow-xl  "
                           :class="tipo_usuario === 'D' ?'bg-gradient-to-l from-blue-500 to-blue-800 dark:from-blue-800 dark:to-blue-900 ':'dark:bg-gray-700 '">
                        <input name="tipo_usuario" type="radio" checked
                               wire:model.defer="tipo_usuario"
                               value="D"/>
                        <span class="md:m-2 m-1 md:font-bold "
                              :class="tipo_usuario === 'D' ?'text-white after:!bg-white ':' text-gray-800 dark:text-gray-100'">Soy Doctor</span>
                        <i class="fa-solid fa-user-doctor fa-2x "
                           :class="tipo_usuario === 'D' ?'text-white after:!bg-white ':'text-gray-500 dark:text-gray-100'"></i>

                    </label>
                </div>
            </div>

            <div class="card-content">
                <div class="flex justify-center" x-show="tipo_usuario === 'D'" x-transition:enter.duration.500ms>
                    <img class="mt-1 w-32 h-32 rounded-full shadow-lg" src="{{asset('img/main/doctors.png')}}"/>
                </div>

                <div class="flex justify-center" x-show="tipo_usuario !== 'D'" x-transition:enter.duration.500ms>
                    <img class="mt-1 w-32 h-32 rounded-full shadow-lg" src="{{asset('img/main/patients.png')}}"/>
                </div>

                <x-form.input id="nombre"
                              om-capitalize
                              name="nombres"
                              text="Nombres"
                              required
                              class-content="mt-5"
                              wire:model.defer="nombres"/>

                <x-form.input id="apellido"
                              om-capitalize
                              name="apellido"
                              text="Apellido"
                              required
                              wire:model.defer="apellidos"/>

                <div class="relative my-4">

                    <div class="relative">
                        <input datepicker
                               id="fecha_nacimiento"
                               datepicker-title="Fecha De Nacimiento"
                               type="text"
                               required
                               datepicker-format="dd/mm/yyyy"
                               x-mask="99/99/9999"
                               wire:model.defer="fecha_nacimiento"
                               class="block py-2.5 px-0
                           bg-opacity-25 w-full text-sm text-gray-900 border-0 border-b-2
                           border-gray-300 appearance-none dark:text-white dark:border-gray-600
                           dark:focus:border-blue-500 focus:outline-none focus:ring-0
                           focus:placeholder:text-gray-600
                           focus:border-blue-600 peer
                           placeholder:text-transparent"
                               placeholder="DD/MM/YYYY">

                        <label for="fecha_nacimiento"
                               class="absolute text-gray-500 dark:text-gray-400 duration-300 transform -translate-y-6
                               scale-100 top-3 origin-[0] peer-focus:left-0
                                text-lg
                               peer-focus:text-blue-600 peer-focus:dark:text-blue-500
                                peer-placeholder-shown:scale-100 peer-placeholder-shown:translate-y-0
                                peer-focus:scale-100 peer-focus:-translate-y-6">Fecha de nacimiento</label>

                        @error('fecha_nacimiento')<p class="mt-2  text-red-600 dark:text-red-400"><span
                                class="font-medium">{{ $message }}!</span></p> @enderror
                    </div>

                </div>

                <div class="flex justify-around my-4 -mx-2">

                    <label class="flex dark:bg-gray-600 dark:border-gray-600 items-center justify-start w-full waves-effect  rounded border border-gray-200 mx-2 cursor-pointer">
                        <input required wire:model.defer="sexo" type="radio" value="M" name="bordered-radio">
                        <span
                            class=" ml-2 text-blue-600  after:!bg-blue-600 after:!border-blue-600  w-full  font-bold text-gray-900 dark:text-gray-300 font-semibold">Hombre
                        </span>
                        <i class="fa-solid fa-2x  fa-venus  text-blue-600"></i>
                    </label>

                    <label
                        class="flex items-center dark:bg-gray-600 dark:border-gray-600 justify-start w-full waves-effect  rounded border border-gray-200 cursor-pointer mx-2">

                        <input id="female" required wire:model.defer="sexo" type="radio" value="F" name="bordered-radio">

                        <span
                            class=" ml-2 text-pink-600 font-semibold after:!bg-pink-600 after:!border-pink-600 dark:border-gray-200 w-full text-sm font-bold text-gray-900 dark:text-gray-300">Mujer
                        </span>
                        <i class="fa-solid fa-2x  fa-venus  text-pink-600"></i>

                    </label>



                </div>
                @error('sexo')<p class="mt-2 text-xs text-red-600 dark:text-red-400"><span class="font-medium">{{ $message }}!</span>
                </p> @enderror

{{--                <div class="relative z-0 my-4"--}}
{{--                     x-show="tipo_usuario == 'D'"--}}
{{--                     x-transition:enter.duration.500ms--}}
{{--                     x-transition:leave.duration.400ms>--}}
{{--                    <div class="input-field">--}}
{{--                        <input id="exequatur"--}}
{{--                               type="text"--}}
{{--                               wire:model.defer="exequatur"--}}
{{--                             >--}}
{{--                        <label for="exequatur" class="!text-lg">Exequatur</label>--}}
{{--                        <span class="text-blue-600 font-semibold text-sm opacity-70">Estará sujeto a validación </span>--}}
{{--                    </div>--}}
{{--                </div>--}}

                <x-form.input id="exequatur"
                              text="Exequatur"
                              x-show="tipo_usuario === 'D'"
                              wire:model.defer="exequatur"/>

                <x-form.input id="user"
                              name="user"
                              text="Usuario"
                              required
                              wire:model.defer="user"/>

{{--                <div class="input-field">--}}
{{--                    <input type="text" id="user"--}}
{{--                           required--}}
{{--                           wire:model.defer="user"--}}
{{--                           />--}}
{{--                    <label for="user"--}}
{{--                           class="!text-lg">Usuario</label>--}}
{{--                    @error('user')<p id="standard_error_help" class="mt-2 text-xs text-red-600 dark:text-red-400"><span--}}
{{--                            class="font-medium">{{ $message }}!</span></p> @enderror--}}
{{--                </div>--}}

{{--                <x-form.input id="usuario"--}}
{{--                              name="usuario"--}}
{{--                              text="Usuario"--}}
{{--                              ::x-show="tipo_usuario === 'D'"--}}
{{--                              x-transition:enter.duration.500ms--}}
{{--                              x-transition:leave.duration.400ms--}}
{{--                              required--}}
{{--                              wire:model.defer="usuario"/>--}}

                <x-form.input id="correo"
                              name="correo"
                              text="Correo"
                              required
                              type="email"
                              wire:model.defer="correo"/>

{{--                <div class="input-field">--}}
{{--                    <input type="text" id="correo"--}}
{{--                           required--}}
{{--                           wire:model.defer="correo" />--}}
{{--                    <label for="correo"--}}
{{--                           class="!text-lg">E-mail</label>--}}
{{--                    @error('correo')<p id="standard_error_help" class="mt-2 text-xs text-red-600 dark:text-red-400"><span--}}
{{--                            class="font-medium">{{ $message }}!</span></p> @enderror--}}
{{--                </div>--}}

                <x-form.input id="password"
                              name="password"
                              text="Contraseña"
                              required
                              type="password"
                              wire:model.defer="password"/>

                <x-form.input id="password_confirm"
                              name="password_confirm"
                              text="Confirmar contraseña"
                              required
                              type="password"
                              wire:model.defer="password_confirm"/>

{{--                <div class="input-field">--}}
{{--                    <input type="password" id="password"--}}
{{--                           required--}}
{{--                           wire:model.defer="correo"--}}
{{--                    />--}}
{{--                    <label for="password"--}}
{{--                           class="!text-lg">Contraseña</label>--}}
{{--                    @error('password')<p id="standard_error_help" class="mt-2 text-xs text-red-600 dark:text-red-400"><span--}}
{{--                            class="font-medium">{{ $message }}!</span></p> @enderror--}}
{{--                </div>--}}



{{--                <div class="input-field">--}}
{{--                    <input type="password" id="password_confirm"--}}
{{--                           required--}}
{{--                           wire:model.defer="password_confirm"--}}
{{--                    />--}}
{{--                    <label for="password_confirm"--}}
{{--                           class="!text-lg">Confirmar contraseña</label>--}}
{{--                    @error('password_confirm')<p id="standard_error_help" class="mt-2 text-xs text-red-600 dark:text-red-400"><span--}}
{{--                            class="font-medium">{{ $message }}!</span></p> @enderror--}}
{{--                </div>--}}


                <div
                    class="flex items-center p-2 w-full  rounded border border-gray-200 dark:border-gray-700 w-auto cursor-pointer flex justify-start "
                    data-ripple-dark="true">
                    <label>
                        <input type="checkbox" wire:model.defer="terminos"/>
                        <span class="dark:text-gray-100 ">HE LEÍDO Y ACEPTO <a
                                onclick="M.Modal.getInstance(document.getElementById('modal_condiciones')).open();"
                                class="no-underline w-auto hover:underline text-blue-600 dark:text-gray-100 dark:underline">TÉRMINOS Y CONDICIONES</a></span>
                        @error('terminos')<p id="standard_error_help"
                                             class="mt-2 text-xs text-red-600 dark:text-red-400"><span
                                class="font-medium">{{ $message }}!</span></p> @enderror
                    </label>

                </div>
            </div>

            <div class="card-action !bg-transparent !rounded-l-lg">
                <button class="btn-om-primary w-full m-2 waves-effect waves-light  " type="submit">Registrate</button>
                <a class="flex justify-center w-full no-underline hover:underline hover:decoration-blue-600 waves-effect waves-light "
                   onclick="showModalLogin()">
                    <div class="flex items-center m-4">
                        <i class="fa-solid mr-2 fa-right-to-bracket   !text-blue-600 dark:!text-gray-100"></i>
                        <p class="text-blue-600 dark:text-gray-100 underline">Ya tienes una cuenta?</p>
                    </div>
                </a>
            </div>
        </div>
    </form>
</div>

