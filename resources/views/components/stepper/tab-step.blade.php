@props([
    'slot'=>null,
    'index'=>0
])
<div x-show="step === {{$index}}"
     x-transition:enter="transition -translate  duration-1000"
     x-transition:enter-start="opacity-0 transform translate-x-2"
     x-transition:enter-end="opacity-100 transform translate-y-0"

     x-cloak
   >
    {{$slot}}
</div>

