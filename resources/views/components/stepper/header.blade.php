@props([
    'slot'=>null
])
<div  {{$attributes->merge(['class'=>'relative bg-gray-200 p-1 sticky dark:bg-gradient-to-br dark:from-gray-900 dark:via-gray-700 dark:border-b dark:to-gray-900 dark:border-b-gray-600'])}}>
    <div class="uppercase tracking-wide text-xs font-bold text-gray-500 mb-1 leading-tight  dark:text-gray-100"
         x-text="`Step: ${step} of {{$numberTab}}`"
         x-show="step !== 'complete'">
    </div>
    <div class="flex md:items-center justify-between">

            {{$slot}}
            <div class="w-1/2 flex space-x-1 " style="align-items: baseline;"
                 x-show="step !== 'complete'">

                <div class="relative w-full  bg-green-200 rounded-full h-2.5 mb-4 dark:bg-green-200 ">
                    <div class="bg-green-600 h-2.5 rounded-full dark:bg-green-500" :style="'width: '+ parseInt(step / {{$numberTab}} * 100) +'%; transition: width .5s;'"></div>
                </div>

                <div class="text-xs w-10 text-gray-600  dark:text-gray-100"
                     x-text="parseInt(step / {{$numberTab}} * 100) +'%'"></div>
        </div>
    </div>
</div>
