<div x-show="step === 'complete'" {{$attributes->merge(['class'=>'overflow-y-scroll'])}}
x-transition:enter="transition -translate  duration-600"
     x-transition:enter-start="opacity-0 transform translate-x-2"
     x-transition:enter-end="opacity-100 transform translate-y-0"
>
{{$slot}}
</div>
