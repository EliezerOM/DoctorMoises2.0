@props([
    'slot'=>null
])
<div {{$attributes->merge(['class'=>'fixed bottom-0 w-full bg-gray-200 py-2 dark:bg-gradient-to-br dark:from-gray-900 dark:via-gray-800 dark:border-t dark:to-gray-900 dark:border-t-gray-600'])}}
     x-show="step != 'complete'">
    <div class="max-w-3xl mx-auto px-4 ">
        <div class="flex justify-between">
            {{$slot}}
        </div>
    </div>
</div>
