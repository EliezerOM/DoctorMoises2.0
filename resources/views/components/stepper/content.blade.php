@props([
    'slot'=>null
])
<div {{$attributes->merge(['class'=>'h-full w-full dark:bg-gradient-to-t overflow-y-scroll dark:border-gray-700 dark:from-gray-700 dark:to-gray-800 p-2'])}}
     x-show="step != 'complete'"
     x-transition:enter="transition -translate  duration-1000"
     x-transition:enter-start="opacity-0 transform translate-x-2"
     x-transition:enter-end="opacity-100 transform translate-y-0">
    {{$slot}}
</div>
