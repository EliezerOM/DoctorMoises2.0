<!-- Implement the Back Top Top Button -->
<button id="to-top-button" onclick="goToTop()" title="Go To Top"
        data-turbolinks-permanent
        class="hidden fixed z-40  p-2 w-[40px] h-[40px] right-[10px] bottom-[10px] hover:bg-white hover:text-blue-600 bg-blue-600 text-white transition-all rounded-full drop-shadow-md  text-white  font-bold">
    <i class="fa fa-arrow-up"></i>
</button>
@push('scripts')
    <!-- Javascript code -->
    <script data-turbolinks-eval="false">
        let toTopButton = document.getElementById("to-top-button");
        // When the user scrolls down 200px from the top of the document, show the button
        window.onscroll = function () {
            if (document.body.scrollTop > 200 || document.documentElement.scrollTop > 200) {
                toTopButton.classList.remove("hidden");
            } else {
                toTopButton.classList.add("hidden");
            }
        }
        // When the user clicks on the button, scroll to the top of the document
        function goToTop() {
            window.scrollTo({ top: 0, behavior: 'smooth' });
        }
    </script>
@endpush
