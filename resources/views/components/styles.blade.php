<link rel="stylesheet" defer data-turbolinks-track="reload" href="{{asset('css/app.css')}}">
<script data-turbolinks-eval="false" defer type="text/javascript" src="{{asset('js/alpine.js')}}"></script>

<style defer data-turbolinks-track="reload">
    .turbolinks-progress-bar {
        height: 5px !important;
        position: absolute;
       z-index: 99;
        top:100px;
        background-color: red !important;
    }

    body::-webkit-scrollbar {
        width: 12px;               /* width of the entire scrollbar */
    }

    body::-webkit-scrollbar-track {
        background: white;        /* color of the tracking area */
    }

    body::-webkit-scrollbar-thumb {
        background-color: #233876;    /* color of the scroll thumb */
        border-radius: 20px;       /* roundness of the scroll thumb */
        border: 4px solid white;  /* creates padding around scroll thumb */
    }
</style>
{{--<script type="text/javascript" defer src="{{asset('js/turbolinks.js')}}"></script>--}}
@livewireStyles
@laravelPWA
@stack('styles')
