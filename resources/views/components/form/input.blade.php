@props(['text'=>'', 'name'=>'', 'xShow'=>true, 'id'=>uniqid()])
<div x-show="{{$xShow}}"
     x-transition:enter.duration.500ms
     x-transition:leave.duration.400ms>
    <div class="relative z-0 my-5 {{$classContent??''}}" >
        <input  {{ $attributes->merge(['type'=>'text', 'name'=>$id.'_name','required' => false, 'outofocus' => false]) }}
            style="box-shadow: none"
            id="{{$id}}"
                class="block
                py-2.5
                px-0
                w-full
                text-sm
                md:text-lg
                text-gray-900
                bg-transparent
                border-0
                border-b-2
                appearance-none
                dark:text-gray-100
                dark:border-gray-100
                dark:focus:!border-blue-300
                focus:outline-none
                focus:ring-0
                focus:border-blue-800
                peer
                {{$classInput??''}}"
                aria-describedby="{{$id}}_error"
                placeholder="  "/>
        <label for="{{$id}}"
               class="absolute
                      text-lg
                      md:text-base
                      md:text-lg
                      dark:!text-gray-400
                      text-gray-500
                      duration-300
                      transform
                      -translate-y-6
                      scale-100
                      top-3
                      font-semibold
                      -z-10
                      origin-[0]
                      peer-focus:left-0
                      peer-focus:!text-blue-700
                      dark:peer-focus:!text-blue-400
                      peer-placeholder-shown:scale-100
                      peer-placeholder-shown:translate-y-0
                      peer-focus:scale-100
                      {{$classLabel??''}}
                      peer-focus:-translate-y-6">{{$text}}</label>
    </div>
    @error($name)
    <p id="{{$id}}_error" class="mt-1 mb-2 text-xs text-red-600 dark:text-red-400"><span class="font-medium">{{ $message }}</p>
    @enderror
</div>

