<footer class=" bg-gradient-to-b from-blue-700 dark:from-gray-700 relative to-blue-900 dark:to-gray-800 {{!Route::is(listRoute())?'invisible':'' }}">
    <div class="w-full absolute bg-[length:90%] left-0 right-0 top-0 bottom-0 md:bg-[length:19%]  " style="
        background-image: url('{{asset('img/main/logo_tipo.png')}}');
     background-repeat: no-repeat;
      background-origin: content-box;
             background-position: center center;
          opacity: 0.1;

      "></div>
    <div class="md:flex  bg-blue-900 dark:bg-gray-700 dark:border-gray-900 dark:border dark:shadow dark:shadow-white dark:shadow-2xl{{Route::is(listRoute()) ? 'md:justify-between' : 'md:justify-end'}} ">

        @guest
            <livewire:contact-us/>
        @endguest

        <div class="grid z-10 p-4 grid-cols-2 gap-8 sm:gap-6 sm:grid-cols-3">
            <div>
                <h2 class="mb-6 text-sm font-semibold text-gray-200 uppercase ">Informaciones</h2>
                <ul class="text-gray-600 text-gray-400">
                    <li class="mb-4">
                        <div class="hover:underline text-white cursor-pointer">
                            <i class="fa-solid fa-phone"></i>
                            +1 829-629-1029
                        </div>
                    </li>
                    <li class="mb-4">
                        <div class="hover:underline text-white cursor-pointer">
                            <i class="fa-solid fa-envelope"></i>
                            info@doctormoises.com
                        </div>
                    </li>
                    <li>
                        <p class="hover:underline text-white cursor-pointer">
                            <i class="fa-solid fa-location-dot"></i>
                            Santiago de los Caballeros, Republica Dominicana
                        </p>
                    </li>
                </ul>
            </div>
            @guest
                <div>
                    <h2 class="mb-6 text-sm font-semibold text-gray-200 uppercase">Accesos</h2>
                    <ul class="text-gray-600 text-gray-400">
                        <li class="mb-4">
                            <a href="{{route('register')}}" class="hover:underline text-white">Registrar Pacientes</a>
                        </li>
                        <li class="mb-4">
                            <a href="{{route('register', ['tipo_usuario'=>'D'])}}" class="hover:underline text-white">Registrar
                                Doctor</a>
                        </li>
                        <li>
                            <a  onclick="showModalLogin()" class="hover:underline text-white">Acceder</a>
                        </li>
                    </ul>
                </div>
            @endguest

            <div>
                <h2 class="mb-6 text-sm font-semibold text-gray-200 uppercase ">Legal</h2>
                <ul class="text-gray-600 text-gray-400">
                    <li class="mb-4">
                        <button type="button"  class="hover:underline text-white modal-trigger" onclick="M.Modal.getInstance(document.getElementById('modal_condiciones')).open();" >Términos &amp;
                            Condiciones
                        </button>

                    </li>
                    <li>
                        <button type="button" class="hover:underline text-white modal-trigger" onclick="M.Modal.getInstance(document.getElementById('modal_cultura')).open();">Cultura
                            Organizacional
                        </button>

                    </li>
                </ul>
            </div>
        </div>

    </div>

    <div class=" w-full p-8 bg-blue-grey-900 dark:bg-gray-900 border-t border-gray-600 ">

        <div class="flex  items-center {{Route::is(listRoute()) ? 'justify-between' : 'justify-end'}} ">
        <span class="text-sm text-gray-400 sm:text-center ">© 2022 <a href="{{route('home')}}"
                                                                      class="hover:underline text-white">Doctor Moises™</a>. Todos los derechos reservados.
        </span>
            <div class="flex p-4 pr-8 space-x-6 sm:justify-center sm:mt-0">
                <a href="#" class="text-gray-400 hover:text-gray-300 ">
                    <i class="fa-brands fa-facebook fa-2x"></i>
                </a>
                <a href="#" class="text-gray-400 hover:text-gray-300">
                    <i class="fa-brands fa-instagram fa-2x"></i>
                </a>
                <a href="#" class="text-gray-400 hover:text-gray-300">
                    <i class="fa-brands fa-twitter fa-2x"></i>
                </a>

            </div>
        </div>

    </div>




</footer>
