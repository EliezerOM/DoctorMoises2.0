@props([
    'slot'=>null,
    'defaultTab'=>1,
    'name'=>uniqid('stepper')
])
<div  {{$attributes->merge(['class'=>'h-full w-full '])}}
      x-data="app{{$name}}()" x-cloak>
    {{$slot}}
</div>
@pushonce('scripts')
    <script>
        function app{{$name}}() {
            return {
                step: {{$defaultTab}},
            }
        }
    </script>
@endpushonce
