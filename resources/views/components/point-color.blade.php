<span class="flex {{$size??'h-4 w-4'}} mr-2 relative items-center justify-center ">
    @isset($animate)
        <span
            class="absolute inline-flex {{$size??'h-4 w-4'}} {{$animate}} rounded-full {{ isset($color) ? $color: 'bg-gray-800' }} opacity-75"></span>
    @endisset
    <span class="relative inline-flex rounded-full h-3 w-3 {{ isset($color) ? $color: 'bg-gray-800' }} "></span>
</span>
