<div class="bg-white bg-gradient-to-r from-gray-100 dark:from-gray-800 dark:to-gray-700 to-gray-50 border dark:bg-gray-800 dark:border-gray-700 dark:shadow-gray-600 shadow py-1">
    <div class="card dark:text-gray-100 m-1 border bg-gradient-to-r from-gray-100 dark:from-gray-800 dark:to-gray-700 to-gray-50 border dark:bg-gray-800 dark:border-gray-700 dark:shadow-gray-600 shadow">
        <div class="card-title flex justify-center">
            <h2 class="font-bold">Misión</h2>
        </div>
        <div class="card-content dark:text-gray-100">
            <p>excelencia e innovación en el país llevando al mercado
                servicios que garanticen la gestión de manera efectiva conectando pacientes con doctores.</p>
        </div>
    </div>

    <div class="card dark:text-gray-100 m-1 border bg-gradient-to-r from-gray-100 dark:from-gray-800 dark:to-gray-700 to-gray-50 border dark:bg-gray-800 dark:border-gray-700 dark:shadow-gray-600 shadow">
        <div class="card-title flex justify-center">
            <h2 class="font-bold">Visión</h2>
        </div>
        <div class="card-content">
            <p>Ser la plataforma de salud reconocida por sus servicios; así como atenciones de
                alta calidad en el área.</p>
        </div>
    </div>

    <div class="card m-1 dark:text-gray-100 border bg-gradient-to-r from-gray-100 dark:from-gray-800 dark:to-gray-700 to-gray-50 border dark:bg-gray-800 dark:border-gray-700 dark:shadow-gray-600 shadow">
        <div class="card-title flex justify-center">
            <h2 class="font-bold">Valores</h2>
        </div>
        <div class="card-content">
            <ul>
                <li>Responsabilidad. Obrar con rectitud, en consecuencia con los deberes
                    y derechos como asociados, acorde al compromiso con la plataforma.
                </li>
                <br>

                    <li>Compromiso. Con los clientes de la empresa para brindarles un
                    servicio de excelente calidad.
                </li>
                <br>

                <li>
                    Profesionalismo. Tratamos de respetar cada derecho del paciente asi como del doctor cuidando la integridad de ambos.
                </li>
                <br>
                <li>
                    Honestidad. Realizar las operaciones con trasparencia y rectitud.
                </li>
            </ul>
        </div>
    </div>

</div>
