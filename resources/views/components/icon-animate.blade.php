<div style="{{'transform: scale('.$scale??0.5.')'}}">
    <template x-if="type_notify === 'error' || type_notify === 'danger' ">
        <div class="sa {{$classIcon??''}}">
            <div class="sa-error">
                <div class="sa-error-x">
                    <div class="sa-error-left"></div>
                    <div class="sa-error-right"></div>
                </div>
                <div class="sa-error-placeholder"></div>
                <div class="sa-error-fix"></div>
            </div>
        </div>
    </template>

    <template x-if="type_notify === 'warning' ">

        <div class="sa {{$classIcon??''}}">
            <div class="sa-warning">
                <div class="sa-warning-body"></div>
                <div class="sa-warning-dot"></div>
            </div>
        </div>
    </template>

    <template x-if="type_notify === 'info' ">

        <div class="sa {{$classIcon??''}}">
            <div class="sa-info">
                <div class="sa-info-body"></div>
                <div class="sa-info-dot"></div>
            </div>
        </div>
    </template>

    <template x-if="(type_notify??'success') === 'complete'">
        <div class="sa {{$classIcon??''}}">
            <div class="sa-complete">
                <div class="sa-complete-tip"></div>
                <div class="sa-complete-long"></div>
                <div class="sa-complete-placeholder"></div>
                <div class="sa-complete-fix"></div>
            </div>
        </div>
    </template>



    <template
        x-if="(type_notify??'success') === 'success' || (type_notify??'success') !== 'danger' && (type_notify??'success') !== 'error' && (type_notify??'success') !== 'warning' && (type_notify??'success') !== 'complete' && (type_notify??'info') !== 'info'">

        <div class="sa {{$classIcon??''}}">
            <div class="sa-success">
                <div class="sa-success-tip"></div>
                <div class="sa-success-long"></div>
                <div class="sa-success-placeholder"></div>
                <div class="sa-success-fix"></div>
            </div>
        </div>
    </template>


</div>
