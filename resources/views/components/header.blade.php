<nav
    class="{{Route::is(listRoute())? 'h-28 bg-gradient-to-b from-blue-700  dark:from-blue-800  dark:to-blue-900 to-blue-900 ' : 'md:h-20 h-28 dark:from-blue-800 dark:to-blue-900  md:ml-[15rem] w-auto bg-gradient-to-r from-blue-700 to-blue-900 shadow-none'}} rounded-b-xl relative  rounded-t-none">


    <div class="nav-wrapper flex justify-between items-center "
         id="_navabar"
    >

        <div
            class="bg-gradient-to-r from-blue-600 to-blue-900 dark:from-blue-800 dark:to-blue-900 opacity-60 md:hidden left-0 right-0 z-20 top-0 bottom-0 fixed  overflow-y-auto"
            id="overlay" x-show="overlay"
            @click.passive="overlay = false"
            x-transition:enter="transition ease-out duration-200"
            x-transition:enter-start="opacity-0 "
            x-transition:enter-end="opacity-60"
            x-transition:leave="transition ease-in duration-300"
            x-transition:leave-start="opacity-60 "
            x-transition:leave-end="opacity-0 "></div>


        <div class="flex items-center">
            <a class="brand-s {{!Route::is(listRoute()) ?'md:invisible':''}}"
               href="{{route('home')}}">
                <img src="{{asset('img/main/logo.png')}}"
                     class="object-fill md:h-24 h-16 m-2 md:w-60 w-36 " alt="Logo">
            </a>
            @if(Route::is(listRoute()))
                <ul class=" hide-on-med-and-down right">

                    <li>
                        <a class="flex items-center waves-effect waves-light " href="{{route('home')}}"
                           data-ripple-light="true">
                            <i class="fa-solid fa-home mr-1 text-base opacity-60 text-white"></i>
                            <p class="text-white  text-bold ">Inicio</p>
                        </a>
                    </li>

                    <li>
                        <a class="flex items-center waves-effect waves-light " href="{{route('home')}}"
                           data-ripple-light="true">
                            <i class="fas fa-info-circle mr-1 text-base opacity-60 text-white"></i>
                            <p class="text-white text-bold">Sobre Nosotros</p>
                        </a>
                    </li>
                    <li>
                        <a class="flex items-center waves-effect waves-light " href="{{route('home')}}"
                           data-ripple-light="true">
                            <i class="fas fa-calendar-check mr-1 text-base opacity-60 text-white"></i>
                            <p class="text-white text-bold">Reserva Una Cita</p>
                        </a>
                    </li>

                    <li><a class="flex items-center waves-effect waves-light " href="{{route('home')}}"
                           data-ripple-light="true">
                            <i class="fab fa-blogger mr-1 text-base opacity-60 text-white"></i>
                            <p class="text-white text-bold ">Blog</p>
                        </a>
                    </li>

                    <li>
                        <a class="flex items-center waves-effect waves-light " href="{{route('home')}}"
                           data-ripple-light="true">
                            <i class="fas fa-address-book mr-1 text-base opacity-60 text-white"></i>
                            <p class="text-white text-bold">Contactanos</p>
                        </a>
                    </li>

                    @auth
                        <li>
                            <a class="flex items-center waves-effect waves-light "
                               href="{{typeUser() === "pat" ?  route('patient.main') : route('doctor.main')}}"
                               data-ripple-light="true">
                                <i class="fas fa-user mr-1 text-base opacity-60 text-white"></i>
                                <p class="text-white text-bold">Ir a mi perfil</p>
                            </a>
                        </li>
                    @endauth
                </ul>
            @endif

        </div>
        <ul class="mx-2 " x-data="{dropdown: false}">

            <div class="flex justify-center items-center">
                <div class="flex justify-end w-fit items-center">
                    <button id="theme-toggle" type="button"
                            class="text-gray-800 dark:text-gray-200 hover:bg-gray-100 dark:hover:bg-gray-700 focus:outline-none focus:ring-4 focus:ring-gray-200 focus:bg-white dark:focus:ring-gray-900 dark:bg-gray-800 bg-gray-200 rounded-full text-sm p-2.5 mr-3">
                        <svg id="theme-toggle-dark-icon" class="hidden w-5 h-5" fill="currentColor" viewBox="0 0 20 20"
                             xmlns="http://www.w3.org/2000/svg">
                            <path d="M17.293 13.293A8 8 0 016.707 2.707a8.001 8.001 0 1010.586 10.586z"></path>
                        </svg>
                        <svg id="theme-toggle-light-icon" class="hidden w-5 h-5" fill="currentColor" viewBox="0 0 20 20"
                             xmlns="http://www.w3.org/2000/svg">
                            <path
                                d="M10 2a1 1 0 011 1v1a1 1 0 11-2 0V3a1 1 0 011-1zm4 8a4 4 0 11-8 0 4 4 0 018 0zm-.464 4.95l.707.707a1 1 0 001.414-1.414l-.707-.707a1 1 0 00-1.414 1.414zm2.12-10.607a1 1 0 010 1.414l-.706.707a1 1 0 11-1.414-1.414l.707-.707a1 1 0 011.414 0zM17 11a1 1 0 100-2h-1a1 1 0 100 2h1zm-7 4a1 1 0 011 1v1a1 1 0 11-2 0v-1a1 1 0 011-1zM5.05 6.464A1 1 0 106.465 5.05l-.708-.707a1 1 0 00-1.414 1.414l.707.707zm1.414 8.486l-.707.707a1 1 0 01-1.414-1.414l.707-.707a1 1 0 011.414 1.414zM4 11a1 1 0 100-2H3a1 1 0 000 2h1z"
                                fill-rule="evenodd" clip-rule="evenodd"></path>
                        </svg>
                    </button>
                </div>


                    @auth
                        <livewire:patient.component.notification></livewire:patient.component.notification>

                        <div class="rounded-2xl  md:flex hidden  cursor-pointer ring-4"
                             :class="dropdown? 'h-8 w-8 bg-sky-500':''"
                             x-transition
                        >
                            <img id="avatarButton"
                                 type="button"
                                 x-transition
                                 x-show="!dropdown"
                                 @click="dropdown = !dropdown"
                                 class="w-10 h-10 p-1 rounded-full cursor-pointer ring-4 bg-gray-200"
                                 src="{{file_exists(person()->foto_perfil)? person()->foto_perfil: asset('img/main'). (person()->sexo == 'M'? '/male.png': '/female.png')}}"
                                 alt="User dropdown">

                            <ul x-show="dropdown"
                                x-transition
                                @click.outside="dropdown = false"
                                class="w-max rounded-lg absolute  justify-center flex border grid  shadow-lg z-40 right-2 top-20 text-gray-900 bg-gray-50">
                                <li>
                                    <div class="flex justify-center">
                                        <div class="relative w-[280px]  text-center justify-center grid grid-cols-1">
                                            <div class="relative flex justify-center ">
                                                <div
                                                    class="bg-[url('https://i.pinimg.com/originals/f8/40/7b/f8407b58f9411a40213811ddcae5c0b6.jpg')] w-[368px] -mt-1 -z-10  absolute h-[70%] rounded-t-xl"></div>
                                                <x-img-profile class="w-28 cursor-default ring-4 m-8 bg-gray-200"
                                                               :photo="person()->foto_perfil" :gender="person()->sexo"/>
                                                <div class="!absolute -bottom-2 right-12">
                                                    <i class="waves-effect fa-solid !h-auto  !leading-none fa-camera -top-8 right-8 text-blue-600 hover:scale-125 duration-75 hover:border hover:rounded-xl hover:p-1 hover:bg-blue-600 hover:text-white "></i>
                                                </div>
                                            </div>
                                            <div class="  text-center ">
                                                <p om-capitalize
                                                   class="font-semibold !leading-[2]">{{person()->nombres.' '.person()->apellidos}}</p>
                                                <i class="text-sm !leading-none  !h-auto ">{{auth()->user()->email}}</i>
                                                <i class="text-sm !leading-none mt-1 !h-auto font-bold ">{{auth()->user()->name}}</i>
                                            </div>

                                            <div class="border rounded-full text-center p-1 m-2 waves-effect ">
                                                <a href="{{route('patient.configuration')}}"
                                                   class="text-gray-900 hover:bg-transparent  ">Configurar tu Cuenta</a>
                                            </div>
                                        </div>


                                    </div>
                                </li>
                                <li class="divider bg-gray-900" tabindex="-1"></li>

                                <li><a href="{{route('patient.main')}}" class="text-gray-900 ">Panel</a></li>
                                <li><a href="{{route('home')}}" class="text-gray-900 ">Ir a Principal</a></li>
                                <li class="divider bg-gray-900" tabindex="-1"></li>
                                <li><a href="/logout" data-turbolinks="false" class="text-gray-900 ">Cerrar Sesión</a>
                                </li>
                                <li class="divider bg-gray-900" tabindex="-1"></li>
                                <li>

                                    <div class="flex justify-between ">
                                        <a data-turbolinks="false"
                                           class="text-gray-900 !leading-10 waves-effect hover:bg-transparent"
                                           onclick="M.Modal.getInstance(document.getElementById('modal_condiciones')).open();">Términos
                                            y Condiciones</a>
                                        <a data-turbolinks="false"
                                           class="text-gray-900 !leading-10 waves-effect hover:bg-transparent"
                                           onclick="M.Modal.getInstance(document.getElementById('modal_cultura')).open();">Cultura
                                            Organizacional</a>
                                    </div>
                                </li>
                            </ul>
                        </div>

                    @else


                <li class="flex  md:flex hidden justify-start">
                    <a class="btn flex !mr-0 items-center hover:z-10 h-auto waves-effect waves-dark  nav-link border rounded-l-lg border-black border-opacity-10 shadow-xl  scale-100 hover:scale-110 hover:bg-white ease-in bg-blue-700  duration-100 group"
                       onclick="showModalLogin()"
                    >
                        <i class="fa-solid fa-right-to-bracket mr-2 text-base  text-white font-bold group-hover:text-blue-900"></i>
                        <p class="text-white text-bold  md:text-base text-xl font-bold group-hover:text-blue-900">
                            Acceder</p>
                    </a>
                    <a class="btn !ml-0  flex h-auto items-center hover:z-10 waves-effect waves-dark   rounded-r-lg border-black border-opacity-10  shadow-xl  scale-100 hover:scale-110 hover:bg-white  ease-in bg-blue-700  duration-100 group"
                       href="{{route('register')}}">
                        <i class="fa-solid fa-user-plus mr-2 text-base text-white group-hover:text-blue-900"></i>
                        <p class="text-white text-bold  md:text-base text-xl font-bold  group-hover:text-blue-900 ">
                            Registro</p>

                    </a>
                </li>

            @endguest
                <button id="mobile-menu-button "
                        data-turbolinks-permanent
                        @click="toggle"
                        class="z-20 ml-auto shadow-none rounded-full  btn w-[53px] h-[58px] h-auto mx-4  waves-light flex md:hidden  no-navigate"
                        type="button">

                    <i x-show="!overlay"
                       class="fa-solid  fa-bars  text-white animate__faster animate__animated animate__rubberBand "
                       style="--animate-duration: 0.4s;"></i>

                    <i x-show="overlay"
                       class="fa-solid fa-arrow-left  text-white animate__faster animate__animated animate__rubberBand"
                       style="--animate-duration: 0.4s;"></i>
                </button>
        </ul>
    </div>

    {{--    Overlay Nav--}}
    @if(!Route::is(listRoute()))
        <div
            class="absolute  bg-gradient-to-r from-blue-700 dark:from-blue-800 to-blue-900 dark:to-blue-900 shadow-xl h-[320px] rounded-b-[3.5rem] top-10 !-z-40  w-full">
            <div class="w-full absolute bg-[length:90%] md:bg-[length:19%]  !-z-40 " style="
                background-image: url('{{asset('img/main/logo_tipo.png')}}');
                background-repeat: no-repeat;
                background-origin: content-box;
                background-position: center center;
                opacity: 0.1;

                "></div>
        </div>
    @endif
</nav>





