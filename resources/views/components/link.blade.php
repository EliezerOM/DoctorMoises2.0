<a href="{{isset($route)? $route :'#'}}"
   @click.passive="overlay = false"
   {{ $attributes->merge(['class' => 'flex items-baseline dark:text-gray-100  p-2 md:text-base text-sm font-normal text-gray-900 rounded-lg transition duration-75 hover:bg-gray-200 waves-effect dark:hover:bg-gray-700  group']) }}>
    {{$slot}}
</a>
