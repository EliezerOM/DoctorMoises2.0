@props(['percent'=>0])
<div class="flex items-center justify-center w-fit h-fit overflow-hidden rounded-full shadow-xl"
     x-data="{ circumference: 50 * 2 * Math.PI }">
    <svg class="w-32 h-32" x-cloak aria-hidden="true">
        <circle
            class="text-gray-300 dark:text-gray-600"
            stroke-width="10"
            stroke="currentColor"
            fill="transparent"
            r="50"
            cx="60"
            cy="60"
        />
        <circle
            class=" text-blue-600 dark:text-blue-500"
            stroke-width="10"
            :stroke-dasharray="circumference"
            :stroke-dashoffset="circumference - {{$percent}} / 100 * circumference"
            stroke-linecap="round"
            stroke="currentColor"
            fill="transparent"
            r="50"
            cx="60"
            cy="60"
        />
    </svg>
    <span class="absolute text-2xl text-blue-700 dark:text-blue-400">{{$percent}}%</span>
</div>
