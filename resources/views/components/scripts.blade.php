@livewireScripts
<script src="//cdn.jsdelivr.net/npm/sweetalert2@11"></script>
<x-livewire-alert::scripts />

<script src="{{ asset('vendor/livewire-alert/livewire-alert.js') }}"></script>
<x-livewire-alert::flash  />

<script type="text/javascript"  data-turbolinks-track="reload"  src="{{asset('js/app.js')}}"></script>
{{--<script src="https://cdn.jsdelivr.net/gh/livewire/turbolinks@v0.1.x/dist/livewire-turbolinks.js"--}}
{{--        data-turbolinks-eval="false"></script>--}}


<script data-turbolinks-track="reload">
    M.AutoInit();
 $('input[om-datepicker]').daterangepicker({
        autoUpdateInput: false,
        autoApply:true,
        ranges: {
            'Hoy': [moment(), moment()],
            'Ayer': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
            'Hace 7 Días': [moment().subtract(6, 'days'), moment()],
            'Hace 30 Días': [moment().subtract(29, 'days'), moment()],
            'Este Mes': [moment().startOf('month'), moment().endOf('month')],
            'Mes Pasado': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
        },
        locale: {
            cancelLabel: 'Limpiar',
            format:'DD/MM/YYYY',
        },
        applyButtonClasses:'bg-blue-700 waves-effect',
        cancelButtonClasses:'bg-red-700 waves-effect'
    });

        document.addEventListener('turbolinks:click', function(){
            // $('#main').addClass('animate__fadeOutRight');
            // $('#main').removeClass('animate__fadeInLeft');
            // $('#main').fadeOut('slow');
          console.log('click');

        })
    document.addEventListener('turbolinks:load', function(){
        // $('#main').addClass('animate__fadeInLeft');
        // $('#main').removeClass('animate__fadeOutRight');
        // $('#main').fadeIn('slow');
        console.log('load');

    })

        document.addEventListener('alpine:init', () => {

            Alpine.data('options', () => ({
                overlay: Alpine.$persist(false),
                expanded : Alpine.$persist(false),
                expanded2 : Alpine.$persist(false),
                persistent: Alpine.$persist(false),
                isOverlay : false,
                toggle() {
                    this.overlay = !this.overlay;
                }
            }))
        })
        function showModalLogin (){
            M.Modal.init(document.getElementById('modal_login'), {onOpenEnd: function (){
                    $('#name').focus();
                }}).open();
        }
        // Format icon
        function iconFormat(icon) {
            const originalOption = icon.element;
            if (!icon.id) { return icon.text; }
            const $icon = "<i class='"+$(icon.element).data('icon')+"'></i>" + icon.text;
            return $icon;
        }

    function rangeDate(value){
        let dates = value.split(' ')
        let start = dates[0];
        let end = dates[2];
        let range = {
            start,
            end
        }
        if (start && end) {
            range.start = moment(dates[0],'DD/MM/YYYY').format('YYYY-MM-DD');
            range.end = moment(dates[2],'DD/MM/YYYY').format('YYYY-MM-DD');
        }
        else{
            range.start = null;
            range.end = null;
        }
        return range;
    }
    function sleep(ms) {
        return new Promise(resolve => setTimeout(resolve, ms));
    }

    // On page load or when changing themes, best to add inline in `head` to avoid FOUC
    if (localStorage.getItem('color-theme') === 'dark' || (!('color-theme' in localStorage) && window.matchMedia('(prefers-color-scheme: dark)').matches)) {
        document.documentElement.classList.add('dark');
    } else {
        document.documentElement.classList.remove('dark')
    }

    var themeToggleDarkIcon = document.getElementById('theme-toggle-dark-icon');
    var themeToggleLightIcon = document.getElementById('theme-toggle-light-icon');

    // Change the icons inside the button based on previous settings
    if (localStorage.getItem('color-theme') === 'dark' || (!('color-theme' in localStorage) && window.matchMedia('(prefers-color-scheme: dark)').matches)) {
        themeToggleLightIcon.classList.remove('hidden');
    } else {
        themeToggleDarkIcon.classList.remove('hidden');
    }

    var themeToggleBtn = document.getElementById('theme-toggle');

    themeToggleBtn.addEventListener('click', function() {

        // toggle icons inside button
        themeToggleDarkIcon.classList.toggle('hidden');
        themeToggleLightIcon.classList.toggle('hidden');

        // if set via local storage previously
        if (localStorage.getItem('color-theme')) {
            if (localStorage.getItem('color-theme') === 'light') {
                document.documentElement.classList.add('dark');
                localStorage.setItem('color-theme', 'dark');
            } else {
                document.documentElement.classList.remove('dark');
                localStorage.setItem('color-theme', 'light');
            }

            // if NOT set via local storage previously
        } else {
            if (document.documentElement.classList.contains('dark')) {
                document.documentElement.classList.remove('dark');
                localStorage.setItem('color-theme', 'light');
            } else {
                document.documentElement.classList.add('dark');
                localStorage.setItem('color-theme', 'dark');
            }
        }

    });

</script>

<!-- Scripts -->
@stack('scripts')
<!--End Scripts-->
