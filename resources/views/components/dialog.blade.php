@props([
    'slot'=>null,
    'footer'=>null,
    'title'=>null,
])
<!-- Dialogo -->
<div class="dialog ">
    <div class="dialog-overlay" dialog-close="true"></div>

    <div class="flex justify-center modal-dialog'">
        <div {{ $attributes->merge(['class'=>'!max-w-none w-auto dialog-content !mt-[5%] ']) }} >
            <div class="dialog-header !bg-blue-800 !mt-[-20px] !ml-[30px] !mr-[30px] rounded-full !shadow-xl " x-show="{{ empty($title)?'false': 'true' }}"
            style="border-top-left-radius: 9999px; border-top-right-radius: 9999px; padding: 0px">
                {{ $title }}

                <button type="button"
                        class="me-0  btn-om-danger rounded-full  border-0 shadow-none "
                        style="min-height: 37px;"
                        dialog-close="true"
                        aria-label="Close">
                    <i class="fa-solid fa-xmark text-gray-50"></i>
                </button>
            </div>
            <div class="dialog-body p-2">
                {{ $slot }}
                <div class="dialog-footer" x-show="{{ empty($footer) ? 'false': 'true'}}">
                   {{ $footer }}
                </div>
            </div>
        </div>
    </div>
</div>
<!-- Dialogo -->
