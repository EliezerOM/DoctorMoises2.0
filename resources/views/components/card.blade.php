<div {{ $attributes->merge(['class' => 'flex flex-col  rounded-lg bg-gradient-to-r from-gray-100 dark:from-gray-800 dark:to-gray-700 to-gray-50 border dark:bg-gray-800 dark:border-gray-700 dark:shadow-gray-600 shadow' ]) }}>
    {{ $slot }}
</div>
