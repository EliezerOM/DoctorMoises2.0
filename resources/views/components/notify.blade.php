<div x-data="{ groups: [] }"
     wire:ignore
     @alert-toast.window="

     $event.detail.forEach((x)=>{
          groups.push(x);
     });
     "
     :style=" window.matchMedia('screen and (max-width: 768px)').matches && {'justify-content': 'center', 'display': 'flex'}"
>
    <template x-for="(group, index_g) in groups" :key="'GROUP_'+index_g">

        <div class="fixed flex flex-col z-[99999]"
             :class="
                 _screen = window.matchMedia('screen and (max-width: 768px)').matches;
                if((group.position??'tc') === 'tr' && !_screen)
                {
                    return 'right-0 top-0';
                }
                else if((group.position??'tc') === 'tl' && !_screen )
                {
                    return 'left-0 top-0';
                }
                else if((group.position??'tc') === 'bl'  && !_screen)
                {
                    return 'left-0 bottom-0';
                }
                else if((group.position??'tc') === 'br'  && !_screen)
                {
                    return 'right-0 bottom-0';
                }
                else if((group.position??'tc') === 'bc' )
                {
                    if(_screen){
                    return ' bottom-0 ';
                    }
                    return 'left-[50%] bottom-0';
                }
                else
                {
                 if(_screen){
                    return ' top-0 ';
                    }
                    return 'left-[50%] top-0 ' ;
                }
            "
             :style="(((group.position??'tc') === 'tc' && !window.matchMedia('screen and (max-width: 768px)').matches) || ((group.position??'tc') === 'bc' && !window.matchMedia('screen and (max-width: 768px)').matches) ) && { transform:'translateX(-50%)' }"
        >

            <template x-for="(notify, index) in group.notifications" :key="'NOTIFY_'+index">
                <div x-data="
             {
             persist: notify.persist??false,
             duration : notify.duration??3000,
             open: false,
             isOverlayOpen: false,
             date_time: notify.time,
             delay: notify.delay ??200,
             position: notify.position??'tc',
             animate: notify.animate??'y',
             type_notify: notify.type??'success',
             listClass: '',
             listStyle: '',
             popNotify(state){
                if(state) setTimeout(()=> { this.open = true; if(notify.typeNotification ==='confirm')isOverlay = true; }, this.delay);
                 if(!this.persist)
                 {
                     setTimeout(()=> {
                     this.open = false;
                     if(notify.typeNotification ==='confirm'  && !isOverlayOpen)isOverlay = false;
                     setTimeout(()=> {
                      $parent.groups.forEach((x)=>{
                         if(x.notifications.length > 0)
                         {

                            x.notifications = x.notifications.filter((y) => { return  y.id !== notify.id; })
                           }
                      })}, this.duration);
                     }, this.duration + this.delay);


                         }
                         if(!state) {
                         setTimeout(()=> {
                         this.open = false;
                         if(notify.typeNotification ==='confirm'  && !isOverlayOpen)isOverlay = false;

                        setTimeout(()=> {
                            $parent.groups.forEach((x)=>{
                             if(x.notifications.length > 0)
                             {

                                x.notifications = x.notifications.filter((y) => { return  y.id !== notify.id; })
                               }
                          })}, this.duration);
                          }, 0 );

                         }
                      }
                    }"
                     x-init="
                     this.isOverlayOpen = isOverlay;
                      notify.time = Date.now();
             listClass = customListClasNotify(position, type_notify);
             listStyle = customListStyleNotify(position, animate);
             popNotify(true);"

                >
                    <div x-show="open"
                         @click.outside.stop="if(notify.outside)popNotify(false);  else  isOverlay = open && notify.typeNotification !== 'toast';"
                         @click.stop=""
                         @keydown.escape.window="popNotify(false);"
                         x-transition.duration.300ms
                         :class="listClass"
                         class="m-2 dark:bg-gray-700 max-w-xl dark:border-t-gray-600 dark:border-r-gray-600 dark:border-b-gray-600"
                         :style="listStyle"
                         role="alert">
                        <div class="flex flex-col ">
                            <div class="flex items-center"
                                 :class="notify.typeNotification === 'confirm'? 'flex-col':''">
                                <div class="inline-flex   flex-shrink-0 justify-center items-center w-10 m-3"
                                     :class="notify.typeNotification === 'confirm' ?'h-auto':'h-10'">
                                    <template x-if="notify.typeNotification === 'confirm'">
                                        <x-icon-animate :scale="1" class-icon="dark:bg-gray-700"/>
                                    </template>
                                    <template x-if="notify.typeNotification !== 'confirm'">
                                        <x-icon-animate :scale="0.5" class-icon="dark:bg-gray-700"/>
                                    </template>
                                </div>
                                <div class="flex items-center">

                                    <a href="#">
                                        <div class="ml-3 m-3 text-gray-500   font-semibold"
                                             :class="notify.typeNotification === 'confirm' ? 'text-xl' :'text-lg'"><p
                                                x-text="notify.message"
                                                class="text-justify wrapper dark:text-gray-100"></p>
                                        </div>
                                    </a>

                                    <button type="button"
                                            @click.prevent="popNotify(false);"
                                            x-show="notify.showButtonClose"
                                            class="ml-auto  -mx-1.5 -my-1.5 bg-white text-gray-400 hover:text-gray-900 rounded-lg  p-1.5 hover:bg-gray-100 inline-flex h-8 w-8 "
                                            aria-label="Close">
                                        <span class="sr-only">Close</span>
                                        <svg aria-hidden="true" class="w-5 h-5" fill="currentColor" viewBox="0 0 20 20"
                                             xmlns="http://www.w3.org/2000/svg">
                                            <path fill-rule="evenodd"
                                                  d="M4.293 4.293a1 1 0 011.414 0L10 8.586l4.293-4.293a1 1 0 111.414 1.414L11.414 10l4.293 4.293a1 1 0 01-1.414 1.414L10 11.414l-4.293 4.293a1 1 0 01-1.414-1.414L8.586 10 4.293 5.707a1 1 0 010-1.414z"
                                                  clip-rule="evenodd"></path>
                                        </svg>
                                    </button>
                                </div>

                            </div>
                            <template x-if="notify.isTextComment">


                                <div>
                                    <div class="relative z-0 my-5 {{$classContent??''}}">
                                        <input
                                            class="block placeholder-shown:text-gray-500 placeholder:italic py-2.5 px-0 w-full text-sm md:text-lg text-gray-900 bg-transparent border-0 border-b-2 appearance-none dark:text-gray-100 dark:border-gray-100 dark:focus:!border-blue-300 focus:outline-none focus:ring-0 focus:border-blue-800 peer placeholder:text-transparent"
                                            placeholder="Opcional"
                                            :name="notify.id"
                                            :id="notify.id"
                                            x-model="notify.textComment"/>
                                        <label :for="notify.id"
                                               class="absolute text-sm md:text-lg dark:!text-gray-400 text-gray-500 duration-300 transform -translate-y-6 scale-100 top-3 font-semibold -z-10 origin-[0] peer-focus:left-0 peer-focus:!text-blue-700 dark:peer-focus:!text-blue-400 peer-placeholder-shown:scale-100 peer-placeholder-shown:translate-y-0 peer-focus:scale-100   peer-focus:-translate-y-6"
                                               x-html="notify.textTitleComment">
                                        </label>
                                    </div>
                                    <div class="flex flex-wrap">
                                        <template x-for="(comment, index) in notify.tagsComment" :key="'TAG_'+index">
                                            <button
                                                class="bg-gradient-to-r from-blue-500 to-blue-800 p-1 cursor-pointer ring-1 shadow text-white font-semibold text-xs rounded-3xl dark:shadow-white m-1"
                                                @click="notify.textComment = comment" x-text="comment"></button>
                                        </template>

                                    </div>
                                </div>
                            </template>

                            <template x-if="notify.typeNotification !== 'confirm'">

                                <div class="flex justify-end space-x-1">

                                    <x-btn tipo-btn="primary"
                                           x-show="notify.showButtonAceptar"
                                           class="text-sm py-1"
                                           @click.prevent="if(notify.onAceptar)Livewire.emit(notify.onAceptar, notify); this.isOverlayOpen = false;  popNotify(false); "
                                           x-html="notify.textButtonAceptar">
                                    </x-btn>
                                    <x-btn tipo-btn="danger"
                                           class="text-sm py-1"
                                           x-show="notify.showButtonCancelar"
                                           @click.stop="if(notify.onCancelar)Livewire.emit(notify.onCancelar, notify); popNotify(false);"
                                           x-html="notify.textButtonCancelar">Cancelar
                                    </x-btn>
                                </div>
                            </template>


                            <template x-if="notify.typeNotification === 'confirm'">


                                <div class="flex justify-end space-x-1">
                                    <x-btn tipo-btn="primary"
                                           x-show="notify.showButtonAceptar"
                                           class="text-base py-1"
                                           @click.prevent="if(notify.onAceptar)Livewire.emit(notify.onAceptar, notify); this.isOverlayOpen = false; popNotify(false);  "
                                           x-html="notify.textButtonAceptar">
                                    </x-btn>
                                    <x-btn tipo-btn="danger"
                                           class="text-base py-1"
                                           x-show="notify.showButtonCancelar"
                                           @click.stop="if(notify.onCancelar)Livewire.emit(notify.onCancelar, notify); popNotify(false);"
                                           x-html="notify.textButtonCancelar">
                                    </x-btn>
                                </div>
                            </template>

                        </div>
                    </div>

                </div>
            </template>
        </div>
    </template>
</div>

@once
    @push('scripts')
        <script>
            function groupBy(list, keyGetter) {
                const map = new Map();
                list.forEach((item) => {
                    const key = keyGetter(item);
                    const collection = map.get(key);
                    if (!collection) {
                        map.set(key, [item]);
                    } else {
                        collection.push(item);
                    }
                });

                console.log(map)
                return map;
            }

            function groupArrayOfObjects(list, key) {
                return list.reduce(function (rv, x) {
                    (rv[x[key]] = rv[x[key]] || []).push(x);
                    console.log(rv)
                    return rv;
                }, {});
            };

            function customListStyleNotify(position, animate) {
                _screen = window.matchMedia('screen and (max-width: 768px)').matches;
                let listStyle = {};
                if (position === 'bc') //Bottom-Center
                {
                    listStyle = {transform: 'translateY(-2.5rem)'};
                } else if (position === 'tr' && animate === 'y' && !_screen) //Top-Right-Y
                {
                    listStyle = {transform: 'translateY(2.5rem)'};
                } else if (position === 'tr' && animate === 'x' && !_screen) //Top-Right-X
                {
                    listStyle = {transform: 'translateX(-2.5rem)'};
                } else if (position === 'tl' && animate === 'x' && !_screen) //Top-Left-X
                {
                    listStyle = {transform: 'translateX(2.5rem)'};
                } else if (position === 'tl' && animate === 'y' && !_screen) //Top-Left-Y
                {
                    listStyle = {transform: 'translateY(2.5rem)'};
                } else if (position === 'bl' && animate === 'y' && !_screen) //Bottom-Left-Y
                {
                    listStyle = {transform: 'translateY(-2.5rem)'};
                } else if (position === 'bl' && animate === 'x' && !_screen) //Bottom-Left-X
                {
                    listStyle = {transform: 'translateX(2.5rem)'};
                } else if (position === 'br' && animate === 'x' && !_screen) //Bottom-Left-X
                {
                    listStyle = {transform: 'translateX(-2.5rem)'};
                } else if (position === 'br' && animate === 'y' && !_screen) //Bottom-Left-X
                {
                    listStyle = {transform: 'translateY(-2.5rem)'};
                } else {
                    listStyle = {transform: 'translateY(2.5rem)'};
                }
                return listStyle;
            }

            function customListClasNotify(position, type_notify) {
                let listClass = '';

                if (type_notify === 'complete' || type_notify === 'info') {
                    listClass += ' border-l-blue-800 md:min-w-[280px] md:w-auto flex message-toast-om   border-grey-500 border-l-8 shadow-xl z-[9999] md:m-4 border items-center p-2 text-gray-800 bg-white rounded-lg shadow transition-all ';
                } else if (type_notify === 'warning') {
                    listClass += ' border-l-yellow-400 md:min-w-[280px] md:w-auto flex message-toast-om   border-grey-500 border-l-8 shadow-xl z-[9999] md:m-4 border items-center p-2 text-gray-800 bg-white rounded-lg shadow transition-all ';
                } else if (type_notify === 'error' || type_notify === 'danger') {
                    listClass += ' border-l-red-600 md:min-w-[280px] md:w-auto flex message-toast-om   border-grey-500 border-l-8 shadow-xl z-[9999] md:m-4 border items-center p-2 text-gray-800 bg-white rounded-lg shadow transition-all ';
                } else {
                    listClass += ' border-l-green-500 md:min-w-[280px] md:w-auto flex message-toast-om border-grey-500 border-l-8 items-center shadow-xl md:m-4 border  p-2 text-gray-800 bg-white rounded-lg shadow transition-all ';
                }

                return listClass;
            }

            function arrayRemove(arr, value) {

                return arr.filter(function (ele) {
                    return ele !== value;
                });
            }
        </script>

    @endpush
@endonce


