@guest
    <li>
        <a href="{{route('home')}}"
           @click.passive="overlay = false"
           class="flex items-baseline waves-effect waves-light  p-2 text-base font-normal text-gray-900 rounded-lg transition duration-75 hover:bg-gray-100 dark:hover:bg-gray-700 dark:text-white group">
            <i class="fa-solid fa-home fa-lg dark:text-gray-100  dark:group-hover:text-gray-300 flex-shrink-0 w-6 h-6 text-gray-500 transition duration-75  group-hover:text-gray-900"></i>
            <span class="ml-3 md:text-lg text-sm">Inicio</span>
        </a>
    </li>
    <li>
        <a href="#"
           @click.passive="overlay = false"
           class="flex items-baseline waves-effect waves-light  p-2 text-base font-normal text-gray-900 rounded-lg transition duration-75 hover:bg-gray-100 dark:hover:bg-gray-700 dark:text-white group">
            <i class="fa-solid fa-info-circle fa-lg dark:text-gray-100 dark:group-hover:text-gray-300 flex-shrink-0 w-6 h-6 text-gray-500 transition duration-75  group-hover:text-gray-900"></i>
            <span class="ml-3 md:text-lg text-sm">Sobre Nosotros</span>
        </a>
    </li>
    <li>
        <a href="#"
           @click.passive="overlay = false"
           class="flex items-baseline  waves-effect waves-light p-2 text-base font-normal text-gray-900 rounded-lg transition duration-75 hover:bg-gray-100 dark:hover:bg-gray-700 dark:text-white group">
            <i class="fas fa-calendar-check fa-lg  dark:text-gray-100 dark:group-hover:text-gray-300 flex-shrink-0 w-6 h-6 text-gray-500 transition duration-75  group-hover:text-gray-900"></i>
            <span class="ml-3 md:text-lg text-sm">Reserva Una Cita</span>
        </a>
    </li>

    <li>
        <a href="#"
           @click.passive="overlay = false"
           class="flex items-baseline waves-effect waves-light  p-2 text-base font-normal text-gray-900 rounded-lg transition duration-75 hover:bg-gray-100 dark:hover:bg-gray-700 dark:text-white group">
            <i class="fab fa-blogger fa-lg  dark:text-gray-100 dark:group-hover:text-gray-300 flex-shrink-0 w-6 h-6 text-gray-500 transition duration-75  group-hover:text-gray-900"></i>
            <span class="ml-3 md:text-lg text-sm">Blog</span>
        </a>
    </li>

    <li>
        <a href="{{route('patient.main')}}"
           @click.passive="overlay = false"
           class="flex items-baseline waves-effect waves-light p-2 text-base font-normal text-gray-900 rounded-lg transition duration-75 hover:bg-gray-100 dark:hover:bg-gray-700 dark:text-white group">
            <i class="fas fa-address-book fa-lg dark:text-gray-100 dark:group-hover:text-gray-300 flex-shrink-0 w-6 h-6 text-gray-500 transition duration-75  group-hover:text-gray-900"></i>
            <span class="ml-3 md:text-lg text-sm">Contactos</span>
        </a>
    </li>

@else


    <div x-show="expanded2" x-collapse>

    </div>

@endguest



