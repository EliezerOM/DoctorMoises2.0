@if ($paginator->hasPages())
    <nav class="bg-transparent shadow-lg md:flex h-auto py-2 justify-between px-2 items-center rounded border dark:border-gray-700">

        <div>
            <p class="text-base dark:text-gray-100 text-gray-700 leading-5">
                <span>Mostrando</span>
                <span class="font-medium">{{ $paginator->firstItem() }}</span>
                <span>a</span>
                <span class="font-medium">{{ $paginator->lastItem() }}</span>
                <span>de</span>
                <span class="font-medium">{{ $paginator->total() }}</span>
                <span>resultados</span>
            </p>
        </div>

        <ul class="inline-flex items-center -space-x-px dark:text-gray-700">

            {{-- Previous Page Link --}}
            @if ($paginator->onFirstPage())
                <li>
                    <button disabled
                            class="block focus:bg-transparent cursor-not-allowed  py-2 px-3 ml-0 leading-tight bg-gray-200 text-gray-500 bg-white dark:bg-gray-700 dark:border-gray-800 dark:text-gray-100 rounded-l-lg border border-gray-200 ">
                        <span class="sr-only">Previous</span>
                        <svg aria-hidden="true"
                             class="w-5 h-5"
                             fill="currentColor"
                             viewBox="0 0 20 20"
                             xmlns="http://www.w3.org/2000/svg">
                            <path fill-rule="evenodd"
                                  d="M12.707 5.293a1 1 0 010 1.414L9.414 10l3.293 3.293a1 1 0 01-1.414 1.414l-4-4a1 1 0 010-1.414l4-4a1 1 0 011.414 0z"
                                  clip-rule="evenodd"></path>
                        </svg>
                    </button>
                </li>
            @else
                <button wire:click="previousPage"
                        dusk="previousPage{{ $paginator->getPageName() == 'page' ? '' : '.' . $paginator->getPageName() }}.before"
                        wire:loading.attr="disabled"
                        rel="prev"
                        class="block focus:bg-transparent cursor-pointer py-2 waves-effect px-3 ml-0 leading-tight text-gray-500 bg-white rounded-l-lg border border-gray-300 dark:bg-gray-700 dark:border-gray-800 dark:text-gray-100">
                    <span class="sr-only">Previous</span>
                    <svg aria-hidden="true"
                         class="w-5 h-5"
                         fill="currentColor"
                         viewBox="0 0 20 20"
                         xmlns="http://www.w3.org/2000/svg">
                        <path fill-rule="evenodd"
                              d="M12.707 5.293a1 1 0 010 1.414L9.414 10l3.293 3.293a1 1 0 01-1.414 1.414l-4-4a1 1 0 010-1.414l4-4a1 1 0 011.414 0z"
                              clip-rule="evenodd"></path>
                    </svg>
                </button>

            @endif
            {{-- Previous Page Link --}}

            {{-- Pagination Elements --}}
            @foreach ($elements as $element)
                {{-- "Three Dots" Separator --}}
                @if (is_string($element))
                    <span aria-disabled="true">
                                    <span
                                        class="relative inline-flex items-center px-4 py-2 -ml-px text-sm font-medium text-gray-700 bg-white border border-gray-300 cursor-default leading-5 select-none">{{ $element }}</span>
                                </span>
                @endif

                {{-- Array Of Links --}}
                @if (is_array($element))
                    @foreach ($element as $page => $url)
                        {{--                            <span wire:key="paginator-{{ $paginator->getPageName() }}-{{ $this->numberOfPaginatorsRendered[$paginator->getPageName()] }}-page{{ $page }}">--}}
                        @if ($page == $paginator->currentPage())
                            <a desabled
                               class=" cursor-default py-2 px-3 ml-0 leading-tight focus:bg-blue-100 border border-blue-200 bg-blue-100 text-blue-800 z-10 dark:bg-gray-900 dark:text-gray-100 dark:border-gray-900 ">
                                {{$page}}
                            </a>

                        @else
                            <a wire:click="gotoPage({{ $page }}, '{{ $paginator->getPageName() }}')"
                               class=" inline-flex items-center px-4 py-2 -ml-px focus:bg-transparent dark:bg-gray-700 dark:text-gray-100 text-gray-700 waves-effect bg-white border dark:border-gray-900 border-gray-300 leading-5 cursor-pointer">
                                {{ $page }}
                            </a>
                        @endif
                        {{--                         </span>--}}
                    @endforeach
                @endif
            @endforeach
            {{-- Pagination Elements --}}


            {{-- Next  Page Link --}}
            @if ($paginator->hasMorePages())
                <li>
                    <button
                        class=" block focus:bg-transparent cursor-pointer py-2 px-3 ml-0 leading-tight text-gray-500 bg-white rounded-r-lg border dark:bg-gray-700 dark:border-gray-800 dark:text-gray-100  waves-effect border-gray-300 "
                        wire:click="nextPage"
                        dusk="nextPage{{ $paginator->getPageName() == 'page' ? '' : '.' . $paginator->getPageName() }}.before"
                        wire:loading.attr="disabled">
                        <span class="sr-only">Next</span>
                        <svg aria-hidden="true" class="w-5 h-5" fill="currentColor" viewBox="0 0 20 20"
                             xmlns="http://www.w3.org/2000/svg">
                            <path fill-rule="evenodd"
                                  d="M7.293 14.707a1 1 0 010-1.414L10.586 10 7.293 6.707a1 1 0 011.414-1.414l4 4a1 1 0 010 1.414l-4 4a1 1 0 01-1.414 0z"
                                  clip-rule="evenodd"></path>
                        </svg>
                    </button>
                </li>
            @else
                <button disabled rel="next"
                        class=" block focus:bg-transparent cursor-not-allowed py-2 px-3 ml-0 leading-tight text-gray-500 bg-white rounded-r-lg border border-gray-200 bg-gray-200 dark:bg-gray-700 dark:border-gray-800 dark:text-gray-100">
                    <span class="sr-only">Next</span>
                    <svg aria-hidden="true" class="w-5 h-5" fill="currentColor" viewBox="0 0 20 20"
                         xmlns="http://www.w3.org/2000/svg">
                        <path fill-rule="evenodd"
                              d="M7.293 14.707a1 1 0 010-1.414L10.586 10 7.293 6.707a1 1 0 011.414-1.414l4 4a1 1 0 010 1.414l-4 4a1 1 0 01-1.414 0z"
                              clip-rule="evenodd"></path>
                    </svg>
                </button>

            @endif
            {{-- Next  Page Link --}}

        </ul>
    </nav>
@endif
