<div {{$attributes->merge(['class'=>'min-w-[310px] bg-gray-200 border-t border-gray-300 shadow-xl rounded-b-lg p-1 w-fit mt-2'])}}>
{{$slot}}
</div>
