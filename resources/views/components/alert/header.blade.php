<div {{$attributes->merge(['class'=>'min-w-[310px] bg-gray-200 border-b border-gray-300 shadow-xl rounded-t-lg p-1 w-fit mb-2'])}}>
{{$slot}}
</div>
