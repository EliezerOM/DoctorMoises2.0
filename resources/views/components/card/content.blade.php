<div {{ $attributes->merge(['class' => 'p-2 h-full relative']) }}
>
{{$slot}}
</div>
