@props([
    'headerOutside' => true
])
<div {{$attributes->merge(['class' =>  ($headerOutside?'relative left-[50%] -translate-x-[50%] -mt-[3rem] mb-[2rem]':'').'  h-fit'])}}>
        {{ $slot }}
</div>
