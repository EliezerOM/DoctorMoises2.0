<div {{$attributes->merge(['class'=>'dark:bg-gray-700 dark:border-gray-700 w-full border-t border-gray-200 p-0.5'])}}>
    {{$slot}}
</div>
