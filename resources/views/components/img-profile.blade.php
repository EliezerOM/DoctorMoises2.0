<img {{ $attributes->merge(['class' => 'w-16 h-auto rounded-full border border-blue-700']) }} style="border-style: solid"
     src="{{ file_exists($photo) || isExternal($photo)? (isExternal($photo) ? $photo : asset($photo)): asset('/img/main'). ($gender??'M' ? (($gender??'M') === 'M'? '/male.png': '/female.png'):'/male.png')}}"
     alt="profile image">
