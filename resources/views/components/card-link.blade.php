<a href="{{isset($route)? $route: '#'}}"
   class="p-7 m-1 text-left bg-local bg-gray-400 bg-center bg-no-repeat bg-cover rounded-lg bg-blend-multiply hover:bg-blend-soft-light dark:hover:bg-blend-darken"
   style="background-image: url({{isset($imageBackground)? $imageBackground : ''}})">

    <div class="backdrop-blur-sm bg-gray/30 p-1  ">
        <p class="max-w-xl font-semibold  tracking-tight leading-tight text-white mb-2">
            {{isset($detail)?$detail:''}}
        </p>

        <button type="button"
                class="inline-flex items-center px-2.5 py-1.5 text-sm font-medium text-center text-white border border-white rounded-lg hover:bg-white hover:text-gray-900 focus:ring-4 focus:outline-none focus:ring-gray-700">
            {{isset($textButton)?$textButton:''}}
            <svg class="ml-1 -mr-1 w-4 h-4" fill="currentColor" viewBox="0 0 20 20" xmlns="http://www.w3.org/2000/svg">
                <path fill-rule="evenodd"
                      d="M12.293 5.293a1 1 0 011.414 0l4 4a1 1 0 010 1.414l-4 4a1 1 0 01-1.414-1.414L14.586 11H3a1 1 0 110-2h11.586l-2.293-2.293a1 1 0 010-1.414z"
                      clip-rule="evenodd"></path>
            </svg>
        </button>
    </div>
</a>
