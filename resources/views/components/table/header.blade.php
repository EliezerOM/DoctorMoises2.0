@props([
    'slot'=>null,
    'sortable' => null,
    'direction'=>null
])
<th {{$attributes->merge(['class'=>'cursor-pointer'])}}>
    @unless($sortable)
        {{$slot}}
    @else
        {{$slot}}
        @if($direction === 'asc')
            <i class="fa-solid fa-chevron-up"></i>
        @elseif ($direction === 'desc')
            <i class="fa-solid fa-chevron-down"></i>
        @endif
    @endunless
</th>
