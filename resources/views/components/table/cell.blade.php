@props([
    'slot'=>null
])
<td {{$attributes->merge(['class'=>''])}}>
    {{$slot}}
</td>
