@props([
    'slot'=>null,
    'classRow'=>null
])
<tbody {{$attributes->merge(['class'=>'dark:bg-gray-800'])}}>
{{$slot}}
</tbody>
