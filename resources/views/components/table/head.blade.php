<thead {{$attributes->merge(['class'=>'text-sm text-gray-700 uppercase bg-gray-50 dark:bg-gray-800 dark:text-gray-100 dark:border-gray-900 rounded-xl '.($showHeaders??true ? "": "!hidden md:!contents")])}}>
    <tr {{$classRow??''}}>
        {{$slot??''}}
    </tr>
</thead>
