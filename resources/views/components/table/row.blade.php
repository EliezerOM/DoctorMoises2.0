@props([
    'slot'=>null
])
<tr {{$attributes->merge(['class'=>'bg-white  cursor-pointer border-b md:border-r-none md:border-l-none border-r md:border-gray-200 dark:md:border-gray-500 border-r-blue-700  p-0 dark:bg-gray-700 hover:bg-gray-100 dark:hover:bg-gray-600'])}}
    style="width: -webkit-fill-available;">
    {{ $slot  }}
</tr>
