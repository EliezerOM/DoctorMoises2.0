@props(
    [
        'slot'=>null,
        'nameDialog' => null,
        'visiblyOverlay' => true,
        'clickOutside' => true,
]
)
<div
>
    @if($visiblyOverlay)
        <div wire:ignore
            class="h-full w-full bg-blue-700 fixed -left-[50%] w-[150%]  h-[150%] -top-[50%] z-[9999] opacity-30" <?php echo 'x-show="Alpine.store(\'modal_' . $nameDialog . '\').' . $nameDialog . '"'?>></div>
    @endif
    <div
        {{$attributes->merge(['class'=>'dark:bg-gray-800 bg-gray-100 z-[9999] transition-all w-[95%] md:w-fit absolute duration-300 shadow-lg dark:shadow-gray-500 border dark:border-gray-900 left-[50%] -translate-x-[50%] rounded-lg'])}}
        x-transition:enter-start="opacity-0 transform -translate-y-2 duration-700"
        x-transition:enter-end="opacity-100 transform"
        x-transition:leave="transition ease-in duration-300"
        x-transition:leave-end="opacity-0 transform -translate-y-2"

    <?php if ($nameDialog){
        echo 'x-data x-show="Alpine.store(\'modal_' . $nameDialog . '\').' . $nameDialog . '" x-init="Alpine.store(\'modal_' . $nameDialog . '\').' . $nameDialog . ' = false"';
    } ?>

    <?php if($clickOutside)
        echo '@click.outside="Alpine.store(\'modal_' . $nameDialog . '\').' . $nameDialog . ' = false"';
    ?>


    >
        {{$slot}}
    </div>
</div>

@push('scripts')
    <script>
        document.addEventListener('alpine:init', () => {
            Alpine.store('{!! 'modal_'.$nameDialog !!}', {
                {!! $nameDialog !!}: false,
                toggle() {
                    this.{!! $nameDialog !!} = !this.{!! $nameDialog !!};
                },
                off(){
                    this.{!! $nameDialog !!} = false;
                }
            })
        });
    </script>
@endpush
