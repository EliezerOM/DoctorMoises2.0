@props([
    'slot'=>null
])
<div {{$attributes->merge(['class'=>'dark:bg-gray-800 bg-gray-100'])}}>
    {{$slot}}
</div>
