@props([
    'slot'=>null,
    'nameDialog'=>null,
    'close' => false
])
<div <?php if($nameDialog) echo ' @click="Alpine.store(\'modal_'.$nameDialog.'\').'.$nameDialog.' = '.(!$close ?'true': 'false').'"' ?> >
    {{$slot}}
</div>
