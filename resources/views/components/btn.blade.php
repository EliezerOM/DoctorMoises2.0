<?php $type = '' ?>
@if(($tipoBtn??'info') === 'danger')
    <?php $type = 'border-pink-600 border !bg-red-600 text-white' ?>

@elseif(($tipoBtn??'info') === 'success')
    <?php $type = 'border-green-400 border text-white bg-gradient-to-br from-green-500 via-green-400 to-green-600' ?>

@elseif(($tipoBtn??'info') === 'warning')
    <?php $type = 'border-yellow-600 border !bg-yellow-600 text-white' ?>


@elseif(($tipoBtn??'info') === 'secondary')
    <?php $type = 'border-gray-600 border !bg-gray-600 text-white' ?>

@else
    <?php $type = 'border-blue-600 border !bg-blue-600 text-white' ?>
@endif
<button type="button"
    {{ $attributes->merge(['class'=>'waves-dark shadow active:ring-2 rounded waves-effect p-[7px] text-base h-fit '.$type]) }}>
    {{$slot}}
</button>
