<ul class="pt-4 mt-4 space-y-2 border-t border-gray-400 dark:border-gray-700">


    @auth
        <li>
            <a href="{{route('home')}}"
               type="button"
               @click.passive="overlay =false"
               data-turbolinks-permanent
               class="md:hidden flex no-navigate items-baseline p-2 text-base font-normal text-gray-900 rounded-lg transition  hover:bg-gray-100 dark:hover:bg-gray-700 dark:text-white group">
                <i class="fa-solid fa-home fa-lg no-navigate dark:text-gray-100  flex-shrink-0 w-6 h-6 text-gray-500 dark:group-hover:text-gray-300   group-hover:text-gray-900"></i>
                <span class="ml-3 no-navigate md:text-lg text-sm" sidebar-toggle-item>Doctor Moises</span>
            </a>
        </li>
        <x-cerrar-sesion class="w-full" />
    @else

        <li>
            <button type="button"
                    onclick="showModalLogin();"
                    @click.passive="overlay = false"
                    class="flex items-baseline waves-effect waves-light p-2 text-base font-normal text-gray-900 w-full rounded-lg transition duration-75 hover:bg-gray-100 dark:hover:bg-gray-700 dark:text-white group">
                <i class="fa-solid fa-right-to-bracket dark:text-gray-100 flex-shrink-0 w-6 h-6 text-gray-500 transition duration-75 dark:group-hover:text-gray-300   group-hover:text-gray-900"></i>
                <span class="ml-3 md:text-lg text-sm">Acceder</span>
            </button>

            <a href="{{route('register')}}"
               @click.passive="overlay = false"
               class="flex items-baseline waves-effect waves-light p-2 text-base font-normal text-gray-900 rounded-lg transition duration-75 hover:bg-gray-100 dark:hover:bg-gray-700 dark:text-white group ">
                <i class="fa-solid fa-user-plus fa-lg dark:text-gray-100  flex-shrink-0 w-6 h-6 text-gray-500 transition duration-75 dark:group-hover:text-gray-300   group-hover:text-gray-900"></i>
                <span class="ml-3 md:text-lg text-sm">Registro</span>
            </a>
        </li>
    @endguest
</ul>
