<div>
    <div id="modal_login" class="modal md:w-max shadow-none border-none transparent">
        <livewire:login/>
    </div>

    <div id="modal_condiciones" data-turbolinks-permanent class="modal bottom-sheet !max-h-[85%] dark:text-gray-100 bg-gradient-to-r from-gray-100 dark:from-gray-800 dark:to-gray-700 to-gray-50 border dark:bg-gray-800 dark:border-gray-700 dark:shadow-gray-600 shadow">

            <div class="modal-content ">
                <p class="center text-[2rem]  font-bold sticky top-0 z-20 w-full backdrop-blur bg-black/5 ">Términos y Condiciones</p>
                <x-terminos-condiciones/>
            </div>
    </div>


    <div  id="modal_cultura" data-turbolinks-permanent class="modal bottom-sheet !max-h-[85%] dark:text-gray-100 bg-gradient-to-r from-gray-100 dark:from-gray-800 dark:to-gray-700 to-gray-50 border dark:bg-gray-800 dark:border-gray-700 dark:shadow-gray-600 shadow">

            <div class="modal-content">
                <p class="center text-[2rem]  font-bold sticky top-0 z-20 w-full backdrop-blur bg-black/5 ">Cultura Organizacional</p>
                <x-cultura-organizacional/>
            </div>
    </div>
</div>
