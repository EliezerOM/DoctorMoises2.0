<div x-show="{{ $tabs??'tabs' }} === {{ $index??0 }}"
     x-transition
    {{$attributes->merge(['class'=>'transition-all '])}}>
    {{$slot}}
</div>
