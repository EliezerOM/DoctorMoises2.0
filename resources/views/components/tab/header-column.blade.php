    <li @click.stop="{{ $tabs??'tabs' }} = {{$index??0}}" {{$attributes->merge(['class'=>'relative cursor-pointer bg-gray-100 waves-effect flex items-center'])}}>
       @isset($icon)
        <i class="dark:text-gray-100 {{$icon??''}}"></i>
        @endisset
        <p class="m-2"
           :class="{{ $tabs??'tabs' }} === {{$index??0}} ? 'font-semibold {{$colorText??'text-blue-700 dark:text-gray-100'}}':''">{{$name??'Tab'}}</p>
        <span class="absolute bottom-0 z-10 h-[3px] w-full {{$colorSlide??'bg-blue-700 dark:bg-gray-100'}} " x-transition
              x-show="{{ $tabs??'tabs' }} === {{$index??0}}"></span>
    </li>
