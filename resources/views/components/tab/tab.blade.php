<div  {{$attributes->merge(['class'=>'h-full w-full overflow-y-scroll '])}}>
    {{$slot}}
</div>

