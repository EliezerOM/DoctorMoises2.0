<div {{$attributes->merge(['class'=>'h-full w-full dark:bg-gradient-to-t dark:border-gray-700 dark:from-gray-700 dark:to-gray-800'])}}>
    {{$slot}}
</div>
