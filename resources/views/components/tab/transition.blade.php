@props([
    'isOverlay'=>'$isOverlay',
    'slot' => null,
    'isSlide'=>'slideInfo',
    'placement' => 'right',
    'position'=>'right-0',
    'transition'=>'translate-x-full'
])
<?php
if($placement === 'left') {$position = 'left-0 '; $transition = '-translate-x-full';}
else {$placement = 'right-0'; $transition = 'translate-x-full'; }
?>
<div {{$attributes->merge(['class'=>'bg-white w-4/5 md:w-1/2 lg:w-1/4 fixed scroll-auto opacity-0 '.$position.' top-0 bottom-0   transition-all !z-[999]'])}}
      <?php if($isSlide)  echo 'x-on:click.outside="'.$isSlide.' = false; '.$isOverlay.' = false;"  :class="slideInfo? \'translate-x-2.5 opacity-100\': \' '.$transition.' opacity-50 transform duration-200\'"' ?>
     x-cloak>
    {{$slot}}
</div>
