@props(
    ['tipoBtn' =>'info']
)
<?php $type = '' ?>
@if($tipoBtn === 'danger' || $tipoBtn === 'error')
    <?php $type = '!bg-red-200 border-pink-600 border text-red-600' ?>

@elseif($tipoBtn === 'success')
    <?php $type = '!bg-green-200 border-green-600 border text-green-600' ?>

@elseif($tipoBtn === 'info')
    <?php $type = '!bg-blue-200 border-blue-600 border text-blue-600' ?>

@elseif($tipoBtn === 'warning')
    <?php $type = '!bg-yellow-200 border-yellow-600 border text-yellow-600' ?>

@endif
<button {{ $attributes->merge(['class'=>'px-2 py-1 waves-dark rounded waves-effect text-lg  md:text-base '.$type]) }}>
    {{$slot}}
</button>
