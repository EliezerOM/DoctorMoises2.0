                <li>
                    <x-link :route="route('patient.main')"
                    class="{{ Route::is('patient.main') ?'bg-gradient-to-r from-sky-400 to-sky-600 ':''}}  rounded-r-xl mr-2 rounded-none" >
                        <i class="fa-solid dark:group-hover:text-gray-300 fa-chart-pie fa-lg {{ Route::is('patient.main') ? 'text-white' : 'text-gray-500'}} flex-shrink-0 w-6 h-6 dark:text-gray-100"></i>
                        <span class="ml-3 font-semibold dark:text-gray-100 md:text-lg text-sm {{ Route::is('patient.main') ? 'text-white' : 'text-gray-500'}}">Panel</span>
                    </x-link>

                </li>

                <li>
                    <x-link :route="route('patient.personal-schedule')"
                            class="{{ Route::is('patient.personal-schedule') ?'bg-gradient-to-r from-sky-400 to-sky-600 ':''}}  rounded-r-xl mr-2 rounded-none">
                        <i class="fa-solid dark:group-hover:text-gray-300 fa-book fa-lg dark:text-gray-100 {{ Route::is('patient.personal-schedule') ? 'text-white' : 'text-gray-500'}} flex-shrink-0 w-6 h-6"></i>
                        <span class="ml-3 font-semibold dark:text-gray-100 md:text-lg text-sm {{ Route::is('patient.personal-schedule') ? 'text-white' : 'text-gray-500'}}">Agenda Personal</span>
                    </x-link>
                </li>



                <li>
                    <div class="space-y-2">
                        <a href="{{route('patient.configuration')}}"
                           @click.passive="expanded = !expanded;"
                           type="button"
                           data-turbolinks-permanent
                           class=" flex  items-baseline p-2 rounded-r-xl mr-2 rounded-none text-lg font-semibold  {{ Route::is(['patient.configuration','patient.personal-information','patient.security-privacy', 'patient.subscripcion-payments' ]) ?'bg-gradient-to-r from-sky-400 to-sky-600 ':''}}">
                            <i class="fa-solid dark:group-hover:text-gray-300 fa-user-doctor dark:text-gray-100 fa-lg no-navigate {{ Route::is(['patient.configuration','patient.personal-information','patient.security-privacy', 'patient.subscripcion-payments' ]) ? 'text-white' : 'text-gray-500'}}  flex-shrink-0 w-6 h-6 "></i>
                            <span class="ml-3 font-semibold dark:text-gray-100 md:text-lg text-sm {{ Route::is(['patient.configuration','patient.personal-information','patient.security-privacy', 'patient.subscripcion-payments' ]) ? 'text-white' : 'text-gray-500'}}" sidebar-toggle-item>Administrar Cuenta</span>

                        </a>
                        <div x-show="expanded" x-collapse>
                            <ul class="pr-2 space-y-2 ">
                                <li>
                                    <x-link :route="route('patient.personal-information')" class="dark:text-gray-100 p-2 pl-11 w-full rounded-none rounded-r-xl  {{ Route::is('patient.personal-information') ?'bg-gradient-to-r from-sky-300 to-sky-400 ':''}}">
                                        <span class=" font-semibold dark:text-gray-100 text-sm {{ Route::is('patient.personal-information') ? 'text-white' : 'text-gray-500'}}">Configuración</span>
                                    </x-link>
                                </li>
                                <li>
                                    <x-link :route="route('patient.personal-information')" class="dark:text-gray-100 p-2 pl-11 w-full rounded-none rounded-r-xl  {{ Route::is('patient.personal-information') ?'bg-gradient-to-r from-sky-300 to-sky-400 ':''}}">
                                        <span class=" font-semibold dark:text-gray-100 text-sm {{ Route::is('patient.personal-information') ? 'text-white' : 'text-gray-500'}}">Información Personal</span>
                                    </x-link>
                                </li>
                                <li>
                                    <x-link :route="route('patient.security-privacy')" class=" p-2 pl-11 w-full rounded-none rounded-r-xl  {{ Route::is('patient.security-privacy') ?'bg-gradient-to-r from-sky-300 to-sky-400 ':''}}">
                                        <span class=" font-semibold dark:text-gray-100 text-sm {{ Route::is('patient.security-privacy') ? 'text-white' : 'text-gray-500'}}">Seguridad & Privacidad</span>
                                    </x-link>
                                </li>

                                <li>
                                    <x-link :route="route('patient.subscripcion-payments')" class=" p-2 pl-11 w-full rounded-none rounded-r-xl  {{ Route::is('patient.subscripcion-payments') ?'bg-gradient-to-r from-sky-300 to-sky-400 ':''}}">
                                        <span class=" font-semibold dark:text-gray-100 text-sm {{ Route::is('patient.subscripcion-payments') ? 'text-white' : 'text-gray-500'}}"> Suscripción & Pagos</span>
                                    </x-link>
                                </li>
                            </ul>
                        </div>
                    </div>
                </li>
