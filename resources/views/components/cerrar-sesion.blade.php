<a href="/logout" data-turbolinks="false"
   @click.passive="overlay = false"
   onclick="Livewire.restart()"
   {{$attributes->merge(['class'=>'flex items-baseline p-2 md:text-base text-sm font-normal text-gray-900 rounded-lg transition duration-75 hover:bg-gray-100 dark:hover:bg-gray-700 dark:text-white group'])}}>
    <i class="fa-solid fa-circle-xmark dark:text-gray-100 fa-lg flex-shrink-0 w-6 h-6 text-gray-500 transition duration-75  group-hover:text-gray-900 dark:group-hover:text-gray-100"></i>
    <span class="ml-3 dark:text-gray-100 text-gray-800">Cerrar Sesión</span>
</a>
