<div>
    <div class="grid justify-center w-full  ">
        <div
            class="rounded-full animate-pulse  ring-4 ring-gray-500 flex m-2 mt-4 shadow-xl justify-center items-center h-[12em] w-[12em]  bg-gray-300">
            <i class="fa-solid animate-pulse fa-9x fa-2x text-gray-300 fa-calendar-check"></i>
        </div>
    </div>
    <div class=" bg-gray-300 animate-pulse w-full mt-1 h-5 rounded-lg"></div>
    <div class=" bg-gray-300 animate-pulse w-full mt-1 h-10 rounded-lg"></div>
    <div class=" bg-gray-300 animate-pulse w-full mt-1 h-10 rounded-lg"></div>
    <div class=" bg-gray-300 animate-pulse w-full mt-1 h-5 rounded-lg"></div>
    <div class=" bg-gray-300 animate-pulse w-full mt-1 h-10 rounded-lg"></div>
    <div class=" bg-gray-300 animate-pulse w-full mt-1 h-screen rounded-lg"></div>
</div>
