<div>

    <div class="relative m-0 mb-10  w-[115%]"
         :class="tabs2 !== 0 && !slideInfo ? 'invisible': '' ">

        <div class="bg-gray-300 animate-pulse h-[18rem] w-full shadow-lg rounded-r rounded-l-lg -ml-16"></div>
        <div class="w-[30%] h-[51%] bg-gray-300 shadow-xl rounded-full animate-pulse ml-2 bg-white absolute -bottom-10 ring-2"></div>

    </div>
    <div class="mx-2 mt-14">
        <div class=" bg-gray-300 animate-pulse w-full mt-1 h-[20rem] rounded-lg"></div>
    </div>
</div>

