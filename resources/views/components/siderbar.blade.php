<div x-data="{ expanded : $persist(false) }">
    {{--    md:relative md:grid md:h-full--}}
    <div id="_siderbar"
         class="sidebar  fixed w-64 top-0 bottom-0 z-20  visible {{!Route::is(listRoute())? 'md:translate-x-0 ' : 'md:hidden '}} transition duration-100 ease-in-out"
         :class="overlay? '':'-translate-x-full'"
         aria-label="Sidebar">
        <div
            class="overflow-y-auto h-full  bg-gradient-to-l from-gray-50 to-gray-200 dark:border-2  shadow-2xl border-gray-400 dark:border-gray-800 dark:border-opacity-5 border-opacity-20  dark:from-gray-800 dark:to-gray-700">
            <ul class="space-y-1">
                <li class="h-40 mb-6  relative">
                    <div
                        class="bg-gradient-to-l from-blue-700 to-blue-900 w-full h-full absolute -z-0  shadow-xl"></div>
                    <a href="{{route('home')}}">
                        <img src="{{asset('img/main/logo.png')}}"
                             class="object-fill h-28 shadow-xl w-60 absolute" alt="Logo">
                    </a>
                    @if(auth()->check())
                        <x-img-profile :photo="person()->foto_perfil"
                                       :gender="person()->sexo"
                                       class=" w-16 bg-gray-100 ml-2 ring-2 -bottom-6 absolute shadow-xl"/>
                    @endif
                </li>

                <!-- Menu Patient -->
                @if(typeUser() === 'pat' && auth()->check())
                    <x-main-patient/>
                @endif
            <!-- Menu Patient -->

                <!-- Menu Doctor -->
                @if(typeUser() === 'doc' && auth()->check())
                    <x-main-doctor/>
            @endif
            <!-- Menu Doctor -->


                <x-main-moises/>
            </ul>

            <x-login-register/>

        </div>

    </div>


</div>

