<div >
    <div class="card bg-gradient-to-r from-gray-100 dark:from-gray-800 dark:to-gray-700 to-gray-50 border dark:bg-gray-800 dark:border-gray-700 dark:shadow-gray-600 shadow">

        <div class="card-content">

            <p class="dark:text-gray-100">Queremos asegurarnos de que comprendas la forma en que se maneja la información dentro de la Plataforma
                Doctor Moisés.</p>
            <br/>
            <h6 class="dark:text-gray-100">1. DEFINICIONES: </h6>
            <p class="text-sm dark:text-gray-100 dark:text-gray-100">1.1 CLIENTE: Es la persona física o jurídica que utiliza la plataforma o la aplicación
                con el
                propósito
                de contactar a un proveedor o administrador de servicios de salud, (más delante de forma específica EL
                PROVEEDOR); es decir, aquel o aquella que accede al sitio web WWW.DOCTORMOISES.COM o descarga la
                aplicación DOCTORMOISES (la APP) para móvil.</p>    <br/>

            <p class="text-sm dark:text-gray-100 dark:text-gray-100">1.2 PLATAFORMA: Es la aplicación móvil y su sitio web DOCTORMOISES que pone a disposición
                del
                cliente y
                del proveedor, la posibilidad de identificar un producto o servicio de la lista de proveedores que
                publican en el portal o en la APP, de modo que pueda o no mostrar interés en contratar con éste los
                servicios o productos de que se trate.</p><br/>
            <p class="text-sm dark:text-gray-100 dark:text-gray-100"> 1.2.1 La referencia a “la plataforma” incluye el sitio de internet DOCTORMOISES.COM.DO,
                y
                las
                aplicaciones DOCTORMOISES y DOCTORMOISES PARTNER; la sociedad comercial DOCTORMOISES, S.A.S., en virtud
                de la cual opera. Asimismo, queda formalmente entendido que las aplicaciones y la plataforma no son más
                que una tecnología para permitir que los clientes de servicios de salud se pongan en contacto de manera
                eficiente y fácil con proveedores y administradores de servicios de salud.</p><br/>

            <p class="text-sm dark:text-gray-100 dark:text-gray-100"> 1.2.2 La aplicación DOCTORMOISES está diseñada para el paciente o cliente y la
                aplicación
                DOCTORMOISES
                PARTNER está diseñada para los proveedores de servicios. En todo caso, independientemente de las
                funciones
                que aplican para uno y otro, ambas están sujetas a estos términos y condiciones generales; y de manera
                particular, cada una tiene un acápite con sus especificaciones sobre uso y funciones.</p><br/>

            <p class="text-sm dark:text-gray-100 dark:text-gray-100"> 1.3 BUENA FE: Es la disposición general de las personas de que sus actuaciones se hacen
                con
                un fin
                legítimo,
                sin la más mínima intención de causar daño o perjuicio alguno; ni sacar ventajas desproporcionadas o
                ilegítimas.</p><br/>

            <p class="text-sm dark:text-gray-100"> 1.4 ESPECIFICIDAD: Queda expresamente entendido que la plataforma y la sociedad a través
                de
                la cual
                opera,
                no se comprometen más allá que las obligaciones contenidas en este acuerdo y el alcance de sus servicios
                son
                hechos en el contexto preciso de estos términos, sin que medien otras obligaciones que no hayan sido
                previamente convenidas.</p><br/>

            <p class="text-sm dark:text-gray-100"> 1.5 PROVEEDOR: Es el profesional de la salud, centro de salud, gestor de servicios de
                salud
                o
                administrador
                de servicios de salud que publica sus servicios o insumos en la plataforma, de modo que los usuarios
                puedan
                utilizar los medios de la plataforma para contactarlo y realizar las funciones que permite la App.</p>
            <br/>

            <p class="text-sm dark:text-gray-100"> 1.6. USUARIO: Es el que utiliza la aplicación y la plataforma, ya sea como cliente o
                como
                proveedor. Las
                personas físicas que sean usuarios deberán ser siempre mayores de edad, es decir, mayor de 18 años.
            </p>
            <br/>
            <h6 class="dark:text-gray-100">2. ACEPTACIÓN DE ESTOS TÉRMINOS POR EL USO DE LA PLATAFORMA O LA APP. </h6>

            <p class="text-sm dark:text-gray-100">2.1 Se reconoce para los usuarios la aceptación de estos términos haciendo clic al final
                del
                documento en la
                sección de “aceptación” que ha leído íntegramente este documento y lo ha comprendido y entendido, así
                como
                aceptado las reglas y deberes indicadas en este.</p><br/>

            <p class="text-sm dark:text-gray-100">2.2 La creación de la cuenta del usuario en el APP es la prueba fehaciente de que el
                usuario
                ha entendido y
                aceptado todos los términos y condiciones establecidos aquí.</p><br/>

            <p class="text-sm dark:text-gray-100">2.2.1 La plataforma no tiene ninguna obligación o deber más allá del indicado en estos
                términos y, por lo
                tanto, ninguna imputación del usuario como deber atribuible a la plataforma es válida o tendrá efecto,
                salvo
                que se demuestre fehacientemente que ésta, a través de los órganos de dirección y control de la sociedad
                DOCTORMOISES, S.A.S, los ha aceptado de manera previa, por escrito.</p><br/>

            <p class="text-sm dark:text-gray-100">2.3. La plataforma se reserva el derecho a modificar los presentes Términos en cualquier
                momento con arreglo
                a esta disposición. Con relación a lo anterior, la APP en la búsqueda del mejor servicio al cliente se
                compromete a realizar las siguientes tareas:</p><br/>

            <ul>
                <li class="dark:text-gray-100">2.3.1 Actualizar las publicaciones en la plataforma digital con las
                    modificaciones realizadas a los
                    presentes Términos.
                </li>
                <br>

                <li class="text-sm dark:text-gray-100">2.3.2 Avisar - notificar a los usuarios suscritos en la plataforma dichas
                    modificaciones mediante correo
                    electrónico, como mínimo, cinco (5) días antes de que estas entren en vigor.
                </li>
                <br>

                <li class="text-sm dark:text-gray-100">2.3.3 Informar del derecho a rescindir o terminar el Contrato en el contenido
                    del correo electrónico de
                    notificación. En el caso en desacuerdo con los Términos modificados el usuario podrá rescindir el
                    presente
                    Contrato de manera inmediata y explícita.
                </li>
                <br>
                <li class="text-sm dark:text-gray-100">
                    2.3.4 Renovar de manera automática el acceso o utilización de la plataforma en caso de que el
                    usuario no
                    exprese de forma explícita dar por terminado su contrato antes de la fecha en que entren en vigor
                    los
                    términos modificados, lo cual se entenderá como la aceptación de los términos actualizados.
                </li>
                <br>
            </ul>
            <br/>
            <br/>

            <h6 class="dark:text-gray-100">3. DECLARACIÓN DE AUTORIDAD Y LIBRE CONSENTIMIENTO.</h6>
            <p class="text-sm dark:text-gray-100">3.1 El usuario reconoce que es mayor de edad y, por tanto, con capacidad legal para
                suscribir
                los presentes
                términos y condiciones, sea directamente o en representación de la persona jurídica que declara
                representar.</p><br/>

            <p class="text-sm dark:text-gray-100">3.2 Reconoce que está en pleno uso de sus facultades mentales y que no ha sido
                coaccionado de
                ninguna manera
                para acceder y aceptar las condiciones y términos requeridos para ingresar a la plataforma.</p><br/>

            <p class="text-sm dark:text-gray-100">3.3 Si el usuario es una persona moral, entonces quien solicita los servicios reconoce y
                declara que tiene la
                capacidad legal para representar a dicha compañía o sociedad y está habilitado por los órganos de
                gestión
                que
                tienen el poder de dirección y administración empresarial.</p>
            <br/>

            <h6 class="dark:text-gray-100">4. CREACIÓN DE LA CUENTA. </h6>
            <p class="text-sm dark:text-gray-100">4.1 Cada usuario (persona o empresa) deberá registrar una cuenta ("Cuenta de la APP")
                para
                acceder a la
                plataforma, no podrá registrar más de una, ni cederla, ni transferirla, la cual llenará con su
                información
                básica que se detalla en el formulario denominado FORMULARIO DE REGISTRO O AFILIACIÓN, disponible en la
                sección
                “sign up” o “regístrate”.</p><br/>

            <p class="text-sm dark:text-gray-100">4.2 El perfil del proveedor, una vez aprobado por el sistema, le permitirá publicar la
                oferta
                de sus bienes y
                servicios para el público en general.</p><br/>

            <p class="text-sm dark:text-gray-100">4.3 Las sociedades o personas jurídicas también podrán registrar un perfil donde deberá
                declarar y garantizar
                que cuenta con la potestad para vincular legalmente a dicha entidad. En estos casos se reconoce que, en
                apariencia de buen derecho, quien registre tiene las facultades para realizarlo; el inicio de la
                provisión
                de
                servicios o de venta de productos o bienes a los usuarios constituye esa prueba de la apariencia. En
                esos
                casos,
                la plataforma exigirá que se entreguen copias de los documentos que demuestran la existencia de la
                sociedad
                y
                sus órganos de dirección.</p><br/>

            <p class="text-sm dark:text-gray-100">4.5 CUENTA DEL DOCTOR. El proveedor creará una cuenta personal mediante el ingreso de su
                correo electrónico,
                su
                nombre clave o usuario y el código de acceso de seguridad que será elegido por éste; indicará sus datos
                personales, como nombre según figura en su pasaporte o documento de identidad nacional, dirección,
                teléfono,
                celular, datos de su lugar de trabajo, descripción, fotos y todos los datos requeridos como “campos
                obligatorios” en el FORMULARIO DE VINCULACIÓN DEL PROVEEDOR disponible en la plataforma.</p><br/>

            <p class="text-sm dark:text-gray-100">4.5.1 EL FORMULARIO DEL DOCTOR indicará lo permitido por el artículo 16 del Código de
                Ética
                del médico, que
                permite dar la información siguiente: a) Las que le faciliten sus relaciones con los enfermos. b. Las
                funciones
                y 1os títulos reconocidos por el Colegio Médico Dominicano y sus sociedades especializadas debidamente
                reconocidas. c) Nombre y especialidad. d) Su sitio de trabajo y horario de atención y la información de
                contacto.</p><br/>

            <p class="text-sm dark:text-gray-100">4.5.2. CUENTA DEL CLIENTE. El cliente creará una cuenta personal mediante el ingreso de
                su
                correo
                electrónico,
                su nombre clave o usuario y el código de acceso de seguridad que será elegido por éste; indicará sus
                datos
                personales, como nombre según figura en su pasaporte o documento de identidad nacional, dirección,
                teléfono,
                celular, descripción, fotos y todos los datos requeridos como “campos obligatorios” en el FORMULARIO DE
                VINCULACIÓN disponible de la APP.</p><br/>

            <p class="text-sm dark:text-gray-100">4.6 Si el usuario tiene distintas direcciones de correo electrónico, deberá usar una sola
                para la
                plataforma.</p><br/>

            <p class="text-sm dark:text-gray-100">4.7 En virtud de que el usuario, proveedor o cliente, es el único con acceso a su cuenta
                y
                con manejo
                absoluto
                de su usuario y código de acceso, este será el único responsable de todas de las operaciones que sean
                realizadas
                a través de la cuenta.</p><br/>

            <p class="text-sm dark:text-gray-100">4.8 El usuario se obliga a notificar a la APP sobre cualquier uso no autorizado de su
                cuenta
                o cualquier
                violación de seguridad que el usuario conozca o deba conocer. El derecho de uso de la plataforma es un
                derecho
                esencial del usuario, otorgado en razón de su persona.</p><br/>

            <p class="text-sm dark:text-gray-100">4.9 La APP no es responsable por la idoneidad de la información provista durante la
                creación
                de la cuenta del
                cliente.</p><br/>

            <p class="text-sm dark:text-gray-100">4.10 Los usuarios aceptan que existe siempre el riesgo en internet de personas que
                pretendan
                o intenten usar
                la
                plataforma sin información precisa, así como la creación de cuentas irreales. Para todos esos casos, la
                APP
                hará
                sus mejores esfuerzos según la disponibilidad de tecnología disponible (bots, inteligencia virtual,
                entre
                otros)
                para evitar cuentas ficticias o eliminar las creadas. Igualmente, la plataforma pone a disposición de
                los
                usuarios una sección de DENUNCIAS, para reportar cualquier tipo de actividad anormal.</p><br/>

            <p class="text-sm dark:text-gray-100">4.11 No obstante, la APP confirmará a los proveedores que se han vinculado a la
                plataforma de
                manera
                presencial
                o virtual, para lo cual deberán suministrar los documentos e informaciones necesarias, aceptables por la
                APP
                para confirmar su identidad conforme a la práctica local. Esos proveedores ingresarán al sistema como
                proveedores confirmados; sin embargo, esto no será garantía de su comportamiento, del cumplimiento de
                sus
                obligaciones frente a los clientes o de su idoneidad como proveedores de bienes y servicios.</p><br/>

            <p class="text-sm dark:text-gray-100">4.12 El usuario deberá notificar a la APP si la información contenida en su perfil ha
                sido
                robada,
                extraviada,
                objeto de apropiación indebida o se ve amenazada de algún modo, así como de cualquier uso o sospecha de
                uso
                no
                autorizado del perfil. En los casos donde el usuario no reporte las situaciones antes mencionadas o
                realice
                actividades ilícitas con su perfil es completamente responsabilidad de la persona que se registró en la
                APP.</p><br/>

            <p class="text-sm dark:text-gray-100">4.13 Los usuarios aceptan que la plataforma almacene información sobre IP, MAC Address o
                cualquier trazo de
                seguridad online que pueda informar a cualquier autoridad competente, para detectar uso indebido de
                ésta. La
                plataforma no es responsable del robo de identidad o información registrada en el sistema. Hará sus
                mejores
                esfuerzos según la tecnología vigente para evitarlo.</p><br/>

            <p class="text-sm dark:text-gray-100">4.14 La plataforma tiene el derecho de exigir de algunos clientes, según corresponda o
                según
                sea necesario,
                de
                información adicional para comprobar su identidad o para confirmar los datos asentados. En caso de que
                un
                usuario no colabore con el requerimiento de información adicional o comprobación de datos, la cuenta
                será
                suspendida o cancelada, según sea pertinente.</p><br/>

            <p class="text-sm dark:text-gray-100">4.15 En el caso de que la APP detecte o sea reportado contenido publicado, difundido,
                enviado
                o transmitido
                fraudulento, falso, engañoso (directamente o por omisión o no actualización de la información) o falaz;
                (ii)
                obsceno, pornográfico, vulgar u ofensivo, difamatorio, injurioso; ó (iii) promueva la discriminación, la
                intolerancia, el racismo, el odio, el acoso o el daño contra cualquier persona o grupo; (iv) sea
                violento o
                amenazador o fomente la violencia o acciones que sean amenazantes sobre cualquier otra persona o animal;
                (v)
                promueva actividades o sustancias ilícitas o dañinas; o (vi) incumpla con los presentes términos y
                condiciones
                de APP o cualquier otra política de DOCTORMOISES, S.A.S. ; sin aviso previo, se retirará o desactivará
                el
                acceso
                al perfil y contenido del usuario.</p><br/>

            <p class="text-sm dark:text-gray-100">4.15.1 Si algún usuario determina que algún contenido en la Plataforma de la APP infringe
                los
                derechos de
                autor
                que posee, podrá notificarlo mediante correo electrónico en la sección de quejas o comentarios de la
                página
                web.
                La APP no será responsable en este caso o eventualidad; habidas cuentas de que no administra el
                contenido
                creado
                por terceros.</p><br/>

            <p class="text-sm dark:text-gray-100">4.16 Los códigos de acceso, contraseñas, usuarios y demás información requerida para el
                acceso a la APP o al
                portal son personales e intransferibles y no pueden ser compartidos con nadie. Solo la persona que haya
                consentido a los Términos y Condiciones, y se haya registrado debidamente, está autorizada para hacer
                uso de
                dicha información y acceder al uso de esta tecnología para facilitar intercambio de bienes y servicios
                entre
                proveedores y clientes.</p><br/>

            <p class="text-sm dark:text-gray-100">4.17 El proveedor se compromete a realizar exclusivamente las citas, turnos y consultas
                virtuales a través de
                la
                APP con los pacientes que lo hayan contactado o se hayan enterado del perfil profesional del doctor a
                través
                de
                DOCTORMOISES. En caso de que el proveedor no cumpla con este acápite, debe ser notificado a través de
                una
                primera alerta. Si la situación permanece, el proveedor será dado de baja de la APP por tiempo
                indefinido, a
                discreción de la compañía.</p>
            <br/>

            <h6 class="dark:text-gray-100">5. OBLIGACIÓN DE INTEGRIDAD DE LA INFORMACIÓN. USO DE LA INFORMACIÓN.</h6>

            <p class="text-sm dark:text-gray-100">5.1 El proveedor es el único responsable de asegurar y dar fe de que el contenido de su
                información no viola
                la
                ley, las leyes o reglamentos, así como derechos de terceros. Debe asegurarse de que el contenido de su
                perfil
                cumpla con los códigos de ética médicos, así como con los reglamentos y leyes del sector salud, así como
                de
                protección al consumidor que les son aplicables.</p><br/>

            <p class="text-sm dark:text-gray-100">5.1.2 La APP aunque no es responsable ni tiene deber de auditoría de la información,
                eliminará cualquier
                información o publicación que sea contraria a la ley, los usos y las buenas costumbres, según su
                criterio y
                a
                discreción propia.</p><br/>

            <p class="text-sm dark:text-gray-100">5.2 Los clientes y proveedores garantizan que no usarán su cuenta para vulnerar, afectar
                o
                violar derechos de
                autor, propiedad industrial. Usarán su cuenta de buena fe y con apego a las normas legales aplicables o
                del
                consumidor vigentes, así como con el cuidado y deber de una persona razonable y de una persona
                prudente.</p>
            <br/>

            <p class="text-sm dark:text-gray-100">5.2.1 El usuario descarga y consecuentemente declara a la APP libre e indemne de
                obligaciones,
                responsabilidades
                y reclamaciones que surjan del mal uso o manejo de su cuenta, o de la violación de estos términos o
                condiciones
                o de negligencia del usuario, sea por acción o por omisión.</p><br/>

            <p class="text-sm dark:text-gray-100">5.3 La APP utilizará canales de distribución en internet para difundir el contenido de
                los
                servicios de salud
                de
                los proveedores de modo que las búsquedas de usuarios sean más fácil y precisa. Esto incluye motores de
                búsqueda, redes sociales, aplicaciones de teléfonos móviles, páginas de internet, entre otros.</p><br/>

            <p class="text-sm dark:text-gray-100">5.5 Los proveedores otorgan y conceden a favor de la APP un permiso para el uso y
                divulgación
                de la
                información
                atinente a sus servicios que han colocado en la plataforma.</p><br/>

            <p class="text-sm dark:text-gray-100">5.6 De igual forma, los usuarios aceptan que la APP se base de información básica (sin
                revelar la identidad a
                terceros) para mejorar la experiencia del usuario por zona, rango de edad, género, etc. Independiente de
                esto,
                los usuarios reconocen que la sección “políticas de privacidad” contiene los lineamientos del manejo de
                la
                información.</p><br/>

            <p class="text-sm dark:text-gray-100">5.7 Los usuarios declaran y garantizan del modo más absoluto posible, que son los
                titulares
                por derecho,
                licencia o uso permitido, de todos los derechos de autor o de propiedad industrial relacionados con la
                información subida a la plataforma.</p><br/>

            <p class="text-sm dark:text-gray-100">5.8 Los proveedores garantizan a la APP y a los demás usuarios, conocidos o no, así como
                al
                público en
                general,
                que tienen las licencias y registros de ley para dar a conocer y proveer los servicios propios de su
                actividad
                profesional.</p><br/>

            <p class="text-sm dark:text-gray-100">5.9 Ninguna información provista por los proveedores se hará para perjudicar cualquier
                derecho de otro
                proveedor, usuario, de la plataforma y de terceros. Los usuarios tienen un deber general de buena fe, de
                usar el
                servicio del modo más adecuado posible, cuidando de la información como un buen padre de familia o
                persona
                razonable y precavida.</p><br/>

            <p class="text-sm dark:text-gray-100">5.10 Cualquier tercero que se sienta afectado tendrá el derecho de pedir a la plataforma
                la
                remoción o
                limitación de cualquier publicación que vulnere o atente contra sus derechos y le otorgará a la
                plataforma
                un
                plazo razonable para tomar cualquier medida que corresponda en procura de sus derechos.</p><br/>

            <p class="text-sm dark:text-gray-100">5.11 El uso fraudulento de tarjeta por un tercero no autorizado se tratará conforme a las
                políticas del banco
                emisor y el tarjetahabiente. La APP no es responsable por el hecho de que terceros usen una tarjeta de
                crédito
                de la cual no sea titular.</p>
            <br/>

            <h6 class="dark:text-gray-100">6. NATURALEZA DEL SERVICIO: </h6>

            <p class="text-sm dark:text-gray-100">6.1 La APP y su plataforma constituyen una simple facilidad al público para intercambio
                de
                información sobre
                los
                administradores y proveedores de servicios de salud. En efecto, sus funciones son las siguientes:</p>
            <br/>

            <h7 class="dark:text-gray-100">6.1.1 Para los clientes.</h7>

            <p class="text-sm dark:text-gray-100">Citas y turnos en línea. Realizar citas en línea y esperar turnos desde la comodidad y
                seguridad del hogar o
                lugar de trabajo, practicar el distanciamiento social y evitar perder horas productivas en largas filas
                de
                espera.</p><br/>

            <p class="text-sm dark:text-gray-100">Ahorrar tiempo con el directorio en línea. Buscar y encontrar de manera más rápida y
                fácil
                los mejores
                médicos
                de la ciudad. Tomar mejores decisiones basadas en reportes, ratings, tiempos de espera, horarios de
                servicio,
                cercanía, precios y trayectoria.</p><br/>

            <p class="text-sm dark:text-gray-100">Recordatorios automatizados: Recibir recordatorios automáticos de citas y turnos a través
                de
                notificaciones
                push, SMS y correo electrónico.</p><br/>

            <h7 class="dark:text-gray-100">6.1.2 para los proveedores:</h7>
            <br/>

            <ul class="dark:text-gray-100">

                <li>
                    Automatización del consultorio. Permite administrar la agenda y lista de espera de manera más
                    eficiente
                    e
                    innovadora, obteniendo más control, mayores ingresos y menos aglomeración de personas en las salas
                    de
                    espera,
                    aumentando los volúmenes de visitas, requiriendo menos espacio físico y poniendo en práctica el
                    distanciamiento
                    social.
                </li>
                <br>

                <li>Telemedicina (consultas virtuales) para aquellos casos donde sea posible la consulta o atención a
                    distancia.
                </li>
                <br>
                <li>Recibir citas y turnos prepagos. Recibir turnos y citas de forma fácil y segura.</li>

                <li>Resultados de estudios y prescripciones online. Recibir y compartir los estudios médicos,
                    diagnósticos y
                    prescripciones a través de la plataforma, eficientizando el tiempo de consulta y descongestionando
                    las
                    salas
                    de espera.
                </li>
                <br>
                <li>Estadísticas y reportes. Verificar en cualquier momento la información y datos de la provisión del
                    servicio.
                </li>
                <br>
                <li>Recordatorios automatizados. Recibir recordatorios automáticos a través de notificaciones push, SMS
                    y
                    correo
                    electrónico para llegar a tiempo a sus consultas.
                </li>
            </ul>
            <br/>
            <p class="text-sm dark:text-gray-100">6.2 Los servicios de DOCTORMOISES van enfocados a facilitar al usuario la experiencia en
                el
                servicio; es
                decir, que en la relación que se formará entre el médico y el paciente, se logre de manera rápida, fácil
                y
                con
                menos tiempos y complicaciones.</p><br/>

            <p class="text-sm dark:text-gray-100">6.3 Los servicios de DOCTORMOISES no son médicos, solo pone a disposición de los usuarios
                del
                servicio de
                salud, una herramienta para que éstos se contacten y una base de datos dinámica para que la información
                de
                las
                cuentas de los usuarios esté completa y accesible para cuando sea necesario para estos, usarla.</p>

            <p class="text-sm dark:text-gray-100">6.4 DOCTORMOISES no proporciona consejos médicos ni consultas médicas. Los proveedores
                listados en la
                plataforma no son contratados por DOCTORMOISES sino que éstos se valen de DOCTORMOISES para hacer
                visibles
                sus
                servicios e información al público en general.</p><br/>

            <p class="text-sm dark:text-gray-100">6.5 DOCTORMOISES no es un agente o intermediario de servicios de salud; es una
                herramienta
                tecnológica que
                compila a los proveedores listados; para que los usuarios tengan acceso a estos servicios de manera
                directa.</p><br/>

            <p class="text-sm dark:text-gray-100">6.6 DOCTORMOISES indica a los proveedores que éstos deben actuar con toda la
                responsabilidad
                frente a los
                usuarios que les impone la Ley 42-01 y sus reglamentos. De igual manera los usuarios y clientes deben
                usar
                la plataforma como un fin primario para el contacto con su proveedor de servicios de salud. Los
                diagnósticos,
                consejos y consultas médicas, pruebas físicas, presenciales, pruebas de laboratorio, imágenes y demás,
                deben
                ser provistos en la forma que dicta la ciencia médica, sin que la plataforma en ningún aspecto pueda
                sustituirlos o realizar su función.</p><br/>

            <p class="text-sm dark:text-gray-100">6.7 DOCTORMOISES no recomienda ningún médico, tratamiento, ni da consejos ni refiere. Los
                servicios de la
                plataforma se limitan a listados de directorios médicos para que los usuarios tengan facilidades
                localizando
                a proveedores y administradores de servicios de salud. De igual manera, DOCTORMOISES solo podrá listar
                aquellos proveedores que escojan registrarse en la plataforma; es decir, que el hecho de que un centro o
                profesional
                de la salud no esté listado, no implica ninguna sugerencia o asunción; simplemente, que no es un
                “usuario de
                la
                plataforma”; de igual manera, aquellos listados simplemente implican que “son usuarios”. De esto se
                infiere
                que el directorio de la plataforma se genera por los usuarios y es creado por éstos; la plataforma solo
                los
                lista y presenta para la conveniencia de los clientes.</p><br/>

            <p class="text-sm dark:text-gray-100">6.7 La APP no es el titular ni detentador de lo ofrecido en la plataforma; tampoco
                interviene
                en forma
                alguna en las eventuales operaciones que se realicen entre usuarios. No participa ni responde por la
                existencia,
                calidad, cantidad, estado, integridad o legitimidad de los servicios ofrecidos, o adquiridos por los
                usuarios.</p><br/>

            <p class="text-sm dark:text-gray-100">6.4. El rol de la APP es la administración de la plataforma de la que se sirven los
                usuarios
                para intercambio
                de
                los servicios publicados por éstos y subidos al portal web y a la aplicación móvil, de modo que estén
                disponibles para consumo.</p><br/>

            <p class="text-sm dark:text-gray-100">6.5 En ese tenor, la APP no será responsable por el efectivo cumplimiento de las
                obligaciones
                asumidas por
                los usuarios. Las relaciones entre los usuarios solo les atañen a éstos y solo entre éstos se pueden
                formular
                reclamaciones. La plataforma quedará libre e indemne por esas eventualidades, salvo los casos donde esta
                genere su propia falta, que deberá quedar establecida. Las obligaciones asumidas por la APP son
                exclusivamente
                aquellas indicadas esta sección para los usuarios puedan cumplir de manera específica con lo indicado en
                la
                sección
                “7” - “de la provisión del servicio” de estos términos y aquellas expresamente definidas como tal.</p>

            <p class="text-sm dark:text-gray-100">6.6 La APP no asume ninguna responsabilidad por el uso de ésta y se libera de toda
                obligación
                o
                responsabilidad
                que pueda resultar por errores u omisiones por el contenido o de la información suministrada por los
                usuarios, incluyendo, pero no limitada a, imprecisiones técnicas y errores de fechas, datos de pago, de
                identificación, referencias a otras páginas o correos, teléfonos, direcciones.</p><br/>

            <p class="text-sm dark:text-gray-100">6.7 La APP no será responsable de mal funcionamiento de hiperenlaces; la falta de
                disponibilidad de acceso a
                la plataforma; a cualquier porción de la misma, ocasionada por trabajos de mantenimiento necesarios o
                por
                acciones de terceros, o el uso de cualquier equipo o software ajeno a la plataforma; así como otros
                eventos
                afines o
                correlativos a los aquí indicados.</p><br/>

            <p class="text-sm dark:text-gray-100">6.8 La APP se reserva el derecho de cambiar palabras del contenido por errores
                tipográficos,
                si es que los
                detecta, ya que su corrección es obligación del usuario. Asimismo, se podrán eliminar datos o imágenes
                que
                sean notoriamente irrelevantes para el contenido de la publicación o eliminar contenido inaceptable
                conforme
                a
                estos términos, las leyes o los reglamentos; asó como la ética, moral o buenas costumbres.</p>
            <br>

            <p class="text-sm dark:text-gray-100">6.9 DOCTORMOISES hará sus mejores diligencias para tener respaldos de la información
                correspondiente a cada
                usuario en sus cuentas. No obstante, el usuario es responsable de realizar copias de seguridad, en su
                propia
                computadora o en otro dispositivo, de cualquier documento, archivo, data o información que esté alojada
                en
                su cuenta en los servidores de DOCTORMOISES.</p>
            <br/>

            <h6 class="dark:text-gray-100">7. DE LA PROVISIÓN DEL SERVICIO. OBLIGACIONES ESPECÍFICAS DE LOS USUARIOS. </h6>
            <p class="text-sm dark:text-gray-100">7.1 La APP pone a disposición de los usuarios la tecnología móvil desarrollada de modo
                que
                proveedores y
                usuarios coincidan en el universo de servicios médicos ofertados y, por tanto, queden conectados de
                manera
                virtual, para entre ellos y de manera directa entre éstos, se contraten servicios.</p><br/>

            <p class="text-sm dark:text-gray-100">7.2. La forma en que se realizará la oferta de bienes y servicios publicadas a un público
                general o
                específico,
                será de la forma que se indica más adelante, salvo que en el propio anuncio lo indique un método
                distinto de
                manera clara y legible.</p><br/>

            <p class="text-sm dark:text-gray-100">7.2.1 Por regla general, cuando los clientes celebran una relación de consumo con un
                proveedor, aceptan que
                es
                un contrato de consumo o servicio hecho de manera directa. La APP ofrece un servicio de intermediación a
                través
                de la tecnología que ofrece. En ese orden, queda establecida la siguiente mecánica:
            </p><br/>
            <ul class="dark:text-gray-100">
                <li>
                    a) El proveedor presentará sus servicios con sus características esenciales, de modo que el cliente
                    o
                    consumidor
                    pueda contactarlo debidamente.
                </li>
                <br>

                <li>b) Una vez el cliente haga contacto con el proveedor, la APP haciendo uso de su tecnología protegida
                    por
                    propiedad intelectual e industrial; generará de forma electrónica “un comando” que conllevará que el
                    proveedor
                    cumpla con responderle y se ponga en relación con el cliente; salvo que uno o el otro decline por
                    razones
                    atendibles.
                </li>
            </ul>
            <br/>

            <p class="dark:text-gray-100">7.4 Con la contratación del servicio, por relación y conexión hecha entre el cliente y el proveedor; se
                hará
                uso
                de la forma de pago en línea que posee la APP, con métodos de pago en línea con tarjeta de débito,
                tarjeta
                de
                crédito y otros instrumentos electrónicos que los servicios financieros regulados permitan y estén
                vigentes.</p><br/>

            <p class="text-sm dark:text-gray-100">7.5 Los pagos por servicios se considerarán definitivos.</p>
            <br/>


            <h6 class="dark:text-gray-100">8. QUEJAS – DENUNCIAS. </h6>
            <p class="text-sm dark:text-gray-100">8.1 En caso de que el cliente tenga una queja o una denuncia, deberá en primer orden
                contactar a su proveedor
                de
                servicios para que éste le proporcione la solución que amerite.</p><br/>

            <p class="text-sm dark:text-gray-100">8.2 Las quejas que atenderá DOCTORMOISES serán únicamente respecto del uso de la
                plataforma o
                su
                accesibilidad.
                Las quejas por el servicio dado por el proveedor al cliente deberán hacerse a éste como se indica
                precedentemente.</p><br/>

            <p class="text-sm dark:text-gray-100">8.3 No obstante, para fines de estadísticas, las quejas por servicio al cliente podrán
                ser
                remitidas a
                contacto@DOCTORMOISES.com.do de modo que la plataforma pueda usar la información y postearla como
                comentario
                u
                observación en el perfil del proveedor. El proveedor podrá igualmente responder a dicha queja en caso de
                que
                entienda que no es justificada.</p>
            <br/>

            <h6 class="dark:text-gray-100">9. NOTIFICACIONES. </h6>
            <p class="text-sm dark:text-gray-100">9.1 La APP podrá enviar a los usuarios las notificaciones que estén relacionadas con su
                perfil, así como
                aquellas que sean programadas por los mismos usuarios.</p><br/>

            <p class="text-sm dark:text-gray-100">9.2 La APP puede enviar mensajes de texto y correos electrónicos a los usuarios con
                informaciones generales
                sobre el sector salud. Los usuarios tendrán la opción de desactivar el envío de mensajes o correos
                automáticos.</p>
            <br/>
            <h6 class="dark:text-gray-100">10. UBICACIÓN POR GEOREFERENCIA. </h6>
            <p class="text-sm dark:text-gray-100">10.1 La APP tendrá la facultad de recoger, utilizar y compartir datos precisos sobre
                localizaciones,
                incluyendo
                la localización geográfica o móvil del usuario, siempre que el usuario lo autorice.</p><br/>

            <p class="text-sm dark:text-gray-100">10.2 Los datos de localización pueden ser recogidos y utilizados por la APP para mostrar
                a
                los usuarios la
                ubicación de los centros o profesionales de la salud dentro de un área determinada. La APP tendrá la
                opción
                de
                desactivar “ubicación” cuando así lo entiendan pertinente; bajo la consideración de las limitaciones que
                esto
                podría conllevarles para un servicio idóneo.</p>
            <br/>


            <h6 class="dark:text-gray-100">11. POLÍTICAS DE RESPONSABILIDAD E INDEMNIDAD. </h6>

            <p class="text-sm dark:text-gray-100">11.1 Responsabilidad. Los usuarios convienen que en ningún caso la APP asume
                responsabilidad
                alguna, tanto
                frente a los proveedores o consumidores, así como frente a terceros, incluyendo entidades u organismos
                públicos, por:</p><br/>
            <ul class="dark:text-gray-100">
                <li>a) La calidad de los servicios ofrecidos a través de la APP, ya que corresponde a cada proveedor y
                    es
                    éste
                    el responsable del cumplimiento de las reglas, lineamientos, deberes y ejecución correcta de sus
                    prestaciones, conforme a las normas que gobiernan su relación con los usuarios.
                </li>
                <br>

                <li>b) En caso de venta de insumos; de sus reemplazos, que deberán ser realizados por los proveedores.
                </li>
                <br>
                <li>c) Atrasos o demoras en la entrega o provisión de los servicios.</li>
                <br>
                <li>d) La diferencia de precio que pudiera existir entre lo publicado en la APP y lo efectivamente
                    facturado
                    por
                    el proveedor al consumidor.
                </li>
                <br>
                <li>e) La garantía que aplique a los productos o insumos que ofrece el proveedor, si aplica.</li>
                <br>
                <li>f) Los daños y perjuicios que pudiera sufrir el proveedor por los comentarios que los consumidores o
                    usuarios realizaren en la APP o su página respecto al servicio prestado por el proveedor o en
                    cualquier
                    otro
                    portal o lugar.
                </li>
                <br>
                <li>g) Uso de nombre comercial y marcas por parte del proveedor.</li>
                <br>
                <li>h) Revocación, insuficiencia o falta de permisos, licencias o habilitaciones que se requieran para
                    que
                    el
                    proveedor pueda ofrecer, comercializar y promover sus servicios a través de una plataforma online.
                </li>
                <br>
                <li>i) Casos de fuerza mayor o fortuitos.</li>
                <br>
                <li>j) Incumplimientos de terceros.</li>

            </ul>
            <br/>

            <p class="text-sm dark:text-gray-100">11.2 Los usuarios reconocen que la APP tiene su base y razón de ser en su reputación,
                buena
                imagen, así como
                por
                el cabal cumplimiento de sus términos y condiciones, así como el respeto de la ley y los
                reglamentos.</p>
            <br/>

            <p class="text-sm dark:text-gray-100">11.3 Los usuarios se comprometen a no realizar conductas o actos que tiendan a mermar la
                imagen de la APP,
                así
                como su buena reputación, lo que será considerado como una falta grave que activa su obligación de
                indemnizar el
                daño integral que le sea causado.</p><br/>

            <p class="text-sm dark:text-gray-100">11.4 Los usuarios entienden, reconocen y aceptan que la APP no garantiza la protección
                contra
                transacciones
                fraudulentas y no válidas, por lo tanto, la APP no será responsable de ninguna transacción no válida que
                se
                procese, en ocasión de una orden realizada a través de la plataforma. Utilizará los medios disponibles
                tecnológicamente para evitar los usos fraudulentos, asumiendo un deber de diligencia y prudencia, sin
                obligarse
                al resultado de evitarlos.</p><br/>

            <p class="text-sm dark:text-gray-100">11.5 Los usuarios indemnizarán y mantendrán indemne a la APP (incluyendo sus directores,
                funcionarios,
                empleados, casa matriz, subsidiarias y filiales) por cualquier daño y perjuicio que ésta pudiera sufrir
                derivado
                de cualquier tipo de reclamo de terceros (incluyendo gastos y honorarios legales de abogados
                representantes)
                que
                tuvieran su causa u origen en el uso de los servicios de los proveedores de la plataforma. En todos los
                supuestos, esta responsabilidad de los usuarios sobrevivirá a la terminación del presente durante el
                plazo
                aplicable de prescripción legal, según sea el caso.</p><br/>

            <p class="text-sm dark:text-gray-100">11.6 Bajo el entendido de que Secure Sockets Layer (SSL; en español capa de puertos
                seguros)
                en la industria
                de
                tecnologías de la información; SSL es la tecnología de seguridad estándar actual para establecer un
                enlace
                cifrado entre un servidor web y un navegador. El vínculo busca garantizar que toda la data compartida
                por y
                entre el servidor web y el navegador se conserve íntegra y confidencial. Los usuarios deberán defender y
                mantener indemne a la APP por cualquier reclamación, demanda, perdidas, que tengan estén directa o
                indirectamente vinculadas con el uso de la capa de puertos seguros (SSL). Queda expresamente convenido
                que
                la
                limitación de responsabilidad aquí indicada tendrá efectos cuando el daño o perdida tenga su origen a un
                ataque
                cibernético o cualquier hecho que las ciencias de la tecnología e información no hubieren podido
                prever.</p>
            <br/>

            <p class="text-sm dark:text-gray-100">11.7 Los usuarios se comprometen a indemnizar y mantener indemne a la APP de cualquier
                daño
                de índole civil,
                comercial, laboral, administrativa y/o penal y cualquier demanda, acción, reclamo judicial,
                extrajudicial o
                de
                cualquier naturaleza asociadas a las mismas, proveniente de situaciones generadas entre proveedor –
                consumidor.
                La APP solo responderá de sus propias faltas que deberán quedar debidamente acreditadas.</p><br/>

            <p class="text-sm dark:text-gray-100">11.8 En ese orden, sin perjuicio de las limitaciones de responsabilidad expresadas en
                estos
                términos y
                condiciones; queda convenido que la solución aceptada por los usuarios frente a DOCTORMOISES es la
                cancelación
                del servicio y una compensación que quedará limitada al importe que la plataforma cobró para sí misma,
                por
                los
                servicios como APP.</p>

            <br/>

            <h6 class="dark:text-gray-100">12. PROTECCIÓN DE DATOS DE TERCEROS. </h6>
            <p class="text-sm dark:text-gray-100">12.1 Los anuncios publicados en la plataforma deben ser hechos en absoluta protección de
                datos de terceros.
                En ese sentido, los usuarios ni divulgarán ninguna información de ninguna naturaleza atinente a un
                tercero,
                relacionado o no, distinto al usuario, como son datos de contacto, datos financieros, imágenes no
                autorizadas, entre otros. Cada usuario será el único responsable de la información que divulgue, aún sea
                por
                su propia decisión, negligencia o imprudencia. </p>
            <br/>


            <h6 class="dark:text-gray-100">13. INCUMPLIMIENTOS DE LOS USUARIOS.</h6>
            <p class="text-sm dark:text-gray-100">13. 1 La APP podrá cancelar o suspender la cuenta de un usuario por violación a una o
                cualquiera de las
                reglas
                de estos términos y condiciones, así como por violación a leyes, reglamentos, buenos usos y costumbres,
                reglas
                de la moral o de la ética, mediando un pre-aviso no mayor de cinco días.</p><br/>

            <p class="text-sm dark:text-gray-100">13.2 En el caso de la suspensión o inhabilitación de un usuario, todos los anuncios que
                tuviera publicados
                serán
                quitados de la plataforma sin derecho de compensación o devolución de pagos por servicios a la
                plataforma,
                de
                haberse generado alguno.</p>
            <br/>


            <h6 class="dark:text-gray-100">14. POLÍTICAS DE PRIVACIDAD. </h6>

            <p class="text-sm dark:text-gray-100">14.1 Este acápite de políticas de privacidad establece los términos en que la APP usa y
                protege la
                información
                que es proporcionada por sus usuarios al momento de utilizar su sitio web o APP, bajo el compromiso que
                esta
                asume de la seguridad de los datos de sus usuarios.</p><br/>

            <p class="text-sm dark:text-gray-100">14.2 Cuando se le solicita llenar los campos de información personal con la cual el
                usuario
                pueda ser
                identificado, lo hacemos asegurando que sólo se empleará de acuerdo con los términos de este documento.
                Sin
                embargo, esta política de privacidad puede cambiar con el tiempo o ser actualizada para lo cual se le
                avisará y
                notificará a fin de que sean aceptadas o se proceda a la cancelación de la cuenta.</p><br/>

            <p class="text-sm dark:text-gray-100">14.3 La APP podrá recoger información personal por ejemplo: Nombre, información de
                contacto
                como dirección de
                correo electrónica e información demográfica. Así mismo cuando sea necesario podrá ser requerida
                información
                específica para procesar algún servicio o función específica como aplicación.</p><br/>

            <p class="text-sm dark:text-gray-100">14.3.1 En efecto, la APP le solicitará a los usuarios y recabará información básica que
                comprende:</p><br/>
            <ul class="dark:text-gray-100">
                <li>a) Nombre completo</li>
                <br>
                <li>b) Correo electrónico</li>
                <br>
                <li>c) Dirección</li>
                <br>
                <li>d) Número de teléfono</li>
                <br>
                <li>e) Ciudad o domicilio y localización.</li>
                <br>
                <li>f) Cualquier dato libremente revelado por el usuario.</li>
                <br>
                <li>g) Datos de confirmación societaria o comercial para los proveedores.</li>
                <br>
                <li>h) Licencias, habilitaciones, títulos, permisos, acreditaciones y afines.</li>

            </ul>
            <br/>
            <p class="text-sm dark:text-gray-100">14.4 La APP emplea la información con el fin de proporcionar el mejor servicio posible,
                particularmente para
                mantener un registro de usuarios y mejorar en general la experiencia del usuario.</p><br/>

            <p class="text-sm dark:text-gray-100">14.5 LA APP está altamente comprometido para cumplir con el compromiso de mantener su
                información segura. Se
                usarán sistemas avanzados que impidan y limiten los accesos no autorizados.</p><br/>

            <p class="text-sm dark:text-gray-100">14.6 Los usuarios reconocen formalmente que es obligación abstenerse de compartir la
                clave de
                acceso a su
                cuenta. Deben usar claves largas y con caracteres especiales.</p><br/>

            <p class="text-sm dark:text-gray-100">14.6 Uso de Cookies. Una cookie se refiere a un fichero que es enviado con la finalidad
                de
                solicitar permiso
                para almacenarse en el ordenador del usuario, al aceptar dicho fichero se crea y la cookie sirve
                entonces
                para
                tener información respecto al tráfico web, y también facilita las futuras visitas a una web recurrente.
                Otra
                función que tienen las cookies es que con ellas las webs pueden reconocer individualmente al usuario y
                por
                tanto
                brindarte el mejor servicio personalizado de su web.</p><br/>

            <p class="text-sm dark:text-gray-100">14.7 El usuario puede eliminar las cookies en cualquier momento desde su ordenador. Sin
                embargo, las cookies
                ayudan a proporcionar un mejor servicio de los sitios web, éstas no dan acceso a información de su
                ordenador
                ni
                de usted, a menos de que usted así lo quiera y la proporcione directamente. El usuario puede aceptar o
                negar
                el
                uso de cookies, sin embargo, la mayoría de los navegadores aceptan cookies automáticamente pues sirve
                para
                tener
                un mejor servicio web. También el usuario puede cambiar la configuración de su ordenador para declinar
                las
                cookies. Si se declinan es posible que no pueda utilizar algunos de nuestros servicios.</p><br/>

            <p class="text-sm dark:text-gray-100">14.8 La APP o su sitio web pudiera contener enlaces a otros sitios que pudieran ser de
                interés de los usuarios,
                en esos casos, las políticas de privacidad aplicables serán la de esos terceros visitados.</p><br/>

            <p class="text-sm dark:text-gray-100">14.9 En cualquier momento el usuario puede restringir la recopilación o el uso de la
                información personal que es
                proporcionada a nuestra APP. Cada vez que se le solicite rellenar un formulario, como el de alta de
                usuario,
                puede marcar o desmarcar la opción de recibir información por correo electrónico. En caso de que haya
                marcado la
                opción de recibir nuestro boletín o publicidad el usuario puede cancelarla en cualquier momento.</p>
            <br/>

            <p class="text-sm dark:text-gray-100">14.10 La APP no venderá, cederá ni distribuirá la información personal que es recopilada
                sin
                su consentimiento,
                salvo que sea requerido por un juez con una orden judicial.</p><br/>

            <p class="text-sm dark:text-gray-100">14.11 El usuario no revelará datos sensibles protegidos por la ley al público en general;
                de
                realizarlo, se
                expone por su negligencia a que dichos datos puedan ser descubiertos o revelados por terceros,
                descargando
                de
                toda responsabilidad a la APP.</p><br/>

            <p class="text-sm dark:text-gray-100">14.12 La APP se compromete a proteger de manera prudente y diligente la integridad y la
                seguridad de la base de
                datos y contenido de la plataforma. No obstante, a lo anterior, el usuario reconoce que, en caso de que
                la
                información que se encuentre en la base de datos de la APP se vea comprometida por caso fortuito o
                fuerza
                mayor,
                es decir, que sea imprevisible e irresistible, esta última quedará eximida de cualquier responsabilidad
                por
                los
                daños que dicha pérdida pueda ocasionar al usuario.</p>
            <br/>

            <p class="dark:text-gray-100">14.13 "Las informaciones que maneje el doctor del paciente es de carácter confidencial, de lo contrario
                puede
                tener consecuencias”.</p><br/>
        </div>
    </div>
</div>

