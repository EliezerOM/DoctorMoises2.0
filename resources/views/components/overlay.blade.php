<div
    {{$attributes->merge(['class' => 'bg-gradient-to-r opacity-30 from-blue-600 to-blue-900 left-0 right-0 z-[999] transition-all top-0 bottom-0 absolute  overflow-auto'])}}
    id="overlay2"
    x-show="isOverlay"
    @click.passive="isOverlay = false"
   ></div>
