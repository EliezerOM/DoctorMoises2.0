@props(['slot' => null])
<ul {{$attributes->merge(['class'=>'space-y-4 text-left text-gray-500 dark:text-gray-400 '])}}>
    {{$slot}}
</ul>
