@props([
    'tooltipName'=>null,
    'slot'=>null,
    'text'=>null,
    'place'=>null,
    'id_tool'=> uniqid('tooltip_')
])
<?php if(!$tooltipName)$tooltipName = uniqid('tooltip_');?>
<div
    class="relative" {!! 'x-data="{ '.$tooltipName.': false }" x-on:mouseover="'.$tooltipName.' = true" x-on:mouseleave="'.$tooltipName.' = false"' !!}>
    {{$slot}}

    <div x-cloak {!! 'x-show="'.$tooltipName.'"' !!}
         x-transition:enter="transition-transform transition-opacity ease-out duration-200"
         <?php
         if ($place === 'bottom') {
             echo(' x-transition:enter-start="opacity-0 transform translate-y-2" ' .
                 ' x-transition:enter-end="opacity-100 transform " ' .
                 ' x-transition:leave-end="opacity-0 transform translate-y-2" ' .
                 ' class="absolute grid z-10 left-[50%] -translate-x-[50%] -bottom-[2.5rem] w-max" ');
         }
         else if($place === 'left'){
             echo(' x-transition:enter-start="opacity-0 transform translate-x-2" ' .
                 ' x-transition:enter-end="opacity-100 transform " ' .
                 ' x-transition:leave-end="opacity-0 transform translate-x-2" ' .
                 ' class="absolute flex z-10 top-0 right-[110%] w-max items-center" ');
         }
         else if($place === 'right'){
             echo(' x-transition:enter-start="opacity-0 transform translate-x-2" ' .
                 ' x-transition:enter-end="opacity-100 transform " ' .
                 ' x-transition:leave-end="opacity-0 transform translate-x-2" ' .
                 ' class="absolute flex z-10 top-0 left-[110%] w-max items-center" ');
         }
         else {
             echo(' x-transition:enter-start="opacity-0 transform -translate-y-2" ' .
                 ' x-transition:enter-end="opacity-100 transform " ' .
                 ' x-transition:leave-end="opacity-0 transform -translate-y-2" ' .
                 ' class="absolute grid z-10  left-[50%] -translate-x-[50%] -top-[2.5rem] w-max" ');
         }

         ?>
         x-transition:leave="transition ease-in duration-250"
         {!! 'x-on:mouseover="'.$tooltipName.' = true"' !!}
         {!! '@mouseover.outside="'.$tooltipName.' = false"' !!}

    >

        <?php
        if ($place === 'bottom') {
            echo(' <i class="fa-solid fa-diamond text-center text-gray-700 -mb-[9px]"></i> ' .
                ' <span class="bg-gray-700 text-gray-100 p-0.5 rounded-md">' . $text . '</span> ');
        }
        else if($place === 'left')
        {
            echo(' <span class="bg-gray-700 z-10 text-gray-100 p-0.5 rounded-md ">' . $text . '</span> '.
                ' <i class="fa-solid  fa-diamond text-center text-gray-700 -ml-[9px]"></i> ');
        }
        else if($place === 'right'){
            echo(' <i class="fa-solid  fa-diamond text-center text-gray-700 -mr-[9px]"></i> '.
                ' <span class="bg-gray-700 z-10 text-gray-100 p-0.5 rounded-md ">' . $text . '</span> '
                );
        }
        else{
            echo(' <span class="bg-gray-700 z-10 text-gray-100 p-0.5 rounded-md ">' . $text . '</span> '.
                ' <i class="fa-solid fa-diamond text-center text-gray-700 -mt-[9px]"></i> ');
        }

        ?>

    </div>
</div>

