import  Alpine from 'alpinejs'
import collapse from '@alpinejs/collapse'
import mask from '@alpinejs/mask';
import persist from '@alpinejs/persist'
import Parent from "@ryangjchandler/alpine-parent";
import focus from '@alpinejs/focus'

window.Alpine= Alpine;
Alpine.plugin(mask);
Alpine.plugin(focus);
Alpine.plugin(Parent);
Alpine.plugin(collapse);
Alpine.plugin(persist);
Alpine.start();
