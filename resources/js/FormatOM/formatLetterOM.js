
export class textOM{
    // const _M_NUMEROS = '#';
    static Upper(pValor) {
        if (typeof pValor === 'string') {
            return pValor.toUpperCase();
        }
    }
    static Lower(pValor) {
        if (typeof pValor === 'string') {
            return pValor.toLowerCase();
        }
    }
    static CapitalizeFirst(pValor) {
        if (typeof pValor === 'string') {
            const val = this.Lower(pValor);
            return val.charAt(0).toUpperCase() + val.slice(1);
        }
    }
    static Capitalize(pValor) {
        if (typeof pValor === 'string') {
            return pValor.toLowerCase().replace(/\w\S*/g, (w) => (w.replace(/^\w/, (c) => c.toUpperCase())));
        }
    }
    static OnlyLetter(pValor){
        if (typeof pValor === 'string'){
            return pValor.replace(/[^\a-zA-Z|ñ|Ñ]/g, (w) => (w.replace(/[^\a-zA-Z|ñ|Ñ]/g, '')));
        }
    }
    static OnlyLetterCapitalize(pValor){
        if (typeof pValor === 'string'){
            const val = this.Capitalize(pValor);
            return this.OnlyLetter(val);
        }
    }
    static OnlyLetterCapitalizeFirst(pValor){
        if (typeof pValor === 'string'){
            return this.OnlyLetter(this.CapitalizeFirst(pValor));
        }
    }
    static OnlyLetterUpper(pValor){
        if (typeof pValor === 'string'){
            return this.OnlyLetter(this.Upper(pValor));
        }
    }
    static OnlyLetterLower(pValor){
        if (typeof pValor === 'string'){
            return this.OnlyLetter(this.Lower(pValor));
        }
    }
    static OnlyLetterWithSpaces(pValor){
        if (typeof pValor === 'string'){
            return pValor.replace(/[^\a-zA-Z|ñ|Ñ|\s]/g, (w) => (w.replace(/[^\a-zA-Z|ñ|Ñ|\s]/g, '')));
        }
    }
    static WithoutSpaces(pValor){
        if (typeof pValor === 'string'){
            return pValor.replace(/[^\S]/g, (w) => (w.replace(/[^\S]/g, '')));
        }
    }
    static OnlyLetterWithSpacesCapitalize(pValor){
        if (typeof pValor === 'string'){
            return this.Capitalize(this.OnlyLetterWithSpaces(pValor));
        }
    }
    static OnlyLetterWithSpacesCapitalizeFirst(pValor){
        if (typeof pValor === 'string'){
            return this.CapitalizeFirst(this.OnlyLetterWithSpaces(pValor));
        }
    }
    static OnlyLetterWithSpacesUpper(pValor){
        if (typeof pValor === 'string'){
            return this.OnlyLetterWithSpaces(this.Upper(pValor));
        }
    }
    static OnlyLetterWithSpacesLower(pValor){
        if (typeof pValor === 'string'){
            return this.OnlyLetterWithSpaces(this.Lower(pValor));
        }
    }

}


