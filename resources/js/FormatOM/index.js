const {textOM} = require("./formatLetterOM");
const {numberOM} = require("./formatNumberOM");
// Require JQuery 3.6.0 ^
//Init
$(document).ready(function () {
(function () {
    $('[om-capitalize]').each(function () {
        let _value = $(this).val();
        if (_value !== undefined) {
            let value = textOM.Capitalize(_value);
            $(this).val(value);
        }
        _value = $(this).text();
        if (_value !== undefined) {
            let value = textOM.Capitalize(_value);
            $(this).text(value);
        }
        _value = $(this).html();
        if (_value !== undefined) {
            let value = textOM.Capitalize(_value);
            $(this).text(value);
        }
    });

    $('[om-only-letter]').each(function () {
        let _value = $(this).val();
        if (_value !== undefined) {
            let value = textOM.OnlyLetter(_value);
            $(this).val(value);
        }
        _value = $(this).text();
        if (_value !== undefined) {
            let value = textOM.OnlyLetter(_value);
            $(this).text(value);
        }
        _value = $(this).html();
        if (_value !== undefined) {
            let value = textOM.OnlyLetter(_value);
            $(this).text(value);
        }
    });

    $('[om-without-space]').each(function () {
        let _value = $(this).val();
        if (_value !== undefined) {
            let value = textOM.WithoutSpaces(_value);
            $(this).val(value);
        }
        _value = $(this).text();
        if (_value !== undefined) {
            let value = textOM.WithoutSpaces(_value);
            $(this).text(value);
        }
        _value = $(this).html();
        if (_value !== undefined) {
            let value = textOM.WithoutSpaces(_value);
            $(this).text(value);
        }
    });

    $('[om-current]').each(function () {
        let _value = $(this).val();
        if (_value !== undefined) {
            let value = numberOM.MoneyDecimal(_value, '');
            $(this).val(value);
        }
        _value = $(this).text();
        if (_value !== undefined) {
            let value = numberOM.MoneyDecimal(_value, '');
            $(this).text(value);

        }

        _value = $(this).html();
        if (_value !== undefined) {
            let value = numberOM.MoneyDecimal(_value, '');
            $(this).text(value);
        }

    });


    $('[om-decimal]').each(function () {
        let _value = $(this).val();

        if (_value !== undefined) {
            let value = numberOM.Decimales(_value);
            $(this).val(value);
        }
        _value = $(this).text();
        if (_value !== undefined) {
            let value = numberOM.Decimales(_value);
            $(this).text(value);

        }

        _value = $(this).html();
        if (_value !== undefined) {
            let value = numberOM.Decimales(_value);
            $(this).text(value);
        }

    });

    $('[om-number]').each(function () {
        let _value = $(this).val();
        let _maxValue = $(this).attr('om-max-value');

        if (_value !== undefined) {
            let value = numberOM.Entero(_value, false, false, _maxValue);
            $(this).val(value);
        }
        _value = $(this).text();
        if (_value !== undefined) {
            let value = numberOM.Entero(_value, false, false, _maxValue);
            $(this).text(value);

        }

        _value = $(this).html();
        if (_value !== undefined) {
            let value = numberOM.Entero(_value, false, false, _maxValue);
            $(this).text(value);
        }

    });
})();

//Directive

    $('[om-number]').on('input', function () {
        let _maxValue = $(this).attr('om-max-value');
        if(!_maxValue) _maxValue = 0;

        let value = numberOM.Entero($(this).val(),false,false, _maxValue);
        let text = numberOM.Entero($(this).text(),false, false, _maxValue);
        $(this).val(value);
        $(this).text(text);
    });

    $('[om-capitalize]').on('input', function () {
        let value = textOM.Capitalize($(this).val());
        let text = textOM.Capitalize($(this).text());
        $(this).val(value);
        $(this).text(text);
    });

    $('[om-only-letter]').on('input', function () {
        let value = textOM.OnlyLetter($(this).val());
        let text = textOM.OnlyLetter($(this).text());
        $(this).val(value);
        $(this).text(text);
    });

    $('[om-without-space]').on('input', function () {
        let value = textOM.WithoutSpaces($(this).val());
        let text = textOM.WithoutSpaces($(this).text());
        $(this).val(value);
        $(this).text(text);
    });
    $('[om-current]').on('input', function () {
        let value = numberOM.MoneyDecimal($(this).val(), '');
        let text = numberOM.MoneyDecimal($(this).text(), '');
        $(this).val(value);
        $(this).text(text);
    });

    $('[om-decimal]').on('input', function () {
        let value = numberOM.Decimales($(this).val());
        let text = numberOM.Decimales($(this).text());
        $(this).val(value);
        $(this).text(text);
    });

    $('[om-select-all]').on('focus', function () {
        $(this).select()
    })


})



