
let decimalSimbolo = '.';
export class numberOM {
    static Entero(pValor, eliminarCeros = false, prefix = false, maxValue = 0 ){
        if (pValor){
            let vResultado = '';
            if (prefix){
                const arrayValor = pValor.split('');
                for (const value of arrayValor) {
                    if (value === decimalSimbolo){
                        break;
                    } else {
                        vResultado += value;
                    }
                }
            } else {
                vResultado = pValor;
            }

            if (eliminarCeros){
                vResultado = vResultado.replace(/[^\d]+/g, (w) => (w.replace(/[^\d]+/g, ''))).replace(/^0+/, (x) => (x.replace(/^0+/, '')));
            } else {
                vResultado = (vResultado || '0').replace(/[^\d]+/g, (w) => (w.replace(/[^\d]+/g, '')));
            }
            if(maxValue > 0 && parseInt(vResultado) > maxValue){
                vResultado = maxValue;
            }
            return vResultado;
        }
    }
    static EnteroNegativo(pValor, eliminarCeros = false, prefix = false){
        if (this.Entero(pValor)){
            let state;
            let val;
            const less = (pValor.match(/[-]/g) || []).length;
            if (less === 1){
                state = true;
                val = pValor.replace(/[-]/g, (w) => (w.replace(/[-]/g, '')));
            } else {
                state = false;
                val = pValor;
            }
            val = this.Entero(val, eliminarCeros, prefix);
            if (state){
                val = '-' + val;
            }
            return val;
        } else {
            return '';
        }
    }
    static Decimales(pValor, cantidadDecimales = 2, focusOut = false, eliminarCeros = true, separadorDecimal = '.'){
        if (pValor){
            decimalSimbolo = separadorDecimal;
            pValor = pValor === separadorDecimal ? '0' + separadorDecimal : pValor;
            const separador = new RegExp('[' + separadorDecimal + ']', 'g');
            if (pValor.match(separador)){
                const arrayValue = pValor.split('');
                let valorIzquierdo = '';
                let valorDerecho = '';
                let finish = false;
                let vResult;
                for (const value of arrayValue) {
                    if (value !== separadorDecimal && !finish){
                        valorIzquierdo += value;
                    } else {
                        finish = true;
                        valorDerecho += value;
                    }
                }
                valorDerecho = valorDerecho.slice(0, cantidadDecimales + 1);
                if (focusOut){
                    vResult = this.Entero(valorIzquierdo || '0', false) + separadorDecimal + this.Entero(valorDerecho.padEnd(cantidadDecimales + 1, '0'));
                } else {
                    vResult = this.Entero(valorIzquierdo || '0', false) + separadorDecimal + this.Entero(valorDerecho);
                }
                // return parseInt(vResult) > 0 ? vResult : '';
                return vResult;
            } else {
                return this.Entero(pValor || '', eliminarCeros);
            }
        } else {
            return '';
        }
    }
    static DecimalesNegativo(pValor, cantidadDecimales = 2, focusOut = false, eliminarCero = true, separadorDecimal = '.'){
        decimalSimbolo = separadorDecimal;
        pValor = pValor === separadorDecimal ? '0' + separadorDecimal : pValor;
        if (this.Entero(pValor)){
            let state;
            let val;
            const less = (pValor.match(/[-]/g) || []).length;
            if (less === 1){
                state = true;
                val = pValor.replace(/[-]/g, (w) => (w.replace(/[-]/g, '')));
            } else {
                state = false;
                val = pValor;
            }
            val = this.Decimales(val, cantidadDecimales, focusOut, eliminarCero, separadorDecimal);
            if (state){
                val = '-' + val;
            }
            return val;
        } else {
            return '';
        }
    }
    static Money(pValor, simbolo = '$', separadorMilesimas = ',', eliminarCeros = true){
        if (this.Entero(pValor)){
            const arrayListNumber = this.Entero(pValor || '0', eliminarCeros, true).split('');
            let m = 1;
            const arrayInvert = [];
            let vResultado = '';
            for (const value of arrayListNumber.reverse()){
                if (m > 3 && (m - 1) % 3 === 0){
                    arrayInvert.push(separadorMilesimas);
                    arrayInvert.push(value);
                } else {
                    arrayInvert.push(value);
                }
                m++;
            }
            for (const value of arrayInvert.reverse()){
                vResultado += value;
            }
            if (vResultado){
                return simbolo + vResultado;
            } else {
                return '';
            }
        } else {
            return '';
        }
    }
    static MoneyNegativo(pValor, simbolo = '$', separadorMilesimas = ','){
        if (this.Entero(pValor)){
            let state;
            let val;
            const less = (pValor.match(/[-]/g) || []).length;
            if (less === 1){
                state = true;
                val = pValor.replace(/[-]/g, (w) => (w.replace(/[-]/g, '')));
            } else {
                state = false;
                val = pValor;
            }
            val = this.Money(val, simbolo, separadorMilesimas);
            if (state){
                val = '-' + val;
            }
            return val;
        } else {
            return '';
        }
    }
    static MoneyDecimal(pValor, simbolo = '$', cantidadDecimales = 2, focusOut = false, eliminarCeros = true, separadorMilesima = ',', separadorDecimal = '.'){
        if (pValor){
            decimalSimbolo = separadorDecimal;
            const separador = new RegExp('[' + separadorDecimal + ']', 'g');
            if (pValor.match(separador)){
                const arrayValue = pValor.split('');
                let valorIzquierdo = '';
                let valorDerecho = '';
                let finish = false;
                let vResult;
                for (const value of arrayValue) {
                    if (value !== separadorDecimal && !finish){
                        valorIzquierdo += value;
                    } else {
                        finish = true;
                        valorDerecho += value;
                    }
                }
                valorDerecho = valorDerecho.slice(0, cantidadDecimales + 1);
                if (focusOut){
                    vResult = this.Money(valorIzquierdo || '0', simbolo, separadorMilesima, false) + separadorDecimal + this.Entero(valorDerecho.padEnd(cantidadDecimales + 1, '0'));
                } else {
                    vResult = this.Money(valorIzquierdo || '0', simbolo, separadorMilesima, false) + separadorDecimal + this.Entero(valorDerecho);
                }
                return vResult;
            } else {
                return this.Money(pValor || '', simbolo, separadorMilesima);
            }
        } else {
            return '';
        }
    }
    static MoneyDecimalNegativo(pValor, simbolo = '$', cantidadDecimales = 2, focusOut = false, eliminarCeros = true, separadorMilesima = ',', separadorDecimal = '.'){
        decimalSimbolo = separadorDecimal;
        pValor = pValor === separadorDecimal ? '0 ' + separadorDecimal : pValor;
        if (this.Entero(pValor)){
            let state;
            let val;
            const less = (pValor.match(/[-]/g) || []).length;
            if (less === 1){
                state = true;
                val = pValor.replace(/[-]/g, (w) => (w.replace(/[-]/g, '')));
            } else {
                state = false;
                val = pValor;
            }
            val = this.MoneyDecimal(val, simbolo, cantidadDecimales, focusOut, eliminarCeros, separadorMilesima, separadorDecimal);
            if (state){
                val = '-' + val;
            }
            return val;
        } else {
            return '';
        }
    }
    static replaceSeparadorOldForNew(pValor, separadorOld, separadorNew){
        if (pValor){
            return pValor.replace(separadorOld, (w) => (w.replace(separadorOld, separadorNew)));
        } else {
            return '';
        }
    }
    static Porciento(pValor){
        if (pValor){
            return this.Money(pValor, '%', '');
        } else {
            return '';
        }
    }
    static PorcientoNegativo(pValor){
        if (pValor){
            return this.MoneyNegativo(pValor, '%', '');
        } else {
            return '';
        }
    }
    static PorcientoDecimal(pValor, cantidadDecimal = 2, focusOut = false, eliminarCeros = true, separadorMilesima = ',', separadorDecimal = '.'){
        if (pValor){
            return this.MoneyDecimal(pValor, '%', cantidadDecimal, focusOut, eliminarCeros, separadorMilesima, separadorDecimal);
        } else {
            return '';
        }
    }
    static PorcientoDecimalNegativo(pValor, cantidadDecimal = 2, focusOut = false, eliminarCeros = true, separadorMilesima = ',', separadorDecimal = '.'){
        if (pValor){
            return this.MoneyDecimalNegativo(pValor, '%', cantidadDecimal, focusOut, eliminarCeros, separadorMilesima, separadorDecimal);
        } else {
            return '';
        }
    }
}
