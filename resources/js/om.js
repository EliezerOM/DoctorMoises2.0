function rippleEffect(event) {
    const _element = event.currentTarget;
    const circle = document.createElement("span");
    const diameter = Math.max(_element.clientWidth, _element.clientHeight);
    const radius = diameter / 2;

    circle.style.width = circle.style.height = `${diameter}px`;
    circle.style.left = `${event.clientX - (_element.offsetLeft + radius)}px`;
    circle.style.top = `${event.clientY - (_element.offsetTop + radius)}px`;
    circle.classList.add("ripple");

    const ripple = _element.getElementsByClassName("ripple-om")[0];

    if (ripple) {
        ripple.remove();
    }

    _element.appendChild(circle);
}

const _els = document.getElementsByClassName("ripple-om");
for (let i = 0; i < _els.length; i++) {
    _els[i].addEventListener("click", rippleEffect)
}

//Agrega clases al input y label de la clase form-control-om-floating
const control_floating = document.getElementsByClassName("form-control-om-floating");
for (let i = 0; i < control_floating.length; i++) {

    control_floating[i].querySelectorAll('input')[0].classList.add('peer');
    control_floating[i].querySelectorAll('label')[0].classList.add('peer-placeholder-shown:text-base');
    control_floating[i].querySelectorAll('label')[0].classList.add('peer-placeholder-shown:text-blue-400');
    control_floating[i].querySelectorAll('label')[0].classList.add('peer-placeholder-shown:top-2');
    control_floating[i].querySelectorAll('label')[0].classList.add('peer-focus:-top-3.5');
    control_floating[i].querySelectorAll('label')[0].classList.add('peer-focus:text-blue-600');
    control_floating[i].querySelectorAll('label')[0].classList.add('peer-focus:text-sm');
    control_floating[i].querySelectorAll('label')[0].classList.add('peer-focus:font-semibold');
}
