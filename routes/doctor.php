<?php

use App\Http\Livewire\MainDoctor;

Route::middleware(['verified','doc'])->group(function () {
    Route::get('/doctor', MainDoctor::class)->name('doctor.main');
});
