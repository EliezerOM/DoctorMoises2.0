<?php

use App\Http\Livewire\Patient\Configuration;
use App\Http\Livewire\Patient\Dashboard;
use App\Http\Livewire\Patient\PasswordChange;
use App\Http\Livewire\Patient\PersonalInformation;
use App\Http\Livewire\Patient\PersonalSchedule;
use App\Http\Livewire\Patient\ReportProblem;
use App\Http\Livewire\Patient\SecurityPrivacy;
use App\Http\Livewire\Patient\SubscriptionPayments;
use App\Http\Livewire\Patient\Notification;


Route::middleware(['verified','pat'])->group(function () {
    Route::get('/patient/dashboard', Dashboard::class)->name('patient.main');
    Route::get('/patient/notification', Notification::class)->name('patient.notification');
    Route::get('/patient/password-change', PasswordChange::class)->name('patient.password-change');
    Route::get('/patient/configuration', Configuration::class)->name('patient.configuration');
    Route::get('/patient/report-problem', ReportProblem::class)->name('patient.report-problem');
    Route::get('/patient/personal-schedule', PersonalSchedule::class)->name('patient.personal-schedule');
    Route::get('/patient/personal-information', PersonalInformation::class)->name('patient.personal-information');
    Route::get('/patient/security-privacy', SecurityPrivacy::class)->name('patient.security-privacy');
    Route::get('/patient/subscripcion-payments', SubscriptionPayments::class)->name('patient.subscripcion-payments');
});
