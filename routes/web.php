<?php

use App\Http\Livewire\ContactUs;
use App\Http\Livewire\EmailVerification;
use App\Http\Livewire\ForgotPassword;
use App\Http\Livewire\Home;
use App\Http\Livewire\Login;
use App\Http\Livewire\Logout;
use App\Http\Livewire\MainDoctor;
use App\Http\Livewire\MainPatient;
use App\Http\Livewire\RecoverPassword;
use App\Http\Livewire\Register;
use App\Http\Livewire\ResetPassword;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::get('/', Home::class)->name('home');
Route::get('/login', Login::class)->name('login')->middleware('guest');
Route::get('/register/{tipo_usuario?}', Register::class)->name('register')->middleware('guest');
Route::get('/forgot-password', ForgotPassword::class)->name('forgot-password')->middleware('guest');
Route::get('/reset-password/{token}&{email}', ResetPassword::class)->name('reset.password');
Route::get('/contact', ContactUs::class)->name('contact.us');
Route::get('/offline', function(){
    return view('vendor.laravelpwa.offline');
});

//Crear el link del storage [patients & doctors]
Route::get('storage-link', function () {
    $path_patients = public_path('storage') . '/patients';
    $path_doctors = public_path('storage') . '/doctors';
    if (file_exists($path_doctors) && file_exists($path_patients)) {
        return 'The "public/storage/patients" or "public/storage/doctors" directory already exists.';
    }

    app('files')->link(
        storage_path('app/public'), public_path('storage')
    );
    if (!File::isDirectory($path_patients)) {
        File::makeDirectory($path_patients);
    }
    if (!File::isDirectory($path_doctors)) {
        File::makeDirectory($path_doctors);
    }
    return 'The [public/storage/patients] & [public/storage/doctors] directory has been linked';
});

Route::middleware(['auth'])->group(function () {
    Route::get('/verification-email', EmailVerification::class)->name('verification-email')->middleware('guest');
    Route::get('/logout', [Logout::class, 'logout'])->name('logout');
    //Paciente
    include __DIR__ . '\patient.php';

    //Doctor
    include __DIR__ . '\doctor.php';

});

