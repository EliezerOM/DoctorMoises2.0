<?php

use App\Models\Doctor;
use App\Models\EspecialidadDoctor;
use App\Models\Notificacion;
use App\Models\Paciente;
use Illuminate\Support\Facades\DB;
if(!function_exists('isExternal')){
    function isExternal($url) {
        $components = parse_url($url);
        if ( empty($components['host']) ) return false;  // we will treat url like '/relative.php' as relative
        if ( strcasecmp($components['host'], 'example.com') === 0 ) return false; // url host looks exactly like the local host
        return strrpos(strtolower($components['host']), '.example.com') !== strlen($components['host']) - strlen('.example.com'); // check if the url host is a subdomain
    }
}

if (!function_exists('fullName')) {
    function fullName()
    {
        $user = auth()->user();
        if (isset($user->patient)) {
            return $user->patient->nombres . ' ' . $user->patient->apellidos;
        } else {
            return $user->doctor->nombres . ' ' . $user->doctor->apellidos;
        }
    }
}

if (!function_exists('infoPaciente')) {
    function infoPaciente()
    {
        $user = auth()->user();
        if($user === null){
            return null;
        }
        if ($user->patient) {
            return $user->patient;
        }
    }
}

if (!function_exists('infoDoctor')) {
    function infoDoctor()
    {
        $user = auth()->user();
        if($user === null){
            return null;
        }
        if ($user->doctor) {
            return $user->doctor;
        }

    }
}

if (!function_exists('typeDate')) {
    function typeDate($value)
    {
        return \Carbon\Carbon::parse($value)->isoFormat('DD/MM/YYYY');
    }
}
if (!function_exists('typeHour')) {
    function typeHour($value)
    {
        return \Carbon\Carbon::parse($value)->isoFormat('hh:mm a');
    }
}

if (!function_exists('infoDoctorOrPaciente')) {
    function infoDoctorOrPaciente()
    {
        $user = auth()->user();
        if ($user->patient) {
            return $user->patient;
        } else {
            return $user->doctor;
        }
    }
}
if (!function_exists('person')) {
    function person()
    {
        if(auth()->check()) {
            $user = auth()->user();
            if ($user->patient) {
                return $user->patient;
            } else if ($user->doctor) {
                return $user->doctor;
            } else {
                return $user->doctor;
            }
        }
    }
}
if(!function_exists('listRoute')){
    function listRoute(){
        return ['home', 'register', 'login', 'forgot-password', 'reset.password', 'contact.us', 'verification-email'];
    }
}


if (!function_exists('`typeUser`')) {
    function typeUser()
    {
        $user = auth()->user();
        if (!$user) {
            return null;
        }
        if ($user->patient) {
            return 'pat';
        } else {
            return 'doc';
        }
    }
}

if (!function_exists('doctores')) {
    function doctores()
    {
        $doctores = Doctor::where(['doctor_valido' => 'S', 'status' => 'A'])->whereExists(function ($query) {
            $query->select(DB::raw(1))->from('centro_doctor')->whereColumn('id_doctor', 'doctores.id')->whereExists(function ($query2) {

                $query2->select(DB::raw(1))->from('centros_medicos')->where('status', 'A')->whereColumn('id', 'centro_doctor.id_centro')->whereExists(function ($query3) {
                    $query3->select(DB::raw(1))->from('horarios')->where('status', 'A')->whereColumn('id_centro_doctor', 'centro_doctor.id');
                });
            });
        })->get();
        return $doctores;
    }
}

if (!function_exists('doctoresQuery')) {
    function doctoresQuery()
    {
        $doctores = Doctor::query();
        return $doctores;
    }
}

if (!function_exists('pacientes')) {
    function pacientes()
    {
        $paciente = Paciente::where(['status' => 'A'])->get();
        return $paciente;
    }
}


if (!function_exists('pacientesQuery')) {
    function pacientesQuery()
    {
        $paciente = Paciente::query();
        return $paciente;
    }
}

if (!function_exists('notificacionesCountPaciente')) {
    function notificacionesCountPaciente()
    {
        $paciente = auth()->user()->patient;
        $notificaciones = Notificacion::where(['id_paciente' => $paciente->id, 'ver_paciente' => 'S', 'doctor_recibido' => 'N', 'status' => 'S']);
        return $notificaciones->count();
    }
}

if (!function_exists('notificacionesCountDoctor')) {
    function notificacionesCountDoctor()
    {
        $doctor = auth()->user()->doctor;
        $notificaciones = Notificacion::where(['id_doctor' => $doctor->id, 'ver_doctor' => 'S', 'doctor_recibido' => 'S', 'status' => 'S']);
        return $notificaciones->count();
    }

}

if (!function_exists('notificacionesCount')) {
    function notificacionesCount()
    {
        if (typeUser() == 'paciente') {
            return notificacionesCountPaciente();
        } else {
            return notificacionesCountDoctor();
        }

    }

}

if (!function_exists('especialidades')) {
    function especialidades()
    {
        $especialidades = EspecialidadDoctor::where('status', 'A')->select('id_especialidad')->distinct('id_especialidad')->get();
        return $especialidades;
    }

}

if (!function_exists('getUser')) {
    function getUser()
    {
        return auth()->user();
    }

}

if (!function_exists('getLang')) {
    function getLang()
    {
        $user = auth()->user();
        if ($user) {
            return $user->idioma;
        } else {
            return 'en';
        }
    }

}
if (!function_exists('percentColumn')) {
    function percentColumn($profile)
    {
        if (!$profile) {
            return 0;
        }
        $columns = preg_grep('/(.+ed_at)|(^.+_id$)|(^id_)|(^id$)/', array_keys($profile->toArray()), PREG_GREP_INVERT);
        $per_total = count($columns);
        $per_dif = 0;
        foreach ($profile->toArray() as $key => $value) {
            if ($value === NULL || $value === [] && in_array($key, $columns)) {
                $per_dif++;
            }
        }
        return round(100 - (($per_dif * 100) / $per_total), 2);
    }
}
