<?php

namespace App\Traits;

 use Illuminate\Database\Eloquent\Model;

 class  Notify
{
    /**
     * Mensaje de la notificación
     * @var string
     */
    public $message;

    /**
     * Tipos a recibir 'info', 'success', 'danger', 'warning', 'error', 'complete'. Default value 'info'.
     * @var string
     */
    public $type = 'info';

    /**
     * Tipos a recibir 'tc' = Top-Center, 'tr' = Top-Right, 'tl' = Top-Left, 'bc' = Bottom-Center, 'bl' = Bottom-Left, 'br' = Bottom-Right. Default value 'tc'.
     * @var string
     */
    public $position = 'tc';

    /**
     * Tiempo en el display del cliente. Default value 3000.
     * @var int
     */
    public $duration = 4000;

    /**
     * Tiempo que tarda para mostrarse en el cliente. Default value 10.
     * @var int
     */
    public $delay = 10;

    /**
     * Mantiene activa la notificación en el cliente hasta que cierre manual. Default false.
     * @var bool
     */
    public $persist = false;

    /**
     * Indica un desplazamiento cuando es mostrado en el cliente de coordenadas X o Y. Default y.
     * @var string
     */
    public $animate = 'y';

    /**
     * Muestra el botón de aceptar
     * @var bool
    */
    public $showButtonAceptar;

    /**
     * Muestra el botón de cancelar
     * @var bool
    */
    public $showButtonCancelar;

    /**
     * Texto del botón aceptar
     * @var string
    */
    public $textButtonAceptar;

    /**
     * Texto del botón Cancelar
     * @var string
    */
    public $textButtonCancelar;

    /**
     * Enlace que se attach en la solicitud al dar click se activa el enlace
     * @var string
    */
    public $notificationLink = '#';

    /**
     * Texto que muestra el enlace attach, default 'Enlace'
     *@var string Texto del enlace attach
    */
    public $textNotificationLink = 'Enlace';

    /**
     * Abre una nueva pestaña
     * @var bool
     */
    public $linkBlank = false;

    /**
     * Enlace que attach al botón Aceptar
     * @var string
    */
    public $linkButtomAceptar;

    /**
     * Enlace que attach al botón Caccelar
     * @var string
    */
    public $linkButtomCancelar;

    /**
    * @var string
     */
    public $onAceptar;

    /**
     * @var mixed
    */
    public $parameterAceptar;

    /**
     * @var string
     */
    public $onCancelar;

    /**
     * @var mixed
    */
    public $parameterCancelar;

    /**
     * @var string
    */
    public $onClose;

    /**
     * Muestra el botón de cerrar la notificación
     * @var bool
    */
    public $showButtonClose;

    /**
     * Tipo de notificación en cuanto a presentación
     * @var string default 'toast', 'toast' & 'confirm'
    */
    public $typeNotification = 'toast';

    /**
    * Id de la notificación
     * @var int
     */
    public $id;

    /**
    * Permite cerrar el dialog-x aun este persistente al dar click fuera del objeto notify
     * @var bool true
     */
    public $outside = false;


    public $isTextComment = false;
    public $textComment;
    public $textTitleComment = 'Comentario';
    public $tagsComment = [];
}

trait WithNotifyTrait
{

    private $notifications = [];

    /**
    * @var Notify $notify
     */
    protected $notify;


    /**
     * @var bool $keep Conserva las notificaciones, Default false.
    */
    public $keep = false;

    /**
     * Envía notificaciones al cliente por el navegador haciendo uso de dispatchBrowserEvent de Livewire
     * @param Notify $notify
     * @return $this
     */
    private $idNotify;
    public function GetId(){
        $this->idNotify = uniqid('notify_');
        return $this->idNotify;
    }
    public function addNotify(Notify $notify)
    {
        $notify->typeNotification = $notify->typeNotification ??'toast';
        $notify->id = $this->idNotify == null ? $this->GetId(): $this->idNotify;
        $notify->position = strtolower(($notify->position ?? 'tc'));
        $notify->type = strtolower(($notify->type ?? 'info'));
        $notify->animate = strtolower(($notify->animate ?? 'y'));
        $notify->delay = $notify->delay ?? 10;
        $notify->persist = $notify->persist ?? false;
        $notify->duration = $notify->duration ?? 3000;

        if(!isset($notify->showButtonAceptar) && $notify->textButtonAceptar ){
            $notify->showButtonAceptar = true;
        }

        if($notify->showButtonAceptar && !isset($notify->textButtonAceptar)){
            $notify->textButtonAceptar = 'Aceptar';
        }

        if($notify->showButtonCancelar && !isset($notify->textButtonCancelar)){
            $notify->textButtonCancelar = 'Cancelar';
        }
        if(!isset($notify->showButtonCancelar) && $notify->textButtonCancelar){
            $notify->showButtonCancelar = true;
        }


        $noti = array('position' => $notify->position, 'notifications' =>
            array(
                array('id'=>$notify->id,
                    'type' => $notify->type,
                    'message' => $notify->message,
                    'position' => $notify->position,
                    'delay' => $notify->delay,
                    'persist' => $notify->persist,
                    'duration' => $notify->duration,
                    'animate' => $notify->animate,
                    'textButtonAceptar' => $notify->textButtonAceptar,
                    'showButtonAceptar' => $notify->showButtonAceptar,
                    'showButtonCancelar' => $notify->showButtonCancelar,
                    'textButtonCancelar' =>$notify->textButtonCancelar,
                    'showButtonClose' => $notify->showButtonClose,
                    'typeNotification' => $notify->typeNotification,
                    'onAceptar' => $notify->onAceptar,
                    'onCancelar' => $notify->onCancelar,
                    'parameterCancelar' => $notify->parameterCancelar,
                    'parameterAceptar' => $notify->parameterAceptar,
                    'outside' => $notify->outside,
                    'textTitleComment'=>$notify->textTitleComment,
                    'textComment'=>$notify->textComment,
                    'isTextComment'=>$notify->isTextComment,
                    'tagsComment' => $notify->tagsComment,
                )));

        if (count($this->notifications) <= 0) {
            $this->notifications[] = $noti;
        } else {
            $exists_position = false;
            foreach ($this->notifications as &$item) {

                if (($item['position'] ?? 'tc') === $notify->position) {
                    $exists_position = true;
                    array_push($item['notifications'],
                        ['id'=>$notify->id,
                            'type' => $notify->type,
                            'message' => $notify->message,
                            'position' => $notify->position,
                            'delay' => $notify->delay,
                            'persist' => $notify->persist,
                            'duration' => $notify->duration,
                            'animate' => $notify->animate,
                            'textButtonAceptar' => $notify->textButtonAceptar,
                            'showButtonAceptar' => $notify->showButtonAceptar,
                            'showButtonCancelar' => $notify->showButtonCancelar,
                            'textButtonCancelar' =>$notify->textButtonCancelar,
                            'showButtonClose' => $notify->showButtonClose,
                            'typeNotification' => $notify->typeNotification,
                            'onAceptar' => $notify->onAceptar,
                            'onCancelar' => $notify->onCancelar,
                            'parameterCancelar' => $notify->parameterCancelar,
                            'parameterAceptar' => $notify->parameterAceptar,
                            'outside' => $notify->outside,
                            'textTitleComment'=>$notify->textTitleComment,
                            'textComment'=>$notify->textComment,
                            'isTextComment'=>$notify->isTextComment,
                            'tagsComment'=>$notify->tagsComment,
                        ]);

                }
            }
            if (!$exists_position) {
                array_push($this->notifications, $noti);
            }
        }
        return $this;
    }
    /**
     * Método que envió las notificaciones en cola.
    */
    public function sendNotify()
    {
        if(count($this->notifications) <= 0 && isset($this->notify->message)){
            $this->addNotify($this->notify);
        }
        $this->dispatchBrowserEvent('alert-toast', $this->notifications);
        if (!$this->keep) {
          $this->resetNotify();
        }
        $this->id =  null;
    }
    public function selfNotify(string $message){
        $this->notify->message = $message;
        $this->addNotify($this->notify);
        $this->sendNotify();
    }

    public function resetNotify()
    {
        $this->notifications = [];
    }
}

