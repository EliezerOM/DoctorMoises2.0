<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use PhpParser\Node\Attribute;

class Doctor extends Model
{
    use hasFactory;

    //
    protected $table = 'doctores';
    protected $fillable = ['id_usuario', 'nombres', 'apellidos', 'fecha_nacimiento', 'sexo'];

    public function doctor_speciality()
    {
        return $this->hasMany(EspecialidadDoctor::class, 'id_doctor', 'id');
    }

    public function doctor_center()
    {
        return $this->hasMany(CentroDoctor::class, 'id_doctor', 'id');
    }

    public function user()
    {
        return $this->hasOne(User::class, 'id', 'id_usuario');
    }

    public function getQualificationAttribute()
    {

        //Buscar todas las citas con valoración al doctor actual.
        $citas = Cita::whereExists(function ($q) {
            $q->select(DB::raw(1))
                ->from('horarios')
                ->whereColumn('horarios.id', 'citas.id_horario')
                ->whereExists(function ($q2) {
                    $q2->select(DB::raw(1))
                        ->from('centro_doctor')
                        ->whereColumn('centro_doctor.id', 'horarios.id_centro_doctor')
                        ->whereExists(function ($q3) {
                            $q3->select(DB::raw(1))
                                ->from('especialidad_doctor')
                                ->whereColumn('especialidad_doctor.id', 'centro_doctor.id_especialidad_doctor')
                                ->where('id_doctor', $this->id);
                        });
                });
        })->where('valoracion', '>=', 0)
            ->where('valoracion', '<>', '')
            ->whereNotNull('valoracion');

        //Retornar el promedio
        $qualification = $citas->avg('valoracion');
        return round($qualification, 2);
    }
}
