<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CentroMedico extends Model
{
    //
    protected $table = 'centros_medicos';
    protected $fillable =
        ['nombre', 'ciudad', 'direccion', 'rnc', 'cedula','nota','telefono','telefono2','email','whatsapp','instagram','facebook','web','provincia','municipio','distrito',
            'seccion','barrio','sub_barrio','zona','sector','ano_apertura','ano_remodelacion','coordenadas','nivel_atencion','extension_territorial','complejidad_servicio',
            'tipo_centro','srs','pertenencia','cantidad_unap'];

    public function doctor_center(){
        return $this->hasMany(CentroDoctor::class, 'id_centro', 'id');
    }

}
