<?php

namespace App\Models;

use App\Models\Doctor;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class CentroDoctor extends Model
{
    use hasFactory;
    //
    protected $table = 'centro_doctor';
    protected $fillable = ['id_centro', 'id_especialidad_doctor', 'extension', 'telefono', 'flota', 'edificio', 'planta', 'consultorio', 'nota', 'valor_cita', 'id_usuario'];

    public function center(){
        return $this->hasOne(CentroMedico::class, 'id', 'id_centro');
    }
    public function speciality_doctor(){
        return $this->hasOne(EspecialidadDoctor::class, 'id', 'id_especialidad_doctor');
    }
    public function  schedules(){
        return $this->hasMany(Horario::class, 'id_centro_doctor', 'id');
    }

    public function center_doctor_insurance(){
        return $this->hasMany(CentroDoctorSeguro::class, 'id_centro_doctor', 'id');
    }


}
