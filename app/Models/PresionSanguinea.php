<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class PresionSanguinea extends Model
{
    use HasFactory;
    protected $fillable = ['sistolica', 'diastolica', 'id_usuario'];
}
