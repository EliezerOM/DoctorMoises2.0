<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Seguros extends Model
{
    use HasFactory;

    public function type_insurance(){
        return $this->hasOne(TipoSeguro::class, 'id_tipo_seguro', 'id');
    }
}
