<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Paciente extends Model
{
    use HasFactory;
    //
    protected $table = 'pacientes';
    protected $fillable = ['id_usuario', 'nombres', 'apellidos', 'fecha_nacimiento', 'sexo'];

//    public function provincia()
//    {
//        return $this->belongsTo('App\Provincia', 'id_provincia', 'id');
//    }
//
    public function user()
    {
        return $this->hasOne(User::class, 'id', 'id_usuario');
    }
    public function schedule(){
        return $this->hasMany(Cita::class, 'id_paciente', 'id');
    }
    public function fullName()
    {
        return $this->nombres.' '.$this->apellidos;
    }
}
