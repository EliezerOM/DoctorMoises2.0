<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Especialidad extends Model
{
    //
    protected  $table = "especialidades";

    public function type_speciality(){
        return $this->hasOne(TipoEspecialidad::class,  'id','id_tipo_especialidad');
    }
    public function speciality_doctor(){
        return $this->hasMany(EspecialidadDoctor::class, 'id_especialidad', 'id');
    }
    public function doctor_center(){
        return $this->hasMany(CentroDoctor::class, 'id_especialidad', 'id');
    }
}
