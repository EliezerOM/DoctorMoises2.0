<?php

namespace App\Models;

use Illuminate\Auth\Notifications\ResetPassword as ResetPasswordNotification;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class EspecialidadDoctor extends Model
{
    use hasFactory;
    //
    protected  $table = "especialidad_doctor";

    public function doctor()
    {
        return $this->hasOne(Doctor::class, 'id', 'id_doctor');
    }
    public function  speciality(){
        return $this->hasOne(Especialidad::class, 'id', 'id_especialidad');
    }
}

