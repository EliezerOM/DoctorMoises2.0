<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Provincia extends Model
{
    //

    public function provincia()
    {
        return $this->hasMany(Paciente::class, 'id_provincia', 'id');
    }
}
