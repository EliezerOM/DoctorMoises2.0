<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class TipoSeguro extends Model
{
    use HasFactory;

    public function ars(){
        return $this->hasOne(Ars::class, 'id', 'id_ars');
    }
}
