<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Horario extends Model
{
    use hasFactory;
    //

    protected $table = 'horarios';
    protected $fillable = ['tanda', 'id_usuario', 'hora_inicio','hora_fin', 'id_centro_doctor', 'dia','cantidad_maxima'];

    public function centro_doctor(){
        return $this->hasOne(CentroDoctor::class, 'id', 'id_centro_doctor');
    }

    public function users()
    {
        return $this->hasOne(User::class, 'id', 'id_usuario');
    }
}
