<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;


class Cita extends Model
{
    use \Znck\Eloquent\Traits\BelongsToThrough;
    use \Znck\Eloquent\Traits\HasTableAlias;
    use HasFactory;

    protected $table = 'citas';
    protected $relations = [];
    protected $fillable = ['id_paciente', 'id_doctor', 'id_centro', 'id_especialidad', 'id_horario', 'valor_cita', 'fecha', 'status', 'id_usuario'];

    public function doctor()
    {
        return $this->belongsToThrough(
            Doctor::class,
            [EspecialidadDoctor::class, CentroDoctor::class, Horario::class],
            null,
            '',
            [
                CentroDoctor::class => 'id_centro_doctor',
                Horario::class => 'id_horario',
                EspecialidadDoctor::class => 'id_especialidad_doctor',
                Doctor::class => 'id_doctor',
            ]
        );
    }

    public function speciality()
    {
//        return $this->belongsTo(Especialidad::class, 'id_especialidad', 'id');
        return $this->belongsToThrough(
            Especialidad::class,
            [EspecialidadDoctor::class, CentroDoctor::class, Horario::class],
            null,
            '',
            [
                CentroDoctor::class => 'id_centro_doctor',
                Horario::class => 'id_horario',
                EspecialidadDoctor::class => 'id_especialidad_doctor',
                Especialidad::class => 'id_especialidad',
            ]
        );
    }

    public function center()
    {
        return $this->belongsToThrough(CentroMedico::class,
            [CentroDoctor::class, Horario::class],
            null,
            '',
            [CentroDoctor::class => 'id_centro_doctor',
                Horario::class => 'id_horario',
                CentroMedico::class => 'id_centro'
            ]);
    }

    public function center_doctor()
    {
        return $this->belongsToThrough(CentroDoctor::class,
            [Horario::class],
            null,
            '',
            [CentroDoctor::class => 'id_centro_doctor',
                Horario::class => 'id_horario',
            ]);
    }

    public function patient()
    {
        return $this->belongsTo(Paciente::class, 'id_paciente', 'id');
    }

    public function schedule()
    {
        return $this->belongsTo(Horario::class, 'id_horario', 'id');
    }


}
