<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class FrecuenciaCardiaca extends Model
{
    use HasFactory;
    protected $fillable = ['minima', 'maxima', 'id_usuario'];
}
