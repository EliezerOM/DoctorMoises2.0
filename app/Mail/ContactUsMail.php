<?php

namespace App\Mail;

use Illuminate\Mail\Mailable;

class ContactUsMail extends Mailable
{
    public $email;
    public $comment;
    public function __construct($comment, $email)
    {
        $this->comment = $comment;
        $this->email = $email;
    }

    public function build()
    {
        return $this->view('emails.contact-us');
    }
}
