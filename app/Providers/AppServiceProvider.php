<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Illuminate\Database\Eloquent\Builder;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
        if ($this->app->environment('local')) {
            $this->app->register(\Laravel\Telescope\TelescopeServiceProvider::class);
            $this->app->register(TelescopeServiceProvider::class);
        }
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Builder::macro('equal', function ($field, $value) {
            return $value ? $this->where($field, '=', $value) : $this;
        });
        Builder::macro('search', function ($field, $value) {
            return $value ? $this->where($field, 'like', '%' . $value . '%') : $this;
        });
        Builder::macro('in', function ($field, $value) {
            return $value ? $this->whereIn($field, $value) : $this;
        });

        Builder::macro('orSearch', function ($field, $value) {
            if(is_array($value)){
                return $value ? $this->orWhere($field, 'like', '%' . $value . '%') : $this;
            }
            else{
                return $value ? $this->orWhere($field, 'like', '%' . $value . '%') : $this;
            }
        });

        builder::macro('belongToSearch', function ($table, $field, $value) {
            return $value ? $this->whereHas($table, function (Builder $query) use ($field, $value) {
                $query->where($field, 'like', '%' . $value . '%');
            }) : $this;
        });

        builder::macro('orBelongToSearch', function ($table, $field, $value) {
            return $value ? $this->orWhereHas($table, function (Builder $query) use ($field, $value) {
                $query->Where($field, 'like', '%' . $value . '%');
            }) : $this;
        });

        builder::macro('searchBetween', function ($field, $value1, $value2) {
            return $value1 && $value2 ? $this->whereBetween($field, [$value1, $value2]) : $this;
        });
    }
}
