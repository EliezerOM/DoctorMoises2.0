<?php

namespace App\Notifications;

use App\Models\Cita;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;
use Illuminate\Notifications\Messages\BroadcastMessage;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Messages\VonageMessage;
use Illuminate\Notifications\Notification;

class ConfirmAppointment extends Notification  implements ShouldBroadcast
{
    use Queueable;

    public $cita;
    public $notificacion;
    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct(Cita $cita)
    {
        //
        $this->cita = $cita;
        $doctor = $this->cita->doctor->nombres.' '.$this->cita->doctor->apellidos;
        $sexo = $this->cita->doctor->sexo;
        $conector = $sexo == "M"? 'Dr.': 'Dra.';

       $this->notificacion = [
           'mensaje'=>'Su cita con '.$conector.' '.$doctor.', ha sido confirmada.',
            'sexo'=> $sexo,
           'doctor'=>$doctor,
            'id_cita'=>$this->cita->id,
            'foto' => $this->cita->doctor->foto_perfil,
            'id_doctor'=> $this->cita->doctor->id,
           'link'=> route('patient.personal-schedule', ['id'=>$this->cita->id]),
       ];
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
//        return ['mail', 'database','broadcast'];
        return ['broadcast','database'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        $doctor = $this->cita->doctor->nombres.' '.$this->cita->doctor->apellidos;
        $paciente = $this->cita->patient->nombres.' '.$this->cita->patient->apellidos;
        $conector = $this->cita->doctor->sexo == "M"? "el": "la";
        return (new MailMessage)
                    ->greeting('Hola '.$paciente)
                    ->subject('Su cita con '.$conector.' '.$doctor.', ha sido confirmada.')
                    ->line("Cita No.: $this->cita->id")
                    ->line('Especialidad: '.$this->cita->especialidad)
                    ->action('Pulse aquí para más información', route('patient.personal-schedule', ['id'=>$this->cita->id]))
                    ->line('Es un gusto tenerte con nosotros!');
    }

    /**
     * Get the Vonage / SMS representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\VonageMessage
     */
    public function toVonage($notifiable)
    {
        return (new VonageMessage())
            ->content('Cita confirmada, pulse aqui para ver información '.route('patient.personal-schedule', ['id'=>$this->cita->id]))->unicode();
    }

    /**
     * Get the broadcastable representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return BroadcastMessage
     */
    public function toBroadcast($notifiable): BroadcastMessage
    {
        return new BroadcastMessage($this->notificacion);
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return $this->notificacion;
    }

}
