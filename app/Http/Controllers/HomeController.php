<?php

namespace App\Http\Controllers;

use App\Models\Doctor;
use App\Models\Paciente;
use Illuminate\Foundation\Auth\EmailVerificationRequest;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        return view('home');
    }
    public function verify(EmailVerificationRequest $request)
    {
        $request->fulfill();
        $isDoctor = Doctor::where('id_usuario', $request->input('id'))->exists();
        $isPaciente = Paciente::where('id_usuario', $request->input('id'))->exists();
//
        $route = '/';
        if ($isDoctor) {
            $route = '/doctor';
        }
        if ($isPaciente) {
            $route = '/patient';
        }
        return redirect()->to($route);// <-- change this to whatever you want
    }
}
