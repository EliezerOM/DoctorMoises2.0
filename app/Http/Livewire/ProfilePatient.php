<?php

namespace App\Http\Livewire;

use Livewire\Component;

class ProfilePatient extends Component
{
    public function render()
    {
        return view('livewire.profile-patient')
            ->layout('layouts.app');
    }
}
