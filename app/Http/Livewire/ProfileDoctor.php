<?php

namespace App\Http\Livewire;

use Livewire\Component;

class ProfileDoctor extends Component
{
    public function render()
    {
        return view('livewire.profile-doctor')
            ->layout('layouts.app');
    }
}
