<?php

namespace App\Http\Livewire;

use App\Models\Doctor;
use App\Models\Paciente;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Auth\Events\Verified;
use Illuminate\Foundation\Auth\EmailVerificationRequest;
use Illuminate\Http\JsonResponse;
use Illuminate\Routing\Route;
use Illuminate\Support\Facades\Auth;
use Livewire\Component;

class EmailVerification extends Component
{
    public $email;

    public function render()
    {
        return view('livewire.email-verification')
            ->layout('layouts.app');
    }

    public function mount()
    {
        $this->email = auth()->user()->email;
    }

    public function resend()
    {
        if (auth()->user()->hasVerifiedEmail()) {
            $isDoctor = Doctor::where('id_usuario', Auth::id())->exists();
            $isPaciente = Paciente::where('id_usuario', Auth::id())->exists();

            $route =  $route = route('home');
            if ($isDoctor) {
                $route = route('main-doctor');
            }
            if ($isPaciente) {
                $route = route('main-patient');
            }
            return redirect()->to($route);
        }
        auth()->user()->sendEmailVerificationNotification();
        session()->flash('message', 'Te enviamos un código de activación. Revisa tu correo electrónico y haz clic en el enlace para verificar');
    }


}
