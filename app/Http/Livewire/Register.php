<?php

namespace App\Http\Livewire;

use App\Http\Middleware\Doctor;
use App\Models\Notificacion;
use App\Models\Paciente;
use App\Models\User;
use Illuminate\Auth\Events\Registered;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;
use Livewire\Component;
use mysql_xdevapi\Exception;
use Illuminate\Support\Facades\Route;

class Register extends Component
{
    public $tipo_usuario = 'P';
    public $nombres;
    public $apellidos;
    public $fecha_nacimiento;
    public $sexo = 'M';
    public $user;
    public $correo;
    public $password;
    public $password_confirm;
    public $terminos;
    public $exequatur;

    protected $rules = [
        'nombres' => 'required',
        'apellidos' => 'required',
        'fecha_nacimiento' => 'required',
        'sexo' => 'required',
        'user' => 'required|unique:users,name',
        'correo' => 'required|unique:users,email',
        'password' => 'required',
        'password_confirm' => 'required',
        'terminos' => 'required',
        'tipo_usuario' => 'required',
    ];

    public function render()
    {
        return view('livewire.register')
            ->layout('layouts.app');
    }

    public function mount()
    {
        $tipo = Route::current()->parameter('tipo_usuario');
        $tipo = $tipo ? $tipo : 'P';
        $this->tipo_usuario = $tipo;
    }


    function register()
    {
        $this->validate();
        try {
            DB::beginTransaction();

            //Creando usuario
            $user = User::create([
                'name' => $this->user,
                'email' => $this->correo,
                'password' => Hash::make($this->password)
            ]);

            $notificacion = new Notificacion();
            $notificacion2 = new Notificacion();

            if ($this->tipo_usuario === 'P') {

                //registrando como paciente
                $paciente = Paciente::create([
                    'id_usuario'=>$user->id,
                     'nombres' => $this->nombres,
                    'apellidos' => $this->apellidos,
                    'fecha_nacimiento' => $this->fecha_nacimiento,
                    'sexo'=> $this->sexo
                ]);

                $notificacion->id_paciente = $paciente->id;
                $notificacion->ver_paciente = 'S';
                $notificacion2->ver_paciente = 'S';
                $notificacion2->id_paciente = $paciente->id;
                $notificacion->nota = 'Hola! ' . Str::ucfirst(trans($paciente->nombres)) . ' ' . Str::ucfirst(trans($paciente->apellidos)) . '


                Gracias por unirte a la Comunidad de Doctor Moises.

                Nuestra misión es proveer un acceso directo a los servicios de salud.
                Ahora puedes realizar citas médicas, llevar tu historial medico en tu perfil, comentar y valorizar tu consultas médicas para ayudar a la comunidad';

                $notificacion2->nota = "Casi terminamos!

                Diríjase a su perfil para terminar de completar los datos correspondiente a su perfil.";

            }
            else{
                //registrando como doctor
                $doctor = Doctor::create([
                    'id_usuario'=>$user->id,
                    'nombres' => $this->nombres,
                    'apellidos' => $this->apellidos,
                    'fecha_nacimiento' => $this->fecha_nacimiento,
                    'sexo'=> $this->sexo,
                    'exequatur'=>$this->exequatur
                ]);

                $notificacion->id_doctor = $doctor->id;
                $notificacion2->id_doctor = $doctor->id;
                $notificacion->ver_doctor = 'S';
                $notificacion->doctor_recibido = 'S';
                $notificacion2->doctor_recibido = 'S';
                $notificacion2->ver_doctor = 'S';
                $notificacion->nota = 'Hola! ' . Str::ucfirst(trans($doctor->nombres)) . ' ' . Str::ucfirst(trans($doctor->apellidos)) . '.


                Gracias por unirte a la Comunidad de Doctor Moises.

                Nuestra misión es proveer un acceso directo a los servicios de Salud.
                Te animamos a que completes las informaciones de tu perfil, para que aparezcas en los resultados de las búsquedas en el Directorio de Doctores y puedas brindar tus servicios a los pacientes.';
                $notificacion2->nota = "Casi terminamos!

                Diríjase a su perfil para terminar de completar los datos correspondiente a su perfil. Registre al menos un consultorio y complete los datos del Exequatur, es importante para la evaluación de su cuenta";

            }

            //NOTIFICACION
            $notificacion->tipo_cita = 'A';
            $notificacion->id_usuario = $user->id;
            $notificacion->titulo = 'Bienvenido a Doctor Moises';
            $notificacion->save();

            $notificacion2->id_usuario = $user->id;
            $notificacion2->tipo_cita = 'I';
            $notificacion2->titulo = 'Completa la información de tu perfil';
            $notificacion2->save();

            auth()->login($user);

            auth()->user()->sendEmailVerificationNotification();
            DB::commit();

            event(new Registered(auth()->user()));

            $page = $this->tipo_usuario === 'P' ? route('main-patient'):route('main-doctor');
            return redirect()->route($page);

        } catch (\Exception $ex) {
            DB::rollback();
            session()->with(['error-message' => $ex->getMessage()]);

        }
    }
}
