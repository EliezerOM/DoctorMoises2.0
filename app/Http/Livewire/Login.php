<?php

namespace App\Http\Livewire;

use App\models\Doctor;
use App\Models\User;
use App\models\Paciente;
use App\Traits\Notify;
use App\Traits\WithNotifyTrait;
use Illuminate\Support\Facades\Auth;
use Livewire\Component;

class Login extends Component
{
    use WithNotifyTrait;

    public $name;
    public $pass;
    public $remember;
    public $recover_email;

    protected $rules = [
        'name' => 'required',
        'pass' => 'required',
    ];

    public function boot(){
        $this->notify = new Notify();
        $this->notify->type = 'danger';
        $this->notify->showButtonClose = false;
        $this->notify->outside = true;
    }

    public function render()
    {
        //  sleep(2);
        return view('livewire.login')
            ->layout('layouts.app');
    }

    public function logon()
    {
//        Validando usuario
        $this->validate();

        $user = User::where(['name' => $this->name, 'status' => 'A'])->first();
        if (!$user) {
            $user = User::where(['email' => $this->name, 'status' => 'A'])->first();
        }
        if (!$user) {
            $this->notify->persist = true;
            $this->selfNotify('¡Usuario incorrecto o no existe!');
            return;
        }

//        Generando la session
        if (Auth::attempt(['name' => $user->name, 'password' => $this->pass], $this->remember === 'on')) {
            request()->session()->regenerate();

            if (!Auth::user()->hasVerifiedEmail()) {
                //Redireccionar a la verificación
                return redirect()->route('verification-email');
            }

//          Verificando si es doctor
            $isDoctor = Doctor::where('id_usuario', Auth::id())->exists();
            $route = route('home');
            if ($isDoctor) {
                $route = route('doctor.main');
            }

//          Verificando si es paciente
            if (!$isDoctor) {
                $isPatient = Paciente::where('id_usuario', Auth::id())->exists();

                if ($isPatient) {
                    $route = route('patient.main');
                }
            }
            if (!$isDoctor && !$isPatient) {
                $this->selfNotify('¡ Ocurrió un error con ésta cuenta !');
                return;
            }
            return redirect()->intended($route);

        } else {
            $this->selfNotify('¡ Ocurrió un error con ésta cuenta !');
            $this->sendNotify();
        }
    }


}
