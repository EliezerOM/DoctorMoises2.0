<?php

namespace App\Http\Livewire;

use Livewire\Component;

class MainDoctor extends Component
{
    public function render()
    {
        return view('livewire.main-doctor')
            ->layout('layouts.app');
    }
}
