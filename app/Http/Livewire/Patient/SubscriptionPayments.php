<?php

namespace App\Http\Livewire\Patient;

use Livewire\Component;

class SubscriptionPayments extends Component
{
    public function render()
    {
        return view('livewire.patient.subscription-payments');
    }
}
