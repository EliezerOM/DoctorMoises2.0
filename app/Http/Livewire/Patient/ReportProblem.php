<?php

namespace App\Http\Livewire\Patient;

use Livewire\Component;

class ReportProblem extends Component
{
    public function render()
    {
        return view('livewire.patient.report-problem')->layout('livewire.patient.main-patient');
    }
}
