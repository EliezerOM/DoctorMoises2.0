<?php

namespace App\Http\Livewire\Patient;

use App\Models\Cita;
use App\Traits\DataTable\WithSorting;
use App\Traits\Notify;
use App\Traits\WithNotifyTrait;
use Illuminate\Database\Eloquent\Builder;
use Livewire\Component;
use Livewire\WithPagination;
use phpDocumentor\Reflection\Types\Array_;

class PersonalSchedule extends Component
{
    use WithPagination, WithNotifyTrait, WithSorting;

    public $search = '';
    public $type_appointment;
    public $endDate;
    public $startDate;
    public $slide;
    public $cita_no;

    protected $listeners = ['refreshSchedule' => 'refresh', 'deleteAppointment'];

    public function paginationView()
    {
        return 'components.pagination-links';
    }

    public function updatingSearch()
    {
        $this->resetPage();
    }

    public function mount()
    {
        $this->search = request()->search;
        $this->cita_no = request()->id;
        $this->slide = false;
        $this->type_appointment = ['S', 'C'];
        $this->sorts = ['status' => 'asc', 'fecha' => 'desc'];
    }

    public function boot()
    {
        $this->notify = new Notify();
    }

    public function render()
    {
        $query = Cita::with('speciality', 'schedule', 'doctor')
            ->in('status', $this->type_appointment)
            ->where('id_paciente', auth()->user()->patient->id)
            ->where('status', '!=', 'D')
            ->searchBetween('fecha', $this->startDate, $this->endDate)
            ->equal('id', $this->cita_no)
            ->Where(function (Builder $query) {
                $query->search('id', $this->search)
                    ->orBelongToSearch('speciality', 'descripcion', $this->search)
                    ->orBelongToSearch('doctor', 'nombres', $this->search)
                    ->orBelongToSearch('doctor', 'apellidos', $this->search);
            });
        $items = $this->applySorting($query)->paginate(8);
        $this->cita_no = '';
        return view('livewire.patient.personal-schedule', ['appointments' => $items]);
    }

    public function deleteAppointment($notify)
    {

        $appointmentSeleted = Cita::find($notify['parameterAceptar']['id']);
        if ($appointmentSeleted) {
            $appointmentSeleted->nota_paciente = $notify['textComment'];
            $appointmentSeleted->status = 'D';
            $appointmentSeleted->save();
            $this->slide = false;

            $this->notify->type = 'warning';
            $this->notify->position = 'tr';
            $this->notify->animate = 'x';
            $this->notify->duration = 6000;
            $this->notify->message = '¡La cita #' . $appointmentSeleted->id . ' Cancelada!';
            $this->addNotify($this->notify);

            $this->notify->delay = 2000;
            $this->notify->duration = 8000;
            $this->notify->type = 'info';
            $this->notify->message = 'El doctor ha sido notificado';
            $this->addNotify($this->notify);
            $this->sendNotify();
        } else {
            $this->notify->type = 'danger';
            $this->selfNotify("Ocurrió un error al cancelar esta cita");
        }
    }
}
