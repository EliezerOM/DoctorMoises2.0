<?php

namespace App\Http\Livewire\Patient;

use Livewire\Component;

class Recibos extends Component
{
    public function render()
    {
        return view('livewire.patient.recibos');
    }
}
