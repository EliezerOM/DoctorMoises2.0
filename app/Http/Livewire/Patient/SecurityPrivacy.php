<?php

namespace App\Http\Livewire\Patient;

use Livewire\Component;

class SecurityPrivacy extends Component
{
    public function render()
    {
        return view('livewire.patient.security-privacy');
    }
}
