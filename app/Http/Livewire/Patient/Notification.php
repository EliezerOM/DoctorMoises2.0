<?php

namespace App\Http\Livewire\Patient;

use Livewire\Component;

class Notification extends Component
{
    protected $listeners = ["actualizaNotificacion" => "render"];
    public function render()
    {
        $notifications = \Auth::user()->notifications->take(15);
        return view('livewire.patient.notification-view', ['notifications'=>$notifications])
            ->layout('livewire.patient.main-patient');
    }

}
