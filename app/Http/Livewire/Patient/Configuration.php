<?php

namespace App\Http\Livewire\Patient;

use Livewire\Component;

class Configuration extends Component
{
    public function render()
    {
        return view('livewire.patient.configuration');
    }
}
