<?php

namespace App\Http\Livewire\Patient;
use App\Traits\Notify;
use App\Traits\WithNotifyTrait;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Livewire\Component;


class Dashboard extends Component
{
    public function render()
    {
        $patient  = Auth::user()->patient;
        $per_profile = self::calculate_profile($patient);

        return view('livewire.patient.dashboard', [$per_profile => 'per_profile']);
    }
    function calculate_profile($profile)
    {
        if ( ! $profile) {
            return 0;
        }
        $columns    = preg_grep('/(.+ed_at)|(^.+_id$)|(^id_)|(^id$)/', array_keys($profile->toArray()), PREG_GREP_INVERT);
        $per_total = count($columns);
        $per_dif = 0;
        foreach ($profile->toArray() as $key => $value) {
            if ($value === NULL || $value === [] && in_array($key, $columns)) {
                $per_dif ++;
            }
        }
        return round(100 - (($per_dif*100)/$per_total), 2);
    }
}
