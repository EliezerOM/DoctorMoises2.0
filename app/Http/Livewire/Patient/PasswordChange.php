<?php

namespace App\Http\Livewire\Patient;

use Livewire\Component;

class PasswordChange extends Component
{
    public function render()
    {
        return view('livewire.patient.password-change')
            ->layout('livewire.patient.main-patient');
    }
}
