<?php

namespace App\Http\Livewire\Patient;

use App\Models\Cita;
use Livewire\Component;

class CitaRecientes extends Component
{
    public function render()
    {
        $citasRecientes = Cita::where('id_paciente', \Auth::user()->patient->id)->orderBy('id', 'desc')->take(15)->get();
        return view('livewire.patient.cita-recientes', ['appointments'=>$citasRecientes]);
    }
}
