<?php

namespace App\Http\Livewire\Patient\Component;

use App\Models\FrecuenciaCardiaca;
use App\Traits\Notify;
use App\Traits\WithNotifyTrait;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Auth;
use Livewire\Component;

class HeartRate extends Component
{
    use WithNotifyTrait;

    public $lastUpdate = null;
    public $min = null;
    public $max = null;
    public $model = null;
    public $valueMax = null;
    public $valueMin = null;
    public function  boot(){
        $this->notify = new Notify();
    }
    public function render()
    {
        $this->model = FrecuenciaCardiaca::where('id_usuario', Auth::id())->orderBy('id', 'desc')->first();
        if($this->model){
            $this->lastUpdate = $this->model->created_at;
            $this->min = $this->model->minima;
            $this->max = $this->model->maxima;
        }

        return view('livewire.patient.component.heart-rate');
    }
    protected $rules = [
        'valueMin' => 'required|digits_between:1,110|lt:valueMax|max:110',
        'valueMax' => 'required|digits_between:1,220|gt:valueMin|max:220',
    ];
    public function heartRateRefresh(){
        $this->validate();
        if(TypeDate($this->model->created_at ?? Carbon::now()->addDay(2)) !== TypeDate(Carbon::now())){

            FrecuenciaCardiaca::create([
                'minima' => $this->valueMin,
                'maxima' => $this->valueMax,
                'id_usuario'=>  Auth::id()
            ]);
            $this->selfNotify('Frecuencia Cardíaca Registrada');
        }
        else{
            $this->model->minima = $this->valueMin;
            $this->model->maxima = $this->valueMax;
            $this->model->update();
            $this->selfNotify('Frecuencia Cardíaca Actualizada');
        }
        $this->closeModal();
    }
    public function  closeModal(){
        $this->dispatchBrowserEvent('show-dialg',['show'=>false]);
    }
}
