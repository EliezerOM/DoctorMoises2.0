<?php

namespace App\Http\Livewire\Patient\Component;

use App\Traits\Notify;
use App\Traits\WithNotifyTrait;
use Illuminate\Support\Facades\Auth;
use Livewire\Component;

class Notification extends Component
{
    use WithNotifyTrait;
    public function  boot(){
        $this->notify = new Notify();
    }

    public function getListeners()
    {
        $id = Auth::user()->id;
        return [
            "echo-private:Users.$id,.Illuminate\\Notifications\\Events\\BroadcastNotificationCreated" => 'notification',
        ];
    }
    public function render()
    {
        $notifications = \Auth::user()->notifications->take(15);
        $notiUnread = \Auth::user()->unreadNotifications;
        return view('livewire.patient.component.notification', ['notifications'=>$notifications, 'unread'=>$notiUnread]);
    }

    public function notification(){
        $this->render();
        $this->emit('actualizaNotificacion');
    }

    public function leido(\Illuminate\Notifications\DatabaseNotification $notification){
        $notification->markAsRead();
       return redirect()->route('patient.personal-schedule', ['id'=>$notification->data['id_cita']]);
    }
}
