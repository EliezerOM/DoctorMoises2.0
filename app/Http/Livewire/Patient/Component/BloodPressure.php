<?php

namespace App\Http\Livewire\Patient\Component;

use App\Models\FrecuenciaCardiaca;
use App\Models\PresionSanguinea;
use App\Traits\Notify;
use App\Traits\WithNotifyTrait;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Auth;
use Livewire\Component;

class BloodPressure extends Component
{
    use WithNotifyTrait;

    public $valueSistolica = null;
    public $valueDiastolica = null;
    public $model = null;
    public $lastUpdate = null;
    public $sistolica = null;
    public $diastolica = null;

    protected $rules = [
        'valueSistolica' => 'required|digits_between:1,110|lt:valueDiastolica|max:110',
        'valueDiastolica' => 'required|digits_between:1,220|gt:valueSistolica|max:220',
    ];
    public function boot(){
        $this->notify = new Notify();
    }
    public function render()
    {
        $this->model = PresionSanguinea::where('id_usuario',Auth::id())->orderBy('id', 'desc')->first();
        if($this->model){
            $this->sistolica = $this->model->sistolica;
            $this->diastolica = $this->model->diastolica;
            $this->lastUpdate = $this->model->created_at;
        }
        return view('livewire.patient.component.blood-pressure');
    }
    public function refreshBloodPresure(){
        $this->validate();
        if(TypeDate($this->model->created_at ?? Carbon::now()->addDay(2)) !== TypeDate(Carbon::now())){

            PresionSanguinea::create([
                'sistolica' => $this->valueSistolica,
                'diastolica' => $this->valueDiastolica,
                'id_usuario'=>  Auth::id()
            ]);
            $this->selfNotify('Presión Sanguínea Registrada');
        }
        else{
            $this->model->sistolica = $this->valueSistolica;
            $this->model->diastolica = $this->valueDiastolica;
            $this->model->update();
            $this->selfNotify('Presión Sanguínea Actualizada');
        }


        $this->closeModal();
    }
    public function  closeModal(){
        $this->dispatchBrowserEvent('show-dialg',['show'=>false]);
    }
}
