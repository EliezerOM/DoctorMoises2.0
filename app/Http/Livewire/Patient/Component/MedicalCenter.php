<?php

namespace App\Http\Livewire\Patient\Component;

use App\Models\CentroMedico;
use App\Models\Cita;
use Cornford\Googlmapper\Mapper;
use Livewire\Component;

class MedicalCenter extends Component
{
    public $medicalCenter;
    public $appointment;
    protected $listeners = ['searchAppointmentMedicalCenter'];

    public function mount(){
        $this->medicalCenter =  new CentroMedico();
    }
    public function render()
    {
        return view('livewire.patient.component.medical-center');
    }

    public function searchAppointmentMedicalCenter(Cita $cita){
        $this->appointment = $cita->load('center');
    }
}
