<?php

namespace App\Http\Livewire\Patient\Component;

use Illuminate\Support\Facades\Auth;
use Livewire\Component;

class CardCompletarPerfil extends Component
{
    public function render()
    {

        $patient  = Auth::user()->patient;
        $per_profile = percentColumn($patient);
        $per_configuration = 12;
        $per_subscription = 0;

        return view('livewire.patient.component.card-completar-perfil', ['per_profile'=>$per_profile, 'per_configuration' => $per_configuration, 'per_subscription'=>$per_subscription]);
    }
}
