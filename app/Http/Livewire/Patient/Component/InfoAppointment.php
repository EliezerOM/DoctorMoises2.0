<?php

namespace App\Http\Livewire\Patient\Component;

use App\Models\Cita;
use Livewire\Component;

class InfoAppointment extends Component
{
    public $appointmentSeleted;
    public $listeners = ['searchAppointment' => 'search'];
    public function mount()
    {
        $this->appointmentSeleted = new Cita();
    }
    public function render()
    {
        return view('livewire.patient.component.info-appointment');
    }
    public function search(Cita $appointment){
        $this->appointmentSeleted = $appointment;
        $this->emit('searchAppointmentDoctor', $appointment);
        $this->emit('searchAppointmentMedicalCenter', $appointment);
    }
}
