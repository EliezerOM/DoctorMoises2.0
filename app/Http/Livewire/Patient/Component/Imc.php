<?php

namespace App\Http\Livewire\Patient\Component;

use App\Models\Altura;
use App\Models\Peso;
use Illuminate\Support\Facades\Auth;
use Livewire\Component;

class Imc extends Component
{
    public $imc = null;
    const METER = 3.281;
    const KILO = 2.205;
    public $listeners = ['renderImc'=>'render'];
    public function render()
    {
        $peso = Peso::where('id_usuario', Auth::id())->orderBy('id', 'desc')->first();
        $altura = Altura::where('id_usuario', Auth::id())->orderBy('id', 'desc')->first();
        if($peso && $altura){
            $cal1 = $peso->peso / self::KILO;
            $cal2 = pow(($altura->altura / self::METER) , 2);
            $this->imc = round($cal1/$cal2, 2);
        }
        return view('livewire.patient.component.imc');
    }
}
