<?php

namespace App\Http\Livewire\Patient\Component;

use App\Models\Cita;
use App\Traits\Notify;
use App\Traits\WithNotifyTrait;
use Jantinnerezo\LivewireAlert\LivewireAlert;
use Livewire\Component;

class CancelarCita extends Component
{
    use WithNotifyTrait;

    public $appointmentSeleted;
    public $idCita;
    public $mini = true;
    public $classButton;
    public $classIcon;

    public function  boot(){
        $this->notify =  new Notify();
    }
    public function render()
    {
        return view('livewire.patient.component.cancelar-cita');
    }

    public function confirmAppointmentSeletedId()
    {
            $this->appointmentSeleted = Cita::find($this->idCita);
            if ($this->appointmentSeleted) {
                $this->notify->type = 'warning';
                $this->notify->animate = 'x';
                $this->notify->typeNotification = 'confirm';
                $this->notify->persist = true;
                $this->notify->showButtonAceptar = true;
                $this->notify->showButtonCancelar = true;
                $this->notify->onAceptar = 'deleteAppointment';
                $this->notify->parameterAceptar = $this->appointmentSeleted;
                $this->notify->isTextComment = true;
                $this->notify->textTitleComment = 'Cual es su motivo?';
                $this->notify->tagsComment = array
                (
                    'Hice la solicitud por error',
                    'Desearía acudir en otro momento',
                    'Tengo otra cita para ese día',
                    'Quiero reprogramar',
                    'Hare una cita nueva',
                    'El horario no me lo permite'
                );

                $this->selfNotify('Seguro que desea cancelar la cita #'. $this->idCita . '?');

            }
    }
}
