<?php

namespace App\Http\Livewire\Patient\Component;

use App\Models\Cita;
use App\Models\Peso;
use App\Notifications\ConfirmAppointment;
use App\Traits\Notify;
use App\Traits\WithNotifyTrait;
use Faker\Core\DateTime;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Auth;
use Livewire\Component;

class Weight extends Component
{
    use WithNotifyTrait;
    public $weight = null;
    public $value = null;
    public $model = null;
    public $lastUpdate = '';
    public $kilograms = null;
    public $pounds = null;
    public $typeWeight = null;
    const KILO_VAL = 2.205;

    protected $rules = [
      'value'=>'numeric|min:1'
    ];
    public function  boot(){
        $this->notify = new Notify();
    }

    public function render()
    {
        $this->model = Peso::where('id_usuario', Auth::id())->orderBy('id', 'desc')->first();


        if ($this->model) {
            $this->weight = $this->model->peso;
            $this->lastUpdate = $this->model->created_at;
            $this->pounds = $this->weight;
            $this->kilograms = $this->weight / self::KILO_VAL;
            if ($this->typeWeight) {
                $this->value = round($this->kilograms, 2);
            } else {
                $this->value = round($this->weight, 2);
            }
        }

        return view('livewire.patient.component.weight');
    }

    public function weightRefresh()
    {
        $this->validate();
        if ($this->value) {
            $weight = $this->value;
            if ($this->typeWeight) {
                $weight = $this->value * self::KILO_VAL;
            }

            if (TypeDate($this->model->created_at ?? Carbon::now()->addDay(1)) !== TypeDate(Carbon::now())) {
                Peso::create([
                    'id_usuario' => Auth::id(),
                    'peso' => $weight
                ]);
                $this->selfNotify('Peso Registrado');

            } else {
                $this->model->peso = $weight;
                $this->model->update();
                $this->selfNotify('Peso Actualizado');

            }
            $this->emit('renderImc');



            $this->closeModal();
            $cita = Cita::where('id_paciente', auth()->user()->patient->id)->first();
            auth()->user()->notify(new ConfirmAppointment($cita));
        }
    }
    public function  closeModal(){
        $this->dispatchBrowserEvent('show-dialg',['show'=>false]);
    }
}
