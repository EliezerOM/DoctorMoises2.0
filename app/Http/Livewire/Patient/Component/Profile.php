<?php

namespace App\Http\Livewire\Patient\Component;

use Livewire\Component;

class Profile extends Component
{
    public function render()
    {
        return view('livewire.patient.component.profile');
    }
}
