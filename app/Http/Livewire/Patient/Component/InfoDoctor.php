<?php

namespace App\Http\Livewire\Patient\Component;

use App\Models\Cita;
use App\Models\Doctor;
use Livewire\Component;

class InfoDoctor extends Component
{
    public $doctor;
    public $appointment;
    protected $listeners = ['searchDoctor', 'searchAppointmentDoctor'];

    public function mount()
    {
        $this->doctor = new Doctor();
    }

    public function render()
    {
        return view('livewire.patient.component.info-doctor');
    }

    public function searchDoctor($id)
    {
        $this->doctor = Doctor::with('doctor_speciality')->find($id);
    }
    public function searchAppointmentDoctor(Cita $appointment){
        $this->appointment = $appointment;
    }
}
