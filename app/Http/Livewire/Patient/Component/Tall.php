<?php

namespace App\Http\Livewire\Patient\Component;

use App\Models\Altura;
use App\Traits\Notify;
use App\Traits\WithNotifyTrait;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Auth;
use Livewire\Component;

class Tall extends Component
{
    use WithNotifyTrait;

    public $typeTall = null;
    public $tall = null;
    public $meter = null;
    public $foot = null;
    public $value = null;
    public $model = null;
    public $lastUpdate = null;
    const METER = 3.281;

    protected $rules = [
        'value'=>'numeric|min:1'
    ];

    public function  boot(){
        $this->notify = new Notify();
    }
    public function render()
    {
        $this->model = Altura::where('id_usuario', Auth::id())->orderBy('id', 'desc')->first();
        if($this->model){
            $this->tall = $this->model->altura;
            $this->lastUpdate = $this->model->created_at;
            $this->foot = $this->tall;
            $this->meter = $this->tall / self::METER;

            if($this->typeTall){
                $this->value = round($this->meter, 2);
            }
            else{
                $this->value = round($this->foot, 2);
            }
        }
        return view('livewire.patient.component.tall');
    }
    public function tallRefresh(){
        $this->validate();

        if($this->value){
            $tall = $this->value;
            if($this->typeTall){
                $tall = $this->value * self::METER;
            }
            if (TypeDate($this->model->created_at ?? Carbon::now()->addDay(2)) !== TypeDate(Carbon::now())) {
                Altura::create([
                    'id_usuario' => Auth::id(),
                    'altura' => $tall
                ]);
                $this->selfNotify('Estatura Registrada');

            }
            else{
            $this->model->altura = $tall;
            $this->model->update();
                $this->selfNotify('Estatura Actualizado');
            }
            $this->emit('renderImc');
            $this->closeModal();
        }
    }
    public function  closeModal(){
        $this->dispatchBrowserEvent('show-dialg',['show'=>false]);
    }
}
