<?php

namespace App\Http\Livewire\Patient\Component;

use App\Models\Glucosa;
use App\Traits\Notify;
use App\Traits\WithNotifyTrait;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Auth;
use Livewire\Component;

class Glucose extends Component
{
    use WithNotifyTrait;

    public $glucose = null;
    public $model = null;
    public $lastUpdate = null;
    public $value = null;

    protected $rules = [
        'value'=>'numeric|min:1'
    ];

    public function boot(){
        $this->notify = new Notify();
    }

    public function render()
    {
        $this->model= Glucosa::where('id_usuario', Auth::id())->orderBy('id', 'desc')->first();
        if($this->model){
            $this->glucose = $this->model->glucosa;
            $this->lastUpdate = $this->model->created_at;
        }
        return view('livewire.patient.component.glucose');
    }
    public function registerGlucose(){
        $this->validate();

        if($this->value){
            if (TypeDate($this->model->created_at ?? Carbon::now()->addDay(1)) !== TypeDate(Carbon::now())){
                Glucosa::create([
                    'id_usuario'=>Auth::id(),
                    'glucosa'=> $this->value,
                ]);
                $this->selfNotify('Nivel De Glucosa Registrado');

            }
            else{
                $this->model= $this->value;
                $this->model->update();
                $this->selfNotify('Nivel De Glucosa Actualizado');

            }
            $this->closeModal();
        }

    }
    public function  closeModal(){
        $this->dispatchBrowserEvent('show-dialg',['show'=>false]);
    }
}
