<?php

namespace App\Http\Livewire\Patient;

use Livewire\Component;

class PersonalInformation extends Component
{
    public function render()
    {
        $user = \Auth::user();
        return view('livewire.patient.personal-information', ['user'=>$user]);
    }
}
