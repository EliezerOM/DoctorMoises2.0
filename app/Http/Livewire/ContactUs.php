<?php

namespace App\Http\Livewire;

use App\Mail\ContactUsMail;
use Illuminate\Support\Facades\Mail;
use Livewire\Component;

class ContactUs extends Component
{
    public $email;
    public $name;
    public $comment;
    protected $rules =
        [
            'email' => 'required|email',
            'name' => 'required',
            'comment' => 'required|max:1000'
        ];

    public function render()
    {
        return view('livewire.contact-us');
    }

    public function enviar()
    {
        $this->validate();
        $contacto = new ContactUsMail($this->comment, $this->email);
        Mail::to('doctormoisesrd@gmail.com')->queue($contacto->subject('Comentario de '.$this->name.', para Doctor Moises'));

        $this->dispatchBrowserEvent('alert-toast',
            ['type' => 'success','position'=>'bly','duration'=>7000, 'message' => '¡Gracias. Su comentario fue recibido,  es muy importante para nosotros!']);

        $this->reset();
    }
}
