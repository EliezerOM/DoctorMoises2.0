<?php

namespace App\Http\Livewire;

use Illuminate\Routing\Route;
use Livewire\Component;

class Logout extends Component
{
    public function render()
    {
        return view('livewire.logout')
            ->layout('layouts.app');
    }

    public function logout()
    {
        auth()->logout();
        return redirect()->to('/');
    }
}
