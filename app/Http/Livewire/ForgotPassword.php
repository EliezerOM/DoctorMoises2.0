<?php

namespace App\Http\Livewire;

use App\Models\User;
use App\Traits\Notify;
use App\Traits\WithNotifyTrait;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Password;
use Illuminate\Support\Str;
use Livewire\Component;
use Illuminate\Foundation\Auth\SendsPasswordResetEmails;

class ForgotPassword extends Component
{
    use WithNotifyTrait;
    public $email;

    public function render()
    {
        return view('livewire.forgot-password');
    }
    public function boot(){
        $this->notify = new Notify();
        $this->notify->type = 'danger';
        $this->notify->showButtonClose = false;
    }

    public function forgot()
    {
        $this->validate(['email' => 'required|email']);

        $user = User::where('email', $this->email)->first();
        if (!$user) {
            $this->selfNotify('¡ Esta cuenta no existe !');
        }
        else
        {
            $status = Password::sendResetLink(['email'=>$this->email]);

            if ($status === Password::RESET_LINK_SENT) {
                $this->notify->type = 'success';
                $this->notify->persist = true;
                $this->selfNotify('¡ Se ha enviado un enlace para restablecer su cuenta, verifique su dirección de correo electrónico !');
            } else {
                $this->selfNotify('¡ Ocurrió un error con la petición !');
            }
            $this->reset();
        }
    }
}
